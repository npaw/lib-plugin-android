package com.npaw.youbora.lib6.comm.transform;

import static com.npaw.youbora.lib6.constants.Services.START;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.flags.Flags;
import com.npaw.youbora.lib6.infinity.Infinity;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.plugin.RequestBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.robolectric.RobolectricTestRunner;

import java.net.HttpURLConnection;
import java.util.HashMap;

@RunWith(RobolectricTestRunner.class)
public class ViewTransformTest {

    private static final String CODE = "U_12345_1s7yd9x92gypdrt5";
    private static final String HOST = "debug-nqs-lw2.a-fds.youborafds01.com";
    private static final String PING_TIME = "5";
    private static final String OFFLINE_CODE = "OFFLINE_MODE";
    private static final String OFFLINE_HOST = "OFFLINE_MODE";
    private static final String OFFLINE_PING_TIME = "60";

    private Request  mockRequest;
    private Plugin   mockPlugin;
    private Options  mockOptions;
    private Infinity mockInfinity;

    private YouboraLog.YouboraLogger mockLogger;

    private class TestViewTransform extends ViewTransform {
        public String viewCodeTimestamp;

        public TestViewTransform(Plugin plugin) {
            super(plugin);
        }

        @Override
        JsonElement toJsonTree( String string )
        {
            return JsonParser.parseString("{\"q\":{\"h\":\"" + HOST + "\",\"pt\":\"" + PING_TIME + "\",\"c\":\"" + CODE + "\"}}");
        }

        @Override
        Request createRequest(String host, String service) {
            return mockRequest;
        }

        @Override
        public String getViewCodeTimeStamp() {
            viewCodeTimestamp = Long.toString(System.currentTimeMillis());
            return viewCodeTimestamp;
        }
    }

    @Before
    public void setUp() throws Exception {

        // Log listener
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);

        // Mocks
        mockRequest = mock(Request.class);
        mockOptions = mock(Options.class);
        mockInfinity = mock(Infinity.class);
        when(mockInfinity.getFlags()).thenReturn(new Flags());

        mockPlugin = mock(Plugin.class);
        when(mockPlugin.getHost()).thenReturn("host.com");
        RequestBuilder mockBuilder = mock(RequestBuilder.class);
        when(mockBuilder.buildParams(any(HashMap.class), anyString())).thenReturn(new HashMap());
        when(mockPlugin.getRequestBuilder()).thenReturn(mockBuilder);
        when(mockPlugin.getOptions()).thenReturn(mockOptions);
        when(mockPlugin.getInfinity()).thenReturn(mockInfinity);
    }

    @Test
    public void testGetCodeWithoutInit() {
        ViewTransform viewTransform = new ViewTransform(mockPlugin);

        assertNull(viewTransform.nextView());
        assertNull(viewTransform.nextView());
    }

    private void verifyErrorCalled(String fastDataResponse) {
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Capture callback when set
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);

        viewTransform.init(); // Callback set here

        // Capture callback
        verify(mockRequest).addOnSuccessListener(captor.capture());

        // Mock callback response
        captor.getValue().onRequestSuccess(mock(HttpURLConnection.class), fastDataResponse, new HashMap<>(), new HashMap<>());

        verify(mockLogger, times(1)).logYouboraMessage(anyString(), eq(YouboraLog.Level.ERROR));
    }

    @Test
    public void testEmptyResponse() {
        verifyErrorCalled("");
    }

    @Test
    public void testNullResponse() {
        verifyErrorCalled(null);
    }

    @Test
    public void testErrorRequest() {
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Capture callback when set
        ArgumentCaptor<Request.RequestErrorListener> captor = ArgumentCaptor.forClass(Request.RequestErrorListener.class);

        viewTransform.init(); // Callback set here

        // Capture callback
        verify(mockRequest).addOnErrorListener(captor.capture());

        // Mock callback response
        captor.getValue().onRequestError(mock(HttpURLConnection.class));

        verify(mockLogger, times(1)).logYouboraMessage(anyString(), eq(YouboraLog.Level.ERROR));
    }

    @Test
    public void testRequestData() {

        // ViewTransform to test
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);
        when(mockOptions.isHttpSecure()).thenReturn(true);

        // Capture callback when set
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);

        viewTransform.init(); // Callback set here

        // Capture callback
        verify(mockRequest).addOnSuccessListener(captor.capture());

        // Mock callback response
        captor.getValue().onRequestSuccess(mock(HttpURLConnection.class), "response", new HashMap<>(), null);

        // Shouldn't block anymore (Due to offline may be blocked anyway)
        assertFalse(viewTransform.isBlocking(mockRequest));

        ViewTransform.FastDataConfig config = viewTransform.fastDataConfig;

        // Assert config params
        assertNotNull(config);
        assertEquals((Integer) Integer.parseInt(PING_TIME), config.pingTime);
        assertEquals(CODE, config.code);
        assertEquals("https://" + HOST, config.host);
    }

    @Test
    public void testRequestDataOfflineMode() {
        //Prepare mocks for offline
        when(mockPlugin.getOptions().isOffline()).thenReturn(true);

        // ViewTransform to test
        ViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Capture callback when set
        ArgumentCaptor<Request.RequestSuccessListener> captor = ArgumentCaptor.forClass(Request.RequestSuccessListener.class);

        viewTransform.init(); // Callback set here

        // Shouldn't block anymore (Due to offline may be blocked anyway)
        assertFalse(viewTransform.isBlocking(mockRequest));

        ViewTransform.FastDataConfig config = viewTransform.fastDataConfig;

        // Assert config params
        assertNotNull(config);
        assertEquals((Integer) Integer.parseInt(OFFLINE_PING_TIME), config.pingTime);
        assertEquals(OFFLINE_CODE, config.code);
        assertEquals(OFFLINE_HOST, config.host);
    }

    @Test
    public void testParseRequests() {
        // ViewTransform to test
        TestViewTransform viewTransform = new TestViewTransform(mockPlugin);

        // Fill FastData responses
        viewTransform.fastDataConfig = new TestViewTransform.FastDataConfig();
        viewTransform.fastDataConfig.host = "https://" + HOST;
        viewTransform.fastDataConfig.pingTime = Integer.parseInt(PING_TIME);
        viewTransform.fastDataConfig.code = CODE;

        // create first view code
        viewTransform.nextView();

        // Mock /start
        Request mockStart = mock(Request.class);
        HashMap<String, Object> mockMap = new HashMap<>();
        when(mockStart.getParams()).thenReturn(mockMap);
        when(mockStart.getService()).thenReturn(START);

        // Parse requests
        viewTransform.parse(mockStart);

        assertEquals(CODE + "_" + viewTransform.viewCodeTimestamp, mockMap.get("code"));
        assertEquals(PING_TIME, mockMap.get("pingTime").toString());
        verify(mockStart, times(1)).setHost("https://" + HOST);

        // increment view code
        viewTransform.nextView();

        // Mock /ping
        Request mockPing = mock(Request.class);
        mockMap = new HashMap<>();
        when(mockPing.getParams()).thenReturn(mockMap);
        when(mockPing.getService()).thenReturn(START);

        viewTransform.parse(mockPing);

        assertEquals(CODE + "_" + viewTransform.viewCodeTimestamp, mockMap.get("code"));
        assertEquals(PING_TIME, mockMap.get("pingTime").toString());
        verify(mockStart, times(1)).setHost("https://" + HOST);
    }
}