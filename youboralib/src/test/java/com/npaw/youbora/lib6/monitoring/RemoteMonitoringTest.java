package com.npaw.youbora.lib6.monitoring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import androidx.annotation.NonNull;
import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.plugin.Plugin;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import java.lang.reflect.Field;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
public class RemoteMonitoringTest {

    class RemoteMonitoringListenerTest implements RemoteMonitoringListener {
        @Override
        public void onSend(@NonNull String data) {

        }
    }
    class RemoteMonitoringMock extends RemoteMonitoring {

        public RemoteMonitoringMock(@NonNull RemoteMonitoringListener listener, Plugin plugin) {
            super(listener, plugin);
            remoteMonitoringListener = listener;
        }

        @Override
        public Timer createRemoteMonitoringTimer(Timer.TimerEventListener listener, long interval) {
            return mockTimer;
        }
        @Override
        public JSONObject createJSONFromString(String string) throws JSONException { return mockOuterJson.getJSONObject(string); }
    }

    private Timer mockTimer = null;
    private Plugin mockPlugin = null;
    private JSONObject mockOuterJson;
    private RemoteMonitoringListener remoteMonitoringListener = null;
    @Before
    public void before() {
        reset();
        mockTimer = mock(Timer.class);
        mockPlugin = mock(Plugin.class);
        mockOuterJson = mock(JSONObject.class);

    }

    @Test
    public void initRemoteMonitoringTest() {
        List<YouboraLog.YouboraLogger> loggers = getYouboraLoggers();
        int previousLoggersSize = 0;
        if(loggers != null)
            previousLoggersSize = loggers.size();
        RemoteMonitoring remoteMonitoring = new RemoteMonitoringMock(new RemoteMonitoringListenerTest(), mockPlugin);
        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());
        assertFalse(remoteMonitoring.getStoppingRemoteMonitoringTimer());

        loggers = getYouboraLoggers();
        assertEquals(previousLoggersSize+1, loggers.size());
    }

    @Test
    public void startWhenIsNotEnableTest() {

        RemoteMonitoring remoteMonitoring = new RemoteMonitoringMock(new RemoteMonitoringListenerTest(), mockPlugin);

        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());
        remoteMonitoring.startRemoteMonitoringTimer();
        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());
        verify(mockTimer, never()).start();

    }

    @Test
    public void remoteMonitoringInterceptorRequestSuccessListenerTest_NullOrEmptyOrIncorrect() throws JSONException {

        RemoteMonitoring remoteMonitoring = new RemoteMonitoringMock(new RemoteMonitoringListenerTest(), mockPlugin);

        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());

        remoteMonitoring.getRemoteMonitoringInterceptorRequestSuccessListener().onRequestSuccess(null,null, null, null);
        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());

        remoteMonitoring.getRemoteMonitoringInterceptorRequestSuccessListener().onRequestSuccess(null,"", null, null);
        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());

        String incorrectInput ="{\"123\":\"incorrectInput\"}";
        JSONObject responseJSON = mock(JSONObject.class);
        when(mockOuterJson.getJSONObject(incorrectInput)).thenReturn(responseJSON);
        when(responseJSON.has("enabledLogs")).thenReturn(false);

        remoteMonitoring.getRemoteMonitoringInterceptorRequestSuccessListener().onRequestSuccess(null, incorrectInput, null, null);
        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());
    }

    @Test
    public void remoteMonitoringInterceptorRequestSuccessListenerTest() {

        RemoteMonitoring remoteMonitoring = new RemoteMonitoringMock(new RemoteMonitoringListenerTest(), mockPlugin);

        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());
        startRemoteMonitoring(remoteMonitoring);
        assertTrue(remoteMonitoring.getRemoteMonitoringEnable());
    }

    @Test
    public void stopWhenNotStartedTest() {
        List<YouboraLog.YouboraLogger> loggers = getYouboraLoggers();
        int previousLoggersSize = 0;
        if(loggers != null)
            previousLoggersSize = loggers.size();
        RemoteMonitoring remoteMonitoring = new RemoteMonitoringMock(new RemoteMonitoringListenerTest(), mockPlugin );

        assertFalse(remoteMonitoring.getRemoteMonitoringEnable());

        loggers = getYouboraLoggers();
        assertEquals(previousLoggersSize+1, loggers.size());

        remoteMonitoring.stopRemoteDebuggingTimer();

        loggers = getYouboraLoggers();
        assertEquals(previousLoggersSize, loggers.size());

    }

    private void startRemoteMonitoring(RemoteMonitoring remoteMonitoring){
        String correctInput = "{\"enabledLogs\":true}";
        JSONObject responseJSON = mock(JSONObject.class);;
        try {
            when(responseJSON.getBoolean("enabledLogs")).thenReturn(true);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        try {
            when(mockOuterJson.getJSONObject(correctInput)).thenReturn(responseJSON);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        when(responseJSON.has("enabledLogs")).thenReturn(true);
        remoteMonitoring.getRemoteMonitoringInterceptorRequestSuccessListener().onRequestSuccess(null, correctInput, null, null);
    }

    private List<YouboraLog.YouboraLogger> getYouboraLoggers() {
        List<YouboraLog.YouboraLogger> loggers = null;
        try {
            Field instance = YouboraLog.class.getDeclaredField("loggers");
            instance.setAccessible(true);
            loggers = (List<YouboraLog.YouboraLogger>) instance.get(YouboraLog.Companion);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return loggers;
    }
}
