package com.npaw.youbora.lib6.plugin;

import com.npaw.youbora.lib6.constants.RequestParams;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.npaw.youbora.lib6.constants.RequestParams.DIMENSIONS;
import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.npaw.youbora.lib6.constants.RequestParams.TRIGGERED_EVENTS;
import static com.npaw.youbora.lib6.constants.Services.JOIN;

public class RequestBuilderTest {

    private RequestBuilder builder;
    private Plugin mockPlugin;

    private static final String [] ALL_PARAMS = new String[] {"playhead", "playrate", "fps",
            "droppedFrames", RequestParams.SEGMENT_DURATION, "mediaDuration", "bitrate", "throughput", "rendition", "title",
            "program", "live", "mediaResource", "transactionCode", "properties", "playerVersion",
            "player", "cdn", "pluginVersion", "libVersion", "param1", "param2", "param3", "param4", "param5",
            "param6", "param7", "param8", "param9", "param10",
            RequestParams.AD_POSITION, DIMENSIONS, RequestParams.PARSED_RESOURCE,
            RequestParams.TOTAL_BYTES, "adPlayhead", "adDuration",
            "adBitrate", "adTitle", "adResource", "adPlayerVersion", "adProperties",
            "adAdapterVersion", "pluginInfo", "isp", "connectionType", "ip", "deviceCode",
            "system", "accountCode", "username", "anonymousUser", RequestParams.PRIVACY_PROTOCOL, "preloadDuration", "joinDuration",
            "bufferDuration", "seekDuration", "pauseDuration", "adJoinDuration", "adBufferDuration",
            "adPauseDuration", "adTotalDuration", "nodeHost", "nodeType", "nodeTypeString",
            "streamingProtocol", "extraparam1", "extraparam2", "extraparam3", "extraparam4",
            "extraparam5", "extraparam6", "extraparam7", "extraparam8", "extraparam9",
            "extraparam10", "adCampaign", "adCreativeId", "adProvider", "obfuscateIp", "userType",
            "deviceInfo", "experiments", "latency", "packetLoss", "packetSent", "householdId",
            RequestParams.P2P_DOWNLOADED_TRAFFIC, RequestParams.CDN_DOWNLOADED_TRAFFIC, RequestParams.UPLOADED_TRAFFIC, RequestParams.CDN_BALANCER_RESPONSE_UUID,
            "smartswitchConfigCode", "smartswitchGroupCode", "smartswitchContractCode",
            "deviceUUID", "sessionMetrics", "metrics", "email", "package", "saga", "tvshow",
            "season", "titleEpisode", "channel", "contentId", "imdbID", "gracenoteID",
            "contentType", "genre", "contractedResolution", "cost", "price", "playbackType",
            "drm", "videoCodec", "audioCodec", "codecSettings", "codecProfile", "containerFormat",
            "givenBreaks", "expectedBreaks", "expectedPattern", "breaksTime", "givenAds",
            "expectedAds", "adsExpected", "audio", "skippable", "fullscreen", "profileName", "v" };

    @Before
    public void setUp() {
        mockPlugin = mock(Plugin.class);
        builder = spy(new RequestBuilder(mockPlugin));

        // Mocks
        when(mockPlugin.getCustomDimensions()).thenReturn("a");
        when(mockPlugin.getPlayhead()).thenReturn(1.0);
        when(mockPlugin.getPlayrate()).thenReturn(2.0);
        when(mockPlugin.getFramesPerSecond()).thenReturn(3.0);
        when(mockPlugin.getDroppedFrames()).thenReturn(4);
        when(mockPlugin.getDuration()).thenReturn(5.0);
        when(mockPlugin.getSegmentDuration()).thenReturn(7L);
        when(mockPlugin.getBitrate()).thenReturn(6L);
        when(mockPlugin.getThroughput()).thenReturn(7L);
        when(mockPlugin.getRendition()).thenReturn("a");
        when(mockPlugin.getTitle()).thenReturn("b");
        when(mockPlugin.getProgram()).thenReturn("c");
        when(mockPlugin.getIsLive()).thenReturn(true);
        when(mockPlugin.getResource()).thenReturn("d");
        when(mockPlugin.getTransactionCode()).thenReturn("e");
        when(mockPlugin.getContentMetadata()).thenReturn("f");
        when(mockPlugin.getPlayerVersion()).thenReturn("g");
        when(mockPlugin.getPlayerName()).thenReturn("h");
        when(mockPlugin.getCdn()).thenReturn("i");
        when(mockPlugin.getPluginVersion()).thenReturn("j");
        when(mockPlugin.getLibVersion()).thenReturn("v");
        when(mockPlugin.getContentCustomDimension1()).thenReturn("j");
        when(mockPlugin.getContentCustomDimension2()).thenReturn("l");
        when(mockPlugin.getContentCustomDimension3()).thenReturn("m");
        when(mockPlugin.getContentCustomDimension4()).thenReturn("n");
        when(mockPlugin.getContentCustomDimension5()).thenReturn("o");
        when(mockPlugin.getContentCustomDimension6()).thenReturn("p");
        when(mockPlugin.getContentCustomDimension7()).thenReturn("q");
        when(mockPlugin.getContentCustomDimension8()).thenReturn("r");
        when(mockPlugin.getContentCustomDimension9()).thenReturn("s");
        when(mockPlugin.getContentCustomDimension10()).thenReturn("t");
        when(mockPlugin.getAdPosition()).thenReturn("u");
        when(mockPlugin.getAdPlayhead()).thenReturn(8.0);
        when(mockPlugin.getAdDuration()).thenReturn(9.0);
        when(mockPlugin.getAdBitrate()).thenReturn(10L);
        when(mockPlugin.getAdTitle()).thenReturn("w");
        when(mockPlugin.getAdResource()).thenReturn("x");
        when(mockPlugin.getAdPlayerVersion()).thenReturn("y");
        when(mockPlugin.getAdMetadata()).thenReturn("z");
        when(mockPlugin.getAdAdapterVersion()).thenReturn("aa");
        when(mockPlugin.getPluginInfo()).thenReturn("ab");
        when(mockPlugin.getIsp()).thenReturn("ac");
        when(mockPlugin.getConnectionType()).thenReturn("ad");
        when(mockPlugin.getIp()).thenReturn("ae");
        when(mockPlugin.getAccountCode()).thenReturn("agah");
        when(mockPlugin.getUsername()).thenReturn("ai");
        when(mockPlugin.getUserAnonymousId()).thenReturn("anon");
        when(mockPlugin.getUserPrivacyProtocol()).thenReturn("optin");
        when(mockPlugin.getUserProfileId()).thenReturn("profileId");
        when(mockPlugin.getPreloadDuration()).thenReturn(11L);
        when(mockPlugin.getJoinDuration()).thenReturn(12L);
        when(mockPlugin.getBufferDuration()).thenReturn(13L);
        when(mockPlugin.getSeekDuration()).thenReturn(14L);
        when(mockPlugin.getPauseDuration()).thenReturn(15L);
        when(mockPlugin.getAdJoinDuration()).thenReturn(16L);
        when(mockPlugin.getAdBufferDuration()).thenReturn(17L);
        when(mockPlugin.getAdPauseDuration()).thenReturn(18L);
        when(mockPlugin.getAdTotalDuration()).thenReturn(19L);
        when(mockPlugin.getNodeHost()).thenReturn("aj");
        when(mockPlugin.getNodeType()).thenReturn("ak");
        when(mockPlugin.getNodeTypeString()).thenReturn("al");
        when(mockPlugin.getAppName()).thenReturn("appName");
        when(mockPlugin.getAppReleaseVersion()).thenReturn("appReleaseVersion");
        when(mockPlugin.getCdnBalancerVersion()).thenReturn("1.0.0-android");
        when(mockPlugin.getCdnBalancerProfileName()).thenReturn("profile");
    }

    @Test
    public void testBuildParamsFetch() {
        // Test that buildparams fetches the corresponding params
        builder.buildParams(null, JOIN);

        verify(builder, atLeastOnce()).fetchParams(nullable(Map.class),
                eq(new String[] {"joinDuration", "playhead", "triggeredEvents"}), eq(false));
    }

    @Test
    public void testParamsFetchedFromPlugin() {
        Map<String, String> params = builder.fetchParams(null, ALL_PARAMS, false);

        assertEquals("u", params.get(RequestParams.AD_POSITION));
        assertEquals("a", params.get(DIMENSIONS));
        assertEquals("1.0", params.get("playhead"));
        assertEquals("2.0", params.get("playrate"));
        assertEquals("3.0", params.get("fps"));
        assertEquals("4", params.get("droppedFrames"));
        assertEquals("5.0", params.get("mediaDuration"));
        assertEquals("7", params.get("segmentDuration"));
        assertEquals("6", params.get("bitrate"));
        assertEquals("7", params.get("throughput"));
        assertEquals("a", params.get("rendition"));
        assertEquals("b", params.get("title"));
        assertEquals("c", params.get("program"));
        assertEquals("true", params.get("live"));
        assertEquals("d", params.get("mediaResource"));
        assertEquals("e", params.get("transactionCode"));
        assertEquals("f", params.get("properties"));
        assertEquals("g", params.get("playerVersion"));
        assertEquals("h", params.get("player"));
        assertEquals("i", params.get("cdn"));
        assertEquals("j", params.get("pluginVersion"));
        assertEquals("v", params.get("libVersion"));
        assertEquals("j", params.get("param1"));
        assertEquals("l", params.get("param2"));
        assertEquals("m", params.get("param3"));
        assertEquals("n", params.get("param4"));
        assertEquals("o", params.get("param5"));
        assertEquals("p", params.get("param6"));
        assertEquals("q", params.get("param7"));
        assertEquals("r", params.get("param8"));
        assertEquals("s", params.get("param9"));
        assertEquals("t", params.get("param10"));
        assertEquals("8.0", params.get("adPlayhead"));
        assertEquals("9.0", params.get("adDuration"));
        assertEquals("10", params.get("adBitrate"));
        assertEquals("w", params.get("adTitle"));
        assertEquals("x", params.get("adResource"));
        assertEquals("y", params.get("adPlayerVersion"));
        assertEquals("z", params.get("adProperties"));
        assertEquals("aa", params.get("adAdapterVersion"));
        assertEquals("ab", params.get("pluginInfo"));
        assertEquals("ac", params.get("isp"));
        assertEquals("ad", params.get("connectionType"));
        assertEquals("ae", params.get("ip"));
        assertEquals("agah", params.get("system"));
        assertEquals("agah", params.get("accountCode"));
        assertEquals("ai", params.get("username"));
        assertEquals("anon", params.get("anonymousUser"));
        assertEquals("optin", params.get("privacyProtocol"));
        assertEquals("11", params.get("preloadDuration"));
        assertEquals("12", params.get("joinDuration"));
        assertEquals("13", params.get("bufferDuration"));
        assertEquals("14", params.get("seekDuration"));
        assertEquals("15", params.get("pauseDuration"));
        assertEquals("16", params.get("adJoinDuration"));
        assertEquals("17", params.get("adBufferDuration"));
        assertEquals("18", params.get("adPauseDuration"));
        assertEquals("19", params.get("adTotalDuration"));
        assertEquals("aj", params.get("nodeHost"));
        assertEquals("ak", params.get("nodeType"));
        assertEquals("al", params.get("nodeTypeString"));
        assertEquals("1.0.0-android", params.get("v"));
        assertEquals("profile", params.get("profileName"));
    }

    @Test
    public void testAdNumber() {
        // Prerolls
        when(mockPlugin.getAdPosition()).thenReturn("pre");

        for (int i = 1; i<=10; i++) {
            assertEquals(Integer.toString(i), builder.getNewAdNumber());
            builder.fetchParams(null, new String[]{RequestParams.AD_POSITION}, false);
        }

        // Midrolls
        when(mockPlugin.getAdPosition()).thenReturn("mid");

        for (int i = 1; i<=10; i++) {
            assertEquals(Integer.toString(i), builder.getNewAdNumber());
            builder.fetchParams(null, new String[]{RequestParams.AD_POSITION}, false);
        }

        // Postrolls
        when(mockPlugin.getAdPosition()).thenReturn("post");

        for (int i = 1; i<=10; i++) {
            assertEquals(Integer.toString(i), builder.getNewAdNumber());
            builder.fetchParams(null, new String[]{RequestParams.AD_POSITION}, false);
        }
    }

    @Test
    public void testAdNumberInBreak() {
        for (int i = 1; i <= 10; i++)
            assertEquals(Integer.toString(i), builder.getNewAdNumberInBreak());
    }

    @Test
    public void newAdBreakNumber() {
        for (int i = 1; i <= 10; i++) {
            assertEquals(Integer.toString(i), builder.getNewAdBreakNumber());
            assertEquals("1", builder.getNewAdNumberInBreak());
            assertEquals("2", builder.getNewAdNumberInBreak());
            assertEquals("3", builder.getNewAdNumberInBreak());
        }
    }

    @Test
    public void testInformedParamsAreNotOverwritten() {
        Map<String, String> params = new HashMap<>(3);
        params.put("playhead", "informedPlayhead");
        params.put("playrate", "informedPlayrate");
        params.put("nodeTypeString", "informedNodeTypeString");

        params = builder.fetchParams(params, ALL_PARAMS, false);

        assertEquals("informedPlayhead", params.get("playhead"));
        assertEquals("informedPlayrate", params.get("playrate"));
        assertEquals("informedNodeTypeString", params.get("nodeTypeString"));
    }

    @Test
    public void testChangedEntities() {
        builder.fetchParams(null, ALL_PARAMS, false);

        Map<String, String> params = builder.getChangedEntities();

        assertEquals(0, params.size());

        when(mockPlugin.getProgram()).thenReturn("newProgram");

        params = builder.getChangedEntities();

        assertEquals(1, params.size());

        assertEquals("newProgram", params.get("program"));
    }

    @Test
    public void testFirstTriggeredEvents() {
        Map<String, String> params = builder.fetchParams(null, new String[] { TRIGGERED_EVENTS }, false);
        assertEquals(1, params.size());
        assertTrue( ((String) params.get("triggeredEvents")).startsWith("youbora.lib6.plugin.RequestBuilderTest::testFirstTriggeredEvents"));
    }
}