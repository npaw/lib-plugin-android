package com.npaw.youbora.lib6.persistence;

import androidx.test.core.app.ApplicationProvider;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.persistence.dao.EventDAO;
import com.npaw.youbora.lib6.persistence.dao.TestableEventDAO;
import com.npaw.youbora.lib6.persistence.datasource.EventDataSource;
import com.npaw.youbora.lib6.persistence.datasource.QuerySuccessListener;
import com.npaw.youbora.lib6.persistence.entity.Event;

import com.npaw.youbora.lib6.persistence.helper.EventDbHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.npaw.youbora.lib6.persistence.helper.EventDbHelper.DATABASE_NAME;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Enrique on 03/01/2018.
 */
@SuppressWarnings("unchecked")
@RunWith(RobolectricTestRunner.class)
public class EventDataSourceTest {

    private EventDataSource dataSource;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        YouboraLog.YouboraLogger mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);

        reset();

        EventDAO eventDAO = new TestableEventDAO(new EventDbHelper(RuntimeEnvironment.getApplication()), ApplicationProvider.getApplicationContext().getDatabasePath(DATABASE_NAME));
        dataSource = new EventDataSource(eventDAO);
    }

    @After
    public void closeDb() {
        dataSource.close();
    }

    @Test
    public void testInsertNewElement() throws InterruptedException {
        final CountDownLatch latchTicks = new CountDownLatch(1);

        ArgumentCaptor<Long> mapCaptor = ArgumentCaptor.forClass(Long.class);
        QuerySuccessListener<Long> listener = (QuerySuccessListener<Long>) mock(QuerySuccessListener.class);
        dataSource.insertNewElement(getMockEvent1(), listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(1L, mapCaptor.getValue().longValue()); //new row uId
    }

    @Test
    public void testInsertNewElements() throws InterruptedException {
        CountDownLatch latchTicks = new CountDownLatch(1);
        List<Event> events = new ArrayList<Event>(){{
            add(getMockEvent1());
        }};
        ArgumentCaptor<Long> mapCaptor = ArgumentCaptor.forClass(Long.class);
        QuerySuccessListener<Long> listener = (QuerySuccessListener<Long>) mock(QuerySuccessListener.class);
        dataSource.insertElements(events, listener);

        latchTicks.await(10, TimeUnit.SECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(1L, mapCaptor.getValue().longValue()); //last inserted row id
    }

    @Test
    public void testGetLastId() throws InterruptedException {
        CountDownLatch latchTicks = new CountDownLatch(4);

        dataSource.insertNewElement(getMockEvent1(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ArgumentCaptor<Integer> mapCaptor = ArgumentCaptor.forClass(Integer.class);
        QuerySuccessListener<Integer> listener = (QuerySuccessListener<Integer>) mock(QuerySuccessListener.class);
        dataSource.getLastId(listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(0, mapCaptor.getValue().intValue());

        dataSource.insertNewElement(getMockEvent2(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        mapCaptor = ArgumentCaptor.forClass(Integer.class);
        listener = (QuerySuccessListener<Integer>) mock(QuerySuccessListener.class);
        dataSource.getLastId(listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(1, mapCaptor.getValue().intValue());
    }

    @Test
    public void testGetByOfflineId() throws InterruptedException {
        CountDownLatch latchTicks = new CountDownLatch(5);

        dataSource.insertNewElement(getMockEvent1(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ArgumentCaptor<List<Event>> mapCaptor = ArgumentCaptor.forClass(List.class);
        QuerySuccessListener<List<Event>> listener = (QuerySuccessListener<List<Event>>) mock(QuerySuccessListener.class);
        dataSource.getByOfflineId(0, listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(1, mapCaptor.getValue().size());
        assertEquals(1, mapCaptor.getValue().get(0).getUid());

        dataSource.insertNewElement(getMockEvent2(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        mapCaptor = ArgumentCaptor.forClass(List.class);
        listener = (QuerySuccessListener<List<Event>>) mock(QuerySuccessListener.class);
        dataSource.getByOfflineId(1, listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(1, mapCaptor.getValue().size());
        assertEquals(2, mapCaptor.getValue().get(0).getUid());

        mapCaptor = ArgumentCaptor.forClass(List.class);
        listener = (QuerySuccessListener<List<Event>>) mock(QuerySuccessListener.class);
        dataSource.getByOfflineId(2, listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(0, mapCaptor.getValue().size());
    }

    @Test
    public void testGetFirstId() throws InterruptedException {
        CountDownLatch latchTicks = new CountDownLatch(4);

        dataSource.insertNewElement(getMockEvent1(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ArgumentCaptor<Integer> mapCaptor = ArgumentCaptor.forClass(Integer.class);
        QuerySuccessListener<Integer> listener = (QuerySuccessListener<Integer>) mock(QuerySuccessListener.class);
        dataSource.getFirstOfflineId(listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(0, (int)mapCaptor.getValue());

        dataSource.insertNewElement(getMockEvent2(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        mapCaptor = ArgumentCaptor.forClass(Integer.class);
        listener = (QuerySuccessListener<Integer>) mock(QuerySuccessListener.class);
        dataSource.getFirstOfflineId(listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(0, (int)mapCaptor.getValue());
    }

    @Test
    public void testGetAll() throws InterruptedException {
        CountDownLatch latchTicks = new CountDownLatch(4);

        dataSource.insertNewElement(getMockEvent1(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ArgumentCaptor<List<Event>> mapCaptor = ArgumentCaptor.forClass(List.class);
        QuerySuccessListener<List<Event>> listener = (QuerySuccessListener<List<Event>>) mock(QuerySuccessListener.class);
        dataSource.getAll(listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(1, mapCaptor.getValue().size());
        assertEquals(1, mapCaptor.getValue().get(0).getUid());

        dataSource.insertNewElement(getMockEvent1(), null);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        mapCaptor = ArgumentCaptor.forClass(List.class);
        listener = (QuerySuccessListener<List<Event>>) mock(QuerySuccessListener.class);
        dataSource.getAll(listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(2, mapCaptor.getValue().size());
        assertEquals(1, mapCaptor.getValue().get(0).getUid());
        assertEquals(2, mapCaptor.getValue().get(1).getUid());
    }

    @Test
    public void testDeleteEvent() throws InterruptedException {
        CountDownLatch latchTicks = new CountDownLatch(2);
        List<Event> events = new ArrayList<Event>(){{
            add(getMockEvent1());
            add(getMockEvent2());
        }};
        dataSource.insertElements(events, null);

        latchTicks.await(3, TimeUnit.SECONDS);

        ArgumentCaptor<Integer> mapCaptor = ArgumentCaptor.forClass(Integer.class);
        QuerySuccessListener<Integer> listener = (QuerySuccessListener<Integer>) mock(QuerySuccessListener.class);

        dataSource.deleteElement(getMockEvent1(), listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(1, (int)mapCaptor.getValue());
    }

    @Test
    public void testDeleteAll() throws InterruptedException {
        CountDownLatch latchTicks = new CountDownLatch(2);
        List<Event> events = new ArrayList<Event>(){{
            add(getMockEvent1());
            add(getMockEvent2());
        }};
        dataSource.insertElements(events, null);

        latchTicks.await(3, TimeUnit.SECONDS);

        ArgumentCaptor<Integer> mapCaptor = ArgumentCaptor.forClass(Integer.class);
        QuerySuccessListener<Integer> listener = (QuerySuccessListener<Integer>) mock(QuerySuccessListener.class);

        dataSource.deleteAll(listener);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(listener).onQueryResolved(mapCaptor.capture());
        assertEquals(2, (int)mapCaptor.getValue());
    }

    private Event getMockEvent1() {
        return new Event("dummy events", System.currentTimeMillis(), 0);
    }

    private Event getMockEvent2() {
        return new Event("dummy events", System.currentTimeMillis(), 1);
    }
}
