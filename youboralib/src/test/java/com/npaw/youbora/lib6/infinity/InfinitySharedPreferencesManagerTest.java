package com.npaw.youbora.lib6.infinity;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

@RunWith(RobolectricTestRunner.class)
public class InfinitySharedPreferencesManagerTest {

    InfinityStorageContract infinityStorage;

    @Before
    public void init() {
        infinityStorage = new InfinitySharedPreferencesManager(ApplicationProvider
                .getApplicationContext());
    }

    @Test
    public void testSaveAndGetSessionId() {
        assertNull(infinityStorage.getSessionId());
        infinityStorage.saveSessionId("1");
        assertEquals(infinityStorage.getSessionId(), "1");
    }

    @Test
    public void testSaveAndGetContext() {
        assertNull(infinityStorage.getContext());
        infinityStorage.saveContext("2");
        assertEquals(infinityStorage.getContext(), "2");
    }

    @Test
    public void testSaveAndGetLastActive() {
        assertEquals(infinityStorage.getLastActive(), -1L);

        long currentMillis = System.currentTimeMillis();
        infinityStorage.saveLastActive();

        assertTrue((infinityStorage.getLastActive() - currentMillis) < 5);
    }
}
