package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.flags.BaseFlags;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import org.mockito.ArgumentMatchers;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PlayheadMonitorTest {

    private PlayerAdapter adapter;
    private Chrono mockChrono;
    private Timer mockTimer;
    private BaseFlags mockFlags;

    private PlayheadMonitor monitor;
    private final int MONITORING_INTERVAL = 800;

    // Captured params when setting the timer
    private Timer.TimerEventListener timerListener;
    private long timerInterval;

    private class TestPlayheadMonitor extends PlayheadMonitor {

        TestPlayheadMonitor(PlayerAdapter adapter, int type, int interval) {
            super(adapter, type, interval);
        }

        @NotNull
        @Override
        public Chrono createChrono() {
            return mockChrono;
        }

        @Override
        public Timer createTimer(@NotNull Timer.TimerEventListener listener, long interval) {
            timerListener = listener;
            timerInterval = interval;
            return mockTimer;
        }
    }

    @Before
    public void setUp() {
        adapter = mock(PlayerAdapter.class);
        mockChrono = mock(Chrono.class);
        mockTimer = mock(Timer.class);
        mockFlags = mock(BaseFlags.class);
    }

    @Test
    public void testIntervalAndStartStop() {
        PlayheadMonitor monitor = new TestPlayheadMonitor(adapter, PlayheadMonitor.TYPE_BUFFER,
                MONITORING_INTERVAL);

        // Mock flags
        BaseFlags mockFlags = mock(BaseFlags.class);
        when(adapter.getFlags()).thenReturn(mockFlags);

        long interval = timerInterval;

        assertEquals(MONITORING_INTERVAL, interval);

        monitor.start();
        verify(mockTimer, times(1)).start();
        assertTrue(monitor.isRunning());

        monitor.stop();
        verify(mockTimer, times(1)).stop();
        assertFalse(monitor.isRunning());
    }

    @Test
    public void testHealthy() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        for (int i = 1; i <= 10; i++) {
            when(adapter.getPlayhead()).thenReturn(i*MONITORING_INTERVAL/1000.0);
            timerListener.onTimerEvent(MONITORING_INTERVAL);
        }

        verifyNotBuffering();
        verifyNotSeeking();
    }

    @Test
    public void testBuffer() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead won't progress
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1))
                .fireBufferBegin(anyBoolean(), ArgumentMatchers.<String, String>anyMap());
        when(mockFlags.isBuffering()).thenReturn(true);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(2 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1))
                .fireBufferEnd(ArgumentMatchers.<String, String>anyMap());

        verifyNotSeeking();
    }

    @Test
    public void testSeek() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead jump
        when(adapter.getPlayhead()).thenReturn(10 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1)).fireSeekBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        when(mockFlags.isSeeking()).thenReturn(true);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(11 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1))
                .fireSeekEnd(ArgumentMatchers.<String, String>anyMap());

        verifyNotBuffering();
    }

    @Test
    public void testBufferNotFiredWhenMonitoringSeek() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead won't progress
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(2 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verifyNotBuffering();
        verifyNotSeeking();
    }

    @Test
    public void testSeekNotFiredWhenMonitoringBuffer() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead jump
        when(adapter.getPlayhead()).thenReturn(10 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(11 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verifyNotBuffering();
        verifyNotSeeking();
    }

    @Test
    public void testBufferThenSeek() throws Exception {
        // Here we test buffer -> seek conversion
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // Playhead won't progress
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(1)).fireBufferBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        when(mockFlags.isBuffering()).thenReturn(true);

        // Playhead jump
        when(adapter.getPlayhead()).thenReturn(10 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);
        verify(adapter, times(1)).fireSeekBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        when(mockFlags.isBuffering()).thenReturn(false);
        when(mockFlags.isSeeking()).thenReturn(true);

        // Restore playhead progress
        when(adapter.getPlayhead()).thenReturn(11 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verify(adapter, times(0))
                .fireBufferEnd(ArgumentMatchers.<String, String>anyMap());
        verify(adapter, times(1))
                .fireSeekEnd(ArgumentMatchers.<String, String>anyMap());

    }

    @Test
    public void testSkipNextTick() throws Exception {
        prepareMocks(PlayheadMonitor.TYPE_BUFFER | PlayheadMonitor.TYPE_SEEK);

        when(mockFlags.isStarted()).thenReturn(true);
        when(mockFlags.isJoined()).thenReturn(true);

        when(mockChrono.stop()).thenReturn((long) MONITORING_INTERVAL);

        when(adapter.getPlayhead()).thenReturn(MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        // This will prevent buffer from firing the next tick
        monitor.skipNextTick();

        // Playhead progresses a tiny bit, enough to trigger buffer in normal cases
        when(adapter.getPlayhead()).thenReturn(1.1 * MONITORING_INTERVAL/1000.0);
        timerListener.onTimerEvent(MONITORING_INTERVAL);

        verifyNotBuffering();
        verifyNotSeeking();
    }

    private void prepareMocks(int monitoringType) {

        monitor = new TestPlayheadMonitor(adapter, monitoringType, MONITORING_INTERVAL);

        // Mock flags
        when(adapter.getFlags()).thenReturn(mockFlags);

    }

    private void verifyNotBuffering() {
        verify(adapter, times(0)).fireBufferBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
    }

    private void verifyNotSeeking() {
        verify(adapter, times(0)).fireSeekBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
    }

}