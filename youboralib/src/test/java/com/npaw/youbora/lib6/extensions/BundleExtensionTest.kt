package com.npaw.youbora.lib6.extensions

import android.os.Bundle

import org.junit.Test
import org.junit.runner.RunWith

import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class BundleExtensionTest {

    @Test
    fun testToJson() {
        val bundle = Bundle()
        bundle.putString("key", null)
        bundle.toJson()
    }
}