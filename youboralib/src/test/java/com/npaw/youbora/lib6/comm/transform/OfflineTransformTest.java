package com.npaw.youbora.lib6.comm.transform;

import androidx.test.core.app.ApplicationProvider;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.persistence.dao.EventDAO;
import com.npaw.youbora.lib6.persistence.dao.TestableEventDAO;
import com.npaw.youbora.lib6.persistence.datasource.EventDataSource;
import com.npaw.youbora.lib6.persistence.entity.Event;
import com.npaw.youbora.lib6.persistence.helper.EventDbHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowLooper;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static com.npaw.youbora.lib6.constants.Services.INIT;
import static com.npaw.youbora.lib6.constants.Services.PING;
import static com.npaw.youbora.lib6.constants.Services.START;
import static com.npaw.youbora.lib6.constants.Services.STOP;
import static com.npaw.youbora.lib6.persistence.helper.EventDbHelper.DATABASE_NAME;

/**
 * Created by Enrique A. on 26/12/2017.
 */
@RunWith(RobolectricTestRunner.class)
public class OfflineTransformTest {

    private OfflineTransform offlineTransform;
    private Request mockRequest;
    private EventDAO eventDAO;
    private EventDataSource dataSource;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        YouboraLog.YouboraLogger mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);
        eventDAO = new TestableEventDAO(new EventDbHelper(RuntimeEnvironment.getApplication()), ApplicationProvider.getApplicationContext().getDatabasePath(DATABASE_NAME));
        dataSource = new EventDataSource(eventDAO);
        offlineTransform = new OfflineTransform(dataSource);
        mockRequest = getMockRequest(START);
    }

    @After
    public void tearDown() {
        offlineTransform = null;
        dataSource.close();
    }

    @Test
    public void testHasToSend() {
        Request mockRequest = mock(Request.class);

        assertFalse(offlineTransform.hasToSend(mockRequest));
    }

    @Test
    public void testState() {
        assertEquals(offlineTransform.getState(),Transform.STATE_OFFLINE);
    }

    @Test
    public void testParse() throws InterruptedException {

        final CountDownLatch latchTicks = new CountDownLatch(5);

        offlineTransform.parse(mockRequest);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        List<Event> events = eventDAO.getAll();
        assertTrue(events.size() != 0);
        assertEquals(0, events.get(0).getOfflineId());

        mockRequest = getMockRequest(PING);

        offlineTransform.parse(mockRequest);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        events = eventDAO.getAll();
        assertEquals(2, events.size());
        assertEquals(0, events.get(0).getOfflineId());
        assertEquals(0, events.get(1).getOfflineId());

        mockRequest = getMockRequest(STOP);

        offlineTransform.parse(mockRequest);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        events = eventDAO.getAll();
        assertEquals(3, events.size());
        assertEquals(0, events.get(0).getOfflineId());
        assertEquals(0, events.get(1).getOfflineId());
        assertEquals(0, events.get(2).getOfflineId());

        mockRequest = getMockRequest(INIT);

        offlineTransform.parse(mockRequest);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        events = eventDAO.getAll();
        assertEquals(3, events.size());
        assertEquals(0, events.get(0).getOfflineId());
        assertEquals(0, events.get(1).getOfflineId());
        assertEquals(0, events.get(2).getOfflineId());

        mockRequest = getMockRequest(START);

        offlineTransform.parse(mockRequest);

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        events = eventDAO.getAll();
        assertEquals(4, events.size());
        assertEquals(0, events.get(0).getOfflineId());
        assertEquals(0, events.get(1).getOfflineId());
        assertEquals(0, events.get(2).getOfflineId());
        assertEquals(1, events.get(3).getOfflineId());
    }

    private Request getMockRequest(String service) {
        return new Request(null, service){{
            setParams(new HashMap<String, Object>());
        }};
    }
}
