package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.flags.AdFlags;
import com.npaw.youbora.lib6.flags.BaseFlags;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.robolectric.RobolectricTestRunner;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class PlayerAdapterTest {

    private class CustomAdapter extends PlayerAdapter<String> {
        boolean unregisterListenersCalled = false;
        boolean registerListenersCalled = false;

        CustomAdapter(String player) {
            super(player);
            registerListeners();
        }

        @Override
        public void registerListeners() { registerListenersCalled = true; }

        @Override
        public void unregisterListeners() { unregisterListenersCalled = true; }
    }

    private class CustomAdsAdapter extends AdAdapter<String> {
        AdAdapter.AdPosition adPosition;

        CustomAdsAdapter(String player) {
            super(player);
            registerListeners();
            adPosition = AdAdapter.AdPosition.UNKNOWN;
        }

        @Override
        public AdAdapter.AdPosition getPosition() { return adPosition; }

        private void setAdPosition(AdAdapter.AdPosition adPosition) { this.adPosition = adPosition; }
    }

    @Test
    public void testRegisterUnregister() {
        CustomAdapter adapter = new CustomAdapter("test");

        assertEquals(true, adapter.registerListenersCalled);
        assertEquals(false, adapter.unregisterListenersCalled);

        adapter.dispose();

        assertEquals(true, adapter.unregisterListenersCalled);
    }

    @Test
    public void testDispose() {
        CustomAdapter adapter = spy(new CustomAdapter("test"));

        PlayheadMonitor mockMonitor = mock(PlayheadMonitor.class);
        when(adapter.createPlayheadMonitor(eq(adapter), anyInt(), anyInt()))
                .thenReturn(mockMonitor);

        adapter.monitorPlayhead(true, true, 800);

        adapter.dispose();

        verify(adapter).fireStop(new HashMap<String, String>());
        verify(mockMonitor).stop();
        verify(adapter).unregisterListeners();
        assertNull(adapter.getPlayer());
    }

    @Test
    public void testSettersGetters() {
        CustomAdapter adapter = spy(new CustomAdapter("test"));

        // Plugin
        Plugin mockPlugin = mock(Plugin.class);
        adapter.setPlugin(mockPlugin);
        assertEquals(mockPlugin, adapter.getPlugin());

        // Chronos
        assertNotNull(adapter.getChronos());

        // Flags
        assertNotNull(adapter.getFlags());

        // Monitor null
        assertNull(adapter.getMonitor());

        // Monitor not null
        PlayheadMonitor mockMonitor = mock(PlayheadMonitor.class);
        when(adapter.createPlayheadMonitor(eq(adapter), anyInt(), anyInt()))
                .thenReturn(mockMonitor);
        adapter.monitorPlayhead(true, true, 800);
        assertEquals(mockMonitor, adapter.getMonitor());
    }

    @Test
    public void testInfoMethodsDefaults() {
        CustomAdapter adapter = new CustomAdapter("test");
        CustomAdsAdapter adAdapter = new CustomAdsAdapter("adTest");

        assertNull(adapter.getPlayhead());
        assertEquals(1.0, adapter.getPlayrate());
        assertNull(adapter.getFramesPerSecond());
        assertNull(adapter.getDroppedFrames());
        assertNull(adapter.getDuration());
        assertNull(adapter.getBitrate());
        assertNull(adapter.getThroughput());
        assertNull(adapter.getRendition());
        assertNull(adapter.getTitle());
        assertNull(adapter.getProgram());
        assertNull(adapter.getIsLive());
        assertNull(adapter.getResource());
        assertNull(adapter.getMetrics());
        assertNull(adapter.getPlayerVersion());
        assertNull(adapter.getPlayerName());
        assertNull(adapter.getCdnTraffic());
        assertNull(adapter.getP2PTraffic());
        assertNull(adapter.getIsP2PEnabled());
        assertNull(adapter.getUploadTraffic());
        assertNull(adapter.getLatency());
        assertNull(adapter.getPacketLoss());
        assertNull(adapter.getPacketSent());
        assertNull(adapter.getHouseholdId());
        assertNull(adapter.getTotalBytes());
        assertNull(adapter.getAudioCodec());
        assertNull(adapter.getVideoCodec());
        assertNull(adAdapter.getGivenBreaks());
        assertNull(adAdapter.getExpectedBreaks());
        assertNull(adAdapter.getExpectedPattern());
        assertNull(adAdapter.getBreaksTime());
        assertNull(adAdapter.getGivenAds());
        assertNull(adAdapter.getExpectedAds());
        assertNull(adAdapter.getIsAudioEnabled());
        assertNull(adAdapter.getIsAdSkippable());
        assertNull(adAdapter.getAdProvider());
        assertTrue(adAdapter.getIsFullscreen());
        assertNull(adAdapter.getAdCampaign());
        assertNull(adAdapter.getAdCreativeId());
        assertEquals(adAdapter.getPosition(), AdAdapter.AdPosition.UNKNOWN);
        assertEquals(BuildConfig.VERSION_NAME + "-generic", adapter.getVersion());
    }

    @Test
    public void testFireMethodsFlags() {
        CustomAdapter adapter = new CustomAdapter("test");
        CustomAdsAdapter adAdapter = new CustomAdsAdapter("adTest");

        BaseFlags flags = adapter.getFlags();
        adapter.addEventListener(mock(PlayerAdapter.ContentAdapterEventListener.class));

        AdFlags adFlags = adAdapter.getAdFlags();
        adAdapter.addEventListener(mock(AdAdapter.AdAdapterEventListener.class));

        // Initial state
        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        assertFalse(adFlags.isAdInitiated());
        assertFalse(adFlags.isAdBreakStarted());

        // Ad manifest
        adAdapter.fireManifest();

        // Ad manifest with errorType
        adAdapter.fireManifest(AdAdapter.ManifestError.EMPTY_RESPONSE);

        // Ad break start
        adAdapter.fireAdBreakStart();

        assertFalse(adFlags.isJoined());
        assertFalse(adFlags.isBuffering());
        assertFalse(adFlags.isSeeking());
        assertFalse(adFlags.isPaused());
        assertFalse(adFlags.isAdInitiated());
        assertTrue(adFlags.isAdBreakStarted());

        // Ad init
        adAdapter.fireAdInit();

        assertFalse(adFlags.isJoined());
        assertFalse(adFlags.isBuffering());
        assertFalse(adFlags.isSeeking());
        assertFalse(adFlags.isPaused());
        assertTrue(adFlags.isAdInitiated());
        assertTrue(adFlags.isAdBreakStarted());

        // Ad click
        adAdapter.fireClick();

        // Ad break stop
        adAdapter.fireAdBreakStop();

        assertFalse(adFlags.isJoined());
        assertFalse(adFlags.isBuffering());
        assertFalse(adFlags.isSeeking());
        assertFalse(adFlags.isPaused());
        assertFalse(adFlags.isAdBreakStarted());

        // Start
        adapter.fireStart();

        assertTrue(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Ad quartile
        adAdapter.fireQuartile(1);

        // Join
        adapter.fireJoin();

        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Event
        adapter.fireEvent("", new HashMap<String, String>(), new HashMap<String, Double>(), new HashMap<String, String>());

        // Pause
        adapter.firePause();

        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertTrue(flags.isPaused());

        // Resume
        adapter.fireResume();

        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Buffer start
        adapter.fireBufferBegin();

        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertTrue(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Buffer end
        adapter.fireBufferEnd();

        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Seek start
        adapter.fireSeekBegin();

        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertTrue(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Seek end
        adapter.fireSeekEnd();

        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Stop
        adapter.fireStop();

        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then error (non fatal)
        adapter.fireStart();
        adapter.fireError(null, null, null);

        assertTrue(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then error (non fatal) passing exception
        adapter.fireStart();
        adapter.fireError(null, null, null, null);

        assertTrue(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then fatal error
        adapter.fireStart();
        adapter.fireFatalError(null, null, null);

        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then fatal error passing exception
        adapter.fireStart();
        adapter.fireFatalError(null, null, null, null);

        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then fatal error passing params
        adapter.fireStart();
        adapter.fireFatalError(new HashMap<String, String>());

        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());
    }

    @Test
    public void testFireMethodsCallbacks() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        adapter.addEventListener(mockListener);

        CustomAdsAdapter adAdapter = new CustomAdsAdapter("adTest");
        AdAdapter.AdAdapterEventListener mockAdListener =
                mock(AdAdapter.AdAdapterEventListener.class);

        adAdapter.addEventListener(mockAdListener);

        adapter.fireStart();
        verify(mockListener).onStart(ArgumentMatchers.<String, String>anyMap());

        adapter.fireJoin();
        verify(mockListener).onJoin(ArgumentMatchers.<String, String>anyMap());

        adapter.firePause();
        verify(mockListener).onPause(ArgumentMatchers.<String, String>anyMap());

        adapter.fireResume();
        verify(mockListener).onResume(ArgumentMatchers.<String, String>anyMap());

        adapter.fireBufferBegin();
        verify(mockListener).onBufferBegin(anyBoolean(), ArgumentMatchers.<String, String>anyMap());

        adapter.fireBufferEnd();
        verify(mockListener).onBufferEnd(ArgumentMatchers.<String, String>anyMap());

        adapter.fireSeekBegin();
        verify(mockListener).onSeekBegin(anyBoolean(), ArgumentMatchers.<String, String>anyMap());

        adapter.fireSeekEnd();
        verify(mockListener).onSeekEnd(ArgumentMatchers.<String, String>anyMap());

        adapter.fireStop();
        verify(mockListener).onStop(ArgumentMatchers.<String, String>anyMap());

        adAdapter.fireAdInit();
        verify(mockAdListener).onAdInit(ArgumentMatchers.<String, String>anyMap());

        adAdapter.fireClick();
        verify(mockAdListener).onClick(ArgumentMatchers.<String, String>anyMap());

        // Error
        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        adapter.fireStart();
        verify(mockListener, times(2))
                .onStart(new HashMap<String, String>());

        adapter.fireError();
        verify(mockListener).onError(captor.capture());

        // stop should be still only have been called once
        verify(mockListener).onStop(new HashMap<String, String>());
        //assertEquals("error", captor.getValue().get("errorLevel"));

        // Fatal error
        adapter.fireFatalError();
        verify(mockListener, times(2)).onError(captor.capture());

        // now stop should have been called twice in total
        verify(mockListener, times(2)).onStop(new HashMap<String, String>());
    }

    @Test
    public void testBufferToSeek() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();

        adapter.fireBufferBegin();
        adapter.fireSeekBegin();
        adapter.fireBufferEnd();
        adapter.fireSeekEnd();

        verify(mockListener, times(1))
                .onStart(ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1))
                .onJoin(ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1)).onBufferBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1)).onSeekBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1))
                .onSeekEnd(ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(0))
                .onBufferEnd(ArgumentMatchers.<String, String>anyMap());
    }

    @Test
    public void testFireBufferConvertFromSeek() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();

        // Convert from seek
        adapter.fireSeekBegin();
        adapter.fireBufferBegin(true);
        adapter.fireSeekEnd();
        adapter.fireBufferEnd();

        // Not convert from seek
        adapter.fireSeekBegin();
        adapter.fireBufferBegin(false);
        adapter.fireSeekEnd();
        adapter.fireBufferEnd();

        verify(mockListener, times(1)).onStart(new HashMap<String, String>());
        verify(mockListener, times(1)).onJoin(new HashMap<String, String>());
        verify(mockListener, times(1)).onBufferBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(2)).onSeekBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1)).onSeekEnd(new HashMap<String, String>());
        verify(mockListener, times(1)).onBufferEnd(new HashMap<String, String>());
    }

    @Test
    public void testFireSeekConvertFromBuffer() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();

        //Convert from buffer
        adapter.fireBufferBegin();
        adapter.fireSeekBegin(true);
        adapter.fireBufferEnd();
        adapter.fireSeekEnd();

        //Not convert from buffer
        adapter.fireBufferBegin();
        adapter.fireSeekBegin(false);
        adapter.fireBufferEnd();
        adapter.fireSeekEnd();

        verify(mockListener, times(1))
                .onStart(ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1))
                .onJoin(ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(2)).onBufferBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1)).onSeekBegin(anyBoolean(),
                ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1))
                .onSeekEnd(ArgumentMatchers.<String, String>anyMap());
        verify(mockListener, times(1))
                .onBufferEnd(ArgumentMatchers.<String, String>anyMap());
    }

    @Test
    public void testAddRemoveListener() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        assertFalse(adapter.removeEventListener(mockListener));

        adapter.addEventListener(mockListener);

        assertTrue(adapter.removeEventListener(mockListener));
        assertFalse(adapter.removeEventListener(mockListener));
    }

    @Test
    public void testGetPlayer() {
        CustomAdapter adapter = new CustomAdapter("test");
        assertEquals(adapter.getPlayer(),"test");
    }

    @Test
    public void testSetNullPlayer() {
        CustomAdapter adapter = new CustomAdapter("test");

        adapter.setPlayer(null);

        assertTrue(adapter.registerListenersCalled);
        assertTrue(adapter.unregisterListenersCalled);
    }

    @Test
    public void testSetNonNullPlayer() {
        CustomAdapter adapter = new CustomAdapter("test");

        adapter.setPlayer("test2");

        assertTrue(adapter.registerListenersCalled);
        assertTrue(adapter.unregisterListenersCalled);
    }

    @Test
    public void testFireStopWhilePaused(){
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();
        adapter.firePause();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        adapter.fireStop();
        verify(mockListener,times(1)).onStop(captor.capture());
        assertTrue(captor.getValue().containsKey("pauseDuration"));
        assertNotSame("-1", captor.getValue().get("pauseDuration"));
    }

    @Test
    public void testStartPreroll() {
        CustomAdsAdapter adsAdapter = new CustomAdsAdapter("player");
        adsAdapter.setAdPosition(AdAdapter.AdPosition.PRE);

        // Preroll with adInit
        adsAdapter.fireAdInit();
        assertTrue(adsAdapter.getAdFlags().isAdInitiated());
        assertNotNull(adsAdapter.getChronos().getJoin().getStartTime());
        adsAdapter.fireStart();
        assertTrue(adsAdapter.getFlags().isStarted());
        adsAdapter.fireStop();


        // Not preroll with adInit
        adsAdapter.setAdPosition(AdAdapter.AdPosition.MID);
        adsAdapter.fireAdInit();
        assertTrue(adsAdapter.getAdFlags().isAdInitiated());
        assertNotNull(adsAdapter.getChronos().getJoin().getStartTime());
        adsAdapter.fireStart();
        assertTrue(adsAdapter.getFlags().isStarted());
        adsAdapter.fireStop();

        // Unregister listeners
        adsAdapter.unregisterListeners();
    }

    @Test
    public void checkAdClickWithValidUrl() {
        CustomAdsAdapter adAdapter = new CustomAdsAdapter("test");

        AdAdapter.AdAdapterEventListener mockListener =
                mock(AdAdapter.AdAdapterEventListener.class);

        adAdapter.addEventListener(mockListener);

        adAdapter.fireStart();
        adAdapter.fireClick("Url");

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener, times(1)).onClick(captor.capture());
        assertTrue(captor.getValue().containsKey("adUrl"));
        assertSame("Url", captor.getValue().get("adUrl"));
    }

    @Test
    public void checkAdClickWithInvalidUrl() {
        CustomAdsAdapter adAdapter = new CustomAdsAdapter("test");

        AdAdapter.AdAdapterEventListener mockListener =
                mock(AdAdapter.AdAdapterEventListener.class);

        adAdapter.addEventListener(mockListener);

        adAdapter.fireStart();
        adAdapter.fireClick();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);
        verify(mockListener, times(1)).onClick(captor.capture());
        assertFalse(captor.getValue().containsKey("adUrl"));
    }

    @Test
    public void testFireSkip() {
        CustomAdsAdapter adAdapter = new CustomAdsAdapter("test");

        AdAdapter.AdAdapterEventListener mockListener =
                mock(AdAdapter.AdAdapterEventListener.class);

        adAdapter.addEventListener(mockListener);

        adAdapter.fireStart();
        adAdapter.fireSkip();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener, times(1)).onStop(captor.capture());
        assertTrue(captor.getValue().containsKey("skipped"));
        assertSame("true", captor.getValue().get("skipped"));
    }

    @Test
    public void testFireCast() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireCast();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener, times(1)).onStop(captor.capture());
        assertTrue(captor.getValue().containsKey("casted"));
        assertSame("true", captor.getValue().get("casted"));
    }

    @Test
    public void fireVideoEventWithNullParams() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireEvent();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener, times(1)).onVideoEvent(captor.capture());
        assertEquals("", captor.getValue().get("name"));
        assertEquals("{}", captor.getValue().get("dimensions"));
        assertEquals("{}", captor.getValue().get("values"));
    }

    @Test
    public void fireManifest() {
        CustomAdsAdapter adAdapter = new CustomAdsAdapter("test");

        AdAdapter.AdAdapterEventListener mockListener =
                mock(AdAdapter.AdAdapterEventListener.class);

        adAdapter.addEventListener(mockListener);

        adAdapter.fireManifest(AdAdapter.ManifestError.NO_RESPONSE, "Not found");

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener, times(1)).onManifest(captor.capture());
        assertTrue(captor.getValue().containsKey("errorType"));
        assertSame("NO_RESPONSE", captor.getValue().get("errorType"));
        assertTrue(captor.getValue().containsKey("errorMessage"));
        assertSame("Not found", captor.getValue().get("errorMessage"));
    }

    @Test
    public void fireQuartile() {
        CustomAdsAdapter adAdapter = new CustomAdsAdapter("test");

        AdAdapter.AdAdapterEventListener mockListener =
                mock(AdAdapter.AdAdapterEventListener.class);

        adAdapter.addEventListener(mockListener);

        adAdapter.fireStart();
        adAdapter.fireJoin();
        adAdapter.fireQuartile(1);

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener, times(1)).onQuartile(captor.capture());
        assertTrue(captor.getValue().containsKey("quartile"));
        assertEquals("1", captor.getValue().get("quartile"));
    }
}