package com.npaw.youbora.lib6.plugin;

import static com.npaw.youbora.lib6.plugin.Options.KEY_AUTH_TOKEN;
import static com.npaw.youbora.lib6.plugin.Options.KEY_AUTH_TYPE;

import android.os.Bundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.npaw.youbora.lib6.YouboraLog;

@RunWith(RobolectricTestRunner.class)
public class OptionsTest {
    private Options o;
    private Bundle optBundle;
    private Map<String, Object> fakeBundle;

    private final String authToken = "authToken";
    private final String authType = "authType";

    @Before
    public void setUp() {
        o = new Options();
        optBundle = mock(Bundle.class);
        fakeBundle = new HashMap<>();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                Boolean value = ((Boolean) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putBoolean(anyString(),anyBoolean());

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                String value = ((String) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putString(anyString(),anyString());


        when(optBundle.getString(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                Object[] arguments = invocation.getArguments();
                String key = ((String) arguments[0]);
                return (String)fakeBundle.get(key);
            }
        });


        when(optBundle.getBoolean(anyString())).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) {
                Object[] arguments = invocation.getArguments();
                String key = ((String) arguments[0]);
                return (Boolean)fakeBundle.get(key);
            }
        });

    }

    @After
    public void tearDown() {
        o = null;
    }

    @Test
    public void testOptions() {
        // Setting options using set method
        setValues(o);
        assertValues(o);

        // Setting options using Bundle
        setBundleAsOptions();
    }

    private void setValues(Options o) {
        // Set values
        o.setEnabled(false);
        o.setHttpSecure(true);
        o.setHost("a");
        o.setMethod(Options.Method.POST);
        o.setAccountCode("b");
        o.setUsername("c");
        o.setUserEmail("email@email.email");
        o.setUserAnonymousId("anon");
        o.setUserProfileId("profileId");
        o.setParseHls(true);
        o.setParseCdnNameHeader("d");
        o.setParseNodeHeader("n");
        o.setParseCdnNode(true);
        ArrayList<String> cdnNodes = new ArrayList<>();
        cdnNodes.add("listitem1");
        cdnNodes.add("listitem2");
        o.setParseCdnNodeList(cdnNodes);
        o.setNetworkIP("f");
        o.setNetworkIsp("g");
        o.setNetworkConnectionType("h");
        o.setUserObfuscateIp(true);
        o.setDeviceCode("i");
        o.setContentResource("j");
        o.setContentIsLive(true);
        o.setContentTitle("k");
        o.setProgram("l");
        o.setContentDuration(1.0);
        o.setContentTransactionCode("m");
        o.setContentSegmentDuration(7L);
        o.setContentBitrate(2L);
        o.setContentThroughput(3L);
        o.setContentRendition("n");
        o.setContentCdn("o");
        o.setContentFps(4.0);
        Bundle b = mock(Bundle.class);
        when(b.getString("p")).thenReturn("q");
        b.putString("p", "q");
        o.setContentMetadata(b);
        Bundle b2 = mock(Bundle.class);
        when(b2.getString("r")).thenReturn("s");
        b.putString("r", "s");
        o.setAdMetadata(b2);
        o.setAdIgnore(false);
        o.setAdsAfterStop(0);
        o.setAutoDetectBackground(false);
        o.setAutoStart(true);
        o.setOffline(false);
        o.setContentCustomDimension1("t");
        o.setContentCustomDimension2("u");
        o.setContentCustomDimension3("v");
        o.setContentCustomDimension4("w");
        o.setContentCustomDimension5("x");
        o.setContentCustomDimension6("y");
        o.setContentCustomDimension7("z");
        o.setContentCustomDimension8("aa");
        o.setContentCustomDimension9("ab");
        o.setContentCustomDimension10("ac");
        o.setContentCustomDimension11("ad");
        o.setContentCustomDimension12("ae");
        o.setContentCustomDimension13("af");
        o.setContentCustomDimension14("ag");
        o.setContentCustomDimension15("ah");
        o.setContentCustomDimension16("ai");
        o.setContentCustomDimension17("aj");
        o.setContentCustomDimension18("ak");
        o.setContentCustomDimension19("al");
        o.setContentCustomDimension20("am");
        o.setAdCustomDimension1("t");
        o.setAdCustomDimension2("u");
        o.setAdCustomDimension3("v");
        o.setAdCustomDimension4("w");
        o.setAdCustomDimension5("x");
        o.setAdCustomDimension6("y");
        o.setAdCustomDimension7("z");
        o.setAdCustomDimension8("aa");
        o.setAdCustomDimension9("ab");
        o.setAdCustomDimension10("ac");
        o.setUserType("an");
        o.setUserPrivacyProtocol("optin");
        o.setContentStreamingProtocol(Options.StreamingProtocol.DASH);
        o.setTransportFormat(Options.TransportFormat.TS);
        o.setAppName("appName");
        o.setAppReleaseVersion("appReleaseVersion");
        o.setDeviceEDID(new byte[] {0, -1, -1, -1, -1, -1, -1, 0, 30, 109, 1, 0, 1, 1, 1, 1, 1, 28,
                1, 3, -128, -96, 90, 120, 10, -18, -111, -93, 84, 76, -103, 38, 15, 80, 84, -95, 8,
                0, 49, 64, 69, 64, 97, 64, 113, 64, 1, 1, 1, 1, 1, 1, 1, 1, 102, 33, 80, -80, 81, 0,
                27, 48, 64, 112, 54, 0, 64, -124, 99, 0, 0, 30, 100, 25, 0, 64, 65, 0, 38, 48, 24,
                -120, 54, 0, 64, -124, 99, 0, 0, 24, 0, 0, 0, -3, 0, 58, 62, 30, 83, 16, 0, 10, 32,
                32, 32, 32, 32, 32, 0, 0, 0, -4, 0, 76, 71, 32, 84, 86, 10, 32, 32, 32, 32, 32, 32,
                32, 1, 113});

        o.setAuthToken(authToken);
        o.setAuthType(authType);
    }

    private void assertValues(Options o) {
        // Verify
        assertFalse(o.isEnabled());
        assertTrue(o.isHttpSecure());
        assertEquals("a", o.getHost());
        assertEquals(Options.Method.POST, o.getMethod());
        assertEquals("b", o.getAccountCode());
        assertEquals("c", o.getUsername());
        assertEquals("email@email.email", o.getUserEmail());
        assertEquals("anon", o.getUserAnonymousId());
        assertEquals("profileId", o.getUserProfileId());
        assertTrue(o.isParseHls());
        assertEquals("d", o.getParseCdnNameHeader());
        assertEquals("n", o.getParseNodeHeader());
        assertTrue(o.isParseCdnNode());
        assertEquals("listitem1", o.getParseCdnNodeList().get(0));
        assertEquals("listitem2", o.getParseCdnNodeList().get(1));
        assertEquals("f", o.getNetworkIP());
        assertEquals("g", o.getNetworkIsp());
        assertEquals("h", o.getNetworkConnectionType());
        assertTrue(o.getUserObfuscateIp());
        assertEquals("i", o.getDeviceCode());
        assertEquals("j", o.getContentResource());
        assertEquals(true, o.getContentIsLive());
        assertEquals("k", o.getContentTitle());
        assertEquals("l", o.getProgram());
        assertEquals((Double) 1.0, o.getContentDuration());
        assertEquals("m", o.getContentTransactionCode());
        assertEquals((Long) 7L, o.getContentSegmentDuration());
        assertEquals((Long) 2L, o.getContentBitrate());
        assertEquals((Long) 3L, o.getContentThroughput());
        assertEquals("n", o.getContentRendition());
        assertEquals("o", o.getContentCdn());
        assertEquals((Double) 4.0, o.getContentFps());
        assertEquals("q", o.getContentMetadata().getString("p"));
        assertEquals("s", o.getAdMetadata().getString("r"));
        assertFalse(o.getAdIgnore());
        assertEquals(0,o.getAdsAfterStop());
        assertFalse(o.isAutoDetectBackground());
        assertTrue(o.isAutoStart());
        assertFalse(o.isOffline());
        assertEquals("t", o.getContentCustomDimension1());
        assertEquals("u", o.getContentCustomDimension2());
        assertEquals("v", o.getContentCustomDimension3());
        assertEquals("w", o.getContentCustomDimension4());
        assertEquals("x", o.getContentCustomDimension5());
        assertEquals("y", o.getContentCustomDimension6());
        assertEquals("z", o.getContentCustomDimension7());
        assertEquals("aa", o.getContentCustomDimension8());
        assertEquals("ab", o.getContentCustomDimension9());
        assertEquals("ac", o.getContentCustomDimension10());
        assertEquals("ad", o.getContentCustomDimension11());
        assertEquals("ae", o.getContentCustomDimension12());
        assertEquals("af", o.getContentCustomDimension13());
        assertEquals("ag", o.getContentCustomDimension14());
        assertEquals("ah", o.getContentCustomDimension15());
        assertEquals("ai", o.getContentCustomDimension16());
        assertEquals("aj", o.getContentCustomDimension17());
        assertEquals("ak", o.getContentCustomDimension18());
        assertEquals("al", o.getContentCustomDimension19());
        assertEquals("am", o.getContentCustomDimension20());
        assertEquals("t", o.getAdCustomDimension1());
        assertEquals("u", o.getAdCustomDimension2());
        assertEquals("v", o.getAdCustomDimension3());
        assertEquals("w", o.getAdCustomDimension4());
        assertEquals("x", o.getAdCustomDimension5());
        assertEquals("y", o.getAdCustomDimension6());
        assertEquals("z", o.getAdCustomDimension7());
        assertEquals("aa", o.getAdCustomDimension8());
        assertEquals("ab", o.getAdCustomDimension9());
        assertEquals("ac", o.getAdCustomDimension10());
        assertEquals("an", o.getUserType());
        assertEquals("optin", o.getUserPrivacyProtocol());
        assertEquals(Options.StreamingProtocol.DASH, o.getContentStreamingProtocol());
        assertEquals(Options.TransportFormat.TS, o.getTransportFormat());
        assertEquals("appName", o.getAppName());
        assertEquals("appReleaseVersion", o.getAppReleaseVersion());

        assertEquals(
                java.util.Arrays.toString(new byte[] {0, -1, -1, -1, -1, -1, -1, 0, 30, 109, 1, 0,
                        1, 1, 1, 1, 1, 28, 1, 3, -128, -96, 90, 120, 10, -18, -111, -93, 84, 76,
                        -103, 38, 15, 80, 84, -95, 8, 0, 49, 64, 69, 64, 97, 64, 113, 64, 1, 1, 1,
                        1, 1, 1, 1, 1, 102, 33, 80, -80, 81, 0, 27, 48, 64, 112, 54, 0, 64, -124,
                        99, 0, 0, 30, 100, 25, 0, 64, 65, 0, 38, 48, 24, -120, 54, 0, 64, -124, 99,
                        0, 0, 24, 0, 0, 0, -3, 0, 58, 62, 30, 83, 16, 0, 10, 32, 32, 32, 32, 32, 32,
                        0, 0, 0, -4, 0, 76, 71, 32, 84, 86, 10, 32, 32, 32, 32, 32, 32, 32, 1,
                        113}),
                o.getDeviceEDID()
        );

        assertEquals(o.getAuthToken(), authToken);
        assertEquals(o.getAuthType(), authType);

        // Streaming Protocols options
        // Wrong protocol
        o.setContentStreamingProtocol("rash");
        assertNull(o.getContentStreamingProtocol());

        // Other cases
        o.setContentStreamingProtocol("HDS");
        assertEquals(Options.StreamingProtocol.HDS, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("HLS");
        assertEquals(Options.StreamingProtocol.HLS, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("MSS");
        assertEquals(Options.StreamingProtocol.MSS, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("RTMP");
        assertEquals(Options.StreamingProtocol.RTMP, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("RTP");
        assertEquals(Options.StreamingProtocol.RTP, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("RTSP");
        assertEquals(Options.StreamingProtocol.RTSP, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("MULTICAST");
        assertEquals(Options.StreamingProtocol.MULTICAST, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("DVB");
        assertEquals(Options.StreamingProtocol.DVB, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("DVB-C");
        assertEquals(Options.StreamingProtocol.DVBC, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("DVB-T");
        assertEquals(Options.StreamingProtocol.DVBT, o.getContentStreamingProtocol());

        o.setContentStreamingProtocol("DVB-T2");
        assertEquals(Options.StreamingProtocol.DVBT2, o.getContentStreamingProtocol());

        // non-case sensitive
        o.setContentStreamingProtocol("dash");
        assertEquals(Options.StreamingProtocol.DASH, o.getContentStreamingProtocol());

        o.setTransportFormat("ta");
        assertNull(o.getTransportFormat());
        o.setTransportFormat("ts");
        assertEquals(Options.TransportFormat.TS, o.getTransportFormat());

        o.setDeviceEDID("String");
        assertEquals("String", o.getDeviceEDID());
    }

    private void setBundleAsOptions() {
        ArrayList<String> cdnNodeList = new ArrayList<>();
        cdnNodeList.add("listitem1");
        cdnNodeList.add("listitem2");

        Bundle metadataBundle = mock(Bundle.class);
        metadataBundle.putString("p", "q");

        Bundle adMetadataBundle = mock(Bundle.class);
        adMetadataBundle.putString("r", "s");

        optBundle.putBoolean("enabled", false);
        optBundle.putBoolean("httpSecure", false);
        optBundle.putString("host", "a");
        optBundle.putString("config.accountCode", "b");
        optBundle.putString("username", "c");
        optBundle.putString("anonymousUser", "anon");
        optBundle.putBoolean("parse.Hls", true);
        optBundle.putString("parse.CdnNameHeader", "d");
        optBundle.putBoolean("parse.CdnNode", true);
        optBundle.putStringArrayList("parse.CdnNodeList", cdnNodeList);
        fakeBundle.put("parse.CdnNodeList", cdnNodeList);
        optBundle.putString("network.IP", "f");
        optBundle.putString("network.Isp", "g");
        optBundle.putString("network.connectionType", "h");
        optBundle.putBoolean("network.obfuscateIp", true);
        optBundle.putString("device.code", "i");
        optBundle.putString("content.resource", "j");
        optBundle.putBoolean("content.isLive", true);
        optBundle.putString("content.title", "k");
        optBundle.putString("content.program", "l");
        optBundle.putDouble("content.duration", 1.0);
        optBundle.putString("content.transactionCode", "m");
        optBundle.putLong("content.bitrate", 2L);
        optBundle.putLong("content.throughput", 3L);
        optBundle.putString("content.rendition", "n");
        optBundle.putString("content.cdn", "o");
        optBundle.putDouble("content.fps", 4.0);
        optBundle.putBundle("content.metadata", metadataBundle);
        fakeBundle.put("content.metadata", metadataBundle);
        optBundle.putBundle("ad.metadata", adMetadataBundle);
        fakeBundle.put("ad.metadata", adMetadataBundle);
        optBundle.putBoolean("ad.ignore", false);
        optBundle.putInt("ad.afterStop", 0);
        optBundle.putBoolean("autoDetectBackground", false);
        optBundle.putBoolean("autoStart", true);
        optBundle.putBoolean("offline", false);
        optBundle.putBoolean("isInfinity", false);
        optBundle.putString("custom.dimensions.1", "t");
        optBundle.putString("custom.dimensions.2", "u");
        optBundle.putString("custom.dimensions.3", "v");
        optBundle.putString("custom.dimensions.4", "w");
        optBundle.putString("custom.dimensions.5", "x");
        optBundle.putString("custom.dimensions.6", "y");
        optBundle.putString("custom.dimensions.7", "z");
        optBundle.putString("custom.dimensions.8", "aa");
        optBundle.putString("custom.dimensions.9", "ab");
        optBundle.putString("custom.dimensions.10", "ac");
        optBundle.putString("custom.dimensions.11", "ad");
        optBundle.putString("custom.dimensions.12", "ae");
        optBundle.putString("custom.dimensions.13", "af");
        optBundle.putString("custom.dimensions.14", "ag");
        optBundle.putString("custom.dimensions.15", "ah");
        optBundle.putString("custom.dimensions.16", "ai");
        optBundle.putString("custom.dimensions.17", "aj");
        optBundle.putString("custom.dimensions.18", "ak");
        optBundle.putString("custom.dimensions.19", "al");
        optBundle.putString("custom.dimensions.20", "am");

        optBundle.putString(KEY_AUTH_TOKEN, authToken);
        optBundle.putString(KEY_AUTH_TYPE, authType);

        o = new Options(optBundle);
    }
}