package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.comm.Request

import junit.framework.TestCase.*

import org.junit.Test

import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

import java.net.HttpURLConnection

import kotlin.collections.HashMap

class DashParserTest {

    @Test
    fun testInitialValue() {
        val parser = DashParser()
        assertNull(parser.realResource)
    }

    @Test
    fun testFlowLastManifest() {
        val parser = DashParser()
        val res = "http://bitdash-a.akamaihd.net/content/sintel/sintel.mpd"

        // BaseURL
        val baseUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/live/cenc/" +
            "6028583040001/f39ee0f0-72de-479d-9609-2bf6ea95b427/" +
            "fed9a7f1-499a-469d-bacd-f25a94eac116/"
        val response = "asdf<BaseURL>$baseUrl</BaseURL>asdf"

        parser.parse(res, null, response)
        assertEquals(baseUrl, parser.realResource)

        // SegmentUrl
        val segmentUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/762347563745"
        val segmentResponse = "asdf<SegmentURL param1=\"qwerty\" media=\"$segmentUrl\" " +
            "param2=\"fdas\">asdf</SegmentURL>asdf"
        parser.parse(res, null, segmentResponse)
        assertEquals(segmentUrl, parser.realResource)

        // SegmentTemplate
        val segmentTemplateUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/654632"
        val segmentTemplate = "asdf<SegmentTemplate param1=\"qwerty\" " +
            "media=\"$segmentTemplateUrl\" param2=\"fdas\">asdf</SegmentTemplate>asdf"
        parser.parse(res, null, segmentTemplate)
        assertEquals(segmentTemplateUrl, parser.realResource)

        // Invalid
        val invalidResponse = "foo bar"
        parser.parse(res, null, invalidResponse)
        assertEquals(res, parser.realResource)
    }

    @Test
    fun testRelativeURL() {
        val parser = DashParser()
        val res = "http://bitdash-a.akamaihd.net/content/sintel/sintel.mpd"

        // BaseURL
        val baseUrl = "../../../bpk-tv/Viasat_History_2245/output1/dash/"
        val response = "asdf<BaseURL>$baseUrl</BaseURL>asdf"

        parser.parse(res, null, response)
        assertEquals(res, parser.realResource)
    }

    @Test
    fun testFlowNoLastManifest() {
        val parser = spy(DashParser())
        val res = "http://bitdash-a.akamaihd.net/content/sintel/sintel.mpd"

        val mockRequest = mock(Request::class.java)
        doReturn(mockRequest).`when`(parser).createRequest(nullable(String::class.java))

        parser.parse(res, null, null)

        verify(parser, atMostOnce()).createRequest(any())

        // BaseURL
        val baseUrlCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(baseUrlCaptor.capture())

        val baseUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/live/cenc/" +
                "6028583040001/f39ee0f0-72de-479d-9609-2bf6ea95b427/" +
                "fed9a7f1-499a-469d-bacd-f25a94eac116/"
        val response = "asdf<BaseURL>$baseUrl</BaseURL>asdf"
        baseUrlCaptor.value.onRequestSuccess(mock(HttpURLConnection::class.java), response,
                HashMap<String, Any>(), HashMap<String, List<String>>())
        assertEquals(baseUrl, parser.realResource)

        // SegmentUrl
        val segmentUrlCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(segmentUrlCaptor.capture())

        val segmentUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/762347563745"
        val segmentResponse = "asdf<SegmentURL param1=\"qwerty\" media=\"$segmentUrl\" " +
                "param2=\"fdas\">asdf</SegmentURL>asdf"
        segmentUrlCaptor.value.onRequestSuccess(mock(HttpURLConnection::class.java),
                segmentResponse, HashMap<String, Any>(), HashMap<String, List<String>>())
        assertEquals(segmentUrl, parser.realResource)

        // SegmentTemplate
        val segmentTemplateCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(segmentTemplateCaptor.capture())

        val segmentTemplateUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/654632"
        val segmentTemplate = "asdf<SegmentTemplate param1=\"qwerty\" " +
                "media=\"$segmentTemplateUrl\" param2=\"fdas\">asdf</SegmentTemplate>asdf"
        segmentTemplateCaptor.value.onRequestSuccess(mock(HttpURLConnection::class.java),
                segmentTemplate, HashMap<String, Any>(), HashMap<String, List<String>>())
        assertEquals(segmentTemplateUrl, parser.realResource)

        // BaseURL
        val invalidUrlCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(invalidUrlCaptor.capture())

        // Invalid
        val invalidResponse = "foo bar"
        invalidUrlCaptor.value.onRequestSuccess(mock(HttpURLConnection::class.java), invalidResponse,
            HashMap<String, Any>(), HashMap<String, List<String>>())
        assertEquals(res, parser.realResource)
    }

    @Test
    fun testLocationTag() {
        val parser = spy(DashParser())
        val res = "http://bitdash-a.akamaihd.net/content/sintel/sintel.mpd"

        val mockRequest = mock(Request::class.java)
        doReturn(mockRequest).`when`(parser).createRequest(nullable(String::class.java))

        parser.parse(res, null, null)

        verify(parser, atMostOnce()).createRequest(any())

        // BaseURL
        val baseUrlCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(baseUrlCaptor.capture())

        val locationUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/live/cenc/" +
            "6028583040001/f39ee0f0-72de-479d-9609-2bf6ea95b427/" +
            "fed9a7f1-499a-469d-bacd-f25a94eac116/"
        var response = "asdf<Location>$locationUrl</Location>asdf"
        baseUrlCaptor.value.onRequestSuccess(mock(HttpURLConnection::class.java), response,
            HashMap<String, Any>(), HashMap<String, List<String>>())

        val segmentUrlCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(segmentUrlCaptor.capture())

        val segmentUrl = "https://boltrljDRMTest1-a.akamaihd.net/media/v1/dash/762347563745"
        val segmentResponse = "asdf<SegmentURL param1=\"qwerty\" media=\"$segmentUrl\" " +
            "param2=\"fdas\">asdf</SegmentURL>asdf"
        segmentUrlCaptor.value.onRequestSuccess(mock(HttpURLConnection::class.java),
            segmentResponse, HashMap<String, Any>(), HashMap<String, List<String>>())
        baseUrlCaptor.value.onRequestSuccess(mock(HttpURLConnection::class.java), response,
            HashMap<String, Any>(), HashMap<String, List<String>>())
        assertEquals(segmentUrl, parser.realResource)
    }


    @Test
    fun testListeners() {
        val parser = DashParser()
        val listener = object : Parser.ParserTransformListener {
            override fun onParserTransformDone(parsedResource: String?) { }
        }

        parser.addParserTransformListener(listener)
        assertTrue(parser.removeParserTransformListener(listener))
        parser.addParserTransformListener(listener)
    }

    @Test
    fun testParserExecutability() {
        val parser = DashParser()
        assertFalse(parser.shouldExecute("FOOBAR"))
        assertTrue(parser.shouldExecute("FOO<MPDBAR"))
    }

    @Test
    fun testCreateRequest() {
        val request = DashParser().createRequest(null)
        assertNotNull(request)
    }
}