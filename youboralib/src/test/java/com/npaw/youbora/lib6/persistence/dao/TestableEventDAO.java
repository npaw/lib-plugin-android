package com.npaw.youbora.lib6.persistence.dao;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.test.core.app.ApplicationProvider;

import java.io.File;

public class TestableEventDAO extends EventDAO {

    private SQLiteDatabase testDB;
    private SQLiteOpenHelper helper;

    public TestableEventDAO(SQLiteOpenHelper helper, File dbPath) {
        super(helper);
        this.helper = helper;
        testDB = openOrCreateDatabase(dbPath);
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return testDB;
    }

    @Override
    public synchronized SQLiteDatabase getReadableDatabase() { return testDB;}

    private SQLiteDatabase openOrCreateDatabase(String name) {
        return openOrCreateDatabase(ApplicationProvider.getApplicationContext().getDatabasePath(name));
    }

    private SQLiteDatabase openOrCreateDatabase(File databasePath) {
        SQLiteDatabase database = SQLiteDatabase.openOrCreateDatabase(databasePath, null);
        helper.onCreate(database);
        return database;
    }
}
