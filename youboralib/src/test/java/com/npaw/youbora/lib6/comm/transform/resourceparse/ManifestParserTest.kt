package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.comm.Request
import junit.framework.TestCase.*
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import java.net.HttpURLConnection

class ManifestParserTest {

    @Test
    fun testParse() {
        val parser = spy(ManifestParser())
        val res = "Foo bar"
        val mockRequest = mock(Request::class.java)
        doReturn(mockRequest)
            .`when`(parser).createRequest(nullable(String::class.java))
        doReturn(Request.METHOD_GET).`when`(mockRequest).method
        doReturn(0).`when`(mockRequest).maxRetries

        parser.parse(res, null, null)

        verify(parser, atMostOnce()).createRequest(any())

        val urlCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(urlCaptor.capture())

        val mockURLConnection = mock(HttpURLConnection::class.java)

        urlCaptor.value.onRequestSuccess(mockURLConnection, "foobar", HashMap<String, Any>(), HashMap<String, List<String>>())
        assertEquals("foobar", parser.lastManifest)
    }

    @Test
    fun testParseWrongUrl() {
        val parser = spy(ManifestParser())
        val res = "Foo bar"
        val mockRequest = mock(Request::class.java)
        doReturn(mockRequest)
            .`when`(parser).createRequest(nullable(String::class.java))
        doReturn(Request.METHOD_GET).`when`(mockRequest).method
        doReturn(0).`when`(mockRequest).maxRetries

        parser.parse(res, null, null)

        verify(parser, atMostOnce()).createRequest(any())

        val urlCaptor = ArgumentCaptor.forClass(Request.RequestErrorListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnErrorListener(urlCaptor.capture())

        val mockURLConnection = mock(HttpURLConnection::class.java)

        urlCaptor.value.onRequestError(mockURLConnection)
        verify(parser, atLeastOnce()).done()
    }

    @Test
    fun testShouldExecute() {
        val parser = ManifestParser()
        assertTrue(parser.shouldExecute(null))
        assertFalse(parser.shouldExecute("foo bar"))
    }
}