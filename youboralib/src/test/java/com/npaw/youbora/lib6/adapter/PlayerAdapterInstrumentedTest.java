package com.npaw.youbora.lib6.adapter;

import androidx.test.core.app.ApplicationProvider;

import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricTestRunner;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import android.content.Context;

@RunWith(RobolectricTestRunner.class)
public class PlayerAdapterInstrumentedTest {

    private class CustomAdapter extends PlayerAdapter<String> {
        boolean unregisterListenersCalled = false;
        boolean registerListenersCalled = false;

        CustomAdapter(String player) {
            super(player);
            registerListeners();
        }

        @Override
        public void registerListeners() { registerListenersCalled = true; }

        @Override
        public void unregisterListeners() { unregisterListenersCalled = true; }
    }

    @Test
    public void seekWhenContentIsLiveNoSeek() {
        Options o = new Options();
        o.setContentIsLiveNoSeek(true);

        Context context = ApplicationProvider.getApplicationContext();
        Plugin p = new Plugin(o, context);

        CustomAdapter adapter = new CustomAdapter("test");
        p.setAdapter(adapter);

        adapter.fireSeekBegin();
        adapter.fireSeekEnd();
    }

    @Test
    public void fireVideoEventWithParams() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);

        adapter.addEventListener(mockListener);

        Map dimensions = new HashMap<String, String>() {{
            put("dimen1", "asdf");
        }};

        Map values = new HashMap<String, Double>() {{
            put("value1", 123.4);
        }};

        adapter.fireStart();
        adapter.fireEvent("eventName", dimensions, values);

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener, times(1)).onVideoEvent(captor.capture());
        assertEquals("eventName", captor.getValue().get("name"));
        assertEquals("{\"dimen1\":\"asdf\"}", (captor.getValue().get("dimensions")));
        assertEquals("{\"value1\":123.4}", (captor.getValue().get("values")));
    }
}
