package com.npaw.youbora.lib6.plugin;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;

import org.robolectric.RobolectricTestRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.verify;

import static com.npaw.youbora.lib6.constants.Services.PING;
import static com.npaw.youbora.lib6.constants.Services.PLUGIN_LOGS;

@RunWith(RobolectricTestRunner.class)
public class PluginInstrumentedTest extends PluginMocker {

    @Test
    public void testPingBasicParams() {
        // Basic ping params
        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);

        verify(mockRequestBuilder).buildParams(nullable(Map.class), eq(PING));
        verify(mockRequestBuilder).fetchParams(nullable(Map.class), mapCaptor.capture(), anyBoolean());

        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);
    }

    @Test
    public void testPingPaused() {
        mockAdapter.getFlags().setPaused(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);

        verify(mockRequestBuilder).fetchParams(nullable(Map.class), mapCaptor.capture(), anyBoolean());

        List<String> params = mapCaptor.getValue();
        assertTrue(params.contains("pauseDuration"));
    }

    @Test
    public void testPingBuffering() {
        mockAdapter.getFlags().setBuffering(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);

        verify(mockRequestBuilder).fetchParams(nullable(Map.class), mapCaptor.capture(), anyBoolean());

        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);

        assertTrue(params.contains("bufferDuration"));
    }

    @Test
    public void testPingSeek() {
        mockAdapter.getFlags().setSeeking(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);

        verify(mockRequestBuilder).fetchParams(nullable(Map.class), mapCaptor.capture(), anyBoolean());

        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);

        assertTrue(params.contains("seekDuration"));
    }

    @Test
    public void testPingAds() {
        mockAdAdapter.getFlags().setStarted(true);
        mockAdAdapter.getFlags().setBuffering(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);

        verify(mockRequestBuilder).fetchParams(nullable(Map.class), mapCaptor.capture(), anyBoolean());

        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);

        assertTrue(params.contains("adBitrate"));
        assertTrue(params.contains("adPlayhead"));
        assertTrue(params.contains("adBufferDuration"));
    }

    @Test
    public void testRemoteDebugger() {
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
        remoteMonitoringListener.onSend("exampleData");

        verify(mockRequestBuilder).buildParams(mapCaptor.capture(), eq(PLUGIN_LOGS), eq(true));

        Map<String,String> params = mapCaptor.getValue();

        assertNotNull(params.get("logs"));
    }

    private void verifyBasicParamsPing(List<String> params) {
        assertTrue(params.contains("bitrate"));
        assertTrue(params.contains("throughput"));
        assertTrue(params.contains("fps"));
    }
}