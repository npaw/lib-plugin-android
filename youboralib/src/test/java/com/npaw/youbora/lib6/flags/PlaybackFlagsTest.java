package com.npaw.youbora.lib6.flags;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlaybackFlagsTest {

    @Test
    public void testDefaultValues() {
        BaseFlags flags = new BaseFlags();
        AdFlags adFlags = new AdFlags();

        assertFalse(flags.isBuffering());
        assertFalse(flags.isJoined());
        assertFalse(flags.isPaused());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isStarted());
        assertFalse(adFlags.isAdBreakStarted());
        assertFalse(adFlags.isAdInitiated());
    }

    @Test
    public void testSetValues() {
        BaseFlags flags = new BaseFlags();
        AdFlags adFlags = new AdFlags();

        flags.setBuffering(true);
        flags.setJoined(true);
        flags.setPaused(true);
        flags.setSeeking(true);
        flags.setStarted(true);
        adFlags.setAdBreakStarted(true);
        adFlags.setAdInitiated(true);

        assertTrue(flags.isBuffering());
        assertTrue(flags.isJoined());
        assertTrue(flags.isPaused());
        assertTrue(flags.isSeeking());
        assertTrue(flags.isStarted());
        assertTrue(adFlags.isAdBreakStarted());
        assertTrue(adFlags.isAdInitiated());
    }

    @Test
    public void testReset() {
        BaseFlags flags = new BaseFlags();
        AdFlags adFlags = new AdFlags();

        flags.setBuffering(true);
        flags.setJoined(true);
        flags.setPaused(true);
        flags.setSeeking(true);
        flags.setStarted(true);
        adFlags.setAdInitiated(true);
        adFlags.setAdBreakStarted(true);

        flags.reset();
        adFlags.reset();

        assertFalse(flags.isBuffering());
        assertFalse(flags.isJoined());
        assertFalse(flags.isPaused());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isStarted());
        assertFalse(adFlags.isAdInitiated());
    }
}