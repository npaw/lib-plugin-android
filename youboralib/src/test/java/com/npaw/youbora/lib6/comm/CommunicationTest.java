package com.npaw.youbora.lib6.comm;

import static junit.framework.TestCase.assertEquals;

import com.npaw.youbora.lib6.comm.transform.Transform;
import com.npaw.youbora.lib6.plugin.Options;

import org.junit.Before;
import org.junit.Test;

import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

import java.util.HashMap;

public class CommunicationTest {

    private Communication c;

    @Before
    public void before() {
        c = new Communication(new Options());
    }

    @Test
    public void testAddTransform() {
        Transform mockTransform = mock(Transform.class);
        c.addTransform(mockTransform);

        verify(mockTransform, times(1)).addTransformDoneListener(any(Transform.TransformDoneListener.class));
    }

    @Test
    public void testSendRequest() {
        Request mockRequest = mock(Request.class);
        c.sendRequest(mockRequest, null);

        verify(mockRequest, times(1)).send();
    }

    @Test
    public void testRegisterRequestWithAuthHeader() {
        Options options = new Options();

        String authToken = "authToken";
        String authType = "authType";

        options.setAuthToken(authToken);
        options.setAuthType(authType);

        Communication comm = new Communication(options);
        Request mockRequest = mock(Request.class);

        comm.registerRequest(mockRequest);

        verify(mockRequest, times(1)).send();
        assertEquals(mockRequest.requestHeaders.get("Authorization"), authType + " " + authToken);
    }

    @Test
    public void testTransformNotBlocking() throws Exception {
        Transform mockTransform = Mockito.mock(Transform.class);
        c.addTransform(mockTransform);

        when(mockTransform.isBlocking(any(Request.class))).thenReturn(false);

        Request mockRequest = mock(Request.class);
        c.sendRequest(mockRequest, null);

        verify(mockTransform, times(1)).isBlocking(mockRequest);
        verify(mockTransform, times(1)).parse(mockRequest);
        verify(mockRequest, times(1)).send();
    }

    @Test
    public void testTransformBlocking() throws Exception {
        Transform mockTransform = Mockito.mock(Transform.class);
        c.addTransform(mockTransform);

        when(mockTransform.isBlocking(any(Request.class))).thenReturn(true);

        Request mockRequest = mock(Request.class);
        c.sendRequest(mockRequest, null);

        verify(mockTransform, times(1)).isBlocking(mockRequest);
        verify(mockTransform, never()).parse(any(Request.class));
        verify(mockRequest, never()).send();
    }

    @Test
    public void testRemoveTransform() {

        Transform t = mock(Transform.class);
        c.addTransform(t);
        c.removeTransform(t);

        // Try to remove non existent transforms, no exception should be risen
        c.removeTransform(null);
        c.removeTransform(mock(Transform.class));
    }

    @Test
    public void testTransformCallback() {
        Transform t = mock(Transform.class);

        when(t.isBlocking(any(Request.class))).thenReturn(true);

        // Capture callback when set
        ArgumentCaptor<Transform.TransformDoneListener> captor = ArgumentCaptor.forClass(Transform.TransformDoneListener.class);

        c.addTransform(t); // Callback set here

        verify(t, times(0)).parse(any(Request.class));

        Request mockRequest = mock(Request.class);

        c.sendRequest(mockRequest, null);

        verify(t, times(0)).parse(any(Request.class));

        // Capture callback
        verify(t).addTransformDoneListener(captor.capture());

        // Transform done
        when(t.isBlocking(any(Request.class))).thenReturn(false);
        captor.getValue().onTransformDone(t);

        // Transform done should trigger request processing and thus
        // the transforms parsing
        verify(t, times(1)).parse(any(Request.class));

    }

}