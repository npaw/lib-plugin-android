package com.npaw.youbora.lib6

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ChronoTests {

    private lateinit var chrono: Chrono

    @Before
    fun setUp() { chrono = Chrono() }


    @Test
    fun pause() {
        chrono.pause()
        assertTrue(chrono.pauseTime!! > 0)
    }

    @Test
    fun resume() {
        val now = Chrono.getNow()
        chrono.resume()
        assertEquals(chrono.offset, -now)

        chrono.pauseTime = 1
        chrono.resume()

        assertTrue(chrono.offset < 0)
        assertNull(chrono.pauseTime)
    }

    @Test
    fun getDeltaTimeWithPause() {
        chrono.start()

        val pauseTime = 1L
        val now = Chrono.getNow()

        chrono.stop()
        chrono.pauseTime = pauseTime
        val stopDiff = chrono.getDeltaTime(false)

        assertEquals(((chrono.stopTime!! - chrono.startTime!!) - (now - pauseTime)).toDouble(),
            stopDiff.toDouble(), 10.0)
    }

    @Test
    fun getDeltaTimeWithOffset() {
        chrono.start()

        chrono.offset = -5000

        chrono.stopTime = chrono.startTime?.plus(6000)

        val diff = chrono.getDeltaTime(false)

        assertEquals(1000, diff)
    }
}