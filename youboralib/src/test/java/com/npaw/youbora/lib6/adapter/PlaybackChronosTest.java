package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.Chrono;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlaybackChronosTest {

    @Test
    public void testNonNull() {
        PlaybackChronos chronos = new PlaybackChronos();

        assertNotNull(chronos.getBuffer());
        assertNotNull(chronos.getSeek());
        assertNotNull(chronos.getJoin());
        assertNotNull(chronos.getPause());
        assertNotNull(chronos.getTotal());
    }

    @Test
    public void testReset() {
        PlaybackChronos chronos = new PlaybackChronos();

        Chrono chronobuffer = chronos.getBuffer();
        Chrono chronoseek = chronos.getSeek();
        Chrono chronojoin = chronos.getJoin();
        Chrono chronopause = chronos.getPause();
        Chrono chronototal = chronos.getTotal();

        chronos.reset();

        assertNotEquals(chronobuffer, chronos.getBuffer());
        assertNotEquals(chronoseek, chronos.getSeek());
        assertNotEquals(chronojoin, chronos.getJoin());
        assertNotEquals(chronopause, chronos.getPause());
        assertNotEquals(chronototal, chronos.getTotal());
    }

}