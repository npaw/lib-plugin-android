package com.npaw.youbora.lib6.comm.transform;

import android.os.Handler;

import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.resourceparse.*;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.plugin.RequestBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;

import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.npaw.youbora.lib6.constants.Services.START;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.*;

@RunWith(RobolectricTestRunner.class)

public class ResourceTransformTest {

    private HlsParser mockHlsParser;
    private DashParser mockDashParser;
    private LocationHeaderParser mockLocationParser;
    private ManifestParser mockManifestParser;
    private Handler mockHandler;
    private CdnParser mockCdnParser;
    private Map<String, String> manifestHeaders;

    private ResourceTransform createTransformWithMocks(Plugin mockPlugin) {

        ResourceTransform resourceTransform = spy(new ResourceTransform(mockPlugin));

        mockHlsParser = mock(HlsParser.class);
        when(mockHlsParser.shouldExecute(nullable(String.class))).thenReturn(true);
        when(resourceTransform.createHlsParser(manifestHeaders)).thenReturn(mockHlsParser);

        mockManifestParser = mock(ManifestParser.class);
        when(mockManifestParser.shouldExecute(nullable(String.class))).thenReturn(true);
        when(resourceTransform.createManifestParser(manifestHeaders)).thenReturn(mockManifestParser);

        mockDashParser = mock(DashParser.class);
        when(mockDashParser.shouldExecute(nullable(String.class))).thenReturn(true);
        when(resourceTransform.createDashParser(manifestHeaders)).thenReturn(mockDashParser);

        mockCdnParser = mock(CdnParser.class);
        when(resourceTransform.createCdnParser(anyString())).thenReturn(mockCdnParser);

        mockLocationParser = mock(LocationHeaderParser.class);
        when(mockLocationParser.shouldExecute(nullable(String.class))).thenReturn(true);
        when(resourceTransform.createLocationHeaderParser(manifestHeaders)).thenReturn(mockLocationParser);

        mockHandler = mock(Handler.class);
        when(resourceTransform.createHandler()).thenReturn(mockHandler);

        return resourceTransform;
    }

    @Test
    public void testDefaultValues() {
        Plugin mockPlugin = mock(Plugin.class);

        ResourceTransform resourceTransform = new ResourceTransform(mockPlugin);

        // Assert default values
        assertNull(resourceTransform.getCdnName());
        assertNull(resourceTransform.getNodeHost());
        assertNull(resourceTransform.getNodeType());
        assertNull(resourceTransform.getNodeTypeString());
        assertNull(resourceTransform.getResource());
    }

    @Test
    public void testFullFlow() {
        // Mocks
        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.isParseCdnNode()).thenReturn(true);
        when(mockPlugin.isParseManifest()).thenReturn(true);
        when(mockPlugin.getParseCdnNodeNameHeader()).thenReturn("header-name");
        when(mockPlugin.getParseNodeHeader()).thenReturn("node-header");
        when(mockPlugin.getParseManifestAuth()).thenReturn(null);
        List<String> cdns = new ArrayList<>();
        cdns.add("cdn1");
        cdns.add("cdn2");
        when(mockPlugin.getParseCdnNodeList()).thenReturn(cdns);

        // Resource transform to test
        ResourceTransform resourceTransform = createTransformWithMocks(mockPlugin);

        assertFalse(resourceTransform.isBlocking(null));
        // Init, this should start location parsing
        resourceTransform.init("resource");

        assertTrue(resourceTransform.isBlocking(null));

        verify(mockLocationParser, atLeastOnce()).parse(eq("resource"), (String) eq(null), (String) eq(null));

        // Capture callback
        ArgumentCaptor<Parser.ParserTransformListener> captor = ArgumentCaptor.forClass(Parser.ParserTransformListener.class);
        verify(mockLocationParser, times(1)).addParserTransformListener(captor.capture());

        // Invoke callback
        captor.getValue().onParserTransformDone("https://parsed.resource.test");

        /////
        verify(mockManifestParser, atLeastOnce()).parse(eq("https://parsed.resource.test"), (String) eq(null), (String) eq(null));

        // Capture callback
        captor = ArgumentCaptor.forClass(Parser.ParserTransformListener.class);
        verify(mockManifestParser, times(1)).addParserTransformListener(captor.capture());

        // Invoke callback
        captor.getValue().onParserTransformDone("https://parsed.resource.test");
        ////

        verify(mockDashParser, atLeastOnce()).parse(eq("https://parsed.resource.test"), (String) eq(null), (String) eq(null));

        // Capture callback
        captor = ArgumentCaptor.forClass(Parser.ParserTransformListener.class);
        verify(mockDashParser, times(1)).addParserTransformListener(captor.capture());

        // Invoke callback
        captor.getValue().onParserTransformDone("https://parsed.resource.test");

        verify(mockHlsParser, atLeastOnce()).parse(eq("https://parsed.resource.test"), (String) eq(null), (String) eq(null));

        // Capture callback
        captor = ArgumentCaptor.forClass(Parser.ParserTransformListener.class);
        verify(mockHlsParser, times(1)).addParserTransformListener(captor.capture());


        // Prepare mocks for the cdn
        CdnParser mockCdnParser1 = mock(CdnParser.class);
        CdnParser mockCdnParser2 = mock(CdnParser.class);
        when(resourceTransform.createCdnParser(eq("cdn1"))).thenReturn(mockCdnParser1);
        when(resourceTransform.createCdnParser(eq("cdn2"))).thenReturn(mockCdnParser2);

        // Invoke callback
        captor.getValue().onParserTransformDone("https://parsed.parsed.resource.test");

        // That should have updated the resource
        assertEquals("https://parsed.parsed.resource.test", resourceTransform.getResource());

        // Mock cdn values
        when(mockCdnParser2.getCdnName()).thenReturn("parsedCdnName");
        when(mockCdnParser2.getNodeHost()).thenReturn("parsedNodeHost");
        when(mockCdnParser2.getNodeType()).thenReturn(CdnTypeParser.Type.Hit);
        when(mockCdnParser2.getNodeTypeString()).thenReturn("HIT");

        // Capture cdn parser callback
        ArgumentCaptor<CdnParser.CdnTransformListener> cdnCaptor1 = ArgumentCaptor.forClass(CdnParser.CdnTransformListener.class);
        verify(mockCdnParser1, times(1)).addCdnTransformListener(cdnCaptor1.capture());

        // Invoke callback, as the mockedCdn will return null, the second one will be called
        cdnCaptor1.getValue().onCdnTransformDone(mockCdnParser1);

        ArgumentCaptor<CdnParser.CdnTransformListener> cdnCaptor2 = ArgumentCaptor.forClass(CdnParser.CdnTransformListener.class);
        verify(mockCdnParser2, times(1)).addCdnTransformListener(cdnCaptor2.capture());

        // Invoke callback. This time the cdn will provide info
        cdnCaptor2.getValue().onCdnTransformDone(mockCdnParser2);

        assertFalse(resourceTransform.isBlocking(null));

        // Check parsed values
        assertEquals("parsedCdnName", resourceTransform.getCdnName());
        assertEquals("parsedNodeHost", resourceTransform.getNodeHost());
        assertEquals("1", resourceTransform.getNodeType());
        assertEquals("HIT", resourceTransform.getNodeTypeString());
        assertEquals("https://parsed.parsed.resource.test", resourceTransform.getResource());

        // Check parse start request
        Request mockStart = mock(Request.class);
        when(mockStart.getService()).thenReturn(START);

        // Mocks
        RequestBuilder mockBuilder = mock(RequestBuilder.class);
        HashMap<String, String> lastSent = mock(HashMap.class);
        when(mockBuilder.getLastSent()).thenReturn(lastSent);
        when(mockPlugin.getRequestBuilder()).thenReturn(mockBuilder);

        resourceTransform.parse(mockStart);

        verify(mockStart, times(1)).setParam(eq("cdn"), eq("parsedCdnName"));
        verify(mockStart, times(1)).setParam(eq("nodeHost"), eq("parsedNodeHost"));
        verify(mockStart, times(1)).setParam(eq("nodeType"), eq("1"));
        verify(mockStart, times(1)).setParam(eq("nodeTypeString"), eq("HIT"));

        verify(lastSent, times(1)).put(eq("cdn"), eq("parsedCdnName"));
        verify(lastSent, times(1)).put(eq("nodeHost"), eq("parsedNodeHost"));
        verify(lastSent, times(1)).put(eq("nodeType"), eq("1"));
        verify(lastSent, times(1)).put(eq("nodeTypeString"), eq("HIT"));
    }

    @Test
    public void testNothingEnabled() {
        // Mocks
        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.isParseCdnNode()).thenReturn(false);
        when(mockPlugin.isParseManifest()).thenReturn(false);

        // Resource transform to test
        ResourceTransform resourceTransform = createTransformWithMocks(mockPlugin);

        assertFalse(resourceTransform.isBlocking(null));

        resourceTransform.init("resource");

        assertFalse(resourceTransform.isBlocking(null));

    }

    @Test
    public void testStopOnTimeout() {

        // Mocks
        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.isParseCdnNode()).thenReturn(true);
        when(mockPlugin.isParseManifest()).thenReturn(true);

        // Resource transform to test
        ResourceTransform resourceTransform = createTransformWithMocks(mockPlugin);

        assertFalse(resourceTransform.isBlocking(null));

        resourceTransform.init("resource");

        assertTrue(resourceTransform.isBlocking(null));

        // Capture timeout callback
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockHandler, times(1)).postDelayed(captor.capture(), eq(3000L));

        captor.getValue().run();

        assertFalse(resourceTransform.isBlocking(null));
    }

}