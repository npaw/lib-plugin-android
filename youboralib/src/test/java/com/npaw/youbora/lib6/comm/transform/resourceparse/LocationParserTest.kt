package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.comm.Request

import junit.framework.TestCase.*

import org.junit.Test
import org.junit.runner.RunWith

import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

import org.robolectric.RobolectricTestRunner

import java.net.HttpURLConnection
import java.net.URL

import kotlin.collections.HashMap

@RunWith(RobolectricTestRunner::class)
class LocationParserTest {

    @Test
    fun testParse() {
        val parser = spy(LocationHeaderParser())
        val res = "http://Foo bar"
        val mockRequest = mock(Request::class.java)
        doReturn(mockRequest).`when`(parser).createRequest(nullable(String::class.java))
        doReturn(Request.METHOD_HEAD).`when`(mockRequest).method
        doReturn(0).`when`(mockRequest).maxRetries

        parser.parse(res)

        verify(parser, atMostOnce()).createRequest(any())

        val urlCaptor = ArgumentCaptor.forClass(Request.RequestSuccessListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnSuccessListener(urlCaptor.capture())

        val mockURLConnection = mock(HttpURLConnection::class.java)
        doReturn(URL(res)).`when`(mockURLConnection).url

        val headers = mapOf("Location" to listOf("foobar resource"))

        doReturn(headers).`when`(mockURLConnection).headerFields
        urlCaptor.value.onRequestSuccess(mockURLConnection, "foobar", HashMap<String, Any>(), HashMap<String, List<String>>())
        assertEquals("foobar resource", parser.realResource)
    }

    @Test
    fun testParseUnsuccessfully() {
        val parser = spy(LocationHeaderParser())
        val res = "Foo bar"
        val mockRequest = mock(Request::class.java)
        doReturn(mockRequest).`when`(parser).createRequest(nullable(String::class.java))
        doReturn(Request.METHOD_HEAD).`when`(mockRequest).method
        doReturn(0).`when`(mockRequest).maxRetries

        parser.parse(res, null, null)

        verify(parser, atMostOnce()).createRequest(any())

        val urlCaptor = ArgumentCaptor.forClass(Request.RequestErrorListener::class.java)
        verify(mockRequest, atLeastOnce()).addOnErrorListener(urlCaptor.capture())

        val mockURLConnection = mock(HttpURLConnection::class.java)

        urlCaptor.value.onRequestError(mockURLConnection)
        verify(parser, atLeastOnce()).done()
    }

    @Test
    fun testShouldExecute() {
        val parser = LocationHeaderParser()
        assertTrue(parser.shouldExecute(null))
    }

    @Test
    fun testCreateRequest() {
        val request = LocationHeaderParser().createRequest(null)
        assertNotNull(request)
    }
}