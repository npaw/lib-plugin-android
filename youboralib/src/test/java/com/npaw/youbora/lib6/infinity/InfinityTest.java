package com.npaw.youbora.lib6.infinity;

import androidx.test.core.app.ApplicationProvider;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;

import org.robolectric.RobolectricTestRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import android.content.Context;

@RunWith(RobolectricTestRunner.class)
public class InfinityTest {
    private Plugin plugin;
    private Infinity infinityFactory;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        Context context = ApplicationProvider.getApplicationContext();
        plugin = new Plugin(new Options(), context);

        infinityFactory = plugin.getInfinity();
    }

    @After
    public void tearDown() { infinityFactory.end(); }

    @Test
    public void testBeginMethod() {
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);

        assertFalse(infinityFactory.getFlags().isStarted());

        infinityFactory.begin("screenName");
        verify(mockListener).onSessionStart(nullable(String.class), nullable(Map.class));
        assertEquals(infinityFactory.getLastSent(), new Long(-1L));
    }

    @Test
    public void testBeginMethodWithFlagsStarted() {
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);

        infinityFactory.getFlags().setStarted(true);
        assertTrue(infinityFactory.getFlags().isStarted());

        infinityFactory.begin("screenName");
        verify(mockListener).onNav(nullable(String.class));
    }

    @Test
    public void testBeginMethodWithParams() {
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);

        assertFalse(infinityFactory.getFlags().isStarted());

        ArgumentCaptor<String> screenNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Map> dimensionsCaptor = ArgumentCaptor.forClass(Map.class);

        String screenName = "screenName";
        HashMap dimensMap = new HashMap<String, String>(1){{
            put("key","value");
        }};

        infinityFactory.begin(screenName, dimensMap);

        assertTrue(infinityFactory.getFlags().isStarted());

        verify(mockListener, times(1))
                .onSessionStart(screenNameCaptor.capture(),
                        dimensionsCaptor.capture());

        assertEquals(screenName, screenNameCaptor.getValue());
        assertTrue(dimensionsCaptor.getValue().containsKey("key"));
        assertEquals(dimensMap.get("key"), dimensionsCaptor.getValue().get("key"));
    }

    @Test
    public void testBeginMethodWithNullParam() {
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);

        assertFalse(infinityFactory.getFlags().isStarted());

        ArgumentCaptor<String> screenNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Map> dimensionsCaptor = ArgumentCaptor.forClass(Map.class);

        infinityFactory.begin(null);
        assertTrue(infinityFactory.getFlags().isStarted());
        verify(mockListener, times(1))
                .onSessionStart(screenNameCaptor.capture(), dimensionsCaptor.capture());

        assertNull(screenNameCaptor.getValue());
        assertEquals(dimensionsCaptor.getValue(), new HashMap<String, String>());
    }

    @Test
    public void testFireEvent() {
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);
        infinityFactory.fireEvent("eventName");
        verify(mockListener).onEvent(anyString(), ArgumentMatchers.<String, String>anyMap(),
                ArgumentMatchers.<String, Double>anyMap(), ArgumentMatchers.<String, String>anyMap());
        infinityFactory.removeEventListener(mockListener);
    }
}
