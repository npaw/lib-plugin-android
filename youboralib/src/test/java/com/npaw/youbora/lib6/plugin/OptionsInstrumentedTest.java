package com.npaw.youbora.lib6.plugin;

import android.os.Bundle;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;

import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_AKAMAI;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_CLOUDFRONT;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_FASTLY;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_HIGHWINDS;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_LEVEL3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)

public class OptionsInstrumentedTest {

    @Test
    public void testToBundleAndBackDefaultValues () {
        Options o = new Options();
        assertDefaults(new Options(o.toBundle()));
    }

    @Test
    public void testToBundleAndBackCustomValues() {
        Options o = new Options();
        setValues(o);
        assertValues(new Options(o.toBundle()));
    }

    @Test
    public void testBundleConstructor() {
        Options o = new Options();
        Options bo = new Options(new Bundle());

        assertEquals(o.getAccountCode(), bo.getAccountCode());

        assertEquals(o.getHost(), bo.getHost());
        assertEquals(o.getMethod(), bo.getMethod());

        assertEquals(o.isAutoDetectBackground(), bo.isAutoDetectBackground());
        assertEquals(o.isAutoStart(), bo.isAutoStart());
        assertEquals(o.isEnabled(), bo.isHttpSecure());
        assertEquals(o.isHttpSecure(), bo.isHttpSecure());

        assertEquals(o.getParseCdnNameHeader(), bo.getParseCdnNameHeader());
        assertEquals(o.getParseNodeHeader(), bo.getParseNodeHeader());
        assertEquals(o.getParseCdnNodeList(), bo.getParseCdnNodeList());

        assertEquals(o.getPendingMetadata(), bo.getPendingMetadata());
    }

    private void setValues(Options o) {
        // Set values
        o.setEnabled(false);
        o.setHttpSecure(true);
        o.setHost("a");
        o.setMethod(Options.Method.POST);
        o.setAccountCode("b");
        o.setUsername("c");
        o.setUserEmail("email@email.email");
        o.setUserAnonymousId("anon");
        o.setUserType("userType");
        o.setUserPrivacyProtocol("optin");
        o.setUserProfileId("profileId");
        o.setSmartSwitchConfigCode("ssc");
        o.setSmartSwitchGroupCode("ssg");
        o.setSmartSwitchContractCode("ssc");
        o.setParseHls(true);
        o.setParseCdnNameHeader("d");
        o.setParseNodeHeader("n");
        o.setParseCdnNode(true);
        ArrayList<String> cdnNodes = new ArrayList<>();
        cdnNodes.add("listitem1");
        cdnNodes.add("listitem2");
        o.setParseCdnNodeList(cdnNodes);
        o.setNetworkIP("f");
        o.setNetworkIsp("g");
        o.setNetworkConnectionType("h");
        o.setDeviceCode("i");
        o.setDeviceModel("m");
        o.setDeviceBrand("b");
        o.setDeviceType("t");
        o.setDeviceOsName("o");
        o.setDeviceOsVersion("v");
        o.setContentResource("j");
        o.setContentIsLive(true);
        o.setContentTitle("k");
        o.setProgram("l");
        o.setContentDuration(1.0);
        o.setContentTransactionCode("m");
        o.setContentSegmentDuration(7L);
        o.setContentBitrate(2L);
        o.setContentThroughput(3L);
        o.setContentRendition("n");
        o.setContentCdn("o");
        o.setContentCdnNode("ñ");
        o.setContentCdnType("t");
        o.setAdProvider("ap");
        o.setContentFps(4.0);
        o.setContentStreamingProtocol("DASH");
        Bundle b = new Bundle();
        b.putString("p", "q");
        o.setContentMetadata(b);
        o.setContentIsLiveNoSeek(false);
        o.setContentPackage("p");
        o.setContentSaga("s");
        o.setContentTvShow("t");
        o.setContentSeason("s");
        o.setContentEpisodeTitle("e");
        o.setContentChannel("c");
        o.setContentId("i");
        o.setContentImdbId("i");
        o.setContentGracenoteId("g");
        o.setContentType("t");
        o.setContentGenre("g");
        o.setContentLanguage("l");
        o.setContentSubtitles("s");
        o.setContentContractedResolution("c");
        o.setContentCost("c");
        o.setContentPrice("p");
        o.setContentPlaybackType("p");
        o.setContentDrm("d");
        o.setContentEncodingVideoCodec("v");
        o.setContentEncodingAudioCodec("a");
        Bundle b1 = new Bundle();
        b1.putString("c", "s");
        o.setContentEncodingCodecSettings(b1);
        o.setContentEncodingCodecProfile("p");
        o.setContentEncodingContainerFormat("f");
        Bundle b2 = new Bundle();
        b2.putString("r", "s");
        o.setAdMetadata(b2);
        o.setAdCampaign("c");
        o.setAdCreativeId("1234");
        o.setAdResource("r");
        o.setAdTitle("t");
        o.setAdGivenBreaks(3);
        o.setAdExpectedBreaks(3);

        ArrayList<Integer> adsExpected = new ArrayList<Integer>() {{
            add(1);
        }};

        Bundle expectedPattern = new Bundle();
        expectedPattern.putIntegerArrayList("pre", adsExpected);
        expectedPattern.putIntegerArrayList("mid", adsExpected);
        expectedPattern.putIntegerArrayList("post", adsExpected);

        o.setAdExpectedPattern(expectedPattern);

        ArrayList<Integer> adBreaksTime = new ArrayList<>();
        adBreaksTime.add(1);
        adBreaksTime.add(1);
        adBreaksTime.add(1);

        o.setAdBreaksTime(adBreaksTime);
        o.setGivenAds(3);
        o.setContentCustomDimension1("t");
        o.setContentCustomDimension2("u");
        o.setContentCustomDimension3("v");
        o.setContentCustomDimension4("w");
        o.setContentCustomDimension5("x");
        o.setContentCustomDimension6("y");
        o.setContentCustomDimension7("z");
        o.setContentCustomDimension8("aa");
        o.setContentCustomDimension9("ab");
        o.setContentCustomDimension10("ac");
        o.setContentCustomDimension11("ad");
        o.setContentCustomDimension12("ae");
        o.setContentCustomDimension13("af");
        o.setContentCustomDimension14("ag");
        o.setContentCustomDimension15("ah");
        o.setContentCustomDimension16("ai");
        o.setContentCustomDimension17("aj");
        o.setContentCustomDimension18("ak");
        o.setContentCustomDimension19("al");
        o.setContentCustomDimension20("am");
        o.setAdCustomDimension1("t");
        o.setAdCustomDimension2("u");
        o.setAdCustomDimension3("v");
        o.setAdCustomDimension4("w");
        o.setAdCustomDimension5("x");
        o.setAdCustomDimension6("y");
        o.setAdCustomDimension7("z");
        o.setAdCustomDimension8("aa");
        o.setAdCustomDimension9("ab");
        o.setAdCustomDimension10("ac");
        o.setAppName("app");
        o.setAppReleaseVersion("0");
    }

    private void assertDefaults(Options o) {
        // Verify
        assertTrue(o.isEnabled());
        assertTrue(o.isHttpSecure());
        assertEquals("lma.npaw.com", o.getHost());
        assertEquals(Options.Method.GET, o.getMethod());
        assertEquals("nicetest", o.getAccountCode());
        assertNull(o.getUsername());
        assertNull(o.getUserEmail());
        assertNull(o.getUserAnonymousId());
        assertNull(o.getUserType());
        assertNull(o.getUserPrivacyProtocol());
        assertNull(o.getUserProfileId());
        assertNull(o.getSmartSwitchConfigCode());
        assertNull(o.getSmartSwitchGroupCode());
        assertNull(o.getSmartSwitchContractCode());
        assertFalse(o.isParseHls());
        assertEquals("x-cdn-forward", o.getParseCdnNameHeader());
        assertNull(o.getParseNodeHeader());
        assertFalse(o.isParseCdnNode());
        ArrayList<String> cdnList = o.getParseCdnNodeList();
        assertEquals(CDN_NAME_AKAMAI, cdnList.get(0));
        assertEquals(CDN_NAME_CLOUDFRONT, cdnList.get(1));
        assertEquals(CDN_NAME_LEVEL3, cdnList.get(2));
        assertEquals(CDN_NAME_FASTLY, cdnList.get(3));
        assertEquals(CDN_NAME_HIGHWINDS, cdnList.get(4));
        assertNull(o.getNetworkIP());
        assertNull(o.getNetworkIsp());
        assertNull(o.getNetworkConnectionType());
        assertNull(o.getDeviceCode());
        assertNull(o.getDeviceModel());
        assertNull(o.getDeviceBrand());
        assertNull(o.getDeviceType());
        assertNull(o.getDeviceOsName());
        assertNull(o.getDeviceOsVersion());
        assertNull(o.getContentResource());
        assertNull(o.getContentIsLive());
        assertNull(o.getContentTitle());
        assertNull(o.getProgram());
        assertNull(o.getContentDuration());
        assertNull(o.getContentTransactionCode());
        assertNull(o.getContentSegmentDuration());
        assertNull(o.getContentBitrate());
        assertNull(o.getContentThroughput());
        assertNull(o.getContentRendition());
        assertNull(o.getContentCdn());
        assertNull(o.getContentFps());
        assertNull(o.getContentStreamingProtocol());
        Bundle b = o.getContentMetadata();
        assertFalse(o.getContentIsLiveNoSeek());
        assertNull(o.getContentPackage());
        assertNull(o.getContentSaga());
        assertNull(o.getContentTvShow());
        assertNull(o.getContentSeason());
        assertNull(o.getContentEpisodeTitle());
        assertNull(o.getContentChannel());
        assertNull(o.getContentId());
        assertNull(o.getContentImdbId());
        assertNull(o.getContentGracenoteId());
        assertNull(o.getContentType());
        assertNull(o.getContentGenre());
        assertNull(o.getContentLanguage());
        assertNull(o.getContentSubtitles());
        assertNull(o.getContentContractedResolution());
        assertNull(o.getContentCost());
        assertNull(o.getContentPrice());
        assertNull(o.getContentPlaybackType());
        assertNull(o.getContentDrm());
        assertNull(o.getContentEncodingVideoCodec());
        assertNull(o.getContentEncodingAudioCodec());
        assertNull(o.getContentEncodingCodecSettings());
        assertNull(o.getContentEncodingCodecProfile());
        assertNull(o.getContentEncodingContainerFormat());
        assertNotNull(b);
        assertEquals(0, b.size());
        Bundle b2 = o.getAdMetadata();
        assertNotNull(b2);
        assertNull(o.getAdCampaign());
        assertNull(o.getAdResource());
        assertNull(o.getAdTitle());
        assertEquals(0, b2.size());
        assertNull(o.getContentCustomDimension1());
        assertNull(o.getContentCustomDimension2());
        assertNull(o.getContentCustomDimension3());
        assertNull(o.getContentCustomDimension4());
        assertNull(o.getContentCustomDimension5());
        assertNull(o.getContentCustomDimension6());
        assertNull(o.getContentCustomDimension7());
        assertNull(o.getContentCustomDimension8());
        assertNull(o.getContentCustomDimension9());
        assertNull(o.getContentCustomDimension10());
        assertNull(o.getContentCustomDimension11());
        assertNull(o.getContentCustomDimension12());
        assertNull(o.getContentCustomDimension13());
        assertNull(o.getContentCustomDimension14());
        assertNull(o.getContentCustomDimension15());
        assertNull(o.getContentCustomDimension16());
        assertNull(o.getContentCustomDimension17());
        assertNull(o.getContentCustomDimension18());
        assertNull(o.getContentCustomDimension19());
        assertNull(o.getContentCustomDimension20());
        assertNull(o.getAdCustomDimension1());
        assertNull(o.getAdCustomDimension2());
        assertNull(o.getAdCustomDimension3());
        assertNull(o.getAdCustomDimension4());
        assertNull(o.getAdCustomDimension5());
        assertNull(o.getAdCustomDimension6());
        assertNull(o.getAdCustomDimension7());
        assertNull(o.getAdCustomDimension8());
        assertNull(o.getAdCustomDimension9());
        assertNull(o.getAdCustomDimension10());
        assertNull(o.getAppName());
        assertNull(o.getAppReleaseVersion());
    }

    private void assertValues(Options o) {
        // Verify
        assertFalse(o.isEnabled());
        assertTrue(o.isHttpSecure());
        assertEquals("a", o.getHost());
        assertEquals(Options.Method.POST, o.getMethod());
        assertEquals("b", o.getAccountCode());
        assertEquals("c", o.getUsername());
        assertEquals("email@email.email", o.getUserEmail());
        assertEquals("anon", o.getUserAnonymousId());
        assertEquals("userType", o.getUserType());
        assertEquals("optin", o.getUserPrivacyProtocol());
        assertEquals("profileId", o.getUserProfileId());
        assertEquals("ssc", o.getSmartSwitchConfigCode());
        assertEquals("ssg", o.getSmartSwitchGroupCode());
        assertEquals("ssc", o.getSmartSwitchContractCode());
        assertTrue(o.isParseHls());
        assertEquals("d", o.getParseCdnNameHeader());
        assertEquals("n", o.getParseNodeHeader());
        assertTrue(o.isParseCdnNode());
        assertEquals("listitem1", o.getParseCdnNodeList().get(0));
        assertEquals("listitem2", o.getParseCdnNodeList().get(1));
        assertEquals("f", o.getNetworkIP());
        assertEquals("g", o.getNetworkIsp());
        assertEquals("h", o.getNetworkConnectionType());
        assertEquals("i", o.getDeviceCode());
        assertEquals("m", o.getDeviceModel());
        assertEquals("b", o.getDeviceBrand());
        assertEquals("t", o.getDeviceType());
        assertEquals("o", o.getDeviceOsName());
        assertEquals("v", o.getDeviceOsVersion());
        assertEquals("j", o.getContentResource());
        assertTrue(o.getContentIsLive());
        assertEquals("k", o.getContentTitle());
        assertEquals("l", o.getProgram());
        assertEquals((Double) 1.0, o.getContentDuration());
        assertEquals("m", o.getContentTransactionCode());
        assertEquals((Long) 7L, o.getContentSegmentDuration());
        assertEquals((Long) 2L, o.getContentBitrate());
        assertEquals((Long) 3L, o.getContentThroughput());
        assertEquals("n", o.getContentRendition());
        assertEquals("o", o.getContentCdn());
        assertEquals("ñ", o.getContentCdnNode());
        assertEquals("t", o.getContentCdnType());
        assertEquals("ap", o.getAdProvider());
        assertEquals((Double) 4.0, o.getContentFps());
        assertEquals("DASH", o.getContentStreamingProtocol());
        assertEquals("q", o.getContentMetadata().getString("p"));
        assertFalse(o.getContentIsLiveNoSeek());
        assertEquals("p", o.getContentPackage());
        assertEquals("s", o.getContentSaga());
        assertEquals("t", o.getContentTvShow());
        assertEquals("s", o.getContentSeason());
        assertEquals("e", o.getContentEpisodeTitle());
        assertEquals("c", o.getContentChannel());
        assertEquals("i", o.getContentId());
        assertEquals("i", o.getContentImdbId());
        assertEquals("g", o.getContentGracenoteId());
        assertEquals("t", o.getContentType());
        assertEquals("g", o.getContentGenre());
        assertEquals("l", o.getContentLanguage());
        assertEquals("s", o.getContentSubtitles());
        assertEquals("c", o.getContentContractedResolution());
        assertEquals("c", o.getContentCost());
        assertEquals("p", o.getContentPrice());
        assertEquals("p", o.getContentPlaybackType());
        assertEquals("d", o.getContentDrm());
        assertEquals("v", o.getContentEncodingVideoCodec());
        assertEquals("a", o.getContentEncodingAudioCodec());
        assertEquals("s", o.getContentEncodingCodecSettings().getString("c"));
        assertEquals("p", o.getContentEncodingCodecProfile());
        assertEquals("f", o.getContentEncodingContainerFormat());
        assertEquals("s", o.getAdMetadata().getString("r"));
        assertEquals("c", o.getAdCampaign());
        assertEquals("r", o.getAdResource());
        assertEquals("t", o.getAdTitle());
        assertEquals("t", o.getContentCustomDimension1());
        assertEquals("u", o.getContentCustomDimension2());
        assertEquals("v", o.getContentCustomDimension3());
        assertEquals("w", o.getContentCustomDimension4());
        assertEquals("x", o.getContentCustomDimension5());
        assertEquals("y", o.getContentCustomDimension6());
        assertEquals("z", o.getContentCustomDimension7());
        assertEquals("aa", o.getContentCustomDimension8());
        assertEquals("ab", o.getContentCustomDimension9());
        assertEquals("ac", o.getContentCustomDimension10());
        assertEquals("t", o.getAdCustomDimension1());
        assertEquals("u", o.getAdCustomDimension2());
        assertEquals("v", o.getAdCustomDimension3());
        assertEquals("w", o.getAdCustomDimension4());
        assertEquals("x", o.getAdCustomDimension5());
        assertEquals("y", o.getAdCustomDimension6());
        assertEquals("z", o.getAdCustomDimension7());
        assertEquals("aa", o.getAdCustomDimension8());
        assertEquals("ab", o.getAdCustomDimension9());
        assertEquals("ac", o.getAdCustomDimension10());
        assertEquals("app", o.getAppName());
        assertEquals("0", o.getAppReleaseVersion());
    }
}
