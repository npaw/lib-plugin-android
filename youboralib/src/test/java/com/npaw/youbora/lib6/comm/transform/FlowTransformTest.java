package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.comm.Request;

import org.junit.Test;

import static com.npaw.youbora.lib6.constants.Services.ERROR;
import static com.npaw.youbora.lib6.constants.Services.INIT;
import static com.npaw.youbora.lib6.constants.Services.START;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

public class FlowTransformTest {

    @Test
    public void testIsBlocking() {

        Request blockingRequest = mock(Request.class);
        when(blockingRequest.getService()).thenReturn("/service");

        // Init and start should unlock the transform
        Request initRequest = mock(Request.class);
        when(initRequest.getService()).thenReturn(INIT);

        Request startRequest = mock(Request.class);
        when(startRequest.getService()).thenReturn(START);

        Request errorRequest = mock(Request.class);
        when(errorRequest.getService()).thenReturn(ERROR);

        FlowTransform ft = new FlowTransform();

        assertTrue(ft.isBlocking(null));
        assertTrue(ft.isBlocking(blockingRequest));

        // Init and start should unlock the transform, but error only bypass it

        // init
        assertFalse(ft.isBlocking(initRequest));
        assertFalse(ft.isBlocking(blockingRequest));

        // start
        ft = new FlowTransform();
        assertFalse(ft.isBlocking(startRequest));
        assertFalse(ft.isBlocking(blockingRequest));

        // error
        ft = new FlowTransform();
        assertFalse(ft.isBlocking(errorRequest));
        assertTrue(ft.isBlocking(blockingRequest));

    }

    @Test
    public void testParseShouldDoNothing() {
        FlowTransform ft = new FlowTransform();

        Request startRequest = mock(Request.class);
        when(startRequest.getService()).thenReturn(START);

        ft.parse(startRequest);
        assertTrue(ft.isBlocking(null));

        ft.parse(null);
        assertTrue(ft.isBlocking(null));
    }

}