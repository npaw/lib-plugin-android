package com.npaw.youbora.lib6.plugin;

import android.content.Context;
import android.os.Bundle;
import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.AdAdapter;
import com.npaw.youbora.lib6.adapter.PlaybackChronos;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.balancer.models.CdnLoaderStats;
import com.npaw.youbora.lib6.balancer.models.CdnStats;
import com.npaw.youbora.lib6.balancer.models.FailedRequest;
import com.npaw.youbora.lib6.balancer.models.P2PStats;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.Transform;
import com.npaw.youbora.lib6.flags.BaseFlags;
import com.npaw.youbora.lib6.balancer.models.BalancerStats;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.nullable;

import static com.npaw.youbora.lib6.constants.Services.AD_BREAK_START;
import static com.npaw.youbora.lib6.constants.Services.AD_BREAK_STOP;
import static com.npaw.youbora.lib6.constants.Services.AD_BUFFER;
import static com.npaw.youbora.lib6.constants.Services.AD_CLICK;
import static com.npaw.youbora.lib6.constants.Services.AD_ERROR;
import static com.npaw.youbora.lib6.constants.Services.AD_INIT;
import static com.npaw.youbora.lib6.constants.Services.AD_JOIN;
import static com.npaw.youbora.lib6.constants.Services.AD_MANIFEST;
import static com.npaw.youbora.lib6.constants.Services.AD_PAUSE;
import static com.npaw.youbora.lib6.constants.Services.AD_QUARTILE;
import static com.npaw.youbora.lib6.constants.Services.AD_RESUME;
import static com.npaw.youbora.lib6.constants.Services.AD_START;
import static com.npaw.youbora.lib6.constants.Services.AD_STOP;
import static com.npaw.youbora.lib6.constants.Services.BUFFER;
import static com.npaw.youbora.lib6.constants.Services.ERROR;
import static com.npaw.youbora.lib6.constants.Services.INIT;
import static com.npaw.youbora.lib6.constants.Services.JOIN;
import static com.npaw.youbora.lib6.constants.Services.OFFLINE_EVENTS;
import static com.npaw.youbora.lib6.constants.Services.PAUSE;
import static com.npaw.youbora.lib6.constants.Services.RESUME;
import static com.npaw.youbora.lib6.constants.Services.SEEK;
import static com.npaw.youbora.lib6.constants.Services.START;
import static com.npaw.youbora.lib6.constants.Services.STOP;
import static com.npaw.youbora.lib6.constants.Services.VIDEO_EVENT;

import junit.framework.TestCase;

@RunWith(RobolectricTestRunner.class)
public class PluginTest extends PluginMocker {

    private YouboraLog.YouboraLogger mockLogger;
    private BalancerStats balancerStats;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);

        List<CdnStats> cdnList = Arrays.asList(
                new CdnStats("cdnName1", "provider1", 500, 4, 400, 0, 0, 0, 0, 0, 0, 0, 0, true, true),
                new CdnStats("cdnName2", "provider2", 800, 30, 100, 5, 0, 0, 0, 0, 0, 0, 0, true, true)
        );
        CdnLoaderStats cdns = new CdnLoaderStats();
        cdns.setCdns(cdnList);

        P2PStats p2p = new P2PStats("P2P", "P2P", 20000L, 2, 1000L, 12000L, 6, 20000L, 9, 2, 4, 3,
                40L, 0, 0, 0, 0, 0, 0, false, true, 0, 0, 0, 0, 0);

        balancerStats = new BalancerStats("testProfileName", cdns, p2p, 0L, "version");
    }

    @Test
    public void testConstructorPerformedOperations() {
        verify(mockViewTransform, times(1)).addTransformDoneListener(any(Transform.TransformDoneListener.class));
        verify(mockViewTransform, times(1)).init(null);
        verify(mockAdapter, times(1)).addEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }

    @Test
    public void testSetOptions() {
        // Mock options
        Options o = mock(Options.class);
        when(o.getAccountCode()).thenReturn("a");
        when(o.getHost()).thenReturn("host.com");

        Plugin p = new TestPlugin(o);
        assertEquals("a", p.getOptions().getAccountCode());

        p.setOptions(o);
        assertEquals("a", p.getOptions().getAccountCode());
    }

    @Test
    public void testAddAndRemoveAdapters() {
        PlayerAdapter mockAdapter = mock(PlayerAdapter.class);
        p.setAdapter(mockAdapter);

        assertEquals(mockAdapter, p.getAdapter());

        verify(mockAdapter, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        PlayerAdapter mockAdapter2 = mock(PlayerAdapter.class);
        p.setAdapter(mockAdapter2);

        assertEquals(mockAdapter2, p.getAdapter());

        verify(mockAdapter, times(1)).dispose();
        verify(mockAdapter, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));

        verify(mockAdapter2, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        p.removeAdapter();
        verify(mockAdapter2, times(1)).dispose();
        verify(mockAdapter2, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }

    @Test
    public void testAddAndRemoveAdsAdapters() {
        AdAdapter mockAdapter = mock(AdAdapter.class);
        p.setAdsAdapter(mockAdapter);

        assertEquals(mockAdapter, p.getAdsAdapter());

        verify(mockAdapter, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        AdAdapter mockAdapter2 = mock(AdAdapter.class);
        p.setAdsAdapter(mockAdapter2);

        assertEquals(mockAdapter2, p.getAdsAdapter());

        verify(mockAdapter, times(1)).dispose();
        verify(mockAdapter, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));

        verify(mockAdapter2, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        p.removeAdsAdapter();
        verify(mockAdapter2, times(1)).dispose();
        verify(mockAdapter2, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }


    @Test
    public void testEnableDisable() {
        p.enable();
        verify(mockOptions).setEnabled(eq(true));

        p.disable();
        verify(mockOptions).setEnabled(eq(false));
    }

    @Test
    public void testPreloads() {
        p.firePreloadBegin();
        p.firePreloadBegin();
        verify(mockChrono, times(1)).start();

        p.firePreloadEnd();
        p.firePreloadEnd();
        verify(mockChrono, times(1)).stop();
    }

    // Test get info
    @Test
    public void testGetHost() {
        when(mockOptions.getHost()).thenReturn("http://host.com");
        when(mockOptions.isHttpSecure()).thenReturn(true);

        assertEquals("https://host.com", p.getHost());
    }

    @Test
    public void testParseHls() {
        when(mockOptions.isParseHls()).thenReturn(true);
        assertTrue(p.isParseHls());

        when(mockOptions.isParseHls()).thenReturn(false);
        assertFalse(p.isParseHls());
    }

    @Test
    public void testCdnNode() {
        when(mockOptions.isParseCdnNode()).thenReturn(true);
        assertTrue(p.isParseCdnNode());

        when(mockOptions.isParseCdnNode()).thenReturn(false);
        assertFalse(p.isParseHls());
    }

    @Test
    public void testParseCdnNodeList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("item1");
        list.add("item2");
        list.add("item3");
        when(mockOptions.getParseCdnNodeList()).thenReturn(null);
        assertNull(p.getParseCdnNodeList());

        when(mockOptions.getParseCdnNodeList()).thenReturn(list);
        assertEquals(list, p.getParseCdnNodeList());
    }

    @Test
    public void testParseCdnNodeHeader() {
        when(mockOptions.getParseCdnNameHeader()).thenReturn("x-header");
        assertEquals("x-header", p.getParseCdnNodeNameHeader());
    }

    @Test
    public void testParseNodeHeader() {
        when(mockOptions.getParseNodeHeader()).thenReturn("node-header");
        assertEquals("node-header", p.getParseNodeHeader());
    }

    @Test
    public void testPlayhead() {
        // Valid values
        when(mockAdapter.getPlayhead()).thenReturn(-10.0);
        assertEquals(Double.valueOf(0.0), p.getPlayhead());

        when(mockAdapter.getPlayhead()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getPlayhead());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getPlayhead()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getPlayhead());
        }
    }

    @Test
    public void testRate() {
        // Valid values
        when(mockAdapter.getPlayrate()).thenReturn(-0.5);
        assertEquals(Double.valueOf(1.0), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(0.0);
        assertEquals(Double.valueOf(0.0), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getPlayrate());
    }

    @Test
    public void testFps() {
        when(mockOptions.getContentFps()).thenReturn(25.0);
        when(mockAdapter.getFramesPerSecond()).thenReturn(15.0);
        assertEquals(Double.valueOf(25.0), p.getFramesPerSecond());

        when(mockOptions.getContentFps()).thenReturn(null);
        when(mockAdapter.getFramesPerSecond()).thenReturn(15.5);
        assertEquals(Double.valueOf(15.5), p.getFramesPerSecond());

        when(mockOptions.getContentFps()).thenReturn(null);
        when(mockAdapter.getFramesPerSecond()).thenReturn(null);
        assertNull(p.getFramesPerSecond());
    }

    @Test
    public void testDroppedFrames() {
        // Valid value
        when(mockAdapter.getDroppedFrames()).thenReturn(10);
        assertEquals(Integer.valueOf(10), p.getDroppedFrames());

        // Invalid values
        for (Integer i : INTEGER_INVALID) {
            when(mockAdapter.getDroppedFrames()).thenReturn(i);
            assertEquals(Integer.valueOf(0), p.getDroppedFrames());
        }
    }

    @Test
    public void testDuration() {
        when(mockOptions.getContentDuration()).thenReturn(null);

        // Valid values
        when(mockAdapter.getDuration()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getDuration());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getDuration()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getDuration());
        }

        when(mockAdapter.getDuration()).thenReturn(-10.0);
        assertEquals(Double.valueOf(0.0), p.getDuration());

        // Test options prevalence over adapter
        when(mockOptions.getContentDuration()).thenReturn(1.0);

        when(mockAdapter.getDuration()).thenReturn(2.0);
        assertEquals(Double.valueOf(1.0), p.getDuration());
    }

    @Test
    public void testSegmentDuration() {
        when(mockOptions.getContentSegmentDuration()).thenReturn(null);

        // Valid values
        balancerStats.setSegmentDuration(10000L);
        p.cdnBalancerInfo.setBalancerStats(balancerStats);
        assertEquals(Long.valueOf(10000L), p.getSegmentDuration());

        // Invalid values
        for (Long l : LONG_INVALID) {
            balancerStats.setSegmentDuration(l);
            p.cdnBalancerInfo.setBalancerStats(balancerStats);
            assertEquals(Long.valueOf(-1), p.getSegmentDuration());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentSegmentDuration()).thenReturn(8000L);

        balancerStats.setSegmentDuration(20000L);
        p.cdnBalancerInfo.setBalancerStats(balancerStats);
        assertEquals(Long.valueOf(8000L), p.getSegmentDuration());
    }

    @Test
    public void testBitrate() {
        when(mockOptions.getContentBitrate()).thenReturn(null);

        // Valid values
        when(mockAdapter.getBitrate()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getBitrate());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getBitrate()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getBitrate());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentBitrate()).thenReturn(1000000L);

        when(mockAdapter.getBitrate()).thenReturn(2000000L);
        assertEquals(Long.valueOf(1000000), p.getBitrate());
    }

    @Test
    public void testThroughput() {
        when(mockOptions.getContentThroughput()).thenReturn(null);

        // Valid values
        when(mockAdapter.getThroughput()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getThroughput());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getThroughput()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getThroughput());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentThroughput()).thenReturn(1000000L);

        when(mockAdapter.getThroughput()).thenReturn(2000000L);
        assertEquals(Long.valueOf(1000000), p.getThroughput());
    }

    @Test
    public void testRendition() {
        when(mockOptions.getContentRendition()).thenReturn(null);
        when(mockAdapter.getRendition()).thenReturn("1Mbps");
        assertEquals("1Mbps", p.getRendition());

        when(mockAdapter.getRendition()).thenReturn("");
        assertEquals("", p.getRendition());

        when(mockAdapter.getRendition()).thenReturn(null);
        assertNull(p.getRendition());

        // Test options prevalence over adapter
        when(mockOptions.getContentRendition()).thenReturn("2Mbps");
        when(mockAdapter.getRendition()).thenReturn("1Mbps");
        assertEquals("2Mbps", p.getRendition());

        // unless it's empty
        when(mockOptions.getContentRendition()).thenReturn("");
        assertEquals("1Mbps", p.getRendition());
    }

    @Test
    public void testTitle() {
        when(mockOptions.getContentTitle()).thenReturn(null);
        when(mockAdapter.getTitle()).thenReturn("batman");
        assertEquals("batman", p.getTitle());

        when(mockAdapter.getTitle()).thenReturn("");
        assertEquals("", p.getTitle());

        when(mockAdapter.getTitle()).thenReturn(null);
        assertNull(p.getTitle());

        // Test options prevalence over adapter
        when(mockOptions.getContentTitle()).thenReturn("iron man");
        when(mockAdapter.getTitle()).thenReturn("batman");
        assertEquals("iron man", p.getTitle());

        // unless it's empty
        when(mockOptions.getContentTitle()).thenReturn("");
        assertEquals("batman", p.getTitle());
    }

    @Test
    public void testProgram() {
        when(mockOptions.getProgram()).thenReturn(null);
        when(mockAdapter.getProgram()).thenReturn("episode 1");
        assertEquals("episode 1", p.getProgram());

        when(mockAdapter.getProgram()).thenReturn("");
        assertEquals("", p.getProgram());

        when(mockAdapter.getProgram()).thenReturn(null);
        assertNull(p.getProgram());

        // Test options prevalence over adapter
        when(mockOptions.getProgram()).thenReturn("episode 1");
        when(mockAdapter.getProgram()).thenReturn("episode 2");
        assertEquals("episode 1", p.getProgram());

        // unless it's empty
        when(mockOptions.getProgram()).thenReturn("");
        assertEquals("episode 2", p.getProgram());
    }

    @Test
    public void testLive() {
        when(mockOptions.getContentIsLive()).thenReturn(null);

        // Default
        when(mockAdapter.getIsLive()).thenReturn(null);
        assertFalse(p.getIsLive());

        when(mockAdapter.getIsLive()).thenReturn(false);
        assertFalse(p.getIsLive());

        when(mockAdapter.getIsLive()).thenReturn(true);
        assertTrue(p.getIsLive());

        // Test options prevalence over adapter
        when(mockOptions.getContentIsLive()).thenReturn(false);
        assertFalse(p.getIsLive());

        when(mockOptions.getContentIsLive()).thenReturn(true);
        when(mockAdapter.getIsLive()).thenReturn(false);
        assertTrue(p.getIsLive());
    }

    @Test
    public void testGetResource() {
        when(mockOptions.getContentResource()).thenReturn("ResourceFromOptions");
        when(mockAdapter.getResource()).thenReturn("ResourceFromAdapter");
        assertEquals("ResourceFromOptions", p.getResource());

        when(mockOptions.getContentResource()).thenReturn(null);
        when(mockAdapter.getResource()).thenReturn("Resource");
        assertEquals("Resource", p.getResource());

        when(mockAdapter.getResource()).thenReturn(null);
        assertNull(p.getResource());
    }

    @Test
    public void testGetParsedResource() {
        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(true);
        when(mockResourceTransform.getResource()).thenReturn("ResourceFromTransform");
        assertNull(p.getParsedResource());

        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(false);
        assertEquals("ResourceFromTransform", p.getParsedResource());

        when(p.getResource()).thenReturn("Resource");
        when(mockResourceTransform.getResource()).thenReturn("Resource");
        assertNull(p.getParsedResource());

        when(mockResourceTransform.getResource()).thenReturn(null);
        assertNull(p.getParsedResource());
    }

    @Test
    public void testTransactionCode() {
        when(mockOptions.getContentTransactionCode()).thenReturn("transactionCode");

        assertEquals("transactionCode", p.getTransactionCode());

        when(mockOptions.getContentTransactionCode()).thenReturn(null);

        assertNull(p.getTransactionCode());
    }

    @Test
    public void testPlayerVersion() {
        when(mockAdapter.getPlayerVersion()).thenReturn(null);

        assertEquals("", p.getPlayerVersion());

        when(mockAdapter.getPlayerVersion()).thenReturn("1.2.3");

        assertEquals("1.2.3", p.getPlayerVersion());
    }

    @Test
    public void testPlayerName() {
        when(mockAdapter.getPlayerName()).thenReturn(null);

        assertEquals("", p.getPlayerName());

        when(mockAdapter.getPlayerName()).thenReturn("player-name");

        assertEquals("player-name", p.getPlayerName());
    }

    @Test
    public void testCdn() {
        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(true);
        when(mockOptions.getContentCdn()).thenReturn("CdnFromOptions");
        when(mockResourceTransform.getCdnName()).thenReturn("CdnFromTransform");
        assertEquals("CdnFromOptions", p.getCdn());

        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(false);
        assertEquals("CdnFromTransform", p.getCdn());

        when(mockResourceTransform.getCdnName()).thenReturn(null);
        assertEquals("CdnFromOptions", p.getCdn());

        when(mockOptions.getContentCdn()).thenReturn(null);
        assertNull(p.getCdn());
    }

    @Test
    public void testPluginVersion() {
        when(mockAdapter.getVersion()).thenReturn("6.0.0-CustomPlugin");
        assertEquals("6.0.0-CustomPlugin-Android", p.getPluginVersion());

        p.removeAdapter();
        assertEquals(BuildConfig.VERSION_NAME+"-adapterless-Android", p.getPluginVersion());
    }

    @Test
    public void testLibVersion() {
        assertEquals(BuildConfig.VERSION_NAME, p.getLibVersion());
    }

    @Test
    public void testCustomDimensions() {
        when(mockOptions.getContentCustomDimension1()).thenReturn("value-custom-value1");
        when(mockOptions.getContentCustomDimension2()).thenReturn("value-custom-value2");
        when(mockOptions.getContentCustomDimension3()).thenReturn("value-custom-value3");
        when(mockOptions.getContentCustomDimension4()).thenReturn("value-custom-value4");
        when(mockOptions.getContentCustomDimension5()).thenReturn("value-custom-value5");
        when(mockOptions.getContentCustomDimension6()).thenReturn("value-custom-value6");
        when(mockOptions.getContentCustomDimension7()).thenReturn("value-custom-value7");
        when(mockOptions.getContentCustomDimension8()).thenReturn("value-custom-value8");
        when(mockOptions.getContentCustomDimension9()).thenReturn("value-custom-value9");
        when(mockOptions.getContentCustomDimension10()).thenReturn("value-custom-value10");
        when(mockOptions.getContentCustomDimension11()).thenReturn("value-custom-value11");
        when(mockOptions.getContentCustomDimension12()).thenReturn("value-custom-value12");
        when(mockOptions.getContentCustomDimension13()).thenReturn("value-custom-value13");
        when(mockOptions.getContentCustomDimension14()).thenReturn("value-custom-value14");
        when(mockOptions.getContentCustomDimension15()).thenReturn("value-custom-value15");
        when(mockOptions.getContentCustomDimension16()).thenReturn("value-custom-value16");
        when(mockOptions.getContentCustomDimension17()).thenReturn("value-custom-value17");
        when(mockOptions.getContentCustomDimension18()).thenReturn("value-custom-value18");
        when(mockOptions.getContentCustomDimension19()).thenReturn("value-custom-value19");
        when(mockOptions.getContentCustomDimension20()).thenReturn("value-custom-value20");

        assertEquals("value-custom-value1", p.getContentCustomDimension1());
        assertEquals("value-custom-value2", p.getContentCustomDimension2());
        assertEquals("value-custom-value3", p.getContentCustomDimension3());
        assertEquals("value-custom-value4", p.getContentCustomDimension4());
        assertEquals("value-custom-value5", p.getContentCustomDimension5());
        assertEquals("value-custom-value6", p.getContentCustomDimension6());
        assertEquals("value-custom-value7", p.getContentCustomDimension7());
        assertEquals("value-custom-value8", p.getContentCustomDimension8());
        assertEquals("value-custom-value9", p.getContentCustomDimension9());
        assertEquals("value-custom-value10", p.getContentCustomDimension10());
        assertEquals("value-custom-value11", p.getContentCustomDimension11());
        assertEquals("value-custom-value12", p.getContentCustomDimension12());
        assertEquals("value-custom-value13", p.getContentCustomDimension13());
        assertEquals("value-custom-value14", p.getContentCustomDimension14());
        assertEquals("value-custom-value15", p.getContentCustomDimension15());
        assertEquals("value-custom-value16", p.getContentCustomDimension16());
        assertEquals("value-custom-value17", p.getContentCustomDimension17());
        assertEquals("value-custom-value18", p.getContentCustomDimension18());
        assertEquals("value-custom-value19", p.getContentCustomDimension19());
        assertEquals("value-custom-value20", p.getContentCustomDimension20());

        when(mockOptions.getContentCustomDimension1()).thenReturn(null);
        when(mockOptions.getContentCustomDimension2()).thenReturn(null);
        when(mockOptions.getContentCustomDimension3()).thenReturn(null);
        when(mockOptions.getContentCustomDimension4()).thenReturn(null);
        when(mockOptions.getContentCustomDimension5()).thenReturn(null);
        when(mockOptions.getContentCustomDimension6()).thenReturn(null);
        when(mockOptions.getContentCustomDimension7()).thenReturn(null);
        when(mockOptions.getContentCustomDimension8()).thenReturn(null);
        when(mockOptions.getContentCustomDimension9()).thenReturn(null);
        when(mockOptions.getContentCustomDimension10()).thenReturn(null);

        assertNull(p.getContentCustomDimension1());
        assertNull(p.getContentCustomDimension2());
        assertNull(p.getContentCustomDimension3());
        assertNull(p.getContentCustomDimension4());
        assertNull(p.getContentCustomDimension5());
        assertNull(p.getContentCustomDimension6());
        assertNull(p.getContentCustomDimension7());
        assertNull(p.getContentCustomDimension8());
        assertNull(p.getContentCustomDimension9());
        assertNull(p.getContentCustomDimension10());
    }

    @Test
    public void testAdPlayerVersion() {
        when(mockAdAdapter.getPlayerVersion()).thenReturn(null);
        assertEquals("", p.getAdPlayerVersion());

        when(mockAdAdapter.getPlayerVersion()).thenReturn("player-version");
        assertEquals("player-version", p.getAdPlayerVersion());

        p.removeAdsAdapter();
        assertEquals("", p.getAdPlayerVersion());
    }

    @Test
    public void testAdPosition() {
        when(mockAdAdapter.getPosition()).thenReturn(AdAdapter.AdPosition.PRE);
        assertEquals("pre", p.getAdPosition());

        when(mockAdAdapter.getPosition()).thenReturn(AdAdapter.AdPosition.POST);
        assertEquals("post", p.getAdPosition());

        when(mockAdAdapter.getPosition()).thenReturn(AdAdapter.AdPosition.MID);
        assertEquals("mid", p.getAdPosition());

        // If ad position is unknown, the plugin will try to infer the position depending on
        // the Buffered status of the adapter. This is a workaround and postrolls will be detected
        // as midrolls
        when(mockAdAdapter.getPosition()).thenReturn(AdAdapter.AdPosition.UNKNOWN);

        BaseFlags flags = new BaseFlags();
        flags.setJoined(false);
        when(mockAdapter.getFlags()).thenReturn(flags);

        assertEquals("pre", p.getAdPosition());

        flags.setJoined(true);

        assertEquals("mid", p.getAdPosition());

        // No ads adapter, this should be the same as it returning UNKNOWN, "mid" expected again
        p.removeAdsAdapter();
        assertEquals("mid", p.getAdPosition());

        // No adapter at all
        p.removeAdapter();
        assertEquals("unknown", p.getAdPosition());
    }

    @Test
    public void testAdPlayhead() {
        // Valid values
        when(mockAdAdapter.getPlayhead()).thenReturn(-10.0);
        assertEquals(Double.valueOf(0.0), p.getAdPlayhead());

        when(mockAdAdapter.getPlayhead()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getAdPlayhead());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdAdapter.getPlayhead()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getAdPlayhead());
        }
    }

    @Test
    public void testAdDuration() {
        // Valid values
        when(mockAdAdapter.getDuration()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getAdDuration());

        when(mockAdAdapter.getDuration()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getAdDuration());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdAdapter.getDuration()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getAdDuration());
        }
    }

    @Test
    public void testAdBitrate() {
        // Valid values
        when(mockAdAdapter.getBitrate()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getAdBitrate());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdAdapter.getBitrate()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getAdBitrate());
        }
    }

    @Test
    public void testAdTitle() {
        when(mockAdAdapter.getTitle()).thenReturn("batman");
        assertEquals("batman", p.getAdTitle());

        when(mockAdAdapter.getTitle()).thenReturn("");
        assertEquals("", p.getAdTitle());

        when(mockAdAdapter.getTitle()).thenReturn(null);
        assertNull(p.getAdTitle());
    }

    @Test
    public void testAdGetResource() {
        when(mockAdAdapter.getResource()).thenReturn("AdResourceFromAdapter");
        assertEquals("AdResourceFromAdapter", p.getAdResource());

        when(mockAdAdapter.getResource()).thenReturn(null);
        assertEquals(null, p.getAdResource());
    }

    @Test
    public void testAdPluginVersion() {
        when(mockAdAdapter.getVersion()).thenReturn(null);
        assertNull(p.getAdAdapterVersion());

        when(mockAdAdapter.getVersion()).thenReturn("6.0.0-CustomAdapter");
        assertEquals("6.0.0-CustomAdapter", p.getAdAdapterVersion());

        p.removeAdsAdapter();
        assertEquals(null, p.getAdAdapterVersion());
    }

    @Test
    public void testCdnTraffic() {
        when(mockAdapter.getCdnTraffic()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getCdnTraffic());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getCdnTraffic()).thenReturn(l);
            assertEquals(Long.valueOf(0), p.getCdnTraffic());
        }

        // Test cdnBalancerInfo prevalence over adapter
        CdnLoaderStats cdns = new CdnLoaderStats();
        cdns.setTotalDownloadedBytes(1500L);
        balancerStats.setCdnStats(cdns);
        p.cdnBalancerInfo.setBalancerStats(balancerStats);

        when(mockAdapter.getCdnTraffic()).thenReturn(1000000L);

        assertEquals(Long.valueOf(1500L), p.getCdnTraffic());
    }

    @Test
    public void testP2PTraffic() {
        when(mockAdapter.getP2PTraffic()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getP2PTraffic());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getP2PTraffic()).thenReturn(l);
            assertEquals(Long.valueOf(0), p.getP2PTraffic());
        }

        // Test cdnBalancerInfo prevalence over adapter
        p.cdnBalancerInfo.setBalancerStats(balancerStats);

        when(mockAdapter.getCdnTraffic()).thenReturn(1000000L);

        assertEquals(Long.valueOf(20000L), p.getP2PTraffic());
    }

    @Test
    public void testUploadTraffic() {
        when(mockAdapter.getUploadTraffic()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getUploadTraffic());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getUploadTraffic()).thenReturn(l);
            assertEquals(Long.valueOf(0), p.getUploadTraffic());
        }

        // Test cdnBalancerInfo prevalence over adapter
        p.cdnBalancerInfo.setBalancerStats(balancerStats);

        when(mockAdapter.getUploadTraffic()).thenReturn(1000000L);

        assertEquals(Long.valueOf(12000L), p.getUploadTraffic());
    }

    @Test
    public void testIsP2PEnabled() {
        when(mockAdapter.getIsP2PEnabled()).thenReturn(null);
        assertNull(p.getIsP2PEnabled());

        when(mockAdapter.getIsP2PEnabled()).thenReturn(false);
        assertFalse(p.getIsP2PEnabled());

        when(mockAdapter.getIsP2PEnabled()).thenReturn(true);
        assertTrue(p.getIsP2PEnabled());

        // Test cdnBalancerInfo prevalence over adapter
        p.cdnBalancerInfo.setBalancerStats(balancerStats);

        when(mockAdapter.getIsP2PEnabled()).thenReturn(false);

        assertTrue(p.getIsP2PEnabled());
    }

    @Test
    public void testBalancerResponseId() {
        assertNull(p.getBalancerResponseId());

        CdnLoaderStats cdns = new CdnLoaderStats();
        cdns.setResponseUUID("UUID");
        balancerStats.setCdnStats(cdns);
        p.cdnBalancerInfo.setBalancerStats(balancerStats);

        assertEquals("UUID", p.getBalancerResponseId());
    }

    @Test
    public void testCdnPingInfo() {
        p.cdnBalancerInfo.setBalancerStats(balancerStats);
        assertEquals("{\"cdnName1\":{\"name\":\"cdnName1\",\"provider\":\"provider1\",\"downloaded_bytes\":500,\"downloaded_chunks\":4," +
                     "\"time\":400,\"errors\":0,\"max_bandwidth\":0,\"min_bandwidth\":0,\"banned\":0,\"unbanned\":0,\"avg_ping_time\":0,\"min_ping_time\":0," +
                     "\"max_ping_time\":0,\"is_banned\":true,\"is_active\":true}," +
                     "\"cdnName2\":{\"name\":\"cdnName2\",\"provider\":\"provider2\",\"downloaded_bytes\":800,\"downloaded_chunks\":30,\"time\":100," +
                     "\"errors\":5,\"max_bandwidth\":0,\"min_bandwidth\":0,\"banned\":0,\"unbanned\":0,\"avg_ping_time\":0,\"min_ping_time\":0," +
                     "\"max_ping_time\":0,\"is_banned\":true,\"is_active\":true},\"P2P\":{\"name\":\"P2P\",\"provider\":\"P2P\",\"downloaded_bytes\":20000," +
                     "\"downloaded_chunks\":2,\"time\":1000,\"uploaded_bytes\":12000,\"uploaded_chunks\":6,\"uploaded_time\":20000,\"errors\":9," +
                     "\"missed_downloaded_chunks\":2,\"timeout_errors\":4,\"other_errors\":3,\"max_bandwidth\":40,\"min_bandwidth\":0,\"banned\":0,\"unbanned\":0," +
                     "\"avg_ping_time\":0,\"min_ping_time\":0,\"max_ping_time\":0,\"is_banned\":false,\"is_active\":true,\"active_peers\":0,\"peers\":0," +
                     "\"late_uploaded_chunks\":0,\"late_uploaded_bytes\":0,\"late_downloaded_bytes\":0}}",
                p.cdnBalancerInfo.getCdnPingInfo(false).toString());
    }

    @Test
    public void testGetCdnBalancerVersion() {
        String exampleVersion = "1.0.0-android";
        balancerStats = new BalancerStats(null, null, null, 0L, exampleVersion);

        p.cdnBalancerInfo.setBalancerStats(balancerStats);
        assertEquals(exampleVersion, p.cdnBalancerInfo.getVersion());
    }

    @Test
    public void testIp() {
        assertNull(p.getIp());
        when(mockOptions.getNetworkIP()).thenReturn("1.2.3.4");
        assertEquals("1.2.3.4", p.getIp());
    }

    @Test
    public void testIsp() {
        assertNull(p.getIsp());
        when(mockOptions.getNetworkIsp()).thenReturn("ISP");
        assertEquals("ISP", p.getIsp());
    }

    @Test
    public void testConnectionType() {
        assertNull(p.getConnectionType());
        when(mockOptions.getNetworkConnectionType()).thenReturn("DSL");
        assertEquals("DSL", p.getConnectionType());
    }

    @Test
    public void testAccountCode() {
        assertNull(p.getAccountCode());
        when(mockOptions.getAccountCode()).thenReturn("accountcode");
        assertEquals("accountcode", p.getAccountCode());
    }

    @Test
    public void testUsername() {
        assertNull(p.getUsername());
        when(mockOptions.getUsername()).thenReturn("username");
        assertEquals("username", p.getUsername());
    }

    @Test
    public void testUserProfileId() {
        assertNull(p.getUserProfileId());
        when(mockOptions.getUserProfileId()).thenReturn("profileId");
        assertEquals("profileId", p.getUserProfileId());
    }

    @Test
    public void testNodeHost() {
        assertNull(p.getNodeHost());
        when(mockResourceTransform.getNodeHost()).thenReturn("nodeHost");
        assertEquals("nodeHost", p.getNodeHost());
    }

    @Test
    public void testNodeType() {
        assertNull(p.getNodeType());
        when(mockResourceTransform.getNodeType()).thenReturn("type");
        assertEquals("type", p.getNodeType());
    }

    @Test
    public void testNodeTypeString() {
        assertNull(p.getNodeTypeString());
        when(mockResourceTransform.getNodeTypeString()).thenReturn("typeString");
        assertEquals("typeString", p.getNodeTypeString());
    }

    @Test
    public void testAppName() {
        assertNull(p.getAppName());
        when(mockOptions.getAppName()).thenReturn("appName");
        assertEquals("appName", p.getAppName());
    }

    @Test
    public void testAppReleaseVersion() {
        assertNull(p.getAppReleaseVersion());
        when(mockOptions.getAppReleaseVersion()).thenReturn("appReleaseVersion");
        assertEquals("appReleaseVersion", p.getAppReleaseVersion());
    }

    @Test
    public void testContentMetadata() {
        Bundle metadata = new Bundle();
        metadata.putString("metadata", "asdf");

        when(mockOptions.getContentMetadata()).thenReturn(metadata);
        assertEquals("{\"metadata\":\"asdf\"}", p.getContentMetadata());

        when(mockOptions.getContentMetadata()).thenReturn(null);
        assertNull(p.getContentMetadata());
    }

    @Test
    public void testAdMetadataAndCustomDimensions() {
        Bundle metadata = new Bundle();
        metadata.putString("metadata", "12345");

        String expectedResult = "{\"metadata\":\"12345\"}";

        // Ad metadata
        when(mockOptions.getAdMetadata()).thenReturn(metadata);
        assertEquals(expectedResult, p.getAdMetadata());

        when(mockOptions.getAdMetadata()).thenReturn(null);
        assertNull(p.getAdMetadata());

        // Custom dimensions
        when(mockOptions.getContentCustomDimensions()).thenReturn(metadata);
        assertEquals(expectedResult, p.getCustomDimensions());

        when(mockOptions.getContentCustomDimensions()).thenReturn(null);
        assertNull(p.getCustomDimensions());
    }

    @Test
    public void expectedPattern() {
        assertEquals("{}", p.getExpectedPattern());

        final ArrayList<Integer> adsExpected = new ArrayList<Integer>() {{
            add(1);
        }};

        final ArrayList<Integer> midsExpected = new ArrayList<Integer>() {{
            add(3);
            add(8);
        }};

        Map expectedPattern = new HashMap<String, ArrayList<Integer>>() {{
            put("pre", adsExpected);
            put("post", adsExpected);
            put("mid", midsExpected);
        }};

        when(mockAdAdapter.getExpectedPattern()).thenReturn(expectedPattern);
        assertEquals("{\"pre\":[1],\"post\":[1],\"mid\":[3,8]}", p.getExpectedPattern());

        Bundle expectedPatternB = new Bundle();
        expectedPatternB.putIntegerArrayList("pre", adsExpected);
        expectedPatternB.putIntegerArrayList("post", adsExpected);
        expectedPatternB.putIntegerArrayList("mid", midsExpected);

        when(mockOptions.getAdExpectedPattern()).thenReturn(expectedPatternB);
        assertEquals("{\"mid\":[3,8],\"pre\":[1],\"post\":[1]}", p.getExpectedPattern());
    }

    @Test
    public void breaksTime() {
        assertEquals("[]", p.getBreaksTime());

        ArrayList<Integer> list = new ArrayList<Integer>() {{
            add(0);
            add(15);
            add(-1);
        }};

        when(mockOptions.getAdBreaksTime()).thenReturn(null);
        when(mockAdAdapter.getBreaksTime()).thenReturn(list);
        assertEquals("[0,15,-1]", p.getBreaksTime());

        when(mockOptions.getAdBreaksTime()).thenReturn(list);
        assertEquals("[0,15,-1]", p.getBreaksTime());
    }

    @Test
    public void expectedBreaks() {
        assertEquals(new Integer(0), p.getExpectedBreaks());

        when(mockOptions.getAdExpectedBreaks()).thenReturn(null);
        when(mockAdAdapter.getExpectedPattern()).thenReturn(null);
        when(mockAdAdapter.getExpectedBreaks()).thenReturn(2);
        assertEquals(new Integer(2), p.getExpectedBreaks());

        final ArrayList<Integer> adsExpected = new ArrayList<Integer>() {{
            add(1);
        }};

        final ArrayList<Integer> midsExpected = new ArrayList<Integer>() {{
            add(3);
            add(8);
        }};

        Map expectedPattern = new HashMap<String, ArrayList<Integer>>() {{
            put("pre", adsExpected);
            put("post", adsExpected);
            put("mid", midsExpected);
        }};

        when(mockAdAdapter.getExpectedPattern()).thenReturn(expectedPattern);
        assertEquals(new Integer(4), p.getExpectedBreaks());

        Bundle expectedPatternB = new Bundle();
        expectedPatternB.putIntegerArrayList("pre", adsExpected);
        expectedPatternB.putIntegerArrayList("post", adsExpected);
        expectedPatternB.putIntegerArrayList("mid", midsExpected);

        when(mockOptions.getAdExpectedPattern()).thenReturn(expectedPatternB);
        assertEquals(new Integer(4), p.getExpectedBreaks());
    }

    @Test
    public void expectedAds() {
        assertEquals(new Integer(0), p.getExpectedAds());

        when(mockAdAdapter.getExpectedAds()).thenReturn(1);
        assertEquals(new Integer(1), p.getExpectedAds());

        final ArrayList<Integer> adsExpected = new ArrayList<Integer>() {{
            add(1);
        }};

        final ArrayList<Integer> midsExpected = new ArrayList<Integer>() {{
            add(3);
            add(8);
        }};

        Map expectedPattern = new HashMap<String, ArrayList<Integer>>() {{
            put("pre", adsExpected);
            put("post", adsExpected);
            put("mid", midsExpected);
        }};

        HashMap<String, String> params = new HashMap<String, String>() {{
            put("breakNumber", "3");
        }};

        when(mockRequestBuilder.getLastSent()).thenReturn(params);
        when(mockAdAdapter.getExpectedPattern()).thenReturn(expectedPattern);
        assertEquals(new Integer(8), p.getExpectedAds());

        Bundle expectedPatternB = new Bundle();
        expectedPatternB.putIntegerArrayList("pre", adsExpected);
        expectedPatternB.putIntegerArrayList("post", adsExpected);
        expectedPatternB.putIntegerArrayList("mid", midsExpected);

        HashMap<String, String> params2 = new HashMap<String, String>() {{
            put("breakNumber", "2");
        }};

        when(mockRequestBuilder.getLastSent()).thenReturn(params2);
        when(mockOptions.getAdExpectedPattern()).thenReturn(expectedPatternB);
        assertEquals(new Integer(3), p.getExpectedAds());
    }

    @Test
    public void testStreamingProtocol() {
        when(mockOptions.getContentStreamingProtocol()).thenReturn("HLS");
        assertEquals("HLS", p.getStreamingProtocol());

        when(mockOptions.getContentStreamingProtocol()).thenReturn(null);
        assertNull(p.getStreamingProtocol());
    }

    @Test
    public void testExperimentIds() {
        when(mockOptions.getExperimentIds()).thenReturn(new ArrayList<String>());
        assertEquals(new ArrayList<String>(), p.getExperimentIds());

        when(mockOptions.getExperimentIds()).thenReturn(null);
        assertNull(p.getExperimentIds());
    }

    @Test
    public void testParseLocationHeader() {
        when(mockOptions.isParseLocationHeader()).thenReturn(true);
        assertEquals(true, p.isParseLocationHeader());
    }

    @Test
    public void testLatency() {
        when(mockAdapter.getLatency()).thenReturn(2.0);
        assertEquals(new Double(0.0), p.getLatency());

        when(p.getIsLive()).thenReturn(true);
        assertEquals(new Double(2.0), p.getLatency());

        when(mockAdapter.getLatency()).thenReturn(null);
        assertEquals(new Double(0.0), p.getLatency());
    }

    @Test
    public void testPacketLoss() {
        when(mockAdapter.getPacketLoss()).thenReturn(2);
        assertEquals(new Integer(2), p.getPacketLoss());

        when(mockAdapter.getPacketLoss()).thenReturn(null);
        assertEquals(new Integer(0), p.getPacketLoss());
    }

    @Test
    public void testPacketSent() {
        when(mockAdapter.getPacketSent()).thenReturn(3);
        assertEquals(new Integer(3), p.getPacketSent());

        when(mockAdapter.getPacketSent()).thenReturn(null);
        assertEquals(new Integer(0), p.getPacketSent());
    }

    @Test
    public void testSessionMetrics() {
        Bundle sessionMetrics = new Bundle();
        sessionMetrics.putString("a", "1");

        when(mockOptions.getSessionMetrics()).thenReturn(sessionMetrics);
        assertEquals("{\"a\":\"1\"}", p.getSessionMetrics());

        when(mockOptions.getSessionMetrics()).thenReturn(null);
        assertNull(p.getContentMetadata());
    }

    @Test
    public void testEDID() {
        assertNull(p.getDeviceEDID());

        when(mockOptions.getDeviceEDID()).thenReturn("00ffffffffffff004c2d2a07333230370e14010380331d782a81f1a357539f270a5054bfef8081009500b3008140714f8180a940950f023a801871382d40582c4500fe1f1100001e000000fd00384b1e5111000a202020202020000000fc00534d584c3233373048440a2020000000ff004831414b3530303030300a202000a1");
        assertEquals(
                "00ffffffffffff004c2d2a07333230370e14010380331d782a81f1a357539f270a5054bfef8081009500b3008140714f8180a940950f023a801871382d40582c4500fe1f1100001e000000fd00384b1e5111000a202020202020000000fc00534d584c3233373048440a2020000000ff004831414b3530303030300a202000a1",
                p.getDeviceEDID()
        );
    }

    @Test
    public void testVideoMetrics() {
        Bundle contentMetrics = new Bundle();
        contentMetrics.putString("b", "2");

        Map<String, String> videoMetrics = new HashMap<>();
        videoMetrics.put("c", "3");

        when(mockOptions.getContentMetrics()).thenReturn(contentMetrics);
        assertEquals("{\"b\":{\"value\":\"2\"}}", p.getVideoMetrics());

        when(mockOptions.getContentMetrics()).thenReturn(null);
        when(mockAdapter.getMetrics()).thenReturn(videoMetrics);
        assertEquals("{\"c\":\"3\"}", p.getVideoMetrics());

        when(mockOptions.getContentMetrics()).thenReturn(null);
        when(mockAdapter.getMetrics()).thenReturn(null);
        assertNull(p.getContentMetadata());
    }

    @Test
    public void testAdCampaign() {
        when(mockOptions.getAdCampaign()).thenReturn("adCampaign");
        assertEquals("adCampaign", p.getAdCampaign());

        when(mockOptions.getAdCampaign()).thenReturn(null);
        assertNull(p.getAdCampaign());
    }

    @Test
    public void adCreativeId() {
        when(mockOptions.getAdCreativeId()).thenReturn("adCreativeId");
        assertEquals("adCreativeId", p.getAdCreativeId());

        when(mockOptions.getAdCreativeId()).thenReturn(null);
        assertNull(p.getAdCreativeId());
    }

    @Test
    public void adProvider() {
        when(mockOptions.getAdProvider()).thenReturn("adProvider");
        assertEquals("adProvider", p.getAdProvider());

        when(mockOptions.getAdProvider()).thenReturn(null);
        assertNull(p.getAdProvider());
    }

    @Test
    public void testObfuscateIp() {
        when(mockOptions.getUserObfuscateIp()).thenReturn(true);
        assertEquals("true", p.getObfuscateIp());
    }

    @Test
    public void testUserAnonymousId() {
        when(mockOptions.getUserAnonymousId()).thenReturn("asdfsfsf");
        assertEquals("asdfsfsf", p.getUserAnonymousId());

        when(mockOptions.getUserAnonymousId()).thenReturn(null);
        assertNull(p.getUserAnonymousId());
    }

    @Test
    public void testUserType() {
        when(mockOptions.getUserType()).thenReturn("type");
        assertEquals("type", p.getUserType());

        when(mockOptions.getUserType()).thenReturn(null);
        assertNull(p.getUserType());
    }

    @Test
    public void testPrivacyProtocol() {
        when(mockOptions.getUserPrivacyProtocol()).thenReturn("oPtIn");
        assertEquals("optin", p.getUserPrivacyProtocol());

        when(mockOptions.getUserPrivacyProtocol()).thenReturn("optout");
        assertEquals("optout", p.getUserPrivacyProtocol());

        when(mockOptions.getUserPrivacyProtocol()).thenReturn("asd");
        assertNull(p.getUserPrivacyProtocol());

        when(mockOptions.getUserPrivacyProtocol()).thenReturn(null);
        assertNull(p.getUserPrivacyProtocol());
    }

    @Test
    public void testDeviceInfoString() {
        when(mockOptions.getDeviceBrand()).thenReturn("Huawei");
        when(mockOptions.getDeviceModel()).thenReturn("Nexus 6P");
        when(mockOptions.getDeviceType()).thenReturn("Smartphone");
        when(mockOptions.getDeviceCode()).thenReturn("1");
        when(mockOptions.getDeviceOsName()).thenReturn("Android");
        when(mockOptions.getDeviceOsVersion()).thenReturn("8.1");
        assertEquals("{\"model\":\"Nexus 6P\",\"osVersion\":\"8.1\"," +
                "\"brand\":\"Huawei\",\"deviceType\":\"Smartphone\",\"deviceCode\":\"1\"," +
                "\"osName\":\"Android\",\"browserName\":\"\",\"browserVersion\":\"\"," +
                "\"browserType\":\"\",\"browserEngine\":\"\"}", p.getDeviceInfoString());
    }

    @Test
    public void testHouseholdId() {
        when(mockAdapter.getHouseholdId()).thenReturn("household");
        assertEquals("household", p.getHouseholdId());

        when(mockAdapter.getHouseholdId()).thenReturn(null);
        assertNull(p.getHouseholdId());
    }

    @Test
    public void testSmartSwitchConfigCode() {
        when(mockOptions.getSmartSwitchConfigCode()).thenReturn("sscc");
        assertEquals("sscc", p.getSmartSwitchConfigCode());

        when(mockOptions.getSmartSwitchConfigCode()).thenReturn(null);
        assertNull(p.getSmartSwitchConfigCode());
    }

    @Test
    public void testSmartSwitchGroupCode() {
        when(mockOptions.getSmartSwitchGroupCode()).thenReturn("ssgc");
        assertEquals("ssgc", p.getSmartSwitchGroupCode());

        when(mockOptions.getSmartSwitchGroupCode()).thenReturn(null);
        assertNull(p.getSmartSwitchGroupCode());
    }

    @Test
    public void testSmartSwitchContractCode() {
        when(mockOptions.getSmartSwitchContractCode()).thenReturn("sscontractc");
        assertEquals("sscontractc", p.getSmartSwitchContractCode());

        when(mockOptions.getSmartSwitchContractCode()).thenReturn(null);
        assertNull(p.getSmartSwitchContractCode());
    }

    @Test
    public void testUserEmail() {
        when(mockOptions.getUserEmail()).thenReturn("a@a");
        assertEquals("a@a", p.getUserEmail());

        when(mockOptions.getUserEmail()).thenReturn(null);
        assertNull(p.getUserEmail());
    }

    @Test
    public void testContentPackage() {
        when(mockOptions.getContentPackage()).thenReturn("contentPackage");
        assertEquals("contentPackage", p.getContentPackage());

        when(mockOptions.getContentPackage()).thenReturn(null);
        assertNull(p.getContentPackage());
    }

    @Test
    public void testContentSaga() {
        when(mockOptions.getContentSaga()).thenReturn("contentSaga");
        assertEquals("contentSaga", p.getContentSaga());

        when(mockOptions.getContentSaga()).thenReturn(null);
        assertNull(p.getContentSaga());
    }

    @Test
    public void testContentTvShow() {
        when(mockOptions.getContentTvShow()).thenReturn("contentTvShow");
        assertEquals("contentTvShow", p.getContentTvShow());

        when(mockOptions.getContentTvShow()).thenReturn(null);
        assertNull(p.getContentTvShow());
    }

    @Test
    public void testContentSeason() {
        when(mockOptions.getContentSeason()).thenReturn("contentSeason");
        assertEquals("contentSeason", p.getContentSeason());

        when(mockOptions.getContentSeason()).thenReturn(null);
        assertNull(p.getContentSeason());
    }

    @Test
    public void testContentEpisodeTitle() {
        when(mockOptions.getContentEpisodeTitle()).thenReturn("contentEpisodeTitle");
        assertEquals("contentEpisodeTitle", p.getContentEpisodeTitle());

        when(mockOptions.getContentEpisodeTitle()).thenReturn(null);
        assertNull(p.getContentEpisodeTitle());
    }

    @Test
    public void testContentChannel() {
        when(mockOptions.getContentChannel()).thenReturn("contentChannel");
        assertEquals("contentChannel", p.getContentChannel());

        when(mockOptions.getContentChannel()).thenReturn(null);
        assertNull(p.getContentChannel());
    }

    @Test
    public void testContentId() {
        when(mockOptions.getContentId()).thenReturn("contentId");
        assertEquals("contentId", p.getContentId());

        when(mockOptions.getContentId()).thenReturn(null);
        assertNull(p.getContentId());
    }

    @Test
    public void testContentImbdbId() {
        when(mockOptions.getContentImdbId()).thenReturn("contentImdbId");
        assertEquals("contentImdbId", p.getContentImdbId());

        when(mockOptions.getContentImdbId()).thenReturn(null);
        assertNull(p.getContentImdbId());
    }

    @Test
    public void testContentGracenoteId() {
        when(mockOptions.getContentGracenoteId()).thenReturn("contentGracenoteId");
        assertEquals("contentGracenoteId", p.getContentGracenoteId());

        when(mockOptions.getContentGracenoteId()).thenReturn(null);
        assertNull(p.getContentGracenoteId());
    }

    @Test
    public void testContentType() {
        when(mockOptions.getContentType()).thenReturn("contentType");
        assertEquals("contentType", p.getContentType());

        when(mockOptions.getContentType()).thenReturn(null);
        assertNull(p.getContentType());
    }

    @Test
    public void testContentGenre() {
        when(mockOptions.getContentGenre()).thenReturn("contentGenre");
        assertEquals("contentGenre", p.getContentGenre());

        when(mockOptions.getContentGenre()).thenReturn(null);
        assertNull(p.getContentGenre());
    }

    @Test
    public void testContentLanguage() {
        when(mockOptions.getContentLanguage()).thenReturn("contentLanguage");
        assertEquals("contentLanguage", p.getContentLanguage());

        when(mockOptions.getContentLanguage()).thenReturn(null);
        assertNull(p.getContentLanguage());
    }

    @Test
    public void testContentSubtitles() {
        when(mockOptions.getContentSubtitles()).thenReturn("contentSubtitles");
        assertEquals("contentSubtitles", p.getContentSubtitles());

        when(mockOptions.getContentSubtitles()).thenReturn(null);
        assertNull(p.getContentSubtitles());
    }

    @Test
    public void testContentContractedResolution() {
        when(mockOptions.getContentContractedResolution())
                .thenReturn("contentContractedResolution");
        assertEquals("contentContractedResolution", p.getContentContractedResolution());

        when(mockOptions.getContentContractedResolution()).thenReturn(null);
        assertNull(p.getContentContractedResolution());
    }

    @Test
    public void testContentCost() {
        when(mockOptions.getContentCost()).thenReturn("contentCost");
        assertEquals("contentCost", p.getContentCost());

        when(mockOptions.getContentCost()).thenReturn(null);
        assertNull(p.getContentCost());
    }

    @Test
    public void testContentPrice() {
        when(mockOptions.getContentPrice()).thenReturn("contentPrice");
        assertEquals("contentPrice", p.getContentPrice());

        when(mockOptions.getContentPrice()).thenReturn(null);
        assertNull(p.getContentPrice());
    }

    @Test
    public void testContentPlaybackType() {
        when(mockOptions.getContentPlaybackType()).thenReturn("playbackType");
        when(mockOptions.getContentIsLive()).thenReturn(true);
        assertEquals("playbackType", p.getContentPlaybackType());

        when(mockOptions.getContentPlaybackType()).thenReturn(null);
        when(mockOptions.getContentIsLive()).thenReturn(true);
        assertEquals("Live", p.getContentPlaybackType());

        when(mockOptions.getContentIsLive()).thenReturn(false);
        assertEquals("VoD", p.getContentPlaybackType());

        when(mockOptions.getContentIsLive()).thenReturn(null);
        when(mockAdapter.getIsLive()).thenReturn(null);
        TestCase.assertNull(p.getContentPlaybackType());

        when(mockOptions.isOffline()).thenReturn(true);
        assertEquals("Offline", p.getContentPlaybackType());
    }

    @Test
    public void testContentDrm() {
        when(mockOptions.getContentDrm()).thenReturn("contentDrm");
        assertEquals("contentDrm", p.getContentDrm());

        when(mockOptions.getContentDrm()).thenReturn(null);
        assertNull(p.getContentDrm());
    }

    @Test
    public void testContentEncodingVideoCodec() {
        when(mockOptions.getContentEncodingVideoCodec()).thenReturn("contentEncodingVideoCodec");
        when(mockAdapter.getVideoCodec()).thenReturn("videoCodec");
        assertEquals("contentEncodingVideoCodec", p.getContentEncodingVideoCodec());

        when(mockOptions.getContentEncodingVideoCodec()).thenReturn(null);
        assertEquals("videoCodec", p.getContentEncodingVideoCodec());

        when(mockAdapter.getVideoCodec()).thenReturn(null);
        assertNull(p.getContentEncodingVideoCodec());
    }

    @Test
    public void testContentEncodingAudioCodec() {
        when(mockOptions.getContentEncodingAudioCodec()).thenReturn("contentEncodingAudioCodec");
        when(mockAdapter.getAudioCodec()).thenReturn("audioCodec");
        assertEquals("contentEncodingAudioCodec", p.getContentEncodingAudioCodec());

        when(mockOptions.getContentEncodingAudioCodec()).thenReturn(null);
        assertEquals("audioCodec", p.getContentEncodingAudioCodec());

        when(mockAdapter.getAudioCodec()).thenReturn(null);
        assertNull(p.getContentEncodingAudioCodec());
    }

    @Test
    public void testContentEncodingCodecSettings() {
        Bundle codecSettings = new Bundle();
        codecSettings.putString("setting", "on");

        when(mockOptions.getContentEncodingCodecSettings())
                .thenReturn(codecSettings);
        assertEquals("{\"setting\":\"on\"}", p.getContentEncodingCodecSettings());

        when(mockOptions.getContentEncodingCodecSettings()).thenReturn(null);
        assertNull(p.getContentEncodingCodecSettings());
    }

    @Test
    public void testContentEncodingCodecProfile() {
        when(mockOptions.getContentEncodingCodecProfile())
                .thenReturn("contentEncodingCodecProfile");
        assertEquals("contentEncodingCodecProfile", p.getContentEncodingCodecProfile());

        when(mockOptions.getContentEncodingCodecProfile()).thenReturn(null);
        assertNull(p.getContentEncodingCodecProfile());
    }

    @Test
    public void testContentEncodingContainerFormat() {
        when(mockOptions.getContentEncodingContainerFormat())
                .thenReturn("contentEncodingContainerFormat");
        assertEquals("contentEncodingContainerFormat",
                p.getContentEncodingContainerFormat());

        when(mockOptions.getContentEncodingContainerFormat()).thenReturn(null);
        assertNull(p.getContentEncodingContainerFormat());
    }

    @Test
    public void testChronoTimes() {
        // Init and preload chronos, don't depend on the adapter
        when(mockChrono.getDeltaTime(anyBoolean())).thenReturn(100L);
        assertEquals(100, p.getInitDuration());

        when(mockChrono.getDeltaTime(anyBoolean())).thenReturn(200L);
        assertEquals(200, p.getPreloadDuration());

        // Adapter chronos
        PlaybackChronos c = new PlaybackChronos();
        c.setBuffer(mock(Chrono.class));
        c.setJoin(mock(Chrono.class));
        c.setSeek(mock(Chrono.class));
        c.setPause(mock(Chrono.class));
        c.setTotal(mock(Chrono.class));

        when(c.getBuffer().getDeltaTime(anyBoolean())).thenReturn(100L);
        when(c.getJoin().getDeltaTime(anyBoolean())).thenReturn(200L);
        when(c.getSeek().getDeltaTime(anyBoolean())).thenReturn(300L);
        when(c.getPause().getDeltaTime(anyBoolean())).thenReturn(400L);
        when(c.getTotal().getDeltaTime(anyBoolean())).thenReturn(500L);

        when(mockAdapter.getChronos()).thenReturn(c);

        assertEquals(100, p.getBufferDuration());
        assertEquals(200, p.getJoinDuration());
        assertEquals(300, p.getSeekDuration());
        assertEquals(400, p.getPauseDuration());

        // Change values to test the ads
        when(c.getBuffer().getDeltaTime(anyBoolean())).thenReturn(1000L);
        when(c.getJoin().getDeltaTime(anyBoolean())).thenReturn(2000L);
        when(c.getSeek().getDeltaTime(anyBoolean())).thenReturn(3000L);
        when(c.getPause().getDeltaTime(anyBoolean())).thenReturn(4000L);
        when(c.getTotal().getDeltaTime(anyBoolean())).thenReturn(5000L);

        when(mockAdAdapter.getChronos()).thenReturn(c);

        assertEquals(1000, p.getAdBufferDuration());
        assertEquals(2000, p.getAdJoinDuration());
        assertEquals(4000, p.getAdPauseDuration());
        assertEquals(5000, p.getAdTotalDuration());

        // No adapters
        p.removeAdsAdapter();

        assertEquals(-1, p.getAdBufferDuration());
        assertEquals(-1, p.getAdJoinDuration());
        assertEquals(-1, p.getAdPauseDuration());
        assertEquals(-1, p.getAdTotalDuration());

        p.removeAdapter();

        assertEquals(-1, p.getBufferDuration());
        assertEquals(-1, p.getJoinDuration());
        assertEquals(-1, p.getSeekDuration());
        assertEquals(-1, p.getPauseDuration());
    }

    @Test
    public void testRequestBuilderInstance() {
        assertEquals(mockRequestBuilder, p.getRequestBuilder());
    }

    private Map<String, String> mockListeners() {
        // Make build params return the first argument
        when(mockRequestBuilder.buildParams(nullable(Map.class), anyString())).thenAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return invocation.getArguments()[0];
            }
        });

        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("key", "value");
        return paramsMap;
    }

    // Will send listeners

    @Test
    public void willSendInit() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        p.fireInit(paramsMap);
        p.removeOnWillSendInitListener(listener);
        p.fireInit(paramsMap);
        verify(listener).willSendRequest(eq(INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendInitWhenStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        adapterEventListener.onStart(paramsMap);
        p.removeOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendStart() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(p.getTitle()).thenReturn("title");
        when(p.getResource()).thenReturn("resource");
        when(p.getIsLive()).thenReturn(false);
        when(p.getDuration()).thenReturn(30d);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        p.removeOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willForceInit() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(p.getTitle()).thenReturn("title");
        when(p.getResource()).thenReturn("resource");
        when(p.getIsLive()).thenReturn(false);
        when(p.getDuration()).thenReturn(30d);
        when(p.getOptions().isForceInit()).thenReturn(true);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        adapterEventListener.onStart(paramsMap);
        p.removeOnWillSendInitListener(listener);
        adapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendStartWhenInitiated() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendStartListener(listener);
        p.fireInit(paramsMap);
        adapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendStartListener(listener);
        p.fireInit(paramsMap);
        adapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendJoin() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendJoinListener(listener);
        adapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendJoinListener(listener);
        adapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(JOIN), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendPause() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendPauseListener(listener);
        adapterEventListener.onPause(paramsMap);
        p.removeOnWillSendPauseListener(listener);
        adapterEventListener.onPause(paramsMap);
        verify(listener).willSendRequest(eq(PAUSE), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendResume() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendResumeListener(listener);
        adapterEventListener.onResume(paramsMap);
        p.removeOnWillSendResumeListener(listener);
        adapterEventListener.onResume(paramsMap);
        verify(listener).willSendRequest(eq(RESUME), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendBuffer() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendBufferListener(listener);
        adapterEventListener.onBufferEnd(paramsMap);
        p.removeOnWillSendBufferListener(listener);
        adapterEventListener.onBufferEnd(paramsMap);
        verify(listener).willSendRequest(eq(BUFFER), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendSeek() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendSeekListener(listener);
        adapterEventListener.onSeekEnd(paramsMap);
        p.removeOnWillSendSeekListener(listener);
        adapterEventListener.onSeekEnd(paramsMap);
        verify(listener).willSendRequest(eq(SEEK), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendError() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendErrorListener(listener);
        adapterEventListener.onError(paramsMap);
        p.removeOnWillSendErrorListener(listener);
        adapterEventListener.onError(paramsMap);
        verify(listener).willSendRequest(eq(ERROR), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendStop() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendStopListener(listener);
        adapterEventListener.onStop(paramsMap);
        p.removeOnWillSendStopListener(listener);
        adapterEventListener.onStop(paramsMap);
        verify(listener).willSendRequest(eq(STOP), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdInit() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdInitListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        p.removeOnWillSendAdInitListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        verify(listener).willSendRequest(eq(AD_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdInitWhenAdStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(mockAdAdapter.getPosition()).thenReturn(AdAdapter.AdPosition.MID);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendAdInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(AD_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    /*@Test
    public void willNotSendAdInitWhenAdStartedButPositionIsPost() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(mockAdAdapter.getPosition()).thenReturn(AdAdapter.AdPosition.POST);
        when(p.getAdPosition()).thenReturn("post");

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendAdInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(AD_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }*/

    @Test
    public void willSendAdStart() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(p.getAdDuration()).thenReturn(10d);
        when(p.getAdResource()).thenReturn("a");
        when(p.getAdTitle()).thenReturn("b");
        when(mockAdAdapter.getPosition()).thenReturn(AdAdapter.AdPosition.MID);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdStartListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendAdStartListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(AD_START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdStartWhenAdInitiated() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdStartListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        adAdapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendAdStartListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        adAdapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(AD_START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdJoin() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdJoinListener(listener);
        adAdapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendAdJoinListener(listener);
        adAdapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(AD_JOIN), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdPause() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdPauseListener(listener);
        adAdapterEventListener.onPause(paramsMap);
        p.removeOnWillSendAdPauseListener(listener);
        adAdapterEventListener.onPause(paramsMap);
        verify(listener).willSendRequest(eq(AD_PAUSE), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdResume() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdResumeListener(listener);
        adAdapterEventListener.onResume(paramsMap);
        p.removeOnWillSendAdResumeListener(listener);
        adAdapterEventListener.onResume(paramsMap);
        verify(listener).willSendRequest(eq(AD_RESUME), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdBuffer() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdBufferListener(listener);
        adAdapterEventListener.onBufferEnd(paramsMap);
        p.removeOnWillSendAdBufferListener(listener);
        adAdapterEventListener.onBufferEnd(paramsMap);
        verify(listener).willSendRequest(eq(AD_BUFFER), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdClick() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdClickListener(listener);
        adAdapterEventListener.onClick(paramsMap);
        p.removeOnWillSendAdClick(listener);
        adAdapterEventListener.onClick(paramsMap);
        verify(listener).willSendRequest(eq(AD_CLICK), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdStop() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdStopListener(listener);
        adAdapterEventListener.onStop(paramsMap);
        p.removeOnWillSendAdStopListener(listener);
        adAdapterEventListener.onStop(paramsMap);
        verify(listener).willSendRequest(eq(AD_STOP), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    /*@Test
    public void willSendInitWhenAdStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(INIT), eq(p), mapCaptor.capture());
    }*/

    @Test
    public void willNotSendInitWhenAdStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.fireInit();
        p.addOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener, times(0)).willSendRequest(
                eq(INIT), eq(p), mapCaptor.capture());

        listener = mock(Plugin.WillSendRequestListener.class);

        when(p.getTitle()).thenReturn("title");
        when(p.getResource()).thenReturn("resource");
        when(p.getIsLive()).thenReturn(false);
        when(p.getDuration()).thenReturn(30d);

        p.addOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener, times(0)).willSendRequest(
                eq(INIT), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendAdError() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdErrorListener(listener);
        adAdapterEventListener.onError(paramsMap);
        p.removeOnWillSendAdError(listener);
        adAdapterEventListener.onError(paramsMap);
        verify(listener).willSendRequest(eq(AD_ERROR), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendVideoEvent() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendVideoEventListener(listener);
        adapterEventListener.onVideoEvent(paramsMap);
        p.removeOnWillSendVideoEventListener(listener);
        adapterEventListener.onVideoEvent(paramsMap);
        verify(listener).willSendRequest(eq(VIDEO_EVENT), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendAdManifest() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.fireInit();
        p.addOnWillSendAdManifestListener(listener);
        adAdapterEventListener.onManifest(paramsMap);
        p.removeOnWillSendAdManifest(listener);
        adAdapterEventListener.onManifest(paramsMap);
        verify(listener).willSendRequest(eq(AD_MANIFEST), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendAdBreakStart() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdBreakStart(listener);
        adAdapterEventListener.onAdBreakStart(paramsMap);
        p.removeOnWillSendAdBreakStart(listener);
        adAdapterEventListener.onAdBreakStart(paramsMap);
        verify(listener).willSendRequest(eq(AD_BREAK_START), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendAdBreakStop() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdBreakStop(listener);
        adAdapterEventListener.onAdBreakStop(paramsMap);
        p.removeOnWillSendAdBreakStop(listener);
        adAdapterEventListener.onAdBreakStop(paramsMap);
        verify(listener).willSendRequest(eq(AD_BREAK_STOP), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendAdQuartile() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdQuartile(listener);
        paramsMap.put("quartile", "1");
        adAdapterEventListener.onQuartile(paramsMap);
        p.removeOnWillSendAdQuartile(listener);
        adAdapterEventListener.onQuartile(paramsMap);
        verify(listener).willSendRequest(eq(AD_QUARTILE), eq(p), mapCaptor.capture());
    }

    // Test Youbora methods
    @Test
    public void testInit() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);
        p.fireInit(null);
        verify(p).createRequest(nullable(String.class), eq(INIT));
    }

    @Test
    public void testStop() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit(null);
        verify(p).createRequest(nullable(String.class), eq(INIT));
        p.fireStop(null);
        verify(p).createRequest(nullable(String.class), eq(STOP));
    }

    @Test
    public void testStopNotSentTwice() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit(null);
        verify(p).createRequest(nullable(String.class), eq(INIT));

        p.fireStop();
        verify(p, times(1)).
                createRequest(nullable(String.class), eq(STOP));
        p.fireStop();
        verify(p, times(1)).
                createRequest(nullable(String.class), eq(STOP));
    }

    @Test
    public void testStopCalledWithoutInit() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireStop();
        verify(p, times(0)).
                createRequest(anyString(), eq(STOP));
    }

    @Test
    public void testStopWhenAdapterNotNull() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.setAdapter(new PlayerAdapter("a"));
        PlayerAdapter.ContentAdapterEventListener mockListener =
                mock(PlayerAdapter.ContentAdapterEventListener.class);
        p.getAdapter().addEventListener(mockListener);
        p.getAdapter().fireStart();
        p.fireStop();

        verify(mockListener, times(1)).onStop(new HashMap<String, String>());
    }

    @Test
    public void testError() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit();
        verify(p).createRequest(nullable(String.class), eq(INIT));
        p.fireError(null);
        verify(p).createRequest(nullable(String.class), eq(ERROR));
    }

    @Test
    public void testFatalError() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit();
        verify(p).createRequest(nullable(String.class), eq(INIT));
        p.fireFatalError(null, null, null, null);
        verify(p).createRequest(nullable(String.class), eq(ERROR));
        verify(p).createRequest(nullable(String.class), eq(STOP));
    }

    @Test
    public void testSendOfflineEvents() throws InterruptedException {
        // If offline enabled don't send offlineEvents
        Plugin p = spy(new TestPlugin(null));
        p.setApplicationContext(RuntimeEnvironment.systemContext);
        when(mockOptions.isEnabled()).thenReturn(true);
        when(mockOptions.isOffline()).thenReturn(true);
        p.fireOfflineEvents();
        verify(p,times(0)).createRequest(anyString(), eq(OFFLINE_EVENTS));

        // If adapters are set don't send offline events
        PlayerAdapter mockAdapter = mock(PlayerAdapter.class);
        when(mockOptions.isOffline()).thenReturn(false);
        p.setAdapter(mockAdapter);
        p.fireOfflineEvents();
        verify(p,times(0)).createRequest(anyString(), eq(OFFLINE_EVENTS));

        CountDownLatch latchTicks = new CountDownLatch(1);

        // Offline not enabled and no adapter and no events
        p = spy(new TestPlugin(null));
        p.setApplicationContext(RuntimeEnvironment.systemContext);
        when(mockOptions.isOffline()).thenReturn(false);
        p.fireOfflineEvents();

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        verify(p,times(0)).createRequest(anyString(), eq(OFFLINE_EVENTS));
    }

    @Test
    public void testTransformsOfflineMode() {
        Plugin p = spy(new TestPlugin(null));
        Context mockContext = RuntimeEnvironment.application;
        when(mockOptions.isEnabled()).thenReturn(true);
        when(mockOptions.isOffline()).thenReturn(true);
        when(p.getApplicationContext()).thenReturn(mockContext);

        p.setApplicationContext(mockContext);
        p.fireInit();

        assertEquals(p.getApplicationContext(), mockContext);
    }

    @Test
    public void testDeviceUUID() {
        Plugin p = spy(new TestPlugin(null));
        String deviceId = "1";
        assertNotNull(p.getDeviceId());
        when(mockOptions.getDeviceId()).thenReturn(deviceId);
        assertEquals(deviceId, p.getDeviceId());
    }
}