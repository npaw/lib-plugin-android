package com.npaw.youbora.lib6.persistence.dao;

import androidx.test.core.app.ApplicationProvider;
import com.npaw.youbora.lib6.persistence.OfflineContract;
import com.npaw.youbora.lib6.persistence.entity.Event;
import com.npaw.youbora.lib6.persistence.helper.EventDbHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.List;

import static com.npaw.youbora.lib6.persistence.OfflineContract.OfflineEntry.COLUMN_NAME_UID;
import static com.npaw.youbora.lib6.persistence.helper.EventDbHelper.DATABASE_NAME;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class EventDAOTest {

    private EventDbHelper dbHelper;
    private DAO<Event> eventDAO;

    @Before
    public void before() {
        dbHelper = new EventDbHelper(RuntimeEnvironment.getApplication());
        eventDAO = new TestableEventDAO(dbHelper, ApplicationProvider.getApplicationContext().getDatabasePath(DATABASE_NAME));
    }

    @After
    public void closeDb() {
        dbHelper.close();
    }

    @Test
    public void testInsertEvent() {
        Long uid = eventDAO.insertNewElement(getMockEvent1());

        Event event = eventDAO.getElement(new String[]{COLUMN_NAME_UID}, new String[]{uid.toString()}, null, null,
                null, null);
        assertEquals(1, event.getUid());
    }

    @Test
    public void testInsertEvents() {
        Long uid = eventDAO.insertElements(new ArrayList<Event>(2){{
            add(getMockEvent1());
            add(getMockEvent2());
        }});

        List<Event> events = eventDAO.getElements(null, null, null, null,
                null, null);
        assertEquals(2, events.size());
        assertEquals(2, events.get(1).getUid());
    }

    @Test
    public void testGetEvent() {
        Long uid = eventDAO.insertNewElement(getMockEvent1());

        Event event = eventDAO.getElement(new String[]{COLUMN_NAME_UID}, new String[]{uid.toString()}, null, null,
                null, null);
        assertEquals(1, event.getUid());
    }

    @Test
    public void testGetEvents() {
        Long uid = eventDAO.insertElements(new ArrayList<Event>(2){{
            add(getMockEvent1());
            add(getMockEvent2());
        }});

        List<Event> events = eventDAO.getElements(null, null, null, null,
                null, null);
        assertEquals(2, events.size());
        assertEquals(2, events.get(1).getUid());
        assertEquals(1, events.get(0).getUid());
    }

    @Test
    public void testGetAllEvents() {
        Long uid = eventDAO.insertElements(new ArrayList<Event>(2){{
            add(getMockEvent1());
            add(getMockEvent2());
        }});

        List<Event> events = eventDAO.getAll();
        assertEquals(2, events.size());
        assertEquals(2, events.get(1).getUid());
        assertEquals(1, events.get(0).getUid());
    }

    @Test
    public void testDeleteElement() {
        Long uid = eventDAO.insertElements(new ArrayList<Event>(2){{
            add(getMockEvent1());
            add(getMockEvent2());
            add(new Event("dummy events", System.currentTimeMillis(), 0));
        }});

        Integer rows = eventDAO.deleteElement(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID + " LIKE ?",
                new String[]{String.valueOf(getMockEvent1().getOfflineId())});

        assertEquals(2, (int)rows);
    }

    @Test
    public void testDeleteAll() {
        Long uid = eventDAO.insertElements(new ArrayList<Event>(2){{
            add(getMockEvent1());
            add(getMockEvent2());
        }});

        Integer rows = eventDAO.deleteAll();
        assertEquals(2, (int)rows);

        List<Event> allEvents = eventDAO.getAll();

        assertEquals(0, allEvents.size());
    }

    @Test
    public void testGetElementNotMatching() {
        Event event = eventDAO.getElement(null, null, null, null, null, null);
        assertNull(event);

        Long uid1 = eventDAO.insertNewElement(getMockEvent1());
        eventDAO.insertNewElement(getMockEvent2());

        event = eventDAO.getElement(null, null, null, null, null, null);
        assertEquals((long) uid1, event.getUid());
    }

    @Test
    public void testNullSQLiteOpenHelper() {
        dbHelper = mock(EventDbHelper.class);
        when(dbHelper.getReadableDatabase()).thenReturn(null);
        when(dbHelper.getWritableDatabase()).thenReturn(null);
        eventDAO = new EventDAO(dbHelper);

        Long uid = eventDAO.insertElements(new ArrayList<Event>(2){{
            add(getMockEvent1());
            add(getMockEvent2());
        }});

        assertEquals(-1L, (long)uid);

        List<Event> events = eventDAO.getElements(null, null, null, null, null, null);

        assertNotNull(events);
        assertEquals(0, events.size());

        eventDAO.getAll();
        assertNotNull(events);
        assertEquals(0, events.size());

        Integer rows = eventDAO.deleteElement(null, null);

        assertEquals(0, (int)rows);

        rows = eventDAO.deleteAll();

        assertEquals(0, (int)rows);
    }


    private Event getMockEvent1() {
        return new Event("dummy events", System.currentTimeMillis(), 0);
    }

    private Event getMockEvent2() {
        return new Event("dummy events", System.currentTimeMillis(), 1);
    }
}
