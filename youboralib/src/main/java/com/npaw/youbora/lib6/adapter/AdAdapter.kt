package com.npaw.youbora.lib6.adapter

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.extensions.printMap
import com.npaw.youbora.lib6.flags.AdFlags
import com.npaw.youbora.lib6.flags.BaseFlags

import java.util.HashMap

open class AdAdapter<PlayerT>(player: PlayerT?) : BaseAdapter<PlayerT>(player) {

    /**
     * Enum defining the possible Youbora ad position values. This refers to the timeline position
     * of a particular ad in relation to the content.
     * @see getPosition
     */
    enum class AdPosition {
        PRE,
        MID,
        POST,
        UNKNOWN
    }

    enum class ManifestError {
        NO_RESPONSE,
        EMPTY_RESPONSE,
        WRONG_RESPONSE
    }

    object InsertionType {
        const val ClientSide = "csai"
        const val ServerSide = "ssai"
    }

    override fun initializeFlags(): BaseFlags { return AdFlags() }

    fun getAdFlags(): AdFlags { return flags as AdFlags }

    /**
     * Override to return current ad position.
     * @see AdPosition
     */
    open fun getPosition(): AdPosition { return AdPosition.UNKNOWN }

    /**
     * Override to return how many breaks shown for the active view have been provided by the
     * ad server.
     */
    open fun getGivenBreaks(): Int? { return null }

    /**
     * Override to return the ad structure requested. The sum of pre, mid, and post breaks.
     */
    open fun getExpectedBreaks(): Int? { return null }

    /**
     * Override to return how many ads are requested for each break.
     * Keys that should be used are: "pre", "mid" and "post".
     */
    open fun getExpectedPattern(): MutableMap<String, MutableList<Int>>? { return null }

    /**
     * Override to return an ad breaks begin time list of playheads.
     */
    open fun getBreaksTime(): MutableList<*>? { return null }

    /**
     * Override to return the number of ads given for the break.
     */
    open fun getGivenAds(): Int? { return null }

    /**
     * Override to return the number of ads requested for the break.
     */
    open fun getExpectedAds(): Int? { return null }

    /** Override to return the ad insertion type. InsertionType.ClientSide or
     * InsertionType.ServerSide.
     */
    open fun getAdInsertionType(): String? { return null }

    open fun getAdProvider(): String? { return null }
    open fun getAdCampaign(): String? { return null }
    open fun getAdCreativeId(): String? { return null }
    open fun getIsFullscreen(): Boolean { return true }
    open fun getIsAdSkippable(): Boolean? { return null }
    open fun getIsAudioEnabled(): Boolean? { return null }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params
     */
    @JvmOverloads
    open fun fireAdInit(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireAdInit isAdInitiated:${getAdFlags().isAdInitiated} flags.isStarted: ${flags.isStarted} params: ${params.printMap()}" )
        if (!getAdFlags().isAdInitiated) {
            getAdFlags().isAdInitiated = true

            chronos.adInit.start()
            chronos.join.start()
            chronos.total.start()
        }

        eventListeners.iterator().forEach { if (it is AdAdapterEventListener) it.onAdInit(params) }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params to add to the request. If it is null default values will be added.
     */
    @JvmOverloads
    open fun fireClick(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireClick params: ${params.printMap()}" )
        eventListeners.iterator().forEach { if (it is AdAdapterEventListener) it.onClick(params) }
    }

    /**
     * Shortcut for [fireClick] accepting adUrl.
     * @param adUrl
     */
    open fun fireClick(adUrl: String?) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireClick adUrl: $adUrl" )
        fireClick(HashMap<String, String>().apply {
            adUrl?.let { put("adUrl", it) }
        })
    }

    /**
     * Shortcut for [fireStop].
     */
    open fun fireSkip() {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireSkip" )
        fireStop(HashMap<String, String>().apply { put("skipped", "true") })
    }

    /**
     * Emits related event.
     * @param quartile
     */
    open fun fireQuartile(quartile: Int) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireQuartile: $quartile" )
        if (flags.isJoined && quartile >= 1 && quartile <= 3) {
            eventListeners.iterator().forEach {
                if (it is AdAdapterEventListener) {
                    it.onQuartile(HashMap<String, String>().apply {
                        put("quartile", quartile.toString())
                    })
                }
            }
        }
    }

    /**
     * Emits related event.
     * @param params key value map with values
     */
    @JvmOverloads
    open fun fireManifest(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireManifest params: ${params.printMap()}" )
        eventListeners.iterator().forEach {
            if (it is AdAdapterEventListener) it.onManifest(params)
        }
    }

    /**
     * Shortcut for [fireManifest] accepting errorType and errorMessage.
     * @param errorType constant containing the error type
     * @param errorMessage error description
     */
    @JvmOverloads
    open fun fireManifest(errorType: ManifestError, errorMessage: String? = null) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireManifest errorType: $errorType errorMessage: $errorMessage" )
        fireManifest(HashMap<String, String>().apply {
            put("errorType", errorType.toString())
            errorMessage?.let { put("errorMessage", it) }
        })
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params key value map with values
     */
    @JvmOverloads
    open fun fireAdBreakStart(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireAdBreakStart isAdBreakStarted: ${getAdFlags().isAdBreakStarted} params: ${params.printMap()}" )
        if (!getAdFlags().isAdBreakStarted) {
            getAdFlags().isAdBreakStarted = true

            eventListeners.iterator().forEach {
                if (it is AdAdapterEventListener) it.onAdBreakStart(params)
            }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params key value map with values
     */
    @JvmOverloads
    open fun fireAdBreakStop(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[AdAdapter:$adapterId] fireAdBreakStart fireAdBreakStop: ${getAdFlags().isAdBreakStarted} params: ${params.printMap()}" )
        if (getAdFlags().isAdBreakStarted) {
            getAdFlags().isAdBreakStarted = false

            eventListeners.iterator().forEach {
                if (it is AdAdapterEventListener) it.onAdBreakStop(params)
            }
        }
    }

    interface AdAdapterEventListener : AdapterEventListener {
        /**
         * Adapter an ad click
         * @param params to add to the request
         */
        fun onClick(params: MutableMap<String, String>)

        /**
         * Adapter an ad init
         * @param params to add to the request
         */
        fun onAdInit(params: MutableMap<String, String>)

        /**
         * Adapter detected a quartile.
         * @param params to add to the request
         */
        fun onQuartile(params: MutableMap<String, String>)

        /**
         * Adapter detected the manifest.
         * @param params to add to the request
         */
        fun onManifest(params: MutableMap<String, String>)

        /**
         * Adapter detected the ad break start.
         * @param params to add to the request
         */
        fun onAdBreakStart(params: MutableMap<String, String>)

        /**
         * Adapter detected the ad break stop.
         * @param params to add to the request
         */
        fun onAdBreakStop(params: MutableMap<String, String>)
    }
}