package com.npaw.youbora.lib6.constants

object FastDataConfigFields {
    const val FASTDATA_CONFIG_HOST = "host"
    const val FASTDATA_CONFIG_CODE = "code"
    const val FASTDATA_CONFIG_PINGTIME = "pingTime"
    const val FASTDATA_CONFIG_BEATTIME = "beatTime"
    const val FASTDATA_CONFIG_EXPIRATIONTIME = "expirationTime"
}