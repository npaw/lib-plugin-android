package com.npaw.youbora.lib6.comm.transform;

import static com.npaw.youbora.lib6.constants.Services.CDN_PING;
import static com.npaw.youbora.lib6.constants.Services.DATA;
import static com.npaw.youbora.lib6.constants.Services.ERROR;
import static com.npaw.youbora.lib6.constants.Services.INIT;
import static com.npaw.youbora.lib6.constants.Services.OFFLINE_EVENTS;
import static com.npaw.youbora.lib6.constants.Services.PING;
import static com.npaw.youbora.lib6.constants.Services.SESSION_START;
import static com.npaw.youbora.lib6.constants.Services.START;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.constants.RequestParams;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Manages Fastdata service interaction and view codes generation.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class ViewTransform extends Transform {

    private static final int DEFAULT_PING_TIME       = 5;
    private static final int DEFAULT_BEAT_TIME       = 30;
    private static final int DEFAULT_EXPIRATION_TIME = 300;

    private Request request;
    private Map<String, String> params;
    private int viewIndex;
    private Plugin plugin;
    private String viewCode = null;

    /**
     * Instance of {@link FastDataConfig}
     */
    public FastDataConfig fastDataConfig;

    public ViewTransform(Plugin plugin) {
        super();
        initViewTransform(plugin);
    }

    private void initViewTransform(Plugin plugin) {
        this.plugin = plugin;

        fastDataConfig = new FastDataConfig();
        viewIndex = -1;

        String service = DATA;
        params = new HashMap<>();
        params.put("apiVersion", "v6,v7");
        params.put("outputformat", "json");
        params = plugin.getRequestBuilder().buildParams(params, service);
        if (params != null) {
            if ("nicetest".equals(params.get(RequestParams.SYSTEM))) {
                // "nicetest" is the default accountCode.
                // If found here, it's very likely that the customer has forgotten to set it.
                YouboraLog.error("No accountCode has been set. Please set your accountCode in plugin's options.");
            }

            // Prepare request but don't send it yet
            request = createRequest(plugin.getHost(), service);

            Map<String, Object> paramsObjectMap = new HashMap<>(params);
            request.setParams(paramsObjectMap);
        }
    }

    /**
     * Starts the 'FastData' fetching. This will send the initial request to YOUBORA in order to get
     * the needed info for the rest of the requests.
     *
     * This is an asynchronous process.
     *
     * When the fetch is complete, {@link #fastDataConfig} will contain the parsed info.
     * @see FastDataConfig
     */
    public void init() {
        init(null);
    }

    /**
     * Instead of doing the fetch itself it uses the provided {@link FastDataConfig} object
     * for the sdk setup, skipping the process
     *
     * @see FastDataConfig
     */
    public void init(FastDataConfig fastDataConfig) {
        if (plugin != null && plugin.getOptions() != null && plugin.getOptions().isOffline()) {
            this.fastDataConfig.code = "OFFLINE_MODE";
            this.fastDataConfig.host = "OFFLINE_MODE";
            this.fastDataConfig.pingTime = 60;
            buildCode(true);
            done();
            YouboraLog.debug("Offline mode, skipping fastdata request...");
            plugin.fastDataReceived = false;
            return;
        }

        if (fastDataConfig != null && fastDataConfig.host != null && fastDataConfig.code != null) {
            if (fastDataConfig.pingTime == null || fastDataConfig.pingTime <= 0)
                fastDataConfig.pingTime = DEFAULT_PING_TIME;

            if (fastDataConfig.beatTime == null || fastDataConfig.beatTime <= 0)
                fastDataConfig.beatTime = DEFAULT_BEAT_TIME;

            if (fastDataConfig.expirationTime == null || fastDataConfig.expirationTime <= 0)
                fastDataConfig.expirationTime = DEFAULT_EXPIRATION_TIME;

            this.fastDataConfig = fastDataConfig;
            done();
        } else {
            requestData();
        }
    }

    private void requestData() {
        request.addOnSuccessListener(( connection, response, listenerParams, headers ) -> {

            if (response == null || response.length() == 0) {
                YouboraLog.error("FastData empty response");
                return;
            }

            try {
                JsonObject configJson = toJsonTree(response).getAsJsonObject();
                if (configJson.has("q")) {
                    JsonObject q = configJson.get("q").getAsJsonObject();

                    String host = q.has("h") ? q.get("h").getAsString() : "";
                    String code = q.has("c") ? q.get("c").getAsString() : "";
                    String pt = q.has("pt") ? q.get("pt").getAsString() : "";
                    String bt = "";
                    String exp = "";

                    if ( q.has("i") )
                    {
                        JsonObject i = q.get("i").getAsJsonObject();
                        if ( i.has("bt") )
                        {
                            bt = i.get("bt").getAsString();
                        }

                        if ( i.has("exp") )
                        {
                            exp = i.get("exp").getAsString();
                        }
                    }

                    if (host.length() > 0 && code.length() > 0 && pt.length() > 0) {

                        if (fastDataConfig == null) fastDataConfig = new FastDataConfig();

                        fastDataConfig.code = code;
                        Options options = plugin.getOptions();
                        fastDataConfig.host = YouboraUtil.addProtocol(host, options == null || options.isHttpSecure());
                        fastDataConfig.pingTime = Integer.parseInt(pt);
                        fastDataConfig.beatTime = bt.length() > 0 ? Integer.parseInt(bt) : DEFAULT_BEAT_TIME;
                        fastDataConfig.expirationTime = exp.length() > 0 ? Integer.parseInt(exp) : DEFAULT_EXPIRATION_TIME;

                        buildCode();

                        YouboraLog.notice(String.format("FastData '%s' is ready.", code));
                        done();
                    } else {
                        throw new Exception("Invalid host, code or ping time");
                    }
                } else {
                    throw new Exception("Invalid response format");
                }
            } catch (Exception e) {
                rejectRequest = true;
                YouboraLog.error("FastData response is wrong.");
                YouboraLog.error(e);
            }
        });

        request.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                YouboraLog.error("Fastdata request failed.");
            }

            @Override
            public void onRequestRemovedFromQueue() {
                rejectRequest = true;
            }
        });

        request.send();
    }

    /**
     * {@inheritDoc}
     *
     * ViewTransform will set the correct view code to each request and set the pingTime param
     * for the services that need to carry it.
     */
    @Override
    public void parse(Request request) {
        Map<String, Object> params = request.getParams();
        boolean isInfinitySession = request.getService().contains("session");

        if (request.getHost() == null || request.getHost().length() == 0)
            request.setHost(fastDataConfig.host);

        if ( request.getService().equals(CDN_PING) )
        {
            params.put("code", getViewCode());
            params.put(RequestParams.ACCOUNT_CODE, plugin.getOptions().getAccountCode());
        }

        if (!isInfinitySession && params.get("code") == null) {
            if (request.getService().equals(OFFLINE_EVENTS)) nextView();
            params.put("code", getViewCode());
        }

        if (params.get("sessionRoot") == null) {
            params.put("sessionRoot", fastDataConfig.code);
        }

        if (isInfinitySession && params.get("sessionId") == null) {
            params.put("sessionId", fastDataConfig.code);
        }

        if (plugin.getOptions().getAccountCode() != null)
            params.put(RequestParams.ACCOUNT_CODE, plugin.getOptions().getAccountCode());

        // Request-specific transforms
        switch (request.getService()) {
            case PING:
            case START:
                if (params.get("pingTime") == null) params.put("pingTime", fastDataConfig.pingTime);
                if (params.get("sessionParent") == null)
                    params.put("sessionParent", fastDataConfig.code);
                if (this.plugin.getInfinity() != null && this.plugin.getInfinity().getFlags().isStarted())
                    params.put("parentId", params.get("sessionRoot"));
                break;
            case OFFLINE_EVENTS:
                request.setBody(addCodeToEvents(request.getBody()));
                break;
            case SESSION_START:
                if (params.get("beatTime") == null) params.put("beatTime", fastDataConfig.beatTime);
                break;
            case ERROR:
            case INIT:
                if (this.plugin.getInfinity() != null && this.plugin.getInfinity().getFlags().isStarted())
                    params.put("parentId", params.get("sessionRoot"));
            default:
                break;
        }
    }

    private String addCodeToEvents(String body) {
        if (body != null) return body.replace("[VIEW_CODE]", getViewCode());
        return null;
    }

    JsonElement toJsonTree( String str ) throws JsonSyntaxException
    {
        return JsonParser.parseString(str);
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }

    /**
     * Increments the view counter and generates a new view code.
     * @return the new view code
     */
    public String nextView() {
        viewIndex++;
        buildCode();
        return getViewCode();
    }

    /**
     * Builds the view code. It has the following scheme:
     * [fast data response code]_[view count]
     *
     * The only thing that matters is that view codes are unique. For this reason we only ask the
     * backend only once for a code, and then append a view counter that is incremented with each
     * view.
     * @see {@link #nextView()}
     */
    private void buildCode() {
        buildCode(false);
    }

    /**
     * Builds the view code. It has the following scheme:
     * [fast data response code]_[view count]
     *
     * The only thing that matters is that view codes are unique. For this reason we only ask the
     * backend only once for a code, and then append a view counter that is incremented with each
     * view.
     * @param isOffline
     * @see {@link #nextView()}
     */
    private void buildCode(boolean isOffline) {
        String suffix = isOffline ? "" : getViewCodeTimeStamp();

        if (fastDataConfig.code != null && fastDataConfig.code.length() > 0)
            viewCode = fastDataConfig.code + "_" + suffix;
        else
            viewCode = null;
    }

    /**
     * Current view code
     * @return the current view code
     */
    public String getViewCode() { return viewCode; }

    public String getViewCodeTimeStamp() { return Long.toString(System.currentTimeMillis()); }

    /**
     * Container class that has all the info we need from YOUBORA's 'FastData' service.
     */
    public static class FastDataConfig {

        /**
         * The YOUBORA host where to send traces
         */
        public String host;

        /**
         * Unique view identifier.
         */
        public String code;

        /**
         * Ping time; how often should pings be reported. This is per-account configurable
         * although 99% of the time this is 5 seconds.
         */
        public Integer pingTime;

        /**
         * Beat time: how often should beats be reported. This is a per-account configurable
         * although 99% of the time this is 30 seconds.
         */
        public Integer beatTime;

        /**
         * The maximum time (in seconds) is going to last without expiring and sending session start again
         */
        public Integer expirationTime;

    }
}
