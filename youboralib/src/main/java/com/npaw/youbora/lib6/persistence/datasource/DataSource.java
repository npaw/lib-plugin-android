package com.npaw.youbora.lib6.persistence.datasource;

import java.util.List;

public interface DataSource<T> {

    void insertNewElement(T element, QuerySuccessListener<Long> successListener);

    void insertNewElement(T element, QuerySuccessListener<Long> successListener, QueryUnsuccessListener unsuccessListener);

    void insertElements(List<T> elements, QuerySuccessListener<Long> listener);

    void insertElements(List<T> elements, QuerySuccessListener<Long> listener, QueryUnsuccessListener unsuccessListener);

    void getElement(QuerySuccessListener<T> listener);

    void getElement(QuerySuccessListener<T> listener, QueryUnsuccessListener unsuccessListener);

    void getAll(QuerySuccessListener<List<T>> listener);

    void getAll(QuerySuccessListener<List<T>> listener, QueryUnsuccessListener unsuccessListener);

    void deleteElement(T element, QuerySuccessListener<Integer> listener);

    void deleteElement(T element, QuerySuccessListener<Integer> listener, QueryUnsuccessListener unsuccessListener);

    void deleteAll(QuerySuccessListener<Integer> listener);

    void deleteAll(QuerySuccessListener<Integer> listener, QueryUnsuccessListener unsuccessListener);

}
