package com.npaw.youbora.lib6.comm.transform.resourceparse

abstract class Parser(manifestAuth: MutableMap<String, String>? = null) {

    open var realResource: String? = null
    open var transportFormat: String? = null
    open var lastManifest: String? = null
    open var listeners: ArrayList<ParserTransformListener>? = null

    init {
        listeners = arrayListOf()
    }

    abstract fun parse(resource: String?, parentResource: String? = null,
                       lastManifest: String? = null)

    open fun done() {
        listeners?.forEach { it.onParserTransformDone(realResource) }
    }

    open fun shouldExecute(lastManifest: String?): Boolean {
        return true
    }

    /**
     * Add a listener that will be notified once the parsing finishes.
     * @param listener to add
     */
    open fun addParserTransformListener(listener: ParserTransformListener?) {
        listener?.let { listeners?.add(it) }
    }

    /**
     * Removes a listener.
     * @param listener to remove
     * @return Whether the listener has been removed or not.
     */
    open fun removeParserTransformListener(listener: ParserTransformListener?): Boolean {
        return listeners?.remove(listener) ?: false
    }

    /**
     * Callback interface that will be called when the [LocationHeaderParser] is done parsing the
     * resource.
     */
    interface ParserTransformListener {
        fun onParserTransformDone(parsedResource: String?)
    }

}