package com.npaw.youbora.lib6.plugin

import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.constants.RequestParams.ACCOUNT_CODE
import com.npaw.youbora.lib6.constants.RequestParams.ADS_BLOCKED
import com.npaw.youbora.lib6.constants.RequestParams.ADS_EXPECTED
import com.npaw.youbora.lib6.constants.RequestParams.AD_ADAPTER_VERSION
import com.npaw.youbora.lib6.constants.RequestParams.AD_BITRATE
import com.npaw.youbora.lib6.constants.RequestParams.AD_BUFFER_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.AD_CAMPAIGN
import com.npaw.youbora.lib6.constants.RequestParams.AD_CREATIVE_ID
import com.npaw.youbora.lib6.constants.RequestParams.AD_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.AD_INSERTION_TYPE
import com.npaw.youbora.lib6.constants.RequestParams.AD_JOIN_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.AD_PAUSE_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.AD_PLAYER_VERSION
import com.npaw.youbora.lib6.constants.RequestParams.AD_PLAYHEAD
import com.npaw.youbora.lib6.constants.RequestParams.AD_POSITION
import com.npaw.youbora.lib6.constants.RequestParams.AD_PROPERTIES
import com.npaw.youbora.lib6.constants.RequestParams.AD_PROVIDER
import com.npaw.youbora.lib6.constants.RequestParams.AD_RESOURCE
import com.npaw.youbora.lib6.constants.RequestParams.AD_TITLE
import com.npaw.youbora.lib6.constants.RequestParams.AD_TOTAL_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.AD_URL
import com.npaw.youbora.lib6.constants.RequestParams.AD_VIEWABILITY
import com.npaw.youbora.lib6.constants.RequestParams.AD_VIEWED_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.ANONYMOUS_USER
import com.npaw.youbora.lib6.constants.RequestParams.APP_NAME
import com.npaw.youbora.lib6.constants.RequestParams.APP_RELEASE_VERSION
import com.npaw.youbora.lib6.constants.RequestParams.AUDIO
import com.npaw.youbora.lib6.constants.RequestParams.AUDIO_CODEC
import com.npaw.youbora.lib6.constants.RequestParams.BITRATE
import com.npaw.youbora.lib6.constants.RequestParams.BREAKS_TIME
import com.npaw.youbora.lib6.constants.RequestParams.BUFFER_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.CDN
import com.npaw.youbora.lib6.constants.RequestParams.CDN_BALANCER_RESPONSE_UUID
import com.npaw.youbora.lib6.constants.RequestParams.CDN_BALANCER_VERSION
import com.npaw.youbora.lib6.constants.RequestParams.CDN_DOWNLOADED_TRAFFIC
import com.npaw.youbora.lib6.constants.RequestParams.CDN_PING_DETAILS
import com.npaw.youbora.lib6.constants.RequestParams.CDN_PROFILE_NAME
import com.npaw.youbora.lib6.constants.RequestParams.CHANNEL
import com.npaw.youbora.lib6.constants.RequestParams.CODEC_PROFILE
import com.npaw.youbora.lib6.constants.RequestParams.CODEC_SETTINGS
import com.npaw.youbora.lib6.constants.RequestParams.CONNECTION_TYPE
import com.npaw.youbora.lib6.constants.RequestParams.CONTAINER_FORMAT
import com.npaw.youbora.lib6.constants.RequestParams.CONTENT_ID
import com.npaw.youbora.lib6.constants.RequestParams.CONTENT_LANGUAGE
import com.npaw.youbora.lib6.constants.RequestParams.CONTENT_TYPE
import com.npaw.youbora.lib6.constants.RequestParams.CONTRACTED_RESOLUTION
import com.npaw.youbora.lib6.constants.RequestParams.COST
import com.npaw.youbora.lib6.constants.RequestParams.DEVICE_EDID
import com.npaw.youbora.lib6.constants.RequestParams.DEVICE_ID
import com.npaw.youbora.lib6.constants.RequestParams.DEVICE_INFO
import com.npaw.youbora.lib6.constants.RequestParams.DIMENSIONS
import com.npaw.youbora.lib6.constants.RequestParams.DRM
import com.npaw.youbora.lib6.constants.RequestParams.DROPPED_FRAMES
import com.npaw.youbora.lib6.constants.RequestParams.EMAIL
import com.npaw.youbora.lib6.constants.RequestParams.EXPECTED_ADS
import com.npaw.youbora.lib6.constants.RequestParams.EXPECTED_BREAKS
import com.npaw.youbora.lib6.constants.RequestParams.EXPECTED_PATTERN
import com.npaw.youbora.lib6.constants.RequestParams.EXPERIMENTS
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_1
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_10
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_2
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_3
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_4
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_5
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_6
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_7
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_8
import com.npaw.youbora.lib6.constants.RequestParams.EXTRAPARAM_9
import com.npaw.youbora.lib6.constants.RequestParams.FPS
import com.npaw.youbora.lib6.constants.RequestParams.FULLSCREEN
import com.npaw.youbora.lib6.constants.RequestParams.GENRE
import com.npaw.youbora.lib6.constants.RequestParams.GIVEN_ADS
import com.npaw.youbora.lib6.constants.RequestParams.GIVEN_BREAKS
import com.npaw.youbora.lib6.constants.RequestParams.GRACENOTE_ID
import com.npaw.youbora.lib6.constants.RequestParams.HOUSEHOLD_ID
import com.npaw.youbora.lib6.constants.RequestParams.IMDB_ID
import com.npaw.youbora.lib6.constants.RequestParams.IP
import com.npaw.youbora.lib6.constants.RequestParams.ISP
import com.npaw.youbora.lib6.constants.RequestParams.JOIN_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.LANGUAGE
import com.npaw.youbora.lib6.constants.RequestParams.LATENCY
import com.npaw.youbora.lib6.constants.RequestParams.LIB_VERSION
import com.npaw.youbora.lib6.constants.RequestParams.LIVE
import com.npaw.youbora.lib6.constants.RequestParams.MEDIA_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.MEDIA_RESOURCE
import com.npaw.youbora.lib6.constants.RequestParams.METRICS
import com.npaw.youbora.lib6.constants.RequestParams.NAV_CONTEXT
import com.npaw.youbora.lib6.constants.RequestParams.NODE_HOST
import com.npaw.youbora.lib6.constants.RequestParams.NODE_TYPE
import com.npaw.youbora.lib6.constants.RequestParams.NODE_TYPE_STRING
import com.npaw.youbora.lib6.constants.RequestParams.OBFUSCATE_IP
import com.npaw.youbora.lib6.constants.RequestParams.P2P_DOWNLOADED_TRAFFIC
import com.npaw.youbora.lib6.constants.RequestParams.PACKAGE
import com.npaw.youbora.lib6.constants.RequestParams.PACKET_LOSS
import com.npaw.youbora.lib6.constants.RequestParams.PACKET_SENT
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_1
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_10
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_11
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_12
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_13
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_14
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_15
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_16
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_17
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_18
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_19
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_2
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_20
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_3
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_4
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_5
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_6
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_7
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_8
import com.npaw.youbora.lib6.constants.RequestParams.PARAM_9
import com.npaw.youbora.lib6.constants.RequestParams.PARSED_RESOURCE
import com.npaw.youbora.lib6.constants.RequestParams.PAUSE_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.PLAYBACK_TYPE
import com.npaw.youbora.lib6.constants.RequestParams.PLAYER
import com.npaw.youbora.lib6.constants.RequestParams.PLAYER_VERSION
import com.npaw.youbora.lib6.constants.RequestParams.PLAYHEAD
import com.npaw.youbora.lib6.constants.RequestParams.PLAYRATE
import com.npaw.youbora.lib6.constants.RequestParams.PLUGIN_INFO
import com.npaw.youbora.lib6.constants.RequestParams.PLUGIN_VERSION
import com.npaw.youbora.lib6.constants.RequestParams.PRELOAD_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.PRICE
import com.npaw.youbora.lib6.constants.RequestParams.PRIVACY_PROTOCOL
import com.npaw.youbora.lib6.constants.RequestParams.PROGRAM
import com.npaw.youbora.lib6.constants.RequestParams.PROPERTIES
import com.npaw.youbora.lib6.constants.RequestParams.RENDITION
import com.npaw.youbora.lib6.constants.RequestParams.SAGA
import com.npaw.youbora.lib6.constants.RequestParams.SEASON
import com.npaw.youbora.lib6.constants.RequestParams.SEEK_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.SEGMENT_DURATION
import com.npaw.youbora.lib6.constants.RequestParams.SESSION_METRICS
import com.npaw.youbora.lib6.constants.RequestParams.SKIPPABLE
import com.npaw.youbora.lib6.constants.RequestParams.SMART_SWITCH_CONFIG_CODE
import com.npaw.youbora.lib6.constants.RequestParams.SMART_SWITCH_CONTRACT_CODE
import com.npaw.youbora.lib6.constants.RequestParams.SMART_SWITCH_GROUP_CODE
import com.npaw.youbora.lib6.constants.RequestParams.STREAMING_PROTOCOL
import com.npaw.youbora.lib6.constants.RequestParams.SUBTITLES
import com.npaw.youbora.lib6.constants.RequestParams.SYSTEM
import com.npaw.youbora.lib6.constants.RequestParams.THROUGHPUT
import com.npaw.youbora.lib6.constants.RequestParams.TIMEMARK
import com.npaw.youbora.lib6.constants.RequestParams.TITLE
import com.npaw.youbora.lib6.constants.RequestParams.TITLE_EPISODE
import com.npaw.youbora.lib6.constants.RequestParams.TOTAL_BYTES
import com.npaw.youbora.lib6.constants.RequestParams.TRANSACTION_CODE
import com.npaw.youbora.lib6.constants.RequestParams.TRANSPORT_FORMAT
import com.npaw.youbora.lib6.constants.RequestParams.TRIGGERED_EVENTS
import com.npaw.youbora.lib6.constants.RequestParams.TV_SHOW
import com.npaw.youbora.lib6.constants.RequestParams.UPLOADED_TRAFFIC
import com.npaw.youbora.lib6.constants.RequestParams.USERNAME
import com.npaw.youbora.lib6.constants.RequestParams.USER_TYPE
import com.npaw.youbora.lib6.constants.RequestParams.USER_PROFILE_ID
import com.npaw.youbora.lib6.constants.RequestParams.VIDEO_CODEC
import com.npaw.youbora.lib6.constants.Services.AD_BREAK_START
import com.npaw.youbora.lib6.constants.Services.AD_BREAK_STOP
import com.npaw.youbora.lib6.constants.Services.AD_BUFFER
import com.npaw.youbora.lib6.constants.Services.AD_CLICK
import com.npaw.youbora.lib6.constants.Services.AD_ERROR
import com.npaw.youbora.lib6.constants.Services.AD_INIT
import com.npaw.youbora.lib6.constants.Services.AD_JOIN
import com.npaw.youbora.lib6.constants.Services.AD_MANIFEST
import com.npaw.youbora.lib6.constants.Services.AD_PAUSE
import com.npaw.youbora.lib6.constants.Services.AD_QUARTILE
import com.npaw.youbora.lib6.constants.Services.AD_RESUME
import com.npaw.youbora.lib6.constants.Services.AD_START
import com.npaw.youbora.lib6.constants.Services.AD_STOP
import com.npaw.youbora.lib6.constants.Services.BUFFER
import com.npaw.youbora.lib6.constants.Services.CDN_PING
import com.npaw.youbora.lib6.constants.Services.CONFIGURATION
import com.npaw.youbora.lib6.constants.Services.DATA
import com.npaw.youbora.lib6.constants.Services.ERROR
import com.npaw.youbora.lib6.constants.Services.INIT
import com.npaw.youbora.lib6.constants.Services.JOIN
import com.npaw.youbora.lib6.constants.Services.PAUSE
import com.npaw.youbora.lib6.constants.Services.PING
import com.npaw.youbora.lib6.constants.Services.PLUGIN_LOGS
import com.npaw.youbora.lib6.constants.Services.RESUME
import com.npaw.youbora.lib6.constants.Services.SEEK
import com.npaw.youbora.lib6.constants.Services.SESSION_BEAT
import com.npaw.youbora.lib6.constants.Services.SESSION_EVENT
import com.npaw.youbora.lib6.constants.Services.SESSION_NAV
import com.npaw.youbora.lib6.constants.Services.SESSION_START
import com.npaw.youbora.lib6.constants.Services.SESSION_STOP
import com.npaw.youbora.lib6.constants.Services.START
import com.npaw.youbora.lib6.constants.Services.STOP
import com.npaw.youbora.lib6.constants.Services.VIDEO_EVENT
import com.npaw.youbora.lib6.plugin.Options.Companion.LINKED_VIEW_ID

/**
 * This class helps building params associated with each event: /start, /joinTime...
 * @author      Nice People at Work
 * @since       6.0
 */
open class RequestBuilder(private val plugin: Plugin) {

    /**
     * Map of last values sent in any request. This is used to find changes in those values
     * and report that some of them have changed. This map can be seen as the server state.
     */
    open val lastSent = HashMap<String, String>()

    companion object {
        /** Lists of params used by each service  */
        private val params: HashMap<String, Array<String>> by lazy {
            HashMap<String, Array<String>>().apply {
                val startParams = arrayOf(ACCOUNT_CODE, ADS_EXPECTED, ANONYMOUS_USER, APP_NAME,
                        APP_RELEASE_VERSION, AUDIO_CODEC, CDN, CHANNEL, CODEC_PROFILE,
                        CODEC_SETTINGS, CONNECTION_TYPE, CONTAINER_FORMAT, CONTENT_ID,
                        CONTENT_LANGUAGE, CONTENT_TYPE, COST, CONTRACTED_RESOLUTION, DEVICE_INFO,
                        DEVICE_ID, DIMENSIONS, DRM, EMAIL, EXPERIMENTS, GENRE, GRACENOTE_ID,
                        HOUSEHOLD_ID, IMDB_ID, IP, ISP, LIVE, SEGMENT_DURATION, MEDIA_DURATION, MEDIA_RESOURCE,
                        NAV_CONTEXT, NODE_HOST, NODE_TYPE, LINKED_VIEW_ID, OBFUSCATE_IP, PACKAGE,
                        PARAM_1, PARAM_2, PARAM_3, PARAM_4, PARAM_5, PARAM_6, PARAM_7, PARAM_8,
                        PARAM_9, PARAM_10, PARAM_11, PARAM_12, PARAM_13, PARAM_14, PARAM_15,
                        PARAM_16, PARAM_17, PARAM_18, PARAM_19, PARAM_20, PARSED_RESOURCE,
                        PLAYBACK_TYPE, PLAYER, PLAYER_VERSION, PLAYHEAD, PLUGIN_INFO,
                        PLUGIN_VERSION, LIB_VERSION, PRELOAD_DURATION, PRICE, PROGRAM, PROPERTIES, RENDITION,
                        SAGA, SEASON, SMART_SWITCH_CONFIG_CODE, SMART_SWITCH_CONTRACT_CODE,
                        SMART_SWITCH_GROUP_CODE, STREAMING_PROTOCOL, PRIVACY_PROTOCOL, SUBTITLES, TITLE,
                        TITLE_EPISODE, TRANSACTION_CODE, TRANSPORT_FORMAT, TV_SHOW, USERNAME,
                        USER_TYPE, USER_PROFILE_ID, VIDEO_CODEC, ADS_BLOCKED, DEVICE_EDID, CDN_BALANCER_RESPONSE_UUID, TRIGGERED_EVENTS)

                val adStartParams = arrayOf(AD_ADAPTER_VERSION, AD_CAMPAIGN,
                        AD_CREATIVE_ID, AD_DURATION, AD_PLAYER_VERSION, AD_POSITION, AD_PROPERTIES,
                        AD_PROVIDER, AD_RESOURCE, AD_TITLE, AUDIO, EXTRAPARAM_1, EXTRAPARAM_2,
                        EXTRAPARAM_3, EXTRAPARAM_4, EXTRAPARAM_5, EXTRAPARAM_6, EXTRAPARAM_7,
                        EXTRAPARAM_8, EXTRAPARAM_9, EXTRAPARAM_10, FULLSCREEN, PLAYHEAD, SKIPPABLE,
                        AD_INSERTION_TYPE, ADS_BLOCKED, TRIGGERED_EVENTS)

                put(DATA, arrayOf(PLUGIN_VERSION, SYSTEM, USERNAME, USER_PROFILE_ID))
                put(CONFIGURATION, arrayOf(PLUGIN_VERSION, SYSTEM, PLUGIN_VERSION, APP_NAME, APP_RELEASE_VERSION))
                put(INIT, startParams)
                put(START, startParams)
                put(ERROR, startParams)
                put(JOIN, arrayOf(JOIN_DURATION, PLAYHEAD, TRIGGERED_EVENTS))
                put(PAUSE, arrayOf(PLAYHEAD, TRIGGERED_EVENTS))
                put(RESUME, arrayOf(PAUSE_DURATION, PLAYHEAD, TRIGGERED_EVENTS))
                put(SEEK, arrayOf(PLAYHEAD, SEEK_DURATION, TRIGGERED_EVENTS))
                put(BUFFER, arrayOf(BUFFER_DURATION, PLAYHEAD, TRIGGERED_EVENTS))
                put(STOP, arrayOf(BITRATE, PAUSE_DURATION, PLAYHEAD, TOTAL_BYTES, METRICS, CDN_DOWNLOADED_TRAFFIC, P2P_DOWNLOADED_TRAFFIC, UPLOADED_TRAFFIC, TRIGGERED_EVENTS))
                put(AD_START, adStartParams)
                put(AD_INIT, adStartParams)
                put(AD_JOIN, arrayOf(AD_JOIN_DURATION, AD_PLAYHEAD, AD_POSITION, PLAYHEAD, TRIGGERED_EVENTS))
                put(AD_PAUSE, arrayOf(AD_PLAYHEAD, AD_POSITION, PLAYHEAD, TRIGGERED_EVENTS))
                put(AD_RESUME, arrayOf(AD_PLAYHEAD, AD_POSITION, AD_PAUSE_DURATION, PLAYHEAD, TRIGGERED_EVENTS))
                put(AD_BUFFER, arrayOf(AD_BUFFER_DURATION, AD_PLAYHEAD, AD_POSITION, PLAYHEAD, TRIGGERED_EVENTS))
                put(AD_STOP, arrayOf(AD_BITRATE, AD_PLAYHEAD, AD_POSITION, AD_TOTAL_DURATION,
                        PLAYHEAD, AD_VIEWABILITY, AD_VIEWED_DURATION, TRIGGERED_EVENTS))

                put(AD_CLICK, arrayOf(AD_PLAYHEAD, AD_POSITION, AD_URL, PLAYHEAD, TRIGGERED_EVENTS))
                put(AD_MANIFEST, arrayOf(BREAKS_TIME, EXPECTED_BREAKS, EXPECTED_PATTERN,
                        GIVEN_BREAKS, TRIGGERED_EVENTS))

                put(AD_BREAK_START, arrayOf(AD_POSITION, EXPECTED_ADS, GIVEN_ADS,
                        AD_INSERTION_TYPE, TRIGGERED_EVENTS))

                put(AD_BREAK_STOP, arrayOf(TRIGGERED_EVENTS))
                put(AD_QUARTILE, arrayOf(AD_POSITION, AD_VIEWABILITY, AD_VIEWED_DURATION, TRIGGERED_EVENTS))
                put(PING, arrayOf(DROPPED_FRAMES, LATENCY, METRICS, PACKET_LOSS, PACKET_SENT,
                        PLAYRATE, TOTAL_BYTES, CDN_DOWNLOADED_TRAFFIC, P2P_DOWNLOADED_TRAFFIC, UPLOADED_TRAFFIC, SEGMENT_DURATION))

                val adStartList = adStartParams.toMutableList()
                adStartList.add(AD_TOTAL_DURATION)
                adStartList.add(AD_PLAYHEAD)
                adStartList.add(PLAYER)

                put(AD_ERROR, adStartList.toTypedArray())

                // Infinity
                put(SESSION_START, arrayOf(ACCOUNT_CODE, APP_NAME, LIB_VERSION, APP_RELEASE_VERSION,
                    CONTENT_LANGUAGE, CONNECTION_TYPE, DEVICE_INFO, DEVICE_ID, DIMENSIONS, IP, ISP,
                    LANGUAGE, NAV_CONTEXT, OBFUSCATE_IP, PRIVACY_PROTOCOL, PARAM_1, PARAM_2, PARAM_3, PARAM_4,
                    PARAM_5, PARAM_6, PARAM_7, PARAM_8, PARAM_9, PARAM_10, PARAM_11, PARAM_12,
                    PARAM_13, PARAM_14, PARAM_15, PARAM_16, PARAM_17, PARAM_18, PARAM_19, PARAM_20,
                    PLUGIN_INFO, USERNAME, USER_TYPE, USER_PROFILE_ID, ADS_BLOCKED, DEVICE_EDID))

                put(SESSION_EVENT, arrayOf(ACCOUNT_CODE, NAV_CONTEXT))
                put(SESSION_NAV, arrayOf(NAV_CONTEXT, USERNAME, USER_PROFILE_ID))
                put(SESSION_BEAT, arrayOf(SESSION_METRICS))
                put(SESSION_STOP, arrayOf(SESSION_METRICS))
                put(VIDEO_EVENT, arrayOf(PLAYHEAD))
                put(PLUGIN_LOGS, arrayOf())
                put(CDN_PING, arrayOf(ACCOUNT_CODE, CDN_PROFILE_NAME, CDN_PING_DETAILS, CDN_BALANCER_VERSION))
            }
        }

        /** Array of entities that should be reported in pings if they change mid-view  */
        private val pingEntities: Array<String> by lazy {
            arrayOf(CDN, CONTENT_LANGUAGE, METRICS, PARAM_1, PARAM_2, PARAM_3, PARAM_4,
                    PARAM_5, PARAM_6, PARAM_7, PARAM_8, PARAM_9, PARAM_10, PARAM_11, PARAM_12,
                    PARAM_13, PARAM_14, PARAM_15, PARAM_16, PARAM_17, PARAM_18, PARAM_19, PARAM_20,
                    PROGRAM, RENDITION, SUBTITLES, TITLE, CONTENT_ID)
        }
    }

    /**
     * Adds to params all the entities specified in paramList, unless they are already set.
     *
     * @param params Map of params key:value.
     * @param paramList A list of params to fetch.
     * @param onlyDifferent If true, only fetches params that have changed since the last
     * @param readMode if true, it will not change any state
     * @return fetched params
     */
    open fun fetchParams(params: MutableMap<String?, String?>?, paramList: List<String>?,
                    onlyDifferent: Boolean, readMode: Boolean = false): MutableMap<String?, String?> {
        val p = params ?: HashMap()

        paramList?.let { parameterList ->
            parameterList.forEach { paramName ->
                if (p[paramName] == null) {
                    getParamValue(paramName)?.let { value ->
                        if (!onlyDifferent || value != lastSent[paramName]) {
                            p[paramName] = value
                            if(!readMode)
                                lastSent[paramName] = value
                        }
                    }
                }
            }
        }

        return p
    }

    /**
     * Adds to params all the entities specified in paramList, unless they are already set.
     *
     * @param params Map of params key:value.
     * @param paramList A list of params to fetch.
     * @param onlyDifferent If true, only fetches params that have changed since the last
     * @return fetched params
     */
    open fun fetchParams(params: MutableMap<String?, String?>?, paramList: List<String>?,
                         onlyDifferent: Boolean): MutableMap<String?, String?> {
        return fetchParams(params, paramList, onlyDifferent, readMode = false)
    }

     /**
     * Adds to params all the entities specified in paramList, unless they are already set.
     *
     * Convenience method to call {@link #fetchParams(Map, List, boolean)} with an array instead of
     * a list.
     *
     * @param params Map of params key:value.
     * @param paramList An array of params to fetch.
     * @param onlyDifferent If true, only fetches params that have changed since the last
     * @return fetched params
     */
    open fun fetchParams(params: MutableMap<String?, String?>?, paramList: Array<String>?,
                    onlyDifferent: Boolean): MutableMap<String?, String?> {
        return fetchParams(params, paramList?.toList(), onlyDifferent)
    }
    /**
     * Adds to params Map all the entities specified that belong to the given service.
     *
     * @param param Map of key:value entries
     * @param service The name of the service.
     * @param readMode if true, it will not change any state
     * @return Map with built params
     */
    open fun buildParams(param: MutableMap<String?, String?>?,
                         service: String?, readMode: Boolean = false): Map<String?, String?> {
        var p = param ?: HashMap()

        p = fetchParams(p, params[service]?.toList(), false, readMode)
        p[TIMEMARK] = System.currentTimeMillis().toString()

        return p
    }


    /**
     * Adds to params Map all the entities specified that belong to the given service.
     *
     * @param param Map of key:value entries
     * @param service The name of the service.
     * @return Map with built params
     */
    open fun buildParams(param: MutableMap<String?, String?>?,
                         service: String?): Map<String?, String?> {
        var p = param ?: HashMap()

        p = fetchParams(p, params[service], false)
        p[TIMEMARK] = System.currentTimeMillis().toString()

        return p
    }

    /**
     * Creates an adNumber if it does not exist and stores it in lastSent. If it already exists,
     * it is incremented by 1.
     *
     * @return newly created adNumber
     */
    fun getNewAdNumber(): String {
        val sAdNumber = lastSent["adNumber"]?.let { adNumber ->
            val position = lastSent[AD_POSITION]

            if (position != null && position == plugin.adPosition) {
                (adNumber.toInt() + 1).toString()
            } else {
                null
            }
        } ?: "1"

        lastSent["adNumber"] = sAdNumber
        return sAdNumber
    }

    /**
     * Creates an adNumberInBreak incrementing by 1.
     * @returns {number} adNumberInBreak
     */
    open fun getNewAdNumberInBreak(): String {
        val adNumberInBreaks = lastSent["adNumberInBreak"]?.let {
            (it.toInt() + 1).toString()
        } ?: "1"

        lastSent["adNumberInBreak"] = adNumberInBreaks
        return adNumberInBreaks
    }

    /**
     * Creates an adBreakNumber if it does not exist and stores it in lastSent.
     * If it already exists, it is incremented by 1.
     * Also resets adNumberInBreak
     * @return newly created breakNumber
     */
    fun getNewAdBreakNumber(): String {
        val adBreakNumber = lastSent["breakNumber"]?.let {
            (it.toInt() + 1).toString()
        } ?: "1"
        lastSent["adNumberInBreak"] = "0"
        lastSent["breakNumber"] = adBreakNumber
        return adBreakNumber
    }

    /**
     * @return changed entities since last check
     */
    fun getChangedEntities(): Map<String?, String?> {
        return fetchParams(null, pingEntities, true)
    }

    /**
     * Get the actual value for any param asking the Plugin for it.
     * @param param the param name to fetch
     * @return the param value, or null if not available
     */
    private fun getParamValue(param: String?): String? {
        var value: Any? = null

        when (param) {
            AD_ADAPTER_VERSION -> value = plugin.adAdapterVersion
            AD_BITRATE -> plugin.adBitrate?.let { value = it }
            AD_BUFFER_DURATION -> value = plugin.adBufferDuration
            AD_CAMPAIGN -> value = plugin.adCampaign
            AD_CREATIVE_ID -> value = plugin.adCreativeId
            AD_DURATION -> plugin.adDuration?.let { value = it }
            AD_INSERTION_TYPE -> value = plugin.adInsertionType
            AD_JOIN_DURATION -> value = plugin.adJoinDuration
            AD_PAUSE_DURATION -> value = plugin.adPauseDuration
            AD_PLAYER_VERSION -> value = plugin.adPlayerVersion
            AD_PLAYHEAD -> plugin.adPlayhead?.let { value = it }
            AD_POSITION -> value = plugin.adPosition
            AD_PROPERTIES -> value = plugin.adMetadata
            AD_PROVIDER -> value = plugin.adProvider
            AD_RESOURCE -> value = plugin.adResource
            AD_TITLE -> value = plugin.adTitle
            AD_TOTAL_DURATION -> value = plugin.adTotalDuration
            AD_VIEWABILITY -> value = plugin.adViewability
            AD_VIEWED_DURATION -> value = plugin.adViewedDuration
            ADS_EXPECTED -> value = plugin.adsExpected
            ADS_BLOCKED -> value = plugin.isAdBlockerDetected
            ANONYMOUS_USER -> value = plugin.userAnonymousId
            APP_NAME -> value = plugin.appName
            APP_RELEASE_VERSION -> value = plugin.appReleaseVersion
            AUDIO -> plugin.isAdAudioEnabled?.let { value = it }
            AUDIO_CODEC -> value = plugin.contentEncodingAudioCodec
            BITRATE -> value = plugin.bitrate
            BREAKS_TIME -> value = plugin.breaksTime
            BUFFER_DURATION -> value = plugin.bufferDuration
            CDN -> value = plugin.cdn
            CDN_DOWNLOADED_TRAFFIC -> value = plugin.cdnTraffic
            CHANNEL -> value = plugin.contentChannel
            CODEC_PROFILE -> value = plugin.contentEncodingCodecProfile
            CODEC_SETTINGS -> value = plugin.contentEncodingCodecSettings
            CONNECTION_TYPE -> value = plugin.connectionType
            CONTAINER_FORMAT -> value = plugin.contentEncodingContainerFormat
            CONTENT_ID -> value = plugin.contentId
            CONTENT_LANGUAGE -> value = plugin.contentLanguage
            CONTENT_TYPE -> value = plugin.contentType
            CONTRACTED_RESOLUTION -> value = plugin.contentContractedResolution
            COST -> value = plugin.contentCost
            DEVICE_INFO -> value = plugin.deviceInfoString
            DEVICE_ID -> value = plugin.deviceId
            DIMENSIONS -> value = plugin.customDimensions
            DRM -> value = plugin.contentDrm
            DROPPED_FRAMES -> value = plugin.droppedFrames
            EMAIL -> value = plugin.userEmail
            EXPECTED_ADS -> value = plugin.expectedAds
            EXPECTED_BREAKS -> value = plugin.expectedBreaks
            EXPECTED_PATTERN -> value = plugin.expectedPattern
            EXPERIMENTS -> {
                val experimentsArray = plugin.experimentIds

                if (experimentsArray.isNullOrEmpty()) {
                    var builder = StringBuilder("[")

                    experimentsArray.iterator().forEach { id ->
                        builder.append(String.format("\"%s\",", id))
                    }

                    builder = builder.deleteCharAt(builder.length - 1)
                    builder = builder.append("]")
                    value = builder
                }
            }

            FPS -> plugin.framesPerSecond?.let { value = it }
            FULLSCREEN -> plugin.isFullscreen?.let { value = it }
            GENRE -> value = plugin.contentGenre
            GIVEN_ADS -> value = plugin.givenAds
            GIVEN_BREAKS -> value = plugin.givenBreaks
            GRACENOTE_ID -> value = plugin.contentGracenoteId
            HOUSEHOLD_ID -> value = plugin.householdId
            IMDB_ID -> value = plugin.contentImdbId
            IP -> value = plugin.ip
            ISP -> value = plugin.isp
            JOIN_DURATION -> value = plugin.joinDuration
            LANGUAGE -> value = plugin.language
            LATENCY -> value = plugin.latency
            LINKED_VIEW_ID -> value = plugin.linkedViewId
            LIVE -> value = plugin.isLive
            SEGMENT_DURATION -> value = plugin.segmentDuration
            MEDIA_DURATION -> value = plugin.duration
            MEDIA_RESOURCE -> value = plugin.resource
            METRICS -> value = plugin.videoMetrics
            NAV_CONTEXT -> value = plugin.navContext
            NODE_HOST -> value = plugin.nodeHost
            NODE_TYPE -> value = plugin.nodeType
            NODE_TYPE_STRING -> value = plugin.nodeTypeString
            OBFUSCATE_IP -> value = plugin.obfuscateIp
            P2P_DOWNLOADED_TRAFFIC -> value = plugin.p2PTraffic
            PACKAGE -> value = plugin.contentPackage
            PACKET_LOSS -> value = plugin.packetLoss
            PACKET_SENT -> value = plugin.packetSent
            PARAM_1 -> value = plugin.contentCustomDimension1
            PARAM_2 -> value = plugin.contentCustomDimension2
            PARAM_3 -> value = plugin.contentCustomDimension3
            PARAM_4 -> value = plugin.contentCustomDimension4
            PARAM_5 -> value = plugin.contentCustomDimension5
            PARAM_6 -> value = plugin.contentCustomDimension6
            PARAM_7 -> value = plugin.contentCustomDimension7
            PARAM_8 -> value = plugin.contentCustomDimension8
            PARAM_9 -> value = plugin.contentCustomDimension9
            PARAM_10 -> value = plugin.contentCustomDimension10
            PARAM_11 -> value = plugin.contentCustomDimension11
            PARAM_12 -> value = plugin.contentCustomDimension12
            PARAM_13 -> value = plugin.contentCustomDimension13
            PARAM_14 -> value = plugin.contentCustomDimension14
            PARAM_15 -> value = plugin.contentCustomDimension15
            PARAM_16 -> value = plugin.contentCustomDimension16
            PARAM_17 -> value = plugin.contentCustomDimension17
            PARAM_18 -> value = plugin.contentCustomDimension18
            PARAM_19 -> value = plugin.contentCustomDimension19
            PARAM_20 -> value = plugin.contentCustomDimension20
            EXTRAPARAM_1 -> value = plugin.adCustomDimension1
            EXTRAPARAM_2 -> value = plugin.adCustomDimension2
            EXTRAPARAM_3 -> value = plugin.adCustomDimension3
            EXTRAPARAM_4 -> value = plugin.adCustomDimension4
            EXTRAPARAM_5 -> value = plugin.adCustomDimension5
            EXTRAPARAM_6 -> value = plugin.adCustomDimension6
            EXTRAPARAM_7 -> value = plugin.adCustomDimension7
            EXTRAPARAM_8 -> value = plugin.adCustomDimension8
            EXTRAPARAM_9 -> value = plugin.adCustomDimension9
            EXTRAPARAM_10 -> value = plugin.adCustomDimension10
            PARSED_RESOURCE -> value = plugin.parsedResource
            PAUSE_DURATION -> value = plugin.pauseDuration
            PLAYER -> value = plugin.playerName
            PLAYER_VERSION -> value = plugin.playerVersion
            PLAYBACK_TYPE -> value = plugin.contentPlaybackType
            PLAYHEAD -> value = plugin.playhead
            PLAYRATE -> value = plugin.playrate
            PLUGIN_INFO -> value = plugin.pluginInfo
            PLUGIN_VERSION -> value = plugin.pluginVersion
            LIB_VERSION -> value = plugin.libVersion
            PRELOAD_DURATION -> value = plugin.preloadDuration
            PRICE -> value = plugin.contentPrice
            PROGRAM -> value = plugin.program
            PROPERTIES -> value = plugin.contentMetadata
            RENDITION -> value = plugin.rendition
            SAGA -> value = plugin.contentSaga
            SEASON -> value = plugin.contentSeason
            SEEK_DURATION -> value = plugin.seekDuration
            SESSION_METRICS -> value = plugin.sessionMetrics
            SKIPPABLE -> plugin.isAdSkippable?.let { value = it }
            SMART_SWITCH_CONFIG_CODE -> value = plugin.smartSwitchConfigCode
            SMART_SWITCH_CONTRACT_CODE -> value = plugin.smartSwitchContractCode
            SMART_SWITCH_GROUP_CODE -> value = plugin.smartSwitchGroupCode
            STREAMING_PROTOCOL -> value = plugin.streamingProtocol
            PRIVACY_PROTOCOL -> value = plugin.userPrivacyProtocol
            SUBTITLES -> value = plugin.contentSubtitles
            SYSTEM, ACCOUNT_CODE -> value = plugin.accountCode
            THROUGHPUT -> value = plugin.throughput
            TITLE -> value = plugin.title
            TITLE_EPISODE -> value = plugin.contentEpisodeTitle
            TOTAL_BYTES -> value = plugin.totalBytes
            TRANSACTION_CODE -> value = plugin.transactionCode
            TRANSPORT_FORMAT -> value = plugin.transportFormat
            TV_SHOW -> value = plugin.contentTvShow
            UPLOADED_TRAFFIC -> value = plugin.uploadTraffic
            CDN_BALANCER_RESPONSE_UUID -> value = plugin.balancerResponseId
            CDN_PROFILE_NAME -> value = plugin.cdnBalancerProfileName
            CDN_PING_DETAILS -> value = plugin.cdnPingInfo
            CDN_BALANCER_VERSION -> value = plugin.cdnBalancerVersion
            USERNAME -> value = plugin.username
            USER_TYPE -> value = plugin.userType
            USER_PROFILE_ID -> value = plugin.userProfileId
            VIDEO_CODEC -> value = plugin.contentEncodingVideoCodec
            DEVICE_EDID -> value = plugin.deviceEDID
            TRIGGERED_EVENTS -> value = firstTriggeredEvents()
        }

        return value?.toString()
    }

    private fun firstTriggeredEvents(): String? {
       return YouboraUtil.getTriggeredEventTrace()
    }
}