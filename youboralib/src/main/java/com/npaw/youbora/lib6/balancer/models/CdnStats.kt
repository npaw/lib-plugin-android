package com.npaw.youbora.lib6.balancer.models

import com.google.gson.annotations.SerializedName

open class CdnStats(
    @SerializedName("name")
    val name: String,
    @SerializedName("provider")
    val provider: String,
    @SerializedName("downloaded_bytes")
    val downloadBytes: Long,
    @SerializedName("downloaded_chunks")
    val downloadChunks: Int,
    @SerializedName("time")
    val downloadTime: Long,
    @SerializedName("errors")
    val errors: Int,
    @SerializedName("max_bandwidth")
    val maxBandwidth: Long,
    @SerializedName("min_bandwidth")
    val minBandwidth: Long,
    @SerializedName("banned")
    val banned: Int,
    @SerializedName("unbanned")
    val unbanned: Int,
    @SerializedName("avg_ping_time")
    val avgPingTime: Long,
    @SerializedName("min_ping_time")
    val minPingTime: Long,
    @SerializedName("max_ping_time")
    val maxPingTime: Long,
    @SerializedName("is_banned")
    val isBanned: Boolean,
    @SerializedName("is_active")
    val isActive: Boolean
) {
    fun subtract(other: CdnStats?): CdnStats {
        if (other == null) {
            return this
        }

        val name = this.name
        val provider = this.provider
        val bytes = this.downloadBytes - other.downloadBytes
        val chunks = this.downloadChunks - other.downloadChunks
        val time = this.downloadTime - other.downloadTime
        val errors = this.errors - other.errors
        val maxBandwidth = this.maxBandwidth
        val minBandwidth = this.minBandwidth
        val banned = this.banned - other.banned
        val unbanned = this.unbanned - other.unbanned
        val avgPingTime = this.avgPingTime
        val minPingTime = this.minPingTime
        val maxPingTime = this.maxPingTime
        val isBanned = this.isBanned
        val isActive = this.isActive

        return CdnStats(name, provider, bytes, chunks, time, errors, maxBandwidth, minBandwidth, banned, unbanned, avgPingTime, minPingTime,
            maxPingTime, isBanned, isActive)
    }

    fun anyNonZero(): Boolean {
        return downloadBytes > 0 || downloadChunks > 0 || downloadTime > 0
    }
}