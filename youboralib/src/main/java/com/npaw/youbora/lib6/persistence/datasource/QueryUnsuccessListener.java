package com.npaw.youbora.lib6.persistence.datasource;

/**
 * Unsuccessful query listener.
 */
public interface QueryUnsuccessListener {
    /**
     * Callback invoked when a query from {@link com.npaw.youbora.lib6.persistence.dao.EventDAO} has not been successful.
     *
     * @param exception exception raised
     */
    void onQueryUnresolved(Exception exception);
}
