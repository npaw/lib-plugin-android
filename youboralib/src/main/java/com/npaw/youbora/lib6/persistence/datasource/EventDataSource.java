package com.npaw.youbora.lib6.persistence.datasource;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.npaw.youbora.lib6.persistence.OfflineContract;
import com.npaw.youbora.lib6.persistence.OfflineContract.OfflineEntry;
import static com.npaw.youbora.lib6.persistence.OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID;
import com.npaw.youbora.lib6.persistence.dao.DAO;
import com.npaw.youbora.lib6.persistence.dao.EventDAO;
import com.npaw.youbora.lib6.persistence.entity.Event;
import com.npaw.youbora.lib6.persistence.helper.EventDbHelper;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Enrique on 28/12/2017.
 */

public class EventDataSource implements DataSource<Event>
{
    private final SQLiteOpenHelper databaseInstance;
    private final DAO<Event>       eventDAO;
    private final Executor         executor;

    public EventDataSource(Context context) {
        this.databaseInstance = new EventDbHelper(context);
        this.eventDAO = new EventDAO(databaseInstance);
        this.executor = Executors.newFixedThreadPool(4);
    }

    public EventDataSource(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
        this.databaseInstance = eventDAO.getDatabaseInstance();
        this.executor = Executors.newFixedThreadPool(4);
    }

    @Override
    public void insertNewElement(Event event, QuerySuccessListener<Long> listener) {
        insertNewElement(event, listener, null);
    }

    @Override
    public void insertNewElement(Event element, QuerySuccessListener<Long> listener, QueryUnsuccessListener unsuccessListener) {
        if(databaseInstance != null) {
            final Event finalEvent = element;
            executor.execute(new QueryRunnable<>(() -> eventDAO.insertNewElement(finalEvent), listener));
        }
    }

    @Override
    public void insertElements(List<Event> elements, QuerySuccessListener<Long> listener) {
        insertElements(elements, listener, null);
    }

    @Override
    public void insertElements(List<Event> elements, QuerySuccessListener<Long> listener, QueryUnsuccessListener unsuccessListener) {
        for (int k = 0 ; k < elements.size() ; k++) {
            insertNewElement(elements.get(k), elements.size() - 1 == k ? listener : null);
        }
    }

    public void getLastId(QuerySuccessListener<Integer> listener) {
        if(databaseInstance != null) {
            executor.execute(new QueryRunnable<>(() -> {
                Event event = eventDAO.getElement(null, null, null, null, COLUMN_NAME_OFFLINE_ID + " DESC", "1");
                return event == null ? -1 : event.getOfflineId();
            }, listener));
        }
    }

    public void getByOfflineId(int offlineId, QuerySuccessListener<List<Event>> listener) {
        if(databaseInstance != null) {
            final int finalofflineId = offlineId;
            executor.execute(new QueryRunnable<>(() -> eventDAO.getElements(new String[]{ COLUMN_NAME_OFFLINE_ID }, new String[]{ String.valueOf(finalofflineId) }, null, null, null, null), listener));
        }
    }

    public void getFirstOfflineId(QuerySuccessListener<Integer> listener) {
        if(databaseInstance != null) {
            executor.execute(new QueryRunnable<>(() -> {
                    Event event = eventDAO.getElement(null, null, null, null, COLUMN_NAME_OFFLINE_ID + " ASC", "1");
                    return event == null ? -1 : event.getOfflineId();
            }, listener));
        }
    }

    @Override
    public void getElement(QuerySuccessListener<Event> listener) {
        getElement(listener, null);
    }

    @Override
    public void getElement(QuerySuccessListener<Event> listener, QueryUnsuccessListener unsuccessListener) {
        //Nothing for now
    }

    @Override
    public void getAll(QuerySuccessListener<List<Event>> listener) {
        getAll(listener, null);
    }

    @Override
    public void getAll(QuerySuccessListener<List<Event>> listener, QueryUnsuccessListener unsuccessListener) {
        if(databaseInstance != null) {
            executor.execute(new QueryRunnable<>(eventDAO::getAll, listener));
        }
    }

    @Override
    public void deleteElement(Event element, QuerySuccessListener<Integer> listener) {
        deleteElement(element, listener, null);
    }

    @Override
    public void deleteElement(Event element, QuerySuccessListener<Integer> listener, QueryUnsuccessListener unsuccessListener) {
        if(databaseInstance != null) {
            final Event finalEvent = element;
            executor.execute(new QueryRunnable<>(() -> eventDAO.deleteElement(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID + " LIKE ?", new String[]{String.valueOf(finalEvent.getOfflineId())}), listener));
        }
    }

    @Override
    public void deleteAll(QuerySuccessListener<Integer> listener) {
        deleteAll(listener, null);
    }

    @Override
    public void deleteAll(QuerySuccessListener<Integer> listener, QueryUnsuccessListener unsuccessListener) {
        if(databaseInstance != null) {
            executor.execute(new QueryRunnable<>(eventDAO::deleteAll, listener));
        }
    }

    public void close() {
        if(databaseInstance != null) {
            databaseInstance.close();
        }
    }

    private static class QueryRunnable<T> implements Runnable
    {
        private final Callable<T>  callable;
        private final QuerySuccessListener<T> listener;

        public QueryRunnable( Callable<T> callable, QuerySuccessListener<T> listener )
        {
            this.callable = callable;
            this.listener = listener;
        }

        @Override
        public void run()
        {
            try
            {
                T result = callable.call();
                listener.onQueryResolved(result);
            }
            catch ( Exception ignore )
            {
            }
        }
    }
}
