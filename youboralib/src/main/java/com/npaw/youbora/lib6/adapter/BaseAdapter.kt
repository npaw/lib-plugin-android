package com.npaw.youbora.lib6.adapter

import com.npaw.youbora.lib6.BuildConfig
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.extensions.concatTriggeredEventsAndJoinInParams
import com.npaw.youbora.lib6.extensions.printMap
import com.npaw.youbora.lib6.flags.BaseFlags
import com.npaw.youbora.lib6.plugin.Plugin
import java.util.*

open class BaseAdapter<PlayerT>(player: PlayerT?) {

    open var player: PlayerT? = player
        set(value) {
            field?.let { unregisterListeners() }
            field = value
            field?.let { registerListeners() }
        }

    open var monitor: PlayheadMonitor? = null
    open var flags = initializeFlags()
    open var chronos = PlaybackChronos()
    open var plugin: Plugin? = null

    internal val triggeredEventsSeekMap = mutableMapOf<String,String>()
    internal val triggeredEventsBufferMap = mutableMapOf<String,String>()

    internal val adapterId = UUID.randomUUID()

    // Listener list
    internal var eventListeners = ArrayList<AdapterEventListener>()

    init {
        YouboraLog.notice("Adapter " + getVersion() + " with lib " + BuildConfig.VERSION_NAME +
                " is ready. Unique adapterId: $adapterId")
    }

    internal open fun initializeFlags(): BaseFlags { return BaseFlags() }

    /**
     * Override to set event binders.
     * Call this method from the overridden constructor, or from the client code right after
     * creating the specific adapter instance.
     */
    open fun registerListeners() {}

    /**
     * Override to unset event binders.
     */
    open fun unregisterListeners() {}

    /**
     * Stops view session if applies and clears resources by calling [unregisterListeners].
     */
    open fun dispose() {
        monitor?.stop()
        fireStop()
        player = null
    }

    /**
     * Creates a [PlayheadMonitor] and configures it.
     * @param monitorBuffers whether to watch for buffers or not
     * @param monitorSeeks whether to watch for seeks or not
     * @param interval the interval at which to perform playhead checks.
     * [getPlayhead] will be called once every `interval` milliseconds.
     */
    open fun monitorPlayhead(monitorBuffers: Boolean, monitorSeeks: Boolean, interval: Int) {
        var type = 0

        if (monitorBuffers) type = type or PlayheadMonitor.TYPE_BUFFER
        if (monitorSeeks) type = type or PlayheadMonitor.TYPE_SEEK

        if (type != 0) monitor = createPlayheadMonitor(this, type, interval)
    }

    /*internal*/ open fun createPlayheadMonitor(adapter: BaseAdapter<*>?, type: Int, interval: Int):
            PlayheadMonitor? {
        return adapter?.let { PlayheadMonitor(adapter, type, interval) }
    }

    /**
     * Override to return current playhead of the video (position) in seconds.
     */
    open fun getPlayhead(): Double? { return null }

    /**
     * Override to return video duration of the media in seconds.
     */
    open fun getDuration(): Double? { return null }

    /**
     * Override to return current bitrate.
     * @return the current real (consumed) bitrate in bits per second.
     */
    open fun getBitrate(): Long? { return null }

    /**
     * Override to return rendition.
     * @see com.npaw.youbora.lib6.YouboraUtil.buildRenditionString
     * @return A string that represents the current rendition (quality level).
     */
    open fun getRendition(): String? { return null }

    /**
     * Override to return title of the media being played.
     */
    open fun getTitle(): String? { return null }

    /**
     * Override to return the currently playing resource (URL).
     */
    open fun getResource(): String? { return null }

    /**
     * Override to return player version.
     */
    open fun getPlayerVersion(): String? { return null }

    /**
     * Override to return the player's name.
     */
    open fun getPlayerName(): String? { return null }

    /**
     * Override to return adapter version.
     */
    open fun getVersion(): String { return BuildConfig.VERSION_NAME + "-generic" }

    // FLOW METHODS
    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     */
    @JvmOverloads
    open fun fireStart(params: MutableMap<String, String> = HashMap()) {
        val isPluginStarted = plugin?.isStarted ?: false
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireStart isPluginStarted:$isPluginStarted flags.isStarted: ${flags.isStarted} params: ${params.printMap()}" )
        if (!flags.isStarted || !isPluginStarted) {
            flags.isStarted = true

            if (this is AdAdapter && getAdFlags().isAdInitiated) {
                YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireStart_Ads isAdAdapter position: ${getPosition()}")
                if (getPosition() != AdAdapter.AdPosition.PRE) chronos.join.start()
                chronos.adInit.stop()
            } else {
                chronos.join.start()
                chronos.total.start()
            }

            eventListeners.iterator().forEach { it.onStart(params) }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     */
    @JvmOverloads
    open fun fireJoin(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireJoin flags.isStarted: ${flags.isStarted} flags.isJoined:${flags.isJoined} params: ${params.printMap()}" )
        if (flags.isStarted && !flags.isJoined) {
            monitor?.start()
            flags.isJoined = true
            chronos.join.stop()

            eventListeners.iterator().forEach { it.onJoin(params) }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     */
    @JvmOverloads
    open fun firePause(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId]firePause flags.isStarted: ${flags.isStarted} flags.isPaused:${flags.isPaused} params: ${params.printMap()}" )
        if (flags.isJoined && !flags.isPaused) {
            flags.isPaused = true
            chronos.pause.start()

            eventListeners.iterator().forEach { it.onPause(params) }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     */
    @JvmOverloads
    open fun fireResume(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireResume flags.isStarted: ${flags.isStarted} flags.isPaused:${flags.isPaused} params: ${params.printMap()}" )
        if (flags.isJoined && flags.isPaused) {
            flags.isPaused = false
            // If it can ignore pause small events, and pause duration is less than 50 millis, reset pause chronos
            try {
                if (this.plugin?.options?.ignorePauseSmallEvents == true && this.chronos.pause.getDeltaTime(false) <= 50) {
                    chronos.pause.reset()
                } else {
                    chronos.pause.stop()
                }
            } catch (e: java.lang.Exception) {
               chronos.pause.stop()
            }
            monitor?.skipNextTick()

            eventListeners.iterator().forEach { it.onResume(params) }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     * @param convertFromSeek whether to convert an existing seek into buffer or not
     */
    @JvmOverloads
    open fun fireBufferBegin(convertFromSeek: Boolean = false,
                             params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireBufferBegin flags.isJoined: ${flags.isJoined} flags.isBuffering:${flags.isBuffering} flags.isSeeking:${flags.isSeeking} convertFromSeek:${convertFromSeek} params: ${params.printMap()}" )
        if (flags.isJoined && !flags.isBuffering) {
            if (flags.isSeeking) {
                if (convertFromSeek) {
                    YouboraLog.notice("Converting current buffer to seek")

                    chronos.buffer = chronos.seek.copy()
                    chronos.seek.reset()

                    triggeredEventsBufferMap.putAll(triggeredEventsSeekMap)
                    triggeredEventsSeekMap.clear()

                    flags.isSeeking = false
                } else {
                    return
                }
            } else {
                chronos.buffer.start()
            }

            triggeredEventsBufferMap.concatTriggeredEventsAndJoinInParams(params)

            flags.isBuffering = true

            eventListeners.iterator().forEach { it.onBufferBegin(convertFromSeek, params) }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     */
    @JvmOverloads
    open fun fireBufferEnd(params: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireBufferEnd flags.isJoined: ${flags.isJoined} params: ${params.printMap()}" )
        if (flags.isJoined && flags.isBuffering) {

            flags.isBuffering = false
            chronos.buffer.stop()

            triggeredEventsBufferMap.concatTriggeredEventsAndJoinInParams(params)
            val params = triggeredEventsBufferMap.toMutableMap()

            eventListeners.iterator().forEach { it.onBufferEnd(params) }

            if (chronos.pause.getDeltaTime(false) > 0) {
                chronos.pause.resume();
            }
        }
        triggeredEventsBufferMap.clear()
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     */
    @JvmOverloads
    open fun fireStop(params: MutableMap<String, String> = HashMap()) {
        val isAdAdapter = this is AdAdapter
        val isAdInitiated = if(this is AdAdapter) getAdFlags().isAdInitiated else null
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireStop flags.isStarted: ${flags.isStarted} isAdAdapter: $isAdAdapter isAdInitiated:$isAdInitiated flags.isPaused: ${flags.isPaused} params: ${params.printMap()}" )
        if (flags.isStarted || (isAdAdapter && isAdInitiated == true)) {
            monitor?.stop()
            val wasPaused = flags.isPaused
            flags.reset()

            // We inform of the pauseDuration here to save it before the reset
            if (wasPaused) {
                params["pauseDuration"] = chronos.pause.getDeltaTime(false)
                        .toString()
            }

            chronos.total.stop()
            chronos.join.reset()
            chronos.pause.reset()
            chronos.buffer.reset()
            chronos.seek.reset()
            chronos.adInit.reset()

            eventListeners.iterator().forEach { it.onStop(params) }
        }
    }

    /**
     * Basic error handler. msg, code, errorMetadata and level params can be included in the params
     * argument.
     * @param params to add to the request. If it is null default values will be added.
     */
    open fun fireError(params: MutableMap<String, String>) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireError params: ${params.printMap()}" )
        eventListeners.iterator().forEach { it.onError(params) }
    }

    /**
     * Sends a non-fatal error (with `level = "error"`). By default calls [fireError]
     * @param msg Error message (should be unique for the code).
     * @param code Error code reported.
     * @param errorMetadata Extra error info, if available.
     * @param exception Exception type from player.
     */
    @JvmOverloads
    open fun fireError(code: String? = null, msg: String? = null, errorMetadata: String? = null,
                       exception: Exception? = null) {
        val codeIsNumber = code?.toIntOrNull()
        plugin?.options?.errorsToIgnore?.let { array ->
            array.forEach {
                if (code != null && (codeIsNumber != null && code == it || codeIsNumber == null && code.contains(it))) {
                    YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireError code: $code toIgnore: true" )
                    return
                }
            }
        }

        fireError(YouboraUtil.buildErrorParams(code, msg, errorMetadata))

        plugin?.options?.fatalErrors?.let { array ->
            array.forEach {
                if (code != null && (codeIsNumber != null && code == it || codeIsNumber == null && code.contains(it))) {
                    YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireError code: $code isFatal: true" )
                    fireStop()
                }
            }
        }
    }

    /**
     * Shortcut for [fireError] setting an entry in the map as `errorLevel = "fatal"`.
     * This method will also send a stop after the error.
     * @param params to add to the request. If it is null default values will be added.
     */
    open fun fireFatalError(params: MutableMap<String, String>) {
        params["errorLevel"] = "fatal"
        fireError(params)
        fireStop()
    }

    /**
     * Sends a fatal error (with `level = "fatal"`). By default calls [fireFatalError]
     * This method will also send a stop after the error.
     * @param msg Error message (should be unique for the code).
     * @param code Error code reported.
     * @param errorMetadata Extra error info, if available.
     * @param exception Exception type from player.
     */
    @JvmOverloads
    open fun fireFatalError(code: String? = null, msg: String? = null,
                            errorMetadata: String? = null, exception: Exception? = null) {
        val codeIsNumber = code?.toIntOrNull()

        plugin?.options?.errorsToIgnore?.let { array ->
            array.forEach {
                if (code != null && (codeIsNumber != null && code == it || codeIsNumber == null && code.contains(it))) {
                    YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireFatalError code: $code toIgnore: true" )
                    return
                }
            }
        }

        fireError(YouboraUtil.buildErrorParams(code, msg, errorMetadata))

        plugin?.options?.nonFatalErrors?.let { array ->
            array.forEach {
                if (code != null && (codeIsNumber != null && code == it || codeIsNumber == null && code.contains(it))){
                    YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[BaseAdapter:$adapterId] fireFatalError code: $code nonFatal: true" )
                    return
                }

            }
        }

        fireStop()
    }

    /**
     * Adds an event listener that will be invoked whenever a "fire*" method is called.
     * @param eventListener to be added
     */
    open fun addEventListener(eventListener: AdapterEventListener) {
        eventListeners.add(eventListener)
    }

    /**
     * Removes an event listener.
     * @param eventListener listener to be removed
     * @return Whether the listener has been removed or not.
     */
    open fun removeEventListener(eventListener: AdapterEventListener): Boolean {
        return eventListeners.remove(eventListener)
    }

    /**
     * Listener interface. The methods will be called whenever each corresponding event is fired.
     * These events are listened by the [Plugin] in order to send the corresponding request.
     */
    interface AdapterEventListener {
        /**
         * Adapter detected a start event.
         * @param params to add to the request
         */
        fun onStart(params: MutableMap<String, String>)

        /**
         * Adapter detected a join event.
         * @param params to add to the request
         */
        fun onJoin(params: MutableMap<String, String>)

        /**
         * Adapter detected a pause event.
         * @param params to add to the request
         */
        fun onPause(params: MutableMap<String, String>)

        /**
         * Adapter detected a resume event.
         * @param params to add to the request
         */
        fun onResume(params: MutableMap<String, String>)

        /**
         * Adapter detected a stop event.
         * @param params to add to the request
         */
        fun onStop(params: MutableMap<String, String>)

        /**
         * Adapter detected a buffer begin event.
         * * @param convertFromSeek whether the buffer has been converted from a seek or not
         * @param params to add to the request
         */
        fun onBufferBegin(convertFromSeek: Boolean, params: MutableMap<String, String>)

        /**
         * Adapter detected a buffer end event.
         * @param params to add to the request
         */
        fun onBufferEnd(params: MutableMap<String, String>)

        /**
         * Adapter detected an error event.
         * @param params to add to the request
         */
        fun onError(params: MutableMap<String, String>)
    }
}