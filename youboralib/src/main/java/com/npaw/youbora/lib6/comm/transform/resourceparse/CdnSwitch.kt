package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.comm.Request
import com.npaw.youbora.lib6.plugin.Plugin

import java.net.HttpURLConnection
import java.util.*

class CdnSwitch(val plugin: Plugin) {
    private val listeners = ArrayList<CdnTransformListener>()
    private val headerName = "X-CDN"
    private var timer: Timer? = null

    fun init() {
        var url: String?

        plugin.let { p ->
            p.adapter?.let { a ->
                url = a.getUrlToParse() ?: p.resource
                request(url)
            }
        }
    }

    fun request(url: String?) {
        val request = Request(url, null)

        request.addOnSuccessListener { _, _, _, headers ->
            plugin.options?.parseCdnTTL?.let { recallInXMillis(it) }

            var cdn: String? = null

            headers.iterator().forEach { line ->
                line?.key?.let {
                    val index = it.indexOf(headerName)

                    if (index > -1) {
                        cdn = line.value[0]
                    }
                }
            }

            done(cdn)
        }

        request.addOnErrorListener(object : Request.RequestErrorListener {
            override fun onRequestError(connection: HttpURLConnection?) {
                plugin.options?.parseCdnTTL?.let { recallInXMillis(it) }
                YouboraLog.debug("CDN switch detection request failed")
                error()
            }

            override fun onRequestRemovedFromQueue() { }
        })

        request.send()
    }

    private fun done(cdn: String?) {
        listeners.iterator().forEach { it.onCdnTransformDone(this, cdn) }
    }

    private fun error() {
        listeners.iterator().forEach { it.onCdnTransformError(this) }
    }

    /**
     * Callback interface to inform observers that the parsing is done.
     */
    interface CdnTransformListener {
        fun onCdnTransformDone(cdnSwitch: CdnSwitch?, cdn: String?)
        fun onCdnTransformError(cdnSwitch: CdnSwitch?)
    }

    fun addCdnTransformListener(listener: CdnTransformListener) {
        listeners.add(listener)
    }

    fun removeCdnTransformListener(listener: CdnTransformListener): Boolean {
        return listeners.remove(listener)
    }

    private fun recallInXMillis(seconds: Int) {
        val milliseconds = seconds.toLong() * 1000
        timer = Timer()
        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                init()
                timer?.cancel()
                timer?.purge()
            }
        }, milliseconds, milliseconds)
    }
}