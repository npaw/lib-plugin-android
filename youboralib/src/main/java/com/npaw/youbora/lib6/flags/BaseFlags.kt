package com.npaw.youbora.lib6.flags

open class BaseFlags : Flags() {

    open var isJoined = false
    open var isPaused = false
    open var isSeeking = false
    open var isBuffering = false

    override fun reset() {
        super.reset()
        isJoined = false
        isPaused = false
        isSeeking = false
        isBuffering = false
    }
}