package com.npaw.youbora.lib6.balancer.monitor

import android.content.Context
import com.google.gson.JsonObject
import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.YouboraUtil.Companion.readBalancerStatsFile
import com.npaw.youbora.lib6.balancer.models.BalancerStats
import com.npaw.youbora.lib6.balancer.models.CdnStats
import com.npaw.youbora.lib6.balancer.models.P2PStats

open class CdnBalancerInfo {

    var balancerStats: BalancerStats? = null

    private var lastCdnStats: MutableMap<String, CdnStats> = mutableMapOf()
    private var lastP2PStats: P2PStats? = null

    fun getProfileName(): String? {
        return balancerStats?.profileName
    }

    fun getVersion(): String? {
        return balancerStats?.version
    }

    fun getCdnPingInfo(save: Boolean): JsonObject {
        val obj = JsonObject()

        balancerStats?.let { bStats ->
            bStats.cdnStats?.cdns?.forEach { cdnStats ->
                val cdnStatsDiff = cdnStats.subtract(lastCdnStats[cdnStats.name])

                if (cdnStatsDiff.anyNonZero()) {
                    obj.add(cdnStatsDiff.name, YouboraUtil.toJsonTree(cdnStatsDiff))
                    if (save) {
                        lastCdnStats[cdnStats.name] = cdnStats
                    }
                }
            }

            bStats.p2pStats?.let { p2pStats ->
                val p2pStatsDiff = p2pStats.subtract(lastP2PStats)

                if (p2pStatsDiff.anyNonZero()) {
                    obj.add(p2pStatsDiff.name, YouboraUtil.toJsonTree(p2pStatsDiff))
                    if (save) {
                        lastP2PStats = p2pStats
                    }
                }
            }
            return obj
        }

        return obj
    }

    fun getSegmentDuration(): Long? {
        return balancerStats?.segmentDuration
    }

    fun getBalancerResponseId(): String? {
        return balancerStats?.cdnStats?.responseUUID
    }

    fun getCdnTraffic(): Long? {
        return balancerStats?.cdnStats?.totalDownloadedBytes
    }

    fun getP2PTraffic(): Long? {
        return balancerStats?.p2pStats?.downloadBytes
    }

    fun getUploadTraffic(): Long? {
        return balancerStats?.p2pStats?.uploadBytes
    }

    fun isP2PEnabled(): Boolean? {
        return balancerStats?.p2pStats?.isActive
    }

    fun updateBalancerStats(context: Context) {
        val tmp = readBalancerStatsFile(context)
        if (tmp != null) {
            this.balancerStats = tmp
        }
    }
}