package com.npaw.youbora.lib6.extensions

fun MutableMap<String,String>.concatTriggeredEventsAndJoinInParams(newParams: Map<String, String>) {
    newParams.forEach { (key, value) ->
        if (key == "triggeredEvents") {
            this[key] = if(this.contains(key)) "${this[key]}, $value" else value
        } else {
            this[key] = value
        }
    }
}