package com.npaw.youbora.lib6.balancer.models

import com.google.gson.annotations.SerializedName

data class CdnLoaderStats(
    @SerializedName("responseUUID")
    var responseUUID: String? = null,
    @SerializedName("totalDownloadedBytes")
    var totalDownloadedBytes: Long? = null,
    @SerializedName("cdns")
    var cdns: List<CdnStats>? = null
)