package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.persistence.datasource.EventDataSource;
import com.npaw.youbora.lib6.persistence.datasource.QuerySuccessListener;
import com.npaw.youbora.lib6.persistence.entity.Event;

import java.util.ArrayList;

import static com.npaw.youbora.lib6.constants.Services.INIT;
import static com.npaw.youbora.lib6.constants.Services.START;
import static com.npaw.youbora.lib6.constants.Services.STOP;

/**
 * Created by Enrique on 20/07/2017.
 */

public class OfflineTransform extends Transform {

    private EventDataSource dataSource;

    private boolean startSaved;
    private ArrayList<Request>  queuedEvents;

    public OfflineTransform(EventDataSource datasource) {
        sendRequest = false;
        isBusy = false;

        startSaved = false;
        queuedEvents = new ArrayList<>();

        this.dataSource = datasource;
    }

    @Override
    public void parse(Request request) {
        if (request != null && !request.getService().equals(INIT)
                && request.getParams() != null) {
            saveEvent(request);
        }
    }

    @Override
    public boolean hasToSend(Request request) {
        return false;
    }

    @Override
    public int getState() {
        return Transform.STATE_OFFLINE;
    }

    /**
     * Appends the new offline event to the existing ones
     *
     * @param request Request to save locally
     */
    private void saveEvent(Request request) {
        final Request finalRequest = request;
        if (!request.getParams().containsKey("request")) {
            request.setParam("request", removeServiceSlash(request.getService()));
        }
        if (!request.getParams().containsKey("unixtime")) {
            request.setParam("unixtime", System.currentTimeMillis());
        }

        if (startSaved || request.getService().equals(START)) {
            dataSource.getLastId(new QuerySuccessListener<Integer>() {
                @Override
                public void onQueryResolved(Integer offline_id) {
                    String service = removeServiceSlash(finalRequest.getService());
                    if(finalRequest.getService().equals(START)){
                        offline_id++;
                    }
                    finalRequest.setParam("code", "[VIEW_CODE]");

                    YouboraLog.debug(String.format("Saving offline event %s: %s", service, YouboraUtil.stringifyMap(finalRequest.getParams())));
                    Event event = new Event(YouboraUtil.stringifyMap(finalRequest.getParams()), System.currentTimeMillis(), offline_id);
                    if (finalRequest.getService().equals(START)) {
                        dataSource.insertNewElement(event, new QuerySuccessListener<Long>() {
                            @Override
                            public void onQueryResolved(Long queryResult) {
                                startSaved = true;
                                processQueue();
                            }
                        });
                    } else if (finalRequest.getService().equals(STOP)) {
                        dataSource.insertNewElement(event, new QuerySuccessListener<Long>() {
                            @Override
                            public void onQueryResolved(Long queryResult) {
                                startSaved = false;
                            }
                        });
                    } else {
                        dataSource.insertNewElement(event, null);
                    }
                }
            });
        } else {
            queuedEvents.add(request);
        }
    }

    private void processQueue() {
        for (Request request: queuedEvents) {
            saveEvent(request);
        }
    }

    private String removeServiceSlash(String service) {
        return service.substring(1);
    }
}
