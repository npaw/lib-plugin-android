package com.npaw.youbora.lib6

import java.io.PrintWriter
import java.io.StringWriter
import java.util.ArrayList
import java.util.concurrent.CopyOnWriteArrayList

/**
 * Provides a set of convenience methods to ease the logging.
 * @author      Nice People at Work
 * @since       6.0
 */
open class YouboraLog {

    /**
     * Enum for log levels
     */
    enum class Level constructor(val level: Int) {
        /** No console outputs  */
        SILENT(6),
        /** Console will show errors  */
        ERROR(5),
        /** Console will show warnings  */
        WARNING(4),
        /** Console will show notices (ie: lifecycle logs)  */
        NOTICE(3),
        /** Console will show debug messages (ie: player events)  */
        DEBUG(2),
        /** Console will show verbose messages (ie: Http Requests)  */
        VERBOSE(1);

        /**
         * Returns true if the current log level is less or equal than the param.
         * This is useful to avoid calling logging methods when they're not going to be printed, for
         * instance when it's expensive to generate the string to log.
         * @param lev The [Level] to check agains.
         * @return true if the current [level] is less than or equal to `lev`.
         */
        fun isAtLeast(lev: Level): Boolean { return lev.level <= this.level }
    }

    companion object {
        /**
         * List of [YouboraLogger] instances.
         *
         * You can register to Youbora logs by calling the [addLogger] method.
         *
         * This can be useful for example to dump the log messages to a file.
         * @see YouboraLogger
         */
        private var loggers: MutableList<YouboraLogger>? = null

        /** Tag for logging with the [android.util.Log] class  */
        private val TAG = "Youbora"

        /**
         * Only logs of this imporance or higher will be shown.
         * @see {@link Level}
         */
        private var currentLogLevel = Level.ERROR

        /**
         * Prints a message using Android's [YouboraLog] class. The message is only logged if the
         * [currentLogLevel] is lower or equal than the logLevel. However, all the
         * [YouboraLogger] added with [addLogger] will be called
         * regardless of the log level.
         * @param logLevel The log level at which to log the message
         * @param message The message to log
         */
        @JvmStatic fun reportLogMessage(logLevel: Level, message: String) {
            loggers?.iterator()?.forEach { logger -> logger.logYouboraMessage(message, logLevel) }

            if (currentLogLevel.level <= logLevel.level) {
                when (logLevel) {
                    Level.ERROR -> android.util.Log.e(TAG, message)
                    Level.WARNING -> android.util.Log.w(TAG, message)
                    Level.NOTICE -> android.util.Log.i(TAG, message)
                    Level.DEBUG -> android.util.Log.d(TAG, message)
                    Level.VERBOSE -> android.util.Log.v(TAG, message)
                    else -> {}
                }
            }
        }

        /**
         * Same as calling [reportLogMessage] with [Level.ERROR] level.
         * @param message the message to log
         */
        @JvmStatic fun error(message: String) { reportLogMessage(Level.ERROR, message) }

        /**
         * Logs the exception and prints its stack trace if the [currentLogLevel] is high enough.
         * @param exception The exception to log
         */
        @JvmStatic fun error(exception: Exception) {
            if (currentLogLevel.level <= Level.ERROR.level || (loggers?.size ?: -1) > 0) {

                // Stack trace to string
                val sw = StringWriter()
                val pw = PrintWriter(sw)
                exception.printStackTrace(pw)

                reportLogMessage(Level.ERROR, sw.toString())
            }
        }

        /**
         * Same as calling [reportLogMessage] with [Level.WARNING] level.
         * @param message the message to log
         */
        @JvmStatic fun warn(message: String) { reportLogMessage(Level.WARNING, message) }

        /**
         * Same as calling [reportLogMessage] with [Level.NOTICE] level.
         * @param message the message to log
         */
        @JvmStatic fun notice(message: String) { reportLogMessage(Level.NOTICE, message) }

        /**
         * Same as calling [reportLogMessage] with [Level.DEBUG] level.
         * @param message the message to log
         */
        @JvmStatic fun debug(message: String) { reportLogMessage(Level.DEBUG, message) }

        /**
         * Same as calling [reportLogMessage] with [Level.VERBOSE] level.
         * @param message the message to log
         */
        @JvmStatic fun requestLog(message: String) { reportLogMessage(Level.VERBOSE, message) }

        /**
         * Getter for current log level
         * @return the current log level
         */
        @JvmStatic fun debugLevel(): Level { return currentLogLevel }

        /**
         * Sets the log level. Any YouboraLog message with level lower o equal to [.currentLogLevel]
         * will be printed.
         * @param debugLevel the log level to set
         */
        @JvmStatic fun setDebugLevel(debugLevel: Level) { currentLogLevel = debugLevel }

        /**
         * Add an external logger to receive the log messages.
         * @param logger YouboraLogger instance
         */
        @JvmStatic fun addLogger(logger: YouboraLogger) {
            if (loggers == null) loggers = CopyOnWriteArrayList()
            loggers?.add(logger)
        }

        @JvmStatic fun removeLogger(logger: YouboraLogger): Boolean {
            return loggers?.remove(logger) ?: false
        }

        @JvmStatic fun hasLogger(logger: YouboraLogger): Boolean {
            return loggers?.contains(logger) ?: false
        }
    }

    /**
     * Interface that receives log messages as reported by [YouboraLog] class.
     * @author      Nice People at Work
     * @since       6.0
     */
    interface YouboraLogger {
        /**
         * This will be invoked whenever a log message is created
         * @param message The log message
         * @param logLevel The log level of the message
         */
        fun logYouboraMessage(message: String, logLevel: Level)
    }
}