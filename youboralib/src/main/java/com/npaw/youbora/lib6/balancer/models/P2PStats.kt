package com.npaw.youbora.lib6.balancer.models

import com.google.gson.annotations.SerializedName

data class P2PStats(
    @SerializedName("name")
    val name: String,
    @SerializedName("provider")
    val provider: String,

    @SerializedName("downloaded_bytes")
    val downloadBytes: Long,
    @SerializedName("downloaded_chunks")
    val downloadChunks: Int,
    @SerializedName("time")
    val downloadTime: Long,
    @SerializedName("uploaded_bytes")
    val uploadBytes: Long,
    @SerializedName("uploaded_chunks")
    val uploadChunks: Int,
    @SerializedName("uploaded_time")
    val uploadTime: Long,

    @SerializedName("errors")
    val errors: Int,
    @SerializedName("missed_downloaded_chunks")
    val missedChunks: Int,
    @SerializedName("timeout_errors")
    val timeoutErrors: Int,
    @SerializedName("other_errors")
    val otherErrors: Int,

    @SerializedName("max_bandwidth")
    val maxBandwidth: Long,
    @SerializedName("min_bandwidth")
    val minBandwidth: Long,
    @SerializedName("banned")
    val banned: Int,
    @SerializedName("unbanned")
    val unbanned: Int,
    @SerializedName("avg_ping_time")
    val avgPingTime: Long,
    @SerializedName("min_ping_time")
    val minPingTime: Long,
    @SerializedName("max_ping_time")
    val maxPingTime: Long,
    @SerializedName("is_banned")
    val isBanned: Boolean,
    @SerializedName("is_active")
    val isActive: Boolean,

    @SerializedName("active_peers")
    val activePeers: Int,
    @SerializedName("peers")
    val totalPeers: Int,

    @SerializedName("late_uploaded_chunks")
    val discardedUploadedSegment: Int,
    @SerializedName("late_uploaded_bytes")
    val discardedUploadedBytes: Long,
    @SerializedName("late_downloaded_bytes")
    val discardedDownloadedBytes: Long
) {
    fun subtract(other: P2PStats?): P2PStats {
        if (other == null) {
            return this
        }

        val name = this.name
        val provider = this.provider
        val downloadBytes = this.downloadBytes - other.downloadBytes
        val downloadChunks = this.downloadChunks - other.downloadChunks
        val downloadTime = this.downloadTime - other.downloadTime
        val uploadBytes = this.uploadBytes - other.uploadBytes
        val uploadChunks = this.uploadChunks - other.uploadChunks
        val uploadTime = this.uploadTime - other.uploadTime
        val errors = this.errors - other.errors
        val missedChunks = this.missedChunks - other.missedChunks
        val timeoutErrors = this.timeoutErrors - other.timeoutErrors
        val otherErrors = this.otherErrors - other.otherErrors
        val maxBandwidth = this.maxBandwidth
        val minBandwidth = this.minBandwidth
        val banned = this.banned - other.banned
        val unbanned = this.unbanned - other.unbanned
        val avgPingTime = this.avgPingTime
        val minPingTime = this.minPingTime
        val maxPingTime = this.maxPingTime
        val isBanned = this.isBanned
        val isActive = this.isActive
        val activePeers = this.activePeers
        val totalPeers = this.totalPeers
        val discardedUploadedSegment = this.discardedUploadedSegment - other.discardedUploadedSegment
        val discardedUploadedBytes = this.discardedUploadedBytes - other.discardedUploadedBytes
        val discardedDownloadedBytes = this.discardedDownloadedBytes - other.discardedDownloadedBytes

        return P2PStats(name, provider, downloadBytes, downloadChunks, downloadTime, uploadBytes, uploadChunks, uploadTime, errors,
            missedChunks, timeoutErrors, otherErrors, maxBandwidth, minBandwidth, banned, unbanned, avgPingTime, minPingTime, maxPingTime,
            isBanned, isActive, activePeers, totalPeers, discardedUploadedSegment, discardedUploadedBytes, discardedDownloadedBytes)
    }

    fun anyNonZero(): Boolean {
        return downloadBytes > 0 || downloadChunks > 0 || downloadTime > 0 || uploadBytes > 0 || uploadChunks > 0 || uploadTime > 0
    }
}
