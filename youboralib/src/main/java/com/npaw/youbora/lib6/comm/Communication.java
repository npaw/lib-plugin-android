package com.npaw.youbora.lib6.comm;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.comm.transform.Transform;
import com.npaw.youbora.lib6.constants.Services;
import com.npaw.youbora.lib6.plugin.Options;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Communication implements an abstraction layer over API requests.
 * Internally, Communication implements queues of {@link Request} objects.
 * This queue can be blocked depending on its {@link Transform}.
 * @author      Nice People at Work
 * @since       6.0
 */

public class Communication {

    private List<Transform> transforms;
    private Queue<Request> requests;
    private final Options options;

    /**
     * Constructor
     */
    public Communication(Options options) {
        transforms = new ArrayList<>();
        requests = new ConcurrentLinkedQueue<>(); // Queue
        this.options = options;
    }

    /**
     * Adds the {@link Request} to the queue. Doing this will process the pending requests.
     * @param request the request to add to the queue
     * @param callback if not null, added as a success listener to the Request
     */
    public void sendRequest(Request request, Request.RequestSuccessListener callback) {
        sendRequest(request,callback,null);
    }

    /**
     * Adds the {@link Request} to the queue. Doing this will process the pending requests.
     * @param request the request to add to the queue
     * @param callback if not null, added as a success listener to the Request
     * @param listenerParams params to return in case of request success
     */
    public void sendRequest(Request request, Request.RequestSuccessListener callback, Map<String,Object> listenerParams) {
        sendRequest(request, callback, listenerParams, null);
    }

    /**
     * Adds the {@link Request} to the queue. Doing this will process the pending requests.
     * @param request the request to add to the queue.
     * @param callback if not null, added as a success listener to the Request.
     * @param listenerParams params to return in case of request success.
     * @param errorCallback if not null, adds an error callback to the resquest
     */
    public void sendRequest(Request request, Request.RequestSuccessListener callback, Map<String,Object> listenerParams, Request.RequestErrorListener errorCallback) {
        //synchronized (requests) {
            if (request != null) {
                if (callback != null) {
                    if (listenerParams != null) {
                        request.setSuccessListenerParams(listenerParams);
                    }
                    request.addOnSuccessListener(callback);
                }
                if (errorCallback != null) {
                    request.addOnErrorListener(errorCallback);
                }
                registerRequest(request);
            }
        //}
    }

    /**
     * Add a {@link Transform} to the transforms list
     * @param transform transform to add
     */
    public void addTransform(Transform transform) {
        if (transform != null) {
            transform.addTransformDoneListener(new Transform.TransformDoneListener() {
                @Override
                public void onTransformDone(Transform transform) {
                    processRequests();
                }
            });
            transforms.add(transform);
        } else {
            YouboraLog.warn("Transform is null");
        }
    }

    /**
     * Remove a {@link Transform} from the transforms list
     * @param transform the transform to remove
     */
    public void removeTransform(Transform transform) {
        if (!transforms.remove(transform)) {
            YouboraLog.warn("Trying to remove unexisting Transform: " + transform);
        }
    }

    // Private methods
    void registerRequest(Request request) {
        if (options.getAuthToken() != null) {

            if (request.requestHeaders == null) request.requestHeaders = new HashMap<>();

            request.requestHeaders.put(
                    "Authorization",
                    options.getAuthType() + " " + options.getAuthToken()
            );
        }

        if (request != null) requests.add(request);

        processRequests();
    }

    private void processRequests() {

        Queue<Request> auxQueue = requests;
        requests = new ConcurrentLinkedQueue<>();

        while ( !auxQueue.isEmpty() ) {
            Request element = auxQueue.poll();

            if ( element != null ) {
                switch (transform(element)) {
                    case Transform.STATE_BLOCKED:
                        requests.add(element);
                        break;

                    case Transform.STATE_NO_BLOCKED:
                        if(Objects.equals(element.getMethod(), Request.METHOD_POST) && !element.getService().equals(Services.OFFLINE_EVENTS)) {
                            element.setBody(YouboraUtil.stringifyMap(element.getParams()));
                            element.setParams(getParamsForPostMessages(element.getParams()));
                        }
                        element.send();
                        break;
                }
            }
        }
    }

    private int transform(Request request) {

        for (Transform transform : transforms) {
            if(transform.getState() == Transform.STATE_REJECT) {
                return Transform.STATE_REJECT;
            }
            if (transform.isBlocking(request)){
                return Transform.STATE_BLOCKED;
            } else {
                transform.parse(request);
            }
            if(transform.getState() == Transform.STATE_OFFLINE){
                return Transform.STATE_OFFLINE;
            }
        }
        return Transform.STATE_NO_BLOCKED;
    }

    /**
     * Extract only needed params for POST messages.
     * @param params current request params.
     * @return params needed for POST messages.
     */
    private Map<String, Object> getParamsForPostMessages(Map<String, Object> params) {
        Map<String, Object> m = new HashMap<>();

        // Get previous params
        if (params.containsKey("timemark")) {
            m.put("timemark", params.get("timemark"));
        }
        if (params.containsKey("code")) {
            m.put("code", params.get("code"));
        }
        if (params.containsKey("sessionRoot")) {
            m.put("sessionRoot", params.get("sessionRoot"));
        }
        if (params.containsKey("sessionId")) {
            m.put("sessionId", params.get("sessionId"));
        }
        return m;
    }
    /*private boolean transform(Request request) {

        for (Transform transform : transforms) {
            if (transform.isBlocking(request)){
                return false;
            } else {
                transform.parse(request);
            }
        }

        return true;
    }*/
}
