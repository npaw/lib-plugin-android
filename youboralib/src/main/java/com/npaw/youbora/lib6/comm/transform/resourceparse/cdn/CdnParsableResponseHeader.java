package com.npaw.youbora.lib6.comm.transform.resourceparse.cdn;

import java.util.ArrayList;

/**
 * An instance of this class informs what info to extract from a particular header response for
 * a given CDN.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class CdnParsableResponseHeader {

    /**
     * What info should this {@link CdnParsableResponseHeader} should extract from the header
     */
    public Element element;

    /**
     * The name of the header where the information can be found in the CDN response
     */
    public String headerName;

    /**
     * The name list of the headers where the information can be found in the CDN response
     */
    public ArrayList<String> headerNames;

    /**
     * Pattern to extract the information. This works in conjunction with {@link #element}.
     *
     * If only one element is expected ({@link Element#Host}, {@link Element#Type} or {@link Element#Name})
     * the pattern should contain one capturing group. Likewise, two capturing groups are expected
     * if two bits of info are expected ({@link Element#HostAndType} or {@link Element#TypeAndHost}).
     */
    public String regexPattern;

    /**
     * Possible different bits of info we can get from a header.
     * @see #regexPattern
     */
    public enum Element {
        Host,
        Type,
        HostAndType,
        TypeAndHost,
        Name
    }

    /**
     * Constructor
     * @param element the expected info from this header response
     * @param headerName name of the header where the information can be found
     * @param regexPattern pattern to extract the information.
     */
    public CdnParsableResponseHeader(Element element, String headerName, String regexPattern) {
        this.element = element;
        this.headerName = headerName;
        this.regexPattern = regexPattern;
    }
}
