package com.npaw.youbora.lib6.plugin;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.google.gson.JsonObject;
import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.DeviceInfo;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.adapter.PlaybackChronos;
import com.npaw.youbora.lib6.constants.RequestParams;
import com.npaw.youbora.lib6.adapter.AdAdapter;
import com.npaw.youbora.lib6.adapter.AdAdapter.AdPosition;
import com.npaw.youbora.lib6.extensions.HelperExtension;
import com.npaw.youbora.lib6.flags.BaseFlags;
import com.npaw.youbora.lib6.infinity.InfinitySharedPreferencesManager;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.comm.Communication;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.FlowTransform;
import com.npaw.youbora.lib6.comm.transform.OfflineTransform;
import com.npaw.youbora.lib6.comm.transform.ResourceTransform;
import com.npaw.youbora.lib6.comm.transform.Transform;
import com.npaw.youbora.lib6.comm.transform.ViewTransform;
import com.npaw.youbora.lib6.infinity.Infinity;
import com.npaw.youbora.lib6.productAnalytics.ProductAnalytics;
import com.npaw.youbora.lib6.balancer.monitor.CdnBalancerInfo;
import com.npaw.youbora.lib6.monitoring.RemoteMonitoringListener;
import com.npaw.youbora.lib6.persistence.datasource.EventDataSource;
import com.npaw.youbora.lib6.persistence.datasource.QuerySuccessListener;
import com.npaw.youbora.lib6.persistence.entity.Event;
import com.npaw.youbora.lib6.monitoring.RemoteMonitoring;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static com.npaw.youbora.lib6.constants.RequestParams.AD_VIEWABILITY;
import static com.npaw.youbora.lib6.constants.RequestParams.AD_VIEWED_DURATION;
import static com.npaw.youbora.lib6.constants.RequestParams.PLAYHEAD;
import static com.npaw.youbora.lib6.constants.Services.*;
import static com.npaw.youbora.lib6.adapter.AdAdapter.AdPosition.*;

/**
 * This is the main class of video analytics. You may want one instance for each video you want
 * to track. Will need {@link PlayerAdapter}s for both content and ads, manage options and general flow.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Plugin {
    private final UUID pluginId = UUID.randomUUID();
    private ResourceTransform resourceTransform;
    private ViewTransform viewTransform;
    private RequestBuilder requestBuilder;
    private Timer pingTimer;
    private Timer beatTimer;

    private RemoteMonitoring remoteMonitoring;
    private Timer metadataTimer;
    private Timer cdnTimer;
    private Options options;
    private PlayerAdapter adapter;
    private AdAdapter adsAdapter;
    private Infinity infinity;
    private ProductAnalytics productAnalytics;
    private String lastServiceSent;
    protected CdnBalancerInfo cdnBalancerInfo;

    // Necessary for Foreground / Background mechanic

    /** Infinity initial variables **/
    private String startScreenName;
    private Map<String, String> startDimensions;

    /** Application context, mostly used for offline **/
    private Context context;
    /** Activity reference just to check that the events come from our activity **/
    private Activity activity;
    /** Activity reference for sessions **/
    private Activity sessionActivity;
    /** Reference to the activity callbacks **/
    private Application.ActivityLifecycleCallbacks activityLifecycleCallbacks;

    /** Offline database access **/
    private EventDataSource dataSource;
    /** Pending offline events to send **/
    private int pendingOfflineEvents;

    /** Indicates that /init has been called. */
    private boolean isInitiated;
    /** Indicates that /adManifest has been called */
    private boolean isAdsManifestSent;
    /** Indicates that /start has been called. */
    private boolean isStarted;
    /** Indicates that /adStart has been called. */
    private boolean isAdStarted;
    /** Indicates that we are currently in an ad break. */
    public boolean isAdBreakStarted;
    /** Indicates that the content is preloading. */
    private boolean isPreloading;
    /** Chrono for preload times. */
    private Chrono preloadChrono;
    /** Chrono for init to join time. */
    private Chrono initChrono;

    /** Saved manifest */
    private Map<String, String> savedAdManifest;

    private boolean isAdConnected;
    private long adConnectedTime;

    public boolean fastDataReceived = true;

    /**
     * Instance of {@link Communication} used to send {@link Request}s.
     */
    public Communication comm;

    // "Will send" Listeners
    private List<WillSendRequestListener> willSendInitListeners;
    private List<WillSendRequestListener> willSendStartListeners;
    private List<WillSendRequestListener> willSendJoinListeners;
    private List<WillSendRequestListener> willSendPauseListeners;
    private List<WillSendRequestListener> willSendResumeListeners;
    private List<WillSendRequestListener> willSendSeekListeners;
    private List<WillSendRequestListener> willSendBufferListeners;
    private List<WillSendRequestListener> willSendErrorListeners;
    private List<WillSendRequestListener> willSendStopListeners;
    private List<WillSendRequestListener> willSendPingListeners;
    private List<WillSendRequestListener> willSendCdnListeners;

    private List<WillSendRequestListener> willSendAdInitListeners;
    private List<WillSendRequestListener> willSendAdStartListeners;
    private List<WillSendRequestListener> willSendAdJoinListeners;
    private List<WillSendRequestListener> willSendAdClickListeners;
    private List<WillSendRequestListener> willSendAdPauseListeners;
    private List<WillSendRequestListener> willSendAdResumeListeners;
    private List<WillSendRequestListener> willSendAdBufferListeners;
    private List<WillSendRequestListener> willSendAdStopListeners;
    private List<WillSendRequestListener> willSendAdErrorListeners;
    private List<WillSendRequestListener> willSendAdManifestListeners;
    private List<WillSendRequestListener> willSendAdBreakStartListeners;
    private List<WillSendRequestListener> willSendAdBreakStopListeners;
    private List<WillSendRequestListener> willSendAdQuartileListeners;

    private List<WillSendRequestListener> willSendSessionStartListeners;
    private List<WillSendRequestListener> willSendSessionStopListeners;
    private List<WillSendRequestListener> willSendSessionNavListeners;
    private List<WillSendRequestListener> willSendSessionEventListeners;
    private List<WillSendRequestListener> willSendSessionBeatListeners;

    private List<WillSendRequestListener> willSendVideoEventListeners;

    private List<WillSendRequestListener> willSendOfflineEventsListeners;

    private final String SUCCESS_LISTENER_OFFLINE_ID = "offline_id";

    /**
     * Constructor. Calls internally {@link #Plugin(Options, Activity, Context, ViewTransform.FastDataConfig)} with
     * a null adapter, activity and context.
     * @param options instance of {@link Options}
     * @deprecated use {@link #Plugin(Options, Context)} instead.
     */
    public Plugin(Options options) { this(options, null, null, null); }

    /**
     * Constructor. Calls internally {@link #Plugin(Options, Activity, Context, ViewTransform.FastDataConfig)} with
     * a null adapter and activity.
     * @param options instance of {@link Options}
     * @param applicationContext instance of {@link Context}. Can also be specified afterwards with
     * {@link #setApplicationContext(Context)}.
     */
    public Plugin(Options options, Context applicationContext) {
        this(options, null, applicationContext, null);
    }

    /**
     * Constructor. Calls internally {@link #Plugin(Options, Activity, Context, ViewTransform.FastDataConfig)} with
     * a null adapter and activity.
     * @param options instance of {@link Options}
     * @param applicationContext instance of {@link Context}. Can also be specified afterwards with
     * {@link #setApplicationContext(Context)}.
     */
    public Plugin(Options options, Context applicationContext, ViewTransform.FastDataConfig fastDataConfig) {
        this(options, null, applicationContext, fastDataConfig);
    }

    /**
     * Constructor. Calls internally {@link #Plugin(Options, Activity, Context, ViewTransform.FastDataConfig)} with
     * a null adapter.
     * @param options instance of {@link Options}
     * @param activity instance of {@link Activity}. Can also be specified afterwards with
     * {@link #setActivity(Activity)}.
     * @deprecated use {@link #Plugin(Options, Context)} instead. And also
     * {@link #setActivity(Activity)} before setting the adapter.
     */
    public Plugin(Options options, Activity activity) {
        this(options, activity, activity.getApplicationContext(), null);
    }

    /**
     * Constructor.
     * @param options instance of {@link Options}
     * @param activity instance of {@link Activity}
     * @param context instance of {@link Context}
     */
    private Plugin(Options options, Activity activity, Context context, ViewTransform.FastDataConfig fastDataConfig) {
        remoteMonitoring = createRemoteMonitoring( remoteMonitoringListener, this);

        setApplicationContext(context);
        setActivity(activity);

        if (options == null) {
            YouboraLog.warn("Options is null");
            options = createOptions();
        }

        preloadChrono = createChrono();
        initChrono = createChrono();
        this.options = options;
        cdnBalancerInfo = new CdnBalancerInfo();

        if (context != null) dataSource = createEventDataSource();

        pingTimer = createTimer(delta -> {
            sendPing(delta);
            //We "use" the ping timer to check if any metadata was missing too
            //if (isExtraMetadataReady() && getOptions().getWaitForMetadata()) startListener(null);
        }, 5000);

        beatTimer = createBeatTimer(this::sendBeat, 30000);
        cdnTimer = createCdnTimer(delta -> sendCdnPing(), 5000);

        metadataTimer = createMetadataTimer(delta -> {
            if (isExtraMetadataReady()){
                metadataTimer.stop();
                startListener(null);
            }
        }, 5000);

        requestBuilder = createRequestBuilder(this);
        resourceTransform = createResourceTransform(this);

        remoteMonitoring.initRemoteMonitoring();

        initializeViewTransform(fastDataConfig);
    }

    private void initializeViewTransform(ViewTransform.FastDataConfig fastDataConfig) {
        viewTransform = createViewTransform(this);
        viewTransform.addTransformDoneListener(new Transform.TransformDoneListener() {
            @Override
            public void onTransformDone(Transform transform) {
                int interval = getOptions().isOffline() ? 60 : viewTransform.fastDataConfig.pingTime;
                pingTimer.setInterval(interval * 1000);
                if (!getOptions().isOffline()) {
                    interval = viewTransform.fastDataConfig.beatTime;
                    beatTimer.setInterval(interval * 1000);
                }
            }
        });

        viewTransform.init(fastDataConfig);
    }

    /**
     * Resets plugin to initial state
     */
    private void reset() {
        stopPings();
        stopCdnPing();
        remoteMonitoring.stopRemoteDebuggingTimer();
        metadataTimer.stop();

        resourceTransform = createResourceTransform(this);

        isInitiated = false;
        isAdsManifestSent = false;
        isStarted = false;
        isAdStarted = false;
        isAdBreakStarted = false;
        isPreloading = false;
        initChrono.reset();
        preloadChrono.reset();
    }

    /**
     * Returns the {@link ResourceTransform} instance used by the Plugin
     * @return the ResourceTransform instance
     */
    public ResourceTransform getResourceTransform() {
        return resourceTransform;
    }

    // --------------------------------- Instance builders -----------------------------------------
    Chrono createChrono() {
        return new Chrono();
    }

    Options createOptions() {
        return new Options();
    }

    Timer createTimer(Timer.TimerEventListener listener, long interval) {
        return new Timer(listener, interval);
    }

    Timer createBeatTimer(Timer.TimerEventListener listener, long interval) {
        return new Timer(listener, interval);
    }

    Timer createMetadataTimer(Timer.TimerEventListener listener, long interval) {
        return new Timer(listener, interval);
    }

    Timer createCdnTimer( Timer.TimerEventListener listener, long interval )
    {
        return new Timer(listener, interval);
    }

    RequestBuilder createRequestBuilder(Plugin plugin) {
        return new RequestBuilder(plugin);
    }

    ResourceTransform createResourceTransform(Plugin plugin) {
        return new ResourceTransform(plugin);
    }

    ViewTransform createViewTransform(Plugin plugin) {
        return new ViewTransform(plugin);
    }

    Communication createCommunication() {
        return new Communication(options);
    }

    OfflineTransform createOfflineTransform() {
        return new OfflineTransform(dataSource);
    }

    FlowTransform createFlowTransform() {
        return new FlowTransform();
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }

    EventDataSource createEventDataSource() {
        return new EventDataSource(getApplicationContext());
    }

    private Boolean isSessionExpired() {
        boolean val = false;

        if (viewTransform.fastDataConfig.expirationTime != null && infinity != null &&
                infinity.getLastSent() != null) {
            val = (infinity.getLastSent() + viewTransform.fastDataConfig.expirationTime * 1000)
                    < System.currentTimeMillis();
        }

        return val;
    }

    // ------------------------------------- Fast Data --------------------------------------------
    /**
     * Returns the current view code being used
     * @return view code as String
     */
    public ViewTransform.FastDataConfig getFastDataConfig() {
        if (viewTransform != null) {
            return viewTransform.fastDataConfig;
        }
        return null;
    }

    // ------------------------------------- Options ----------------------------------------------
    /**
     * Update the options. This will replace the whole Options object. If you want to change only
     * a few options call {@link #getOptions()} and modify the received object.
     * @param options The {@link Options} to set
     */
    public void setOptions(Options options) {
        this.options = options;
    }

    /**
     * Returns the current plugin Options.
     *
     * This method returns the actual options instance, so any changes performed on them will
     * automatically be reflected without the need to call {@link #setOptions(Options)}
     * @return the options
     */
    public Options getOptions() {
        return options;
    }

    // ------------------------------------- Adapters ----------------------------------------------
    /**
     * Sets an adapter for video content.
     *
     * @param adapter The adapter to set
     */
    public void setAdapter(PlayerAdapter adapter) {

        removeAdapter(false);

        if (adapter != null) {

            this.adapter = adapter;
            adapter.setPlugin(this);

            // Register listeners
            adapter.addEventListener(eventListener);

            registerForActivityCallbacks();

            // Notify Product Analytics

            if ( this.getProductAnalytics() != null ) {
                this.getProductAnalytics().adapterAfterSet(adapter);
            }

        } else {
            YouboraLog.error("Adapter is null in setAdapter");
        }
    }

    /**
     * Returns the current adapter or null if not set
     * @return adapter
     */
    public PlayerAdapter getAdapter() {
        return adapter;
    }

    /**
     * Removes the current adapter. Stopping pings if no ads adapter.
     */
    public void removeAdapter() {
        removeAdapter(true);
    }


    /**
     * Removes adapter. Fires stop if needed. Calls {@link PlayerAdapter#dispose()}
     * @param stopPings if pings should stop
     */
    public void removeAdapter(boolean stopPings) {
        if (adapter != null) {

            if ( this.getProductAnalytics() != null ){
                this.getProductAnalytics().adapterBeforeRemove();
            }

            adapter.dispose();
            adapter.setPlugin(null);

            // Remove listeners
            adapter.removeEventListener(eventListener);
            adapter = null;
        }

        if (stopPings && adsAdapter == null) fireStop();

        if (getInfinity() != null && !getInfinity().getFlags().isStarted()) {
            unregisterForActivityCallbacks();
        }
    }

    /**
     * Sets an adapter for ads.
     *
     * @param adsAdapter adapter to set
     */
    public void setAdsAdapter(AdAdapter adsAdapter) {

        if (adsAdapter == null) {
            YouboraLog.error("Adapter is null in setAdsAdapter");
        } else if (adsAdapter.getPlugin() != null) {
            YouboraLog.warn("Adapters can only be added to a single plugin");
        } else {
            removeAdsAdapter(false);

            this.adsAdapter = adsAdapter;
            this.adsAdapter.setPlugin(this);

            adsAdapter.addEventListener(adEventListener);
        }
    }

    /**
     * Returns the current ads adapter or null
     * @return the current ads adapter or null
     */
    public AdAdapter getAdsAdapter() {
        return adsAdapter;
    }

    /**
     * Removes the current ads adapter. Stopping pings if no adapter.
     */
    public void removeAdsAdapter() {
        removeAdsAdapter(true);
    }

    /**
     * Removes ads adapter. Fires stop if needed. Calls {@link PlayerAdapter#dispose()}
     * @param stopPings if pings should stop
     */
    public void removeAdsAdapter(boolean stopPings) {
        if (adsAdapter != null) {
            adsAdapter.dispose();
            adsAdapter.fireAdBreakStop();
            adsAdapter.setPlugin(null);
            adsAdapter.removeEventListener(adEventListener);
            adsAdapter = null;
        }

        if (stopPings && adapter == null) fireStop();
    }

    // -------------------------------------- INFINITY ----------------------------------------------
    /**
     * @param context
     * @return Infinity instance or null if the Plugin's context is null.
     * @deprecated Use getInfinity() instead.
     */
    public Infinity getInfinity(Context context) {
        if (infinity == null) {
            if (context != null) {
                infinity = new Infinity(context, viewTransform, infinityEventListener, options);
            } else {
                YouboraLog.error("Infinity could not be instantiated since the Plugin's context " +
                        "is null");
            }
        }

        return infinity;
    }

    /**
     * @return Infinity instance or null if the Plugin's context is null.
     */
    public Infinity getInfinity() {
        if (infinity == null) {
            if (getApplicationContext() != null) {
                infinity = new Infinity(getApplicationContext(), viewTransform,
                        infinityEventListener, options);
            } else {
                YouboraLog.error("Infinity could not be instantiated since the Plugin's context " +
                        "is null");
            }
        }

        return infinity;
    }

    // --------------------------------- PRODUCT ANALYTICS -----------------------------------------
    /**
     * @return Product Analytics instance or null if the Plugin's context is null.
     */
    public ProductAnalytics getProductAnalytics() {
        if (productAnalytics == null) {
            if (getApplicationContext() != null) {
                productAnalytics = new ProductAnalytics(this.options, this.getInfinity());
            } else {
                YouboraLog.error("Product Analytics could not be instantiated since the Plugin's context " +
                        "is null");
            }
        }

        return productAnalytics;
    }

    /**
     * Destroy product analytics
     */

    public void destroyProductAnalytics() {
        if ( productAnalytics != null ){
            productAnalytics = null;
        }
    }

    // -------------------------------------- DISABLE ----------------------------------------------
    /**
     * Disable request sending.
     * Same as calling {@link Options#setEnabled(boolean)} with false
     */
    public void disable() {
        options.setEnabled(false);
    }

    /**
     * Re-enable request sending.
     * Same as calling {@link Options#setEnabled(boolean)} with true
     */
    public void enable() {
        options.setEnabled(true);
    }

    // -------------------------------------- ACTIVITY CALLBACKS -----------------------------------

    /**
     * Registers the activity to its events for background
     */
    private void registerForActivityCallbacks() {
        if (getActivity() != null && activityLifecycleCallbacks == null) {

            activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {

                @Override
                public void onActivityCreated(Activity activity, Bundle bundle) { }

                @Override
                public void onActivityStarted(Activity activity) {
                    if (adsAdapter != null &&
                            !adsAdapter.getChronos().getAdViewedPeriods().isEmpty())
                        adsAdapter.getChronos().getAdViewability().start();


                    if (sessionActivity == activity && getInfinity() != null &&
                            getInfinity().getFlags().isStarted()) {
                        if (!isSessionExpired()) {
                            if (beatTimer.getChrono().getStartTime() != null) {
                                sendBeat(Chrono.getNow() - beatTimer.getChrono()
                                        .getStartTime());
                            }

                            startBeats();
                        } else {
                            getInfinity().getFlags().reset();
                            initializeViewTransform(null);
                            getInfinity().setViewTransform(viewTransform);
                            getInfinity().begin(startScreenName, startDimensions);
                        }
                    }
                }

                @Override
                public void onActivityResumed(Activity activity) {
                    setSessionActivity(activity);
                }

                @Override
                public void onActivityPaused(Activity activity) { }

                @Override
                public void onActivityStopped(Activity activity) {
                    if (sessionActivity == activity && getInfinity() != null &&
                            getInfinity().getFlags().isStarted()) {
                        if (beatTimer.getChrono().getStartTime() != null)
                            sendBeat(Chrono.getNow() - beatTimer.getChrono().getStartTime());

                        stopBeats();
                    }

                    if (getOptions().isAutoDetectBackground() && getActivity() == activity) {
                        if (getAdsAdapter() != null && getAdsAdapter().getFlags().isStarted())
                            getAdsAdapter().fireStop();

                        fireStop();
                    } else {
                        stopAdViewabilityChrono();
                    }
                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) { }

                @Override
                public void onActivityDestroyed(Activity activity) {
                    if (getActivity() == activity) {
                        setActivity(null);
                        adapter = null;
                    }
                }
            };

            getActivity().getApplication()
                    .registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        } else if (getActivity() == null) {
                YouboraLog.error("The plugin could not send stop, plugin.setActivity must be " +
                        "called before setting the adapter");
        }
    }

    private void stopAdViewabilityChrono() {
        if (adsAdapter != null) {
            PlaybackChronos chronos = adsAdapter.getChronos();
            chronos.getAdViewedPeriods().add(chronos.getAdViewability().stop());
        }
    }

    /**
     * Unregisters the activity to its events for background
     */
    private void unregisterForActivityCallbacks() {
        if (getActivity() != null) {
            getActivity().getApplication().unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
            activityLifecycleCallbacks = null;
        }

    }

    // ------------------------------------- LISTENERS ---------------------------------------------

    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     */
    private void send(List<WillSendRequestListener> listenerList, String service, Map<String, String> params) {
        send(listenerList, service, params, getOptions().getMethod().name(), null, null, null, false);
    }

    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     * @param method optional {@link Request} method
     * @param body optional {@link Request} body, only if using {@link Request#METHOD_POST}
     * @param successListener callback {@link com.npaw.youbora.lib6.comm.Request.RequestSuccessListener} that will be invoked
     *                        when a Request has been successful
     * @param successListenerParams {@link Map<String,Object>} with params to return if request successful
     * @param readMode optional {@link RequestBuilder} readMode, if true, it will not change any state
     */
    private void send(List<WillSendRequestListener> listenerList, String service,
                      Map<String, String> params, String method, String body,
                      Request.RequestSuccessListener successListener,
                      Map<String, Object> successListenerParams,
                      boolean readMode) {
        send(listenerList, service, params, method, body, successListener, successListenerParams, null, readMode);
    }

    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     * @param method optional {@link Request} method
     * @param body optional {@link Request} body, only if using {@link Request#METHOD_POST}
     * @param successListener callback {@link com.npaw.youbora.lib6.comm.Request.RequestSuccessListener} that will be invoked
     *                        when a Request has been successful
     * @param successListenerParams {@link Map<String,Object>} with params to return if request successful
     * @param errorListener callback {@link com.npaw.youbora.lib6.comm.Request.RequestErrorListener} that will be invoked
     *                      when a Request has failed and when it has been removed from the retry queue.
     * @param readMode optional {@link RequestBuilder} readMode, if true, it will not change any state
     */
    private void send(List<WillSendRequestListener> listenerList, String service,
                      Map<String, String> params, String method, String body,
                      Request.RequestSuccessListener successListener,
                      Map<String, Object> successListenerParams,
                      Request.RequestErrorListener errorListener,
                      boolean readMode) {

        if(readMode)
            params = requestBuilder.buildParams(params, service, true);
        else
            params = requestBuilder.buildParams(params, service);

        if (listenerList != null) {
            for (Plugin.WillSendRequestListener listener : listenerList) {
                try {
                    listener.willSendRequest(service, this, params);
                } catch (Exception e) {
                    YouboraLog.error("Exception while calling willSendRequest");
                    YouboraLog.error(e);
                }
            }
        }

        if (comm != null && params != null && options.isEnabled()) {
            Request r = createRequest(null, service);

            Map<String, Object> m = new HashMap<>();
            m.putAll(params);

            if (!service.equals(OFFLINE_EVENTS)) {
                if (getIsLive()) m.remove(PLAYHEAD);
            }

            r.setParams(m);
            r.setBody(body);
            r.setMethod(method);

            lastServiceSent = r.getService();

            if(viewTransform != null && !options.isOffline() && !service.equals(OFFLINE_EVENTS))
                viewTransform.parse(r);

            comm.sendRequest(r, successListener, successListenerParams, errorListener);
        } else if( comm == null ){
            YouboraLog.warn("Skipped sending the request["+service+"]... comm is null.");
        } else if( params == null ){
            YouboraLog.warn("Skipped sending the request["+service+"]... The \"params\" variable is null.");
        } else if( !options.isEnabled() ){
            YouboraLog.warn("Skipped sending the request["+service+"]... The plugin is disabled in the options.");
        }
    }

    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     */
    private void sendInfinity(List<WillSendRequestListener> listenerList, String service, Map<String, String> params) {
        /*if(Infinity.getInstance().getLastSent() + (5 * 1000) < Chrono.getNow()){
            viewTransform.nextView();
        }*/
        params = requestBuilder.buildParams(params, service);

        if (listenerList != null) {
            for (Plugin.WillSendRequestListener listener : listenerList) {
                try {
                    listener.willSendRequest(service, this, params);
                } catch (Exception e) {
                    YouboraLog.error("Exception while calling willSendRequest");
                    YouboraLog.error(e);
                }
            }
        }

        if (getInfinity().getCommunication() != null && params != null && options.isEnabled()) {
            Request r = createRequest(null, service);

            Map<String, Object> m = new HashMap<>();
            m.putAll(params);
            r.setParams(m);
            r.setMethod(getOptions().getMethod().name());

            lastServiceSent = r.getService();

            getInfinity().getCommunication().sendRequest(r, null, null);
        }
    }

    /**
     * If the current resource is not null this will trigger the resource parsing.
     * If resource parsing has already started or has ended, it should do nothing.
     */
    private void startResourceParsing() {
        String resource = getResource();

        if (getUrlToParse() != null)  resource = getUrlToParse();

        if (resource != null)
            resourceTransform.init(resource);
    }

    /**
     * Initializes comm and its transforms.
     */
    public void initComm() {
        comm = createCommunication();

        comm.addTransform(createFlowTransform());
        comm.addTransform(resourceTransform);

        if (options.isOffline()) {
            if (getApplicationContext() != null) {
                comm.addTransform(createOfflineTransform());
            } else {
                YouboraLog.notice("To use the offline feature you have to set the application context");
            }
        } else {
            comm.addTransform(viewTransform);
        }
    }

    /**
     * Initializes comm with the desired transforms
     * @param transforms List of transforms to be added to {@link Communication} object
     */
    public void initComm(List<Transform> transforms) {
        comm = createCommunication();

        for (Transform transform : transforms)
            comm.addTransform(transform);
    }

    /**
     * Starts preloading state and chronos.
     */
    public void firePreloadBegin() {
        if (!isPreloading) {
            isPreloading = true;
            preloadChrono.start();
        }
    }

    /**
     * Ends preloading state and chronos.
     */
    public void firePreloadEnd() {
        if (isPreloading) {
            isPreloading = false;
            preloadChrono.stop();
        }
    }

    /**
     * Same as calling {@link #fireInit(Map)} with a {@code null} param.
     */
    public void fireInit() { fireInit(null); }

    /**
     * Sends /init. Should be called once the user has requested the content. Does not need
     * a working adapter or player to work. It won't sent start if isInitiated is true.
     *
     * @param params Map of params to add to the request.
     */
    public void fireInit(@Nullable Map<String, String> params) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[Plugin:"+pluginId+"] fireInit isInitiated:"+isInitiated+" isStarted: "+isStarted+" params: "+ HelperExtension.Companion.printMap(params) );
        if (!isInitiated && !isStarted) {
            viewTransform.nextView();
            initComm();
            startPings();
            startCdnPing();
            startMetadataTimer();

            isInitiated = true;
            initChrono.start();

            sendInit(params);
            registerForActivityCallbacks();
        }

        startResourceParsing();
    }

    private void sendInit(Map<String, String> params) {
        params = requestBuilder.buildParams(params, INIT);
        send(willSendInitListeners, INIT, params);
        String titleOrResource = params != null ? params.get("title") : "unknown";

        if (titleOrResource == null) titleOrResource = params.get("mediaResource");

        YouboraLog.notice(INIT + " " + titleOrResource);
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[Plugin:"+pluginId+"] sendInit params: "+ HelperExtension.Companion.printMap(params) );
    }

    public void fireOfflineEvents() { fireOfflineEvents(null); }

    private void fireOfflineEvents(Map<String, String> params) {
        if (pendingOfflineEvents == 0) {
            if (getOptions().isOffline()) {
                YouboraLog.error("To send offline events, offline option must be disabled");
                return;
            }

            if (getAdapter() != null && getAdapter().getFlags() != null &&
                    getAdapter().getFlags().isStarted() && getAdsAdapter() != null &&
                    getAdsAdapter().getFlags().isStarted()) {
                YouboraLog.error("Adapters have to be stopped");
                return;
            }

            if (!fastDataReceived) initializeViewTransform(null);
            comm = createCommunication();
            comm.addTransform(viewTransform);

            try {
                dataSource.getAll(new QuerySuccessListener<List<Event>>() {
                    @Override
                    public void onQueryResolved(List<Event> queryResult) {
                        if(queryResult.size() == 0){
                            YouboraLog.debug("No offline events, skipping...");
                            return;
                        }
                        dataSource.getLastId(new QuerySuccessListener<Integer>() {
                            @Override
                            public void onQueryResolved(Integer queryResult) {
                                int lastId = queryResult;
                                for(int k = 0 ; k < lastId + 1 ; k++){
                                    dataSource.getByOfflineId(k, new QuerySuccessListener<List<Event>>() {
                                        @Override
                                        public void onQueryResolved(List<Event> queryResult) {
                                            String eventsString = generateJsonString(queryResult);
                                            if (queryResult.size() > 0){
                                                sendOfflineEvents(eventsString, queryResult.get(0).getOfflineId());
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            } catch (Exception e) {
                YouboraLog.error(e);
            }
        }
    }

    private String generateJsonString(List<Event> events) {
        String jsonString = "[";
        String stringFormat = "%s,%s";

        for (Event event : events) {
            stringFormat = jsonString.equals("[") ? "%s%s" : "%s,%s";
            jsonString = String.format(stringFormat,jsonString,event.getJsonEvents());
        }

        return String.format("%s]",jsonString);
    }

    private void sendOfflineEvents(String params, int offlineId) {
        HashMap<String,Object> listenerParams = new HashMap<>();
        listenerParams.put(SUCCESS_LISTENER_OFFLINE_ID, offlineId);

        Request.RequestSuccessListener sucessListener = new Request.RequestSuccessListener(){
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response, Map<String, Object> listenerParams, Map<String, List<String>> headers) {
                int offlineId = (int)listenerParams.get(SUCCESS_LISTENER_OFFLINE_ID);
                Event dummyEvent = new Event(); //Dummy event with only the offline id
                dummyEvent.setOfflineId(offlineId);
                dataSource.deleteElement(dummyEvent, new QuerySuccessListener<Integer>() {
                    @Override
                    public void onQueryResolved(Integer queryResult) {
                        pendingOfflineEvents--;
                        YouboraLog.debug("Offline events deleted");
                    }
                });
            }
        };

        Request.RequestErrorListener errorListener = new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {

            }

            @Override
            public void onRequestRemovedFromQueue() {
                pendingOfflineEvents--;
            }
        };

        send(willSendOfflineEventsListeners, OFFLINE_EVENTS, null, Request.METHOD_POST,
                params, sucessListener, listenerParams, false);
        pendingOfflineEvents++;
    }

    /**
     * Basic error handler. msg, code, errorMetadata and level params can be included in the params
     * argument.
     * @param params params to add to the request. If it is null default values will be added.
     */
    public void fireError( Map<String, String> params )
    {
        sendError(params);
    }

    /**
     * Sends a non-fatal error (with {@code level = "error"}).
     * @param msg Error message (should be unique for the code)
     * @param code Error code reported
     * @param errorMetadata Extra error info, if available.
     */
    public void fireError( String code, String msg, String errorMetadata )
    {
        sendError(YouboraUtil.buildErrorParams(code, msg, errorMetadata, "error"));
    }

    /**
     * Sends a fatal error (with {@code level = "error"}).
     * @param msg Error message (should be unique for the code)
     * @param code Error code reported
     * @param errorMetadata Extra error info, if available.
     * @param ex Exception to filter by in adapter to prevent duplicates
     */
    public void fireFatalError( String code, String msg, String errorMetadata, Exception ex )
    {
        if ( adapter != null )
        {
            if ( ex != null )
                adapter.fireFatalError(code, msg, errorMetadata, ex);
            else
                adapter.fireFatalError(code, msg, errorMetadata);
        } else {
            sendError(YouboraUtil.buildErrorParams(code, msg, errorMetadata, ""));
        }

        fireStop();
    }

    /**
     * Sends stop request
     */
    public void fireStop()
    {
        if ( getAdapter() != null && getAdapter().getFlags().isStarted() )
            getAdapter().fireStop();
        else
            fireStop(null);
    }

    /**
     * Sends stop in case of no adapter
     * @param params the params
     */
    public void fireStop( Map<String, String> params )
    {
        if ( isInitiated )
        {
            stopListener(params);
            isInitiated = false;
        }
    }

    private void startListener( Map<String, String> params )
    {
        if ( !isInitiated && !isStarted )
        {
            viewTransform.nextView();
            initComm();
            startPings();
            remoteMonitoring.startRemoteMonitoringTimer();
            startCdnPing();
        }

        startResourceParsing();

        if ( (isInitiated && getAdapter() != null && getAdapter().getFlags().isJoined() &&
              !isStarted && isExtraMetadataReady()) || (getOptions().isOffline() && !isStarted) )
        {
            sendStart(params);
        }

        if ( !isInitiated && !getOptions().isForceInit() &&
             getTitle() != null && getResource() != null && isLiveOrNotNullDuration() &&
             !isStarted && isExtraMetadataReady() )
        {
            sendStart(params);
        }
        else if ( !isInitiated )
        {
            fireInit(params);
        }
    }

    private boolean isLiveOrNotNullDuration()
    {
        return getIsLive() || (getDuration() != null && getDuration() != 0.0);
    }

    /**
     * Sends /init service. Should be called once the user has requested the content.
     *
     * Does not need an adapter or player to work.
     *
     * @param params Map of params to add to the request.
     */
    private void sendStart( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, START);
        send(willSendStartListeners, START, params);

        String titleOrResource = getTitle();

        if ( titleOrResource == null )
            titleOrResource = getResource();

        YouboraLog.notice(START + " " + titleOrResource);
        isStarted = true;

        /**
          * Send Product Analytics trackStart here to ensure it happens right after the START event.
          * This code inside an onStart listener might be called after an INIT event.
          */

        if ( this.getProductAnalytics() != null ){
            this.getProductAnalytics().adapterTrackStart();
        }
    }

    private void joinListener( Map<String, String> params )
    {
        if ( adsAdapter == null || !adsAdapter.getFlags().isStarted() )
        {
            if ( isInitiated && !isStarted && !getOptions().getWaitForMetadata() )
                sendStart(params);

            sendJoin(params);
        }
        else
        {
            // Revert join state
            if ( adapter != null )
            {
                if (adapter.getMonitor() != null) adapter.getMonitor().stop();
                adapter.getFlags().setJoined(false);
                adapter.getChronos().getJoin().setStopTime(null);
            }
        }
    }

    private void sendJoin( Map<String, String> params )
    {
        if (adsAdapter != null) adsAdapter.fireAdBreakStop();

        params = requestBuilder.buildParams(params, JOIN);
        send(willSendJoinListeners, JOIN, params);
        YouboraLog.notice(JOIN + " " + params.get("joinDuration") + "ms");
    }

    private void pauseListener( Map<String, String> params )
    {
        if ( adapter != null )
        {
            if ( adapter.getFlags().isBuffering() || adapter.getFlags().isSeeking() ||
                 (adsAdapter != null && adsAdapter.getFlags().isStarted()) )
            {
                adapter.getChronos().getPause().reset();
            }
        }

        sendPause(params);
    }

    private void sendPause( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, PAUSE);
        send(willSendPauseListeners, PAUSE, params);
        YouboraLog.notice(PAUSE + " at " + params.get("playhead") + "s");
    }

    private void resumeListener( Map<String, String> params )
    {
        sendResume(params);
    }

    private void sendResume( Map<String, String> params )
    {
        if ( adsAdapter != null && adsAdapter.getAdFlags().isAdBreakStarted() )
        {
            adsAdapter.fireAdBreakStop();
        }
        params = requestBuilder.buildParams(params, RESUME);
        send(willSendResumeListeners, RESUME, params);
        YouboraLog.notice(RESUME + " " + params.get("pauseDuration") + "ms");

    }

    private void seekBeginListener()
    {
        // Instead of reset the pause chronos, pause the chronos, to report pause time on scenario when user is pause the content, and later, do a seek, for example
        if ( adapter != null && adapter.getFlags().isPaused() )
            adapter.getChronos().getPause().pause();

        YouboraLog.notice("Seek Begin");
    }

    private void seekEndListener( Map<String, String> params )
    {
        sendSeekEnd(params);
    }

    private void sendSeekEnd( Map<String, String> params )
    {
        YouboraLog.debug("params: " + params);
        params = requestBuilder.buildParams(params, SEEK);
        send(willSendSeekListeners, SEEK, params);
        YouboraLog.notice(SEEK + " to " + params.get("playhead") + " in " + params.get("seekDuration") + "ms");
    }

    private void bufferBeginListener()
    {
        // Instead of reset the pause chronos, pause the chronos, to report pause time on scenario when user is pause the content, and later, do a seek, for example
        if ( adapter != null && adapter.getFlags().isPaused() )
            adapter.getChronos().getPause().pause();

        YouboraLog.notice("Buffer begin");
    }

    private void bufferEndListener( Map<String, String> params )
    {
        sendBufferEnd(params);
    }

    private void sendBufferEnd( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, BUFFER);

        Map<String, String> changedEntities = requestBuilder.getChangedEntities();
        if (changedEntities != null && !changedEntities.isEmpty()) {
            params.put("entities", YouboraUtil.stringifyMap(changedEntities));
        }

        send(willSendBufferListeners, BUFFER, params);
        YouboraLog.notice(BUFFER + " to " + params.get("playhead") + " in " + params.get("bufferDuration") + "ms");
    }

    private void errorListener( Map<String, String> params )
    {
        boolean isFatal = "fatal".equals(params.get("errorLevel"));

        params.remove("errorLevel");

        sendError(params);

        if (isFatal) reset();
    }

    private void sendError( Map<String, String> params )
    {
        fireInit();
        params = requestBuilder.buildParams(params, ERROR);

        Map<String, String> changedEntities = requestBuilder.getChangedEntities();
        if (changedEntities != null && !changedEntities.isEmpty()) {
            params.put("entities", YouboraUtil.stringifyMap(changedEntities));
        }

        send(willSendErrorListeners, ERROR, params);
        YouboraLog.notice(ERROR + " " + " " + params.get("errorCode"));
    }

    private void videoEventListener( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, VIDEO_EVENT);
        send(willSendVideoEventListeners, VIDEO_EVENT, params);
        YouboraLog.notice(VIDEO_EVENT + " " + " " + params.get("name"));
    }

    private void stopListener( Map<String, String> params )
    {
        sendStop(params);
        reset();
    }

    private void sendStop( Map<String, String> params )
    {
        if (adsAdapter != null) adsAdapter.fireAdBreakStop();
        params = requestBuilder.buildParams(params, STOP);

        Map<String, String> changedEntities = requestBuilder.getChangedEntities();
        if (changedEntities != null && !changedEntities.isEmpty()) {
            params.put("entities", YouboraUtil.stringifyMap(changedEntities));
        }

        send(willSendStopListeners, STOP, params);
        requestBuilder.getLastSent().put("breakNumber", null);
        requestBuilder.getLastSent().put("adNumber", null);
        YouboraLog.notice(STOP + " at " + params.get("playhead"));
    }

    // Ads
    private void adInitListener( Map<String, String> params )
    {
        if ( adapter != null && adsAdapter != null )
        {
            adapter.fireSeekEnd();
            adapter.fireBufferEnd();

            if ( adapter.getFlags().isPaused() )
                adapter.getChronos().getPause().reset();
        }

        if ( adsAdapter != null )
        {
            adsAdapter.fireManifest();
            adsAdapter.fireAdBreakStart();
        }

        sendAdInit(params);
    }

    private void sendAdInit( Map<String, String> params )
    {
        String realNumber          = requestBuilder.getNewAdNumber();
        String realAdNumberInBreak = requestBuilder.getNewAdNumberInBreak();
        params = requestBuilder.buildParams(params, AD_INIT);
        params.put("adNumber", realNumber);
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        params.put("adNumberInBreak", realAdNumberInBreak);
        params.put("adDuration", "0");
        params.put("adPlayhead", "0");
        adsAdapter.getAdFlags().setAdInitiated(true);
        send(willSendAdInitListeners, AD_INIT, params);

        YouboraLog.notice(AD_INIT + " " + params.get("position") + params.get("adNumber") +
                          " at " + params.get("playhead") + "s");
    }

    private void adStartListener( Map<String, String> params )
    {
        if ( !isInitiated && !isStarted && !getAdPosition().equals("post") )
        {
            fireInit();
            if (adapter != null) adapter.fireStart();
        }

        if ( adapter != null )
        {
            //adapter.fireStart();
            adapter.fireSeekEnd();
            adapter.fireBufferEnd();

            if ( adapter.getFlags().isPaused() )
                adapter.getChronos().getPause().reset();
        }

        adsAdapter.fireManifest();
        adsAdapter.fireAdBreakStart();

        if ( getAdDuration() != null && getAdTitle() != null && getAdResource() != null &&
             !adsAdapter.getAdFlags().isAdInitiated() )
            sendAdStart(params);
        else if ( !adsAdapter.getAdFlags().isAdInitiated() )
            sendAdInit(params);
    }

    private void sendAdStart( Map<String, String> params )
    {
        startPings();

        String realNumber = adsAdapter.getAdFlags().isAdInitiated() ?
                            requestBuilder.getLastSent().get("adNumber") : requestBuilder.getNewAdNumber();

        String realAdNumberInBreak = adsAdapter.getAdFlags().isAdInitiated() ?
                                     requestBuilder.getLastSent().get("adNumberInBreak") : requestBuilder.getNewAdNumberInBreak();

        params = requestBuilder.buildParams(params, AD_START);
        params.put("adNumber", realNumber);
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        params.put("adNumberInBreak", realAdNumberInBreak);
        send(willSendAdStartListeners, AD_START, params);

        YouboraLog.notice(AD_START + " " + params.get(RequestParams.AD_POSITION) +
                          params.get("adNumber") + " at " + params.get("playhead") + "s");

        isAdStarted = true;
    }

    private void adJoinListener( Map<String, String> params )
    {
        if ( adsAdapter.getAdFlags().isAdInitiated() && !isAdStarted )
            sendAdStart(params);

        sendAdJoin(params);
    }

    private void sendAdJoin( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, AD_JOIN);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        params.put("adNumberInBreak", requestBuilder.getLastSent().get("adNumberInBreak"));
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));

        if ( isAdConnected )
        {
            adsAdapter.getChronos().getJoin().setStartTime(adConnectedTime);
            adsAdapter.getChronos().getTotal().setStartTime(adConnectedTime);
            isAdConnected = false;
        }

        adsAdapter.getChronos().getAdViewability().start();

        send(willSendAdJoinListeners, AD_JOIN, params);
        YouboraLog.notice(AD_JOIN + " " + params.get("adJoinDuration") + "ms");
    }

    private void adPauseListener( Map<String, String> params )
    {
        sendAdPause(params);
    }

    private void sendAdPause( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, AD_PAUSE);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        params.put("adNumberInBreak", requestBuilder.getLastSent().get("adNumberInBreak"));
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        send(willSendAdPauseListeners, AD_PAUSE, params);
        YouboraLog.notice(AD_PAUSE + " at " + params.get("adPlayhead") + "s");
    }

    private void adResumeListener( Map<String, String> params )
    {
        sendAdResume(params);
    }

    private void sendAdResume( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, AD_RESUME);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        params.put("adNumberInBreak", requestBuilder.getLastSent().get("adNumberInBreak"));
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        params.put(RequestParams.AD_POSITION,
                requestBuilder.getLastSent().get(RequestParams.AD_POSITION));

        send(willSendAdResumeListeners, AD_RESUME, params);
        YouboraLog.notice(AD_RESUME + " " + params.get("adPauseDuration") + "ms");
    }

    private void adBufferBeginListener()
    {
        if ( adsAdapter != null && adsAdapter.getFlags().isPaused() )
            adsAdapter.getChronos().getPause().reset();

        YouboraLog.notice("Ad Buffer Begin");
    }

    private void adBufferEndListener( Map<String, String> params )
    {
        sendAdBufferEnd(params);
    }

    private void sendAdBufferEnd( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, AD_BUFFER);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        params.put("adNumberInBreak", requestBuilder.getLastSent().get("adNumberInBreak"));
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        params.put(RequestParams.AD_POSITION,
                requestBuilder.getLastSent().get(RequestParams.AD_POSITION));

        send(willSendAdBufferListeners, AD_BUFFER, params);
        YouboraLog.notice(AD_BUFFER + " " + params.get("adBufferDuration") + "s");
    }

    private void adStopListener( Map<String, String> params )
    {
        // Remove time from joinDuration, "delaying" the start time

        if ( !(adapter != null && adapter.getFlags().isJoined()) && adsAdapter != null )
        {
            // Chrono realJoinChrono = isInitiated ? initChrono : adapter.getChronos().join;
            Chrono realJoinChrono = initChrono;

            if ( adapter != null && adapter.getChronos() != null && !isInitiated )
                realJoinChrono = adapter.getChronos().getJoin();

            Long startTime = realJoinChrono.getStartTime();

            if (startTime == null) startTime = Chrono.getNow();

            Long adDeltaTime = adsAdapter.getChronos().getTotal().getDeltaTime();

            if (adDeltaTime == -1) adDeltaTime = Chrono.getNow();

            startTime = Math.min(startTime + adDeltaTime, Chrono.getNow());
            realJoinChrono.setStartTime(startTime);
        }

        sendAdStop(params);
    }

    private void sendAdStop( Map<String, String> params )
    {
        stopAdViewabilityChrono();
        params = requestBuilder.buildParams(params, AD_STOP);
        adsAdapter.getChronos().getAdViewedPeriods().clear();
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        params.put("adNumberInBreak", requestBuilder.getLastSent().get("adNumberInBreak"));
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        send(willSendAdStopListeners, AD_STOP, params);
        YouboraLog.notice(AD_STOP + " " + params.get("adTotalDuration") + "ms");
        isAdStarted = false;
        isAdConnected = true;
        adConnectedTime = Chrono.getNow();
    }

    private void adClickListener( Map<String, String> params )
    {
        sendAdClick(params);
    }

    private void sendAdClick( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, AD_CLICK);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        params.put("adNumberInBreak", requestBuilder.getLastSent().get("adNumberInBreak"));
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        params.put(RequestParams.AD_POSITION,
                requestBuilder.getLastSent().get(RequestParams.AD_POSITION));

        send(willSendAdClickListeners, AD_CLICK, params);
        YouboraLog.notice(AD_CLICK + " " + params.get("adPlayhead") + "ms");
    }

    private void adErrorListener( Map<String, String> params )
    {
        sendAdError(params);
    }

    private void sendAdError( Map<String, String> params )
    {
        if (!isInitiated && !isStarted) initComm();

        startResourceParsing();

        String realNumber = adsAdapter.getAdFlags().isAdInitiated() ||
                            adsAdapter.getFlags().isStarted() ? requestBuilder.getLastSent().get("adNumber") :
                            requestBuilder.getNewAdNumber();

        String realAdNumberInBreak = adsAdapter.getAdFlags().isAdInitiated() ||
                                     adsAdapter.getFlags().isStarted() ? requestBuilder.getLastSent().get("adNumberInBreak") :
                                     requestBuilder.getNewAdNumberInBreak();

        String realBreakNumber = adsAdapter.getAdFlags().isAdBreakStarted() ?
                                 requestBuilder.getLastSent().get("breakNumber") :
                                 requestBuilder.getNewAdBreakNumber();

        params = requestBuilder.buildParams(params, AD_ERROR);
        params.put("adNumber", realNumber);
        params.put("breakNumber", realBreakNumber);
        params.put("adNumberInBreak", realAdNumberInBreak);
        send(willSendAdErrorListeners, AD_ERROR, params);
        YouboraLog.notice(AD_ERROR + " " + " " + params.get("errorCode"));
    }

    private void adManifestListener( Map<String, String> params )
    {
        if (!isAdsManifestSent && (isInitiated || isStarted)) sendAdManifest(params);
    }

    private void sendAdManifest( Map<String, String> params )
    {
        isAdsManifestSent = true;

        params = requestBuilder.buildParams(params, AD_MANIFEST);
        params.put("adManifest", requestBuilder.getLastSent().get("adManifest"));
        send(willSendAdManifestListeners, AD_MANIFEST, params);
        YouboraLog.notice(AD_MANIFEST + " " + " " + params.get("adManifest"));
    }

    private void adBreakStartListener( Map<String, String> params )
    {
        sendAdBreakStart(params);
    }

    private void sendAdBreakStart( Map<String, String> params )
    {
        if (adapter != null) adapter.firePause();
        String newAdBreakNumber = requestBuilder.getNewAdBreakNumber();
        params = requestBuilder.buildParams(params, AD_BREAK_START);
        params.put("breakNumber", newAdBreakNumber);
        send(willSendAdBreakStartListeners, AD_BREAK_START, params);
        YouboraLog.notice(AD_BREAK_START + " " + " " + params.get("adManifest"));
        isAdBreakStarted = true;
    }

    private void adBreakStopListener( Map<String, String> params )
    {
        sendAdBreakStop(params);
    }

    private void sendAdBreakStop( Map<String, String> params )
    {
        if (adsAdapter != null) adsAdapter.fireStop();

        isAdConnected = false;
        params = requestBuilder.buildParams(params, AD_BREAK_STOP);
        params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
        params.put(RequestParams.AD_POSITION,
                requestBuilder.getLastSent().get(RequestParams.AD_POSITION));

        send(willSendAdBreakStopListeners, AD_BREAK_STOP, params);
        YouboraLog.notice(AD_BREAK_STOP + " " + " " + params.get("adManifest"));

        if ( requestBuilder.getLastSent().get(RequestParams.AD_POSITION) != null &&
             requestBuilder.getLastSent().get(RequestParams.AD_POSITION).equals("post") )
        {
            requestBuilder.getLastSent().put("breakNumber", null);
            fireStop();
        }

        isAdBreakStarted = false;

        if (adapter != null) adapter.fireResume();
    }

    private void adQuartileListener( Map<String, String> params )
    {
        sendAdQuartile(params);
    }

    private void sendAdQuartile( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, AD_QUARTILE);

        if ( params.get("quartile") != null )
        {
            params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
            params.put("adNumberInBreak", requestBuilder.getLastSent().get("adNumberInBreak"));
            params.put(RequestParams.AD_POSITION,
                    requestBuilder.getLastSent().get(RequestParams.AD_POSITION));

            params.put("breakNumber", requestBuilder.getLastSent().get("breakNumber"));
            send(willSendAdQuartileListeners, AD_QUARTILE, params);
            YouboraLog.notice(AD_QUARTILE + " " + " " + params.get("adManifest"));
        }
    }

    // ----------------------------------------- Infinity listeners --------------------------------

    private void sessionStartListener( String screenName, Map<String, String> dimensions )
    {
        viewTransform.nextView();

        startScreenName = screenName;
        startDimensions = dimensions;

        Map<String, String> params = new LinkedHashMap<>();
        params.put("dimensions", YouboraUtil.stringifyMap(dimensions));
        params.put("page", screenName);
        params.put("route", screenName);

        registerForActivityCallbacks();

        sendSessionStart(params);
    }

    private void sendSessionStart( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, SESSION_START);
        sendInfinity(willSendSessionStartListeners, SESSION_START, params);
        startBeats();
        YouboraLog.notice(SESSION_START);
    }

    private void sessionStopListener( Map<String, String> params )
    {
        unregisterForActivityCallbacks();
        sendSessionStop(params);
    }

    private void sendSessionStop( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, SESSION_STOP);
        sendInfinity(willSendSessionStopListeners, SESSION_STOP, params);
        stopBeats();
        YouboraLog.notice(SESSION_STOP);
        initializeViewTransform(null);
        infinity = null;

        if ( productAnalytics != null ){
            productAnalytics.setInfinity(getInfinity());
        }
    }

    private void sessionNavListener( String screenName )
    {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("page", screenName);
        params.put("route", screenName);
        sendSessionNav(params);
    }

    private void sendSessionNav( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, SESSION_NAV);
        sendInfinity(willSendSessionNavListeners, SESSION_NAV, params);
        YouboraLog.notice(SESSION_NAV);

        if ( beatTimer != null )
        {
            long time = beatTimer.getChrono().getStartTime() != null ?
                        (Chrono.getNow() - beatTimer.getChrono().getStartTime()) : 0;
            sendBeat(time);
            beatTimer.getChrono().setStartTime(Chrono.getNow());
        }
    }

    private void sessionEventListener( Map<String, String> dimensions, Map<String, Double> values,
            String eventName, Map<String, String> topLevelDimensions )
    {
        topLevelDimensions.put("dimensions", YouboraUtil.stringifyMap(dimensions));
        topLevelDimensions.put("values", YouboraUtil.stringifyMap(values));
        topLevelDimensions.put("name", eventName);
        sendSessionEvent(topLevelDimensions);
    }

    private void sendSessionEvent( Map<String, String> params )
    {
        params = requestBuilder.buildParams(params, SESSION_EVENT);
        sendInfinity(willSendSessionEventListeners, SESSION_EVENT, params);
        YouboraLog.notice(SESSION_EVENT);
    }

    // ----------------------------------------- BEATS ---------------------------------------------
    private void startBeats()
    {
        if ( !beatTimer.isRunning() )
        {
            beatTimer.start();
        }
    }

    private void stopBeats()
    {
        beatTimer.stop();
    }

    private void sendBeat( long diffTime )
    {
        if ( viewTransform.fastDataConfig.code != null )
        {
            Map<String, String> params = new HashMap<>();
            params.put("diffTime", Long.toString(diffTime));

            List<String> paramList = new LinkedList<>();
            paramList.add("sessions");
            params = requestBuilder.fetchParams(params, paramList, false);

            sendInfinity(willSendSessionBeatListeners, SESSION_BEAT, params);
            YouboraLog.debug(SESSION_BEAT);
        }
    }

    // ----------------------------------------- CDN ---------------------------------------------
    private void startCdnPing()
    {
        if ( !cdnTimer.isRunning() )
        {
            cdnTimer.start();
        }
    }

    private void stopCdnPing()
    {
        cdnTimer.stop();
    }

    private void sendCdnPing()
    {
        if ( hasCdnPingInfo() )
        {
            Map<String, String> params = new HashMap<>();

            params = requestBuilder.buildParams(params, CDN_PING);
            send(willSendCdnListeners, CDN_PING, params);
            YouboraLog.debug(CDN_PING);
        }

        YouboraUtil.getCdnHandler().post(() -> cdnBalancerInfo.updateBalancerStats(context));
    }

    // ----------------------------------------- PINGS ---------------------------------------------
    private void startPings()
    {
        if ( !pingTimer.isRunning() )
        {
            pingTimer.start();
        }
    }

    private void stopPings()
    {
        pingTimer.stop();
    }

    private void sendPing(long diffTime) {
        Map<String, String> params = new HashMap<>();
        params.put("diffTime", Long.toString(diffTime));

        Map<String, String> changedEntities = requestBuilder.getChangedEntities();
        if (changedEntities != null && !changedEntities.isEmpty()) {
            params.put("entities", YouboraUtil.stringifyMap(changedEntities));
        }

        List<String> paramList = new LinkedList<>();

        if (adapter != null) {
            BaseFlags adapterFlags = adapter.getFlags();

            if (adapterFlags.isPaused()) {
                paramList.add("pauseDuration");
            } else {
                paramList.add("bitrate");
                paramList.add("throughput");
                paramList.add("fps");

                if (adsAdapter != null) {
                    BaseFlags adsAdapterFlags = adsAdapter.getFlags();
                    if (adsAdapterFlags.isStarted()){
                        paramList.add("adBitrate");
                    }
                }
            }

            if (adapterFlags.isJoined()) paramList.add("playhead");
            if (adapterFlags.isBuffering()) paramList.add("bufferDuration");
            if (adapterFlags.isSeeking()) paramList.add("seekDuration");

            if (adapter.getIsP2PEnabled() != null && adapter.getIsP2PEnabled()) {
                paramList.add("p2pDownloadedTraffic");
                paramList.add("cdnDownloadedTraffic");
                paramList.add("uploadTraffic");
            }
        }

        if (adsAdapter != null) {
            BaseFlags adsAdapterFlags = adsAdapter.getFlags();

            if (adsAdapterFlags.isStarted()) {
                paramList.add("adPlayhead");
                paramList.add(AD_VIEWABILITY);
                paramList.add(AD_VIEWED_DURATION);
            }

            if (adsAdapterFlags.isBuffering()) paramList.add("adBufferDuration");
            if (adsAdapterFlags.isPaused()) paramList.add("adPauseDuration");
        }

        params = requestBuilder.fetchParams(params, paramList, false);

        send(willSendPingListeners, PING, params);
        YouboraLog.debug(PING);
    }
    // ------------------------------------ REMOTE MONITORING ---------------------------------------
    RemoteMonitoring createRemoteMonitoring( RemoteMonitoringListener listener, Plugin plugin) {
        return new RemoteMonitoring(listener, plugin);
    }

    private RemoteMonitoringListener remoteMonitoringListener = new RemoteMonitoringListener() {

        /**
         * onSend RemoteMonitoringListener
         */
        @Override
        public void onSend(@NotNull String data) {
            Map<String, String> params = new HashMap();
            JSONObject logsData = new JSONObject();
            try {
                logsData.put("data", data);
                params.put("logs", logsData.toString());
                sendRemoteDebug(params);
            } catch (JSONException e) {
                YouboraLog.error("Error RemoteMonitoringListener.onSend creating logs data.");
            }

        }
    };


    /**
     * Send logs to remote logs endpoint
     */
    private void sendRemoteDebug(Map<String, String> params) {

        send(null, PLUGIN_LOGS, params, Request.METHOD_POST, null, null, null, true);

    }
    // ------------------------------------- INFO GETTERS ------------------------------------------
    /**
     * Returns the host where all the NQS traces are sent.
     * @return the gost
     */
    public String getHost() {
        return YouboraUtil.addProtocol(YouboraUtil.stripProtocol(options.getHost()), options == null || options.isHttpSecure());
    }

    public String getLinkedViewId() { return options.getLinkedViewId(); }

    /**
     * Returns the parse HLS flag
     * @return the parse HLS flag
     * @deprecated use {@link #isParseManifest()} instead.
     */
    public boolean isParseHls() {
        return options.isParseHls();
    }

    /**
     * @deprecated use {@link #isParseManifest()} instead.
     */
    public boolean isParseDash() { return options.isParseDash(); }

    /**
     * @return The parse location header flag.
     * @deprecated use {@link #isParseManifest()} instead.
     */
    public boolean isParseLocationHeader() { return options.isParseLocationHeader(); }

    /**
     * @return The parse manifest flag.
     */
    public boolean isParseManifest() { return options.isParseManifest(); }

    /**
     * Returns the parse Cdn node flag
     * @return the parse Cdn node flag
     */
    public boolean isParseCdnNode() {
        return options.isParseCdnNode();
    }

    /**
     * Returns the list of Cdn names that are enabled for the {@link ResourceTransform}.
     * @return the cdn list
     */
    public List<String> getParseCdnNodeList() {
        return options.getParseCdnNodeList();
    }

    public Map<String, String> getParseManifestAuth() {
        return YouboraUtil.bundleToMap(options.getParseManifestAuth());
    }

    /**
     * Returns the Cdn name header. This value is later passed to
     * {@link com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser#setBalancerHeaderName(ArrayList<String>)}.
     * @return the cdn name header.
     */
    public String getParseCdnNodeNameHeader() {
        return options.getParseCdnNameHeader();
    }

    /**
     * Returns the Cdn name headers. This value is later passed to
     * {@link com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser#setBalancerHeaderName(ArrayList<String>)}.
     * @return the cdn name header.
     */
    public ArrayList<String> getParseCdnNodeNameHeaderList() {
        return options.getParseCdnNameHeaderList();
    }

    /**
     * Returns the node header. This value is later passed to
     * {@link com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser#setBalancerNodeHeader(String)}.
     * @return the node header.
     */
    public String getParseNodeHeader() {
        return options.getParseNodeHeader();
    }

    /**
     * Returns the content's playhead in seconds
     * @return the content's playhead
     */
    public Double getPlayhead() {
        Double ph = null;
        if (adapter != null) {
            try {
                ph = adapter.getPlayhead();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayhead");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(ph, 0.0);
    }

    /**
     * Returns the content's Playrate
     * @return the content's Playrate
     */
    public Double getPlayrate() {
        Double val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayrate");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 1.0);
    }

    /**
     * Returns the content's FPS
     * @return the content's FPS
     */
    public Double getFramesPerSecond() {
        Double val = options.getContentFps();
        if (val == null && adapter != null) {
            try {
                val = adapter.getFramesPerSecond();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getFramesPerSecond");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's dropped frames
     * @return the content's dropped frames
     */
    public Integer getDroppedFrames() {
        Integer val = null;
        if (adapter != null) {
            try {
                val = adapter.getDroppedFrames();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getDroppedFrames");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, 0);
    }

    /**
     * Returns the content's duration in seconds
     * @return the content's duration
     */
    public Double getDuration() {
        Double val = options.getContentDuration();
        if (val == null && adapter != null) {
            try {
                if(getIsLive() || adapter.getDuration() == null){
                    val = 0.0;
                }else{
                    val = adapter.getDuration();
                }
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getDuration");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns the content's bitrate in bits per second
     * @return the content's bitrate
     */
    public Long getBitrate() {
        Long val = options.getContentBitrate();
        if (val == null && adapter != null) {
            try {
                val = adapter.getBitrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getBitrate");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    public Long getTotalBytes() {
        Long val = null;

        if (options.getContentSendTotalBytes()) {
            val = options.getContentTotalBytes();

            if (val == null && adapter != null) {
                val = adapter.getTotalBytes();
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns the content's throughput in bits per second
     * @return the content's throughput
     */
    public Long getThroughput() {
        Long val = options.getContentThroughput();
        if (val == null && adapter != null) {
            try {
                val = adapter.getThroughput();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getThroughput");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns the content's rendition
     * @return the content's rendition
     */
    public String getRendition() {
        String val = options.getContentRendition();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getRendition();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getRendition");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's title
     * @return the content's title
     */
    public String getTitle() {
        String val = options.getContentTitle();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getTitle();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getTitle");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns content's program
     * @return program
     */
    public String getProgram() {
        String val = options.getProgram();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getProgram();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getProgram");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns defined streaming protocol
     * @return the content's streaming protocol
     */
    public String getStreamingProtocol() {
        return options.getContentStreamingProtocol();
    }

    public String getTransportFormat() {
        String val = options.getTransportFormat();

        if (val == null && options.isParseManifest()
            && !resourceTransform.isBlocking(null)) {
            val = resourceTransform.getTransportFormat();
        }

        return val;
    }

    /**
     * Returns whether the content is live or not
     * @return whether the content is live or not
     */
    public boolean getIsLive() {
        Boolean val = getIsLiveOrNull();
        return val != null ? val : false;
    }

    /**
     * Returns whether the content is live or not, null if unknown
     * @return whether the content is live or not, null if unknown
     */
    private Boolean getIsLiveOrNull() {
        Boolean val = options.getContentIsLive();
        if (val == null && adapter != null) {
            try {
                val = adapter.getIsLive();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getIsLive");
                YouboraLog.error(e);
            }
        }
        return val;
    }

    /**
     * Returns the content's resource
     * @return the content's resource
     */
    public String getResource() {
        String val = options.getContentResource();

        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getResource();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getResource");
                YouboraLog.error(e);
            }
        }

        if (val != null && val.length() == 0) {
            val = null;
        }

        return val;
    }

    public String getParsedResource() {
        String val = null;

        if (!resourceTransform.isBlocking(null)) {
            val = resourceTransform.getResource();
        }

        if(val == null && adapter != null && adapter.getUrlToParse() != null) val = getUrlToParse();

        if (val == getResource()) val = null;

        return val;
    }

    /**
     * Returns the transaction code
     * @return the transaction code
     */
    public String getTransactionCode() {
        return options.getContentTransactionCode();
    }

    /**
     * Returns experiments list
     * @return all set experiments
     */
    public ArrayList<String> getExperimentIds(){
        return options.getExperimentIds();
    }

    /**
     * Returns the content metadata
     * @return the content metadata
     */
    public String getContentMetadata() {
        return YouboraUtil.stringifyBundle(options.getContentMetadata());
    }

    /**
     * Returns the version of the player that is used to play the content
     * @return the player version
     */
    public String getPlayerVersion() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayerVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayerVersion");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the name of the player that is used to play the content
     * @return the player name
     */
    public String getPlayerName() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayerName();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayerName");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the content cdn
     * @return the content cdn
     */
    public String getCdn() {
        String cdn = null;
        if (!resourceTransform.isBlocking(null)) {
            cdn = resourceTransform.getCdnName();
        }
        if (cdn == null) {
            cdn = options.getContentCdn();
        }
        return cdn;
    }

    /**
     * Returns current latency from player
     * @return current video latency
     */
    public Double getLatency() {
        Double val = null;

        if (adapter != null && getIsLive()) {
            try {
                val = adapter.getLatency();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getLatency");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns current packages being lost
     * @return current video latency
     */
    public Integer getPacketLoss() {
        Integer val = null;
        if (adapter != null) {
            try {
                val = adapter.getPacketLoss();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPacketLoss");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0);
    }

    /**
     * Returns current packages being send
     * @return current video latency
     */
    public Integer getPacketSent(){
        Integer val = null;
        if (adapter != null) {
            try {
                val = adapter.getPacketSent();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPacketLoss");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0);
    }

    public String getUrlToParse() {
        String val = options.getUrlToParse();

        if ((val == null || val.length() == 0) && adapter != null) {
            val = adapter.getUrlToParse();
        }

        if (val != null && val.length() == 0) {
            val = null;
        }

        return val;
    }

    /**
     * Returns display's Extended Display Identification Data (EDID).
     * @return EDID
     */
    public String getDeviceEDID() { return options.getDeviceEDID(); }

    /**
     * Returns the video metrics sent on the pings
     * @return metrics
     */
    public String getVideoMetrics() {
        String metrics = YouboraUtil.stringifyBundle(formatMetrics(options.getContentMetrics()));

        if ((metrics == null || metrics.length() == 0) && adapter != null) {
            try {
                metrics = YouboraUtil.stringifyMap(adapter.getMetrics());
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getVideoMetrics");
                YouboraLog.error(e);
            }
        }

        return metrics;
    }

    public String getAdViewability() {
        Long biggestValue = 0L;
        PlaybackChronos chronos = adsAdapter.getChronos();

        biggestValue = chronos.getAdViewability().getDeltaTime(false);
        for (Long period : adsAdapter.getChronos().getAdViewedPeriods()) {
            if (period > biggestValue) biggestValue = period;
        }

        return biggestValue.toString();
    }

    public String getAdViewedDuration() {
        Long summatory = 0L;
        PlaybackChronos chronos = adsAdapter.getChronos();

        summatory = chronos.getAdViewability().getDeltaTime(false);
        for (Long period : adsAdapter.getChronos().getAdViewedPeriods()) {
            summatory += period;
        }

        return summatory.toString();
    }

    private Bundle formatMetrics(Bundle origBundle) {
        if (origBundle != null) {
            Bundle realBundle = new Bundle();
            for(String key: origBundle.keySet()) {
                Object value = origBundle.get(key);
                Bundle valueBundle = new Bundle();
                if (value != null) {
                    if (value instanceof Float) {
                        valueBundle.putFloat("value", (float)value);
                    } else if (value instanceof Double) {
                        valueBundle.putDouble("value", (double)value);
                    } else if (value instanceof Integer) {
                        valueBundle.putInt("value", (int)value);
                    } else if (value instanceof Long) {
                        valueBundle.putLong("value", (long)value);
                    } else if (value instanceof String) {
                        valueBundle.putString("value", (String)value);
                    }
                }
                realBundle.putBundle(key, valueBundle);
            }
            return realBundle;
        }
        return null;
    }

    /**
     * Returns the session metrics sent on the beats
     * @return The session metrics.
     */
    public String getSessionMetrics() {
        return YouboraUtil.stringifyBundle(options.getSessionMetrics());
    }

    /**
     * Returns the LibVersion
     * @return the LibVersion
     */
    public String getLibVersion() {
        return BuildConfig.VERSION_NAME;
    }

    /**
     * Returns the PluginVersion
     * @return the PluginVersion
     */
    public String getPluginVersion() {
        String ret = getAdapterVersion();
        if (ret == null) {
            ret = BuildConfig.VERSION_NAME + "-adapterless-Android";
        }
        return ret;
    }

    /**
     * Returns the content {@link PlayerAdapter} version if available
     * @return the content Adapter version
     */
    private String getAdapterVersion() {
        String val = null;

        if (adapter != null) {
            val = adapter.getVersion() + "-Android";
        }

        return val;
    }

    public String getCustomDimensions() {
        return YouboraUtil.stringifyBundle(options.getContentCustomDimensions());
    }

    /**
     * Returns content's contentCustomDimension1
     * @return contentCustomDimension1
     */
    public String getContentCustomDimension1() { return options.getContentCustomDimension1(); }

    /**
     * Returns content's contentCustomDimension2
     * @return contentCustomDimension2
     */
    public String getContentCustomDimension2() { return options.getContentCustomDimension2(); }

    /**
     * Returns content's contentCustomDimension
     * @return contentCustomDimension3
     */
    public String getContentCustomDimension3() { return options.getContentCustomDimension3(); }

    /**
     * Returns content's contentCustomDimension4
     * @return contentCustomDimension4
     */
    public String getContentCustomDimension4() { return options.getContentCustomDimension4(); }

    /**
     * Returns content's contentCustomDimension5
     * @return contentCustomDimension5
     */
    public String getContentCustomDimension5() { return options.getContentCustomDimension5(); }

    /**
     * Returns content's contentCustomDimension6
     * @return contentCustomDimension6
     */
    public String getContentCustomDimension6() { return options.getContentCustomDimension6(); }

    /**
     * Returns content's contentCustomDimension7
     * @return contentCustomDimension7
     */
    public String getContentCustomDimension7() { return options.getContentCustomDimension7(); }

    /**
     * Returns content's contentCustomDimension8
     * @return contentCustomDimension8
     */
    public String getContentCustomDimension8() { return options.getContentCustomDimension8(); }

    /**
     * Returns content's contentCustomDimension9
     * @return contentCustomDimension9
     */
    public String getContentCustomDimension9() { return options.getContentCustomDimension9(); }

    /**
     * Returns content's contentCustomDimension10
     * @return contentCustomDimension10
     */
    public String getContentCustomDimension10() { return options.getContentCustomDimension10(); }

    /**
     * Returns content's contentCustomDimension11
     * @return contentCustomDimension11
     */
    public String getContentCustomDimension11() { return options.getContentCustomDimension11(); }

    /**
     * Returns content's contentCustomDimension12
     * @return contentCustomDimension12
     */
    public String getContentCustomDimension12() { return options.getContentCustomDimension12(); }

    /**
     * Returns content's contentCustomDimension13
     * @return contentCustomDimension13
     */
    public String getContentCustomDimension13() { return options.getContentCustomDimension13(); }

    /**
     * Returns content's contentCustomDimension14
     * @return contentCustomDimension14
     */
    public String getContentCustomDimension14() { return options.getContentCustomDimension14(); }

    /**
     * Returns content's contentCustomDimension15
     * @return contentCustomDimension15
     */
    public String getContentCustomDimension15() { return options.getContentCustomDimension15(); }

    /**
     * Returns content's contentCustomDimension16
     * @return contentCustomDimension16
     */
    public String getContentCustomDimension16() { return options.getContentCustomDimension16(); }

    /**
     * Returns content's contentCustomDimension17
     * @return contentCustomDimension17
     */
    public String getContentCustomDimension17() { return options.getContentCustomDimension17(); }

    /**
     * Returns content's contentCustomDimension18
     * @return contentCustomDimension18
     */
    public String getContentCustomDimension18() { return options.getContentCustomDimension18(); }

    /**
     * Returns content's contentCustomDimension19
     * @return contentCustomDimension19
     */
    public String getContentCustomDimension19() { return options.getContentCustomDimension19(); }

    /**
     * Returns content's contentCustomDimension20
     * @return contentCustomDimension20
     */
    public String getContentCustomDimension20() { return options.getContentCustomDimension20(); }

    /**
     * Returns ad's customDimension1
     * @return adCustomDimension1
     */
    public String getAdCustomDimension1() { return options.getAdCustomDimension1(); }

    /**
     * Returns ad's customDimension2
     * @return adCustomDimension2
     */
    public String getAdCustomDimension2() { return options.getAdCustomDimension2(); }

    /**
     * Returns ad's customDimension
     * @return adCustomDimension3
     */
    public String getAdCustomDimension3() { return options.getAdCustomDimension3(); }

    /**
     * Returns ad's customDimension4
     * @return adCustomDimension4
     */
    public String getAdCustomDimension4() { return options.getAdCustomDimension4(); }

    /**
     * Returns ad's customDimension5
     * @return adCustomDimension5
     */
    public String getAdCustomDimension5() { return options.getAdCustomDimension5(); }

    /**
     * Returns ad's customDimension6
     * @return adCustomDimension6
     */
    public String getAdCustomDimension6() { return options.getAdCustomDimension6(); }

    /**
     * Returns ad's customDimension7
     * @return adCustomDimension7
     */
    public String getAdCustomDimension7() { return options.getAdCustomDimension7(); }

    /**
     * Returns ad's customDimension8
     * @return adCustomDimension8
     */
    public String getAdCustomDimension8() { return options.getAdCustomDimension8(); }

    /**
     * Returns ad's customDimension9
     * @return adCustomDimension9
     */
    public String getAdCustomDimension9() { return options.getAdCustomDimension9(); }

    /**
     * Returns ad's customDimension10
     * @return adCustomDimension10
     */
    public String getAdCustomDimension10() { return options.getAdCustomDimension10(); }

    /**
     * Returns the app name
     * @return appName
     */
    public String getAppName() { return options.getAppName(); }

    /**
     * Returns the app version
     * @return appReleaseVersion
     */
    public String getAppReleaseVersion() { return options.getAppReleaseVersion(); }

    /**
     * Returns the version of the player that is used to play the ad(s)
     * @return the player version
     */
    public String getAdPlayerVersion() {
        String val = null;

        if (adsAdapter != null) {
            try {
                val = adsAdapter.getPlayerVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPlayerVersion");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the ad position as YOUBORA expects it; "pre", "mid", "post" or "unknown"
     * @return the ad position
     */
    public String getAdPosition() {
        AdPosition pos = UNKNOWN;

        if (adsAdapter != null) pos = adsAdapter.getPosition();
        if (pos == AdPosition.UNKNOWN && adapter != null) pos = adapter.getFlags().isJoined() ? MID : PRE;

        String position;

        switch (pos) {
            case PRE:
                position = "pre";
                break;
            case MID:
                position = "mid";
                break;
            case POST:
                position = "post";
                break;
            case UNKNOWN:
            default:
                position = "unknown";
        }

        return position;
    }

    /**
     * Returns ad's playhead in seconds
     * @return ad's playhead
     */
    public Double getAdPlayhead() {
        Double ph = null;

        if (adsAdapter != null) {
            try {
                ph = adsAdapter.getPlayhead();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPlayhead");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(ph, 0.0);
    }

    /**
     * Returns ad's duration in seconds
     * @return ad's duration
     */
    public Double getAdDuration() {
        Double val = null;

        if (adsAdapter != null) {
            try {
                val = adsAdapter.getDuration();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdDuration");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns ad's bitrate in bits per second
     * @return ad's bitrate
     */
    public Long getAdBitrate() {
        Long val = null;

        if (adsAdapter != null) {
            try {
                val = adsAdapter.getBitrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdBitrate");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    public String getAdCampaign() {
        String adCampaign = options.getAdCampaign();

        if ((adCampaign == null || adCampaign.length() == 0) && adsAdapter != null)
            adCampaign = adsAdapter.getAdCampaign();

        return adCampaign;
    }

    public String getAdCreativeId() {
        String creativeId = options.getAdCreativeId();

        if ((creativeId == null || creativeId.length() == 0) && adsAdapter != null)
            creativeId = adsAdapter.getAdCreativeId();

        return creativeId;
    }

    public String getAdProvider() {
        String adProvider = options.getAdProvider();

        if((adProvider == null || adProvider.length() == 0) && adsAdapter != null)
            adProvider = adsAdapter.getAdProvider();

        return adProvider;
    }

    /**
     * Returns ad's title
     * @return ad's title
     */
    public String getAdTitle() {
        String val = options.getAdTitle();
        if ((val == null || val.length() == 0) && adsAdapter != null) {
            try {
                val = adsAdapter.getTitle();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdTitle");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns ad's resource
     * @return ad's resource
     */
    public String getAdResource() {
        String val = options.getAdResource();
        if ((val == null || val.length() == 0) && adsAdapter != null) {
            try {
                val = adsAdapter.getResource();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdResource");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the ad adapter version
     * @return the ad adapter version
     */
    public String getAdAdapterVersion() {
        String val = null;

        if (adsAdapter != null) {
            try {
                val = adsAdapter.getVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdAdapterVersion");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the ad metadata
     * @return the ad metadata
     */
    public String getAdMetadata() {
        return YouboraUtil.stringifyBundle(options.getAdMetadata());
    }

    public Integer getGivenBreaks() {
        Integer givenBreaks = options.getAdGivenBreaks();

        if (givenBreaks == null && adsAdapter != null) givenBreaks = adsAdapter.getGivenBreaks();

        return YouboraUtil.parseNumber(givenBreaks, 0);
    }

    public Integer getExpectedBreaks() {
        Integer expectedBreaks = options.getAdExpectedBreaks();

        if (expectedBreaks == null) {
            if (options.getAdExpectedPattern() != null) {
                expectedBreaks = options.getAdExpectedPattern().getIntegerArrayList("pre") != null ? 1 : 0;
                expectedBreaks += options.getAdExpectedPattern().getIntegerArrayList("mid") != null ?
                                  options.getAdExpectedPattern().getIntegerArrayList("mid").size() : 0;
                expectedBreaks += options.getAdExpectedPattern().getIntegerArrayList("post") != null ? 1 : 0;
            } else if (adsAdapter != null) {
                if (adsAdapter.getExpectedPattern() != null) {
                    expectedBreaks = adsAdapter.getExpectedPattern().get("pre") != null ? 1 : 0;
                    expectedBreaks += adsAdapter.getExpectedPattern().get("mid") != null ?
                                      ((List<Integer>) adsAdapter.getExpectedPattern().get("mid")).size() : 0;
                    expectedBreaks += adsAdapter.getExpectedPattern().get("post") != null ? 1 : 0;
                } else {
                    expectedBreaks = adsAdapter.getExpectedBreaks();
                }
            }
        }

        return YouboraUtil.parseNumber(expectedBreaks, 0);
    }

    public String getExpectedPattern() {
        String expectedPattern = YouboraUtil.stringifyBundle(options.getAdExpectedPattern());

        if (expectedPattern == null && adsAdapter != null)
            expectedPattern = YouboraUtil.stringifyMap(adsAdapter.getExpectedPattern());

        return expectedPattern;
    }

    public String getBreaksTime() {
        List breaksTime = options.getAdBreaksTime();

        if (breaksTime == null && adsAdapter != null) {
            breaksTime = adsAdapter.getBreaksTime();
        }

        return YouboraUtil.stringifyList(breaksTime);
    }

    public Integer getGivenAds() {
        Integer givenAds = options.getGivenAds();

        if (givenAds == null && adsAdapter != null) {
            givenAds = adsAdapter.getGivenAds();
        }

        return YouboraUtil.parseNumber(givenAds, 0);
    }

    /**
     * Returns the ads that are expected for the current break.
     */
    public Integer getExpectedAds() {
        Integer expectedAds = null;
        ArrayList<Integer> list = new ArrayList<>();

        if (options.getAdExpectedPattern() != null) {
            if (options.getAdExpectedPattern().getIntegerArrayList("pre") != null)
                list.add(options.getAdExpectedPattern().getIntegerArrayList("pre").get(0));

            if (options.getAdExpectedPattern().getIntegerArrayList("mid") != null)
                list.addAll(options.getAdExpectedPattern().getIntegerArrayList("mid"));

            if (options.getAdExpectedPattern().getIntegerArrayList("post") != null)
                list.add(options.getAdExpectedPattern().getIntegerArrayList("post").get(0));
        } else if (adsAdapter != null && adsAdapter.getExpectedPattern() != null) {
            if (adsAdapter.getExpectedPattern().get("pre") != null)
                list.add(((List<Integer>) adsAdapter.getExpectedPattern().get("pre")).get(0));

            if (adsAdapter.getExpectedPattern().get("mid") != null)
                list.addAll((List<Integer>) adsAdapter.getExpectedPattern().get("mid"));

            if (adsAdapter.getExpectedPattern().get("post") != null)
                list.add(((List<Integer>) adsAdapter.getExpectedPattern().get("post")).get(0));
        }

        if (list.size() > 0 && requestBuilder.getLastSent().get("breakNumber") != null) {
            int position = Integer.parseInt(requestBuilder.getLastSent().get("breakNumber"));

            if (list.size() >= position)
                expectedAds = list.get(position - 1);
        } else if (adsAdapter != null) {
            expectedAds = adsAdapter.getExpectedAds();
        }

        return YouboraUtil.parseNumber(expectedAds, 0);
    }

    public Integer getAdNumberInBreak() {
        if(requestBuilder.getLastSent().get("adNumberInBreak") != null)
            return Integer.parseInt(requestBuilder.getLastSent().get("adNumberInBreak"));
        else
            return 0;
    }

    public Boolean getAdsExpected() {
        return getExpectedPattern() != null || getGivenAds() > 0;
    }


    /**
     * Returns whether the current view/session is being affected by an Ad Blocker.
     * @return whether the current view/session is being affected by an Ad Blocker.
     */
    public Boolean getIsAdBlockerDetected() { return options.isAdBlockerDetected(); }


    /**
     * Returns a json-formatted string with plugin info
     * @return plugin info
     */
    public String getPluginInfo() {
        Map<String, String> info = new HashMap<>(3);
        info.put("lib", BuildConfig.VERSION_NAME);
        info.put("adapter", getAdapterVersion());
        info.put("adAdapter", getAdAdapterVersion());
        return YouboraUtil.stringifyMap(info);
    }

    /**
     * Returns the Ip
     * @return the Ip
     */
    public String getIp() {
        return options.getNetworkIP();
    }

    public String getObfuscateIp(){
        return String.valueOf(options.getUserObfuscateIp());
    }

    /**
     * Returns the Isp
     * @return the Isp
     */
    public String getIsp() {
        return options.getNetworkIsp();
    }

    /**
     * Returns the connection type
     * @return the conneciton type
     */
    public String getConnectionType() {
        return options.getNetworkConnectionType();
    }

    /**
     * Returns the account code
     * @return the account code
     */
    public String getAccountCode() {
        return options.getAccountCode();
    }

    /**
     * Returns the username
     * @return the username
     */
    public String getUsername() {
        return options.getUsername();
    }

    /**
     * Returns the userAnonymousId.
     * @return The user anonymous ID.
     */
    public String getUserAnonymousId() { return options.getUserAnonymousId(); }

    /**
     * Returns the userType
     * @return the userType
     */
    public String getUserType() {
        return options.getUserType();
    }

    /**
     * Returns privacy protocol option
     * @return the user's privacy protocol
     */
    public String getUserPrivacyProtocol() {
        String pp = options.getUserPrivacyProtocol();
        return (pp != null && (pp.equalsIgnoreCase("optin") || pp.equalsIgnoreCase("optout"))) ? pp.toLowerCase(Locale.ENGLISH) : null;
    }

    /**
     * Returns the user profile identifier
     * @return the user profile identifier
     */
    public String getUserProfileId() {
        return options.getUserProfileId();
    }

    /**
     * Get CDN node
     * @return the CDN node or null if unknown
     */
    public String getNodeHost() {
        String nodeHost = options.getContentCdnNode();

        if (nodeHost == null || nodeHost.length() == 0) {
            nodeHost = resourceTransform.getNodeHost();
        }

        return nodeHost;
    }

    /**
     * Get CDN type, parsed from the type string
     * @return the CDN type
     */
    public String getNodeType() {
        String nodeType = options.getContentCdnType();

        if (nodeType == null || nodeType.length() == 0) {
            nodeType = resourceTransform.getNodeType();
        }

        return nodeType;
    }

    /**
     * Get CDN type string, as returned in the cdn header response
     * @return the CDN type string
     */
    public String getNodeTypeString() {
        return resourceTransform.getNodeTypeString();
    }

    /**
     * Get Device info String.
     * @return The device info in a String.
     */
    public String getDeviceInfoString() {
        DeviceInfo deviceInfo = new DeviceInfo.Builder()
                .setDeviceBrand(options.getDeviceBrand())
                .setDeviceModel(options.getDeviceModel())
                .setDeviceType(options.getDeviceType(), getApplicationContext())
                .setDeviceCode(options.getDeviceCode())
                .setDeviceOsName(options.getDeviceOsName())
                .setDeviceOsVersion(options.getDeviceOsVersion())
                .build();
        return deviceInfo.mapToJSONString();
    }

    // ------------------------------------- P2P and CDN network traffic information --------------------------------------------

    /**
     * Returns the content's segment duration in milliseconds
     * @return the content's segment duration
     */
    public Long getSegmentDuration() {
        Long val = options.getContentSegmentDuration();
        if ((val == null || val == 0)) {
            val = cdnBalancerInfo.getSegmentDuration();
        }
        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns CDN traffic bytes using NPAW balancer, streamroot, peer5 or teltoo. Otherwise null.
     * @return cdn traffic in bytes.
     */
    public Long getCdnTraffic() {
        Long val = cdnBalancerInfo.getCdnTraffic();
        if (val == null && getAdapter() != null) {
            try {
                val = getAdapter().getCdnTraffic();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getCdnTraffic");
                YouboraLog.error(ex);
            }
        }

        return YouboraUtil.parseNumber(val, 0L);
    }

    public boolean hasCdnPingInfo() {
        return cdnBalancerInfo.getCdnPingInfo(false).size() > 0;
    }

    public JsonObject getCdnPingInfo() {
        return cdnBalancerInfo.getCdnPingInfo(true);
    }


    public String getCdnBalancerProfileName() {
        return cdnBalancerInfo.getProfileName();
    }
    public String getCdnBalancerVersion() {
        return cdnBalancerInfo.getVersion();
    }

    /**
     * Returns CDN balancer API response id, available only for NPAW solution.
     * @return balancer UUID.
     */
    public String getBalancerResponseId() {
        return cdnBalancerInfo.getBalancerResponseId();
    }

    /**
     * Returns P2P traffic bytes using NPAW balancer, streamroot, peer5 or teltoo. Otherwise null.
     * @return p2p traffic in bytes.
     */
    public Long getP2PTraffic() {
        Long val = cdnBalancerInfo.getP2PTraffic();
        if (val == null && getAdapter() != null) {
            try {
                val = getAdapter().getP2PTraffic();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getP2PTraffic");
                YouboraLog.error(ex);
            }
        }

        return YouboraUtil.parseNumber(val, 0L);
    }

    /**
     * Returns if P2P is enabled, using NPAW balancer, streamroot or peer5. Otherwise null
     * @return true if P2P is enabled
     */
    public Boolean getIsP2PEnabled() {
        Boolean val = cdnBalancerInfo.isP2PEnabled();
        if (val == null && getAdapter() != null) {
            try {
                val = getAdapter().getIsP2PEnabled();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getIsP2PEnabled");
                YouboraLog.error(ex);
            }
        }

        return val;
    }

    /**
     * Returns P2P traffic sent in bytes, using NPAW balancer, streamroot or peer5. Otherwise null.
     * @return p2p uploaded traffic in bytes.
     */
    public Long getUploadTraffic() {
        Long val = cdnBalancerInfo.getUploadTraffic();
        if (val == null && getAdapter() != null) {
            try {
                val = getAdapter().getUploadTraffic();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getUploadTraffic");
                YouboraLog.error(ex);
            }
        }

        return YouboraUtil.parseNumber(val, 0L);
    }


    // ---------------------------------------- Background --------------------------------------------
    /**
     * Setter for the activity to listen to callback events (needed for background detection)
     * @param activity the activity
     */
    public void setActivity(Activity activity) {
        this.activity = activity;

        if(sessionActivity == null)
            setSessionActivity(activity);

        if (activity != null && getApplicationContext() == null)
            setApplicationContext(activity.getApplicationContext());
    }

    /**
     * Setter for the activity related with sessions to listen to callback events (needed for background detection)
     * @param activity the activity
     */
    private void setSessionActivity(Activity activity) {
        this.sessionActivity = activity;
    }

    /**
     * Getter for the activity (needed for background detection)
     * @return Application context
     */
    public Activity getActivity() {
        return activity;
    }

    // ---------------------------------------- CHRONOS --------------------------------------------
    /**
     * Returns preload chrono delta time
     * @return the preload duration
     */
    public long getPreloadDuration() {
        return preloadChrono.getDeltaTime(false);
    }

    /**
     * Returns init chrono delta time
     * @return the init duration
     */
    public long getInitDuration() {
        return initChrono.getDeltaTime(false);
    }

    /**
     * Returns JoinDuration chrono delta time
     * @return the join duration
     */
    public long getJoinDuration() {
        if (isInitiated) return getInitDuration();
        return adapter != null ? adapter.getChronos().getJoin().getDeltaTime(false) : -1;
    }

    /**
     * Pauses JoinDuration chrono
     */
    public void pauseJoinDuration() {
        if (isInitiated)
            initChrono.pause();
        else if (adapter != null)
            adapter.getChronos().getJoin().pause();
    }

    /**
     * Resumes JoinDuration chrono
     */
    public void resumeJoinDuration() {
        if (isInitiated)
            initChrono.resume();
        else if (adapter != null)
            adapter.getChronos().getJoin().resume();
    }

    /**
     * Returns BufferDuration chrono delta time
     * @return the buffer duration
     */
    public long getBufferDuration() {
        return adapter != null ? adapter.getChronos().getBuffer().getDeltaTime(false) : -1;
    }

    /**
     *  Returns SeekDuration chrono delta time
     * @return the seek duration
     */
    public long getSeekDuration() {
        return adapter != null ? adapter.getChronos().getSeek().getDeltaTime(false) : -1;
    }

    /**
     * Returns pauseDuration chrono delta time
     * @return the pause duration
     */
    public long getPauseDuration() {
        return adapter != null ? adapter.getChronos().getPause().getDeltaTime(false) : -1;
    }

    /**
     * Returns AdJoinDuration chrono delta time
     * @return the ad join duration
     */
    public long getAdJoinDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().getJoin().getDeltaTime(false)
                                  : -1;
    }

    /**
     * Returns AdBufferDuration chrono delta time
     * @return the ad buffer duration
     */
    public long getAdBufferDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().getBuffer().getDeltaTime(false)
                                  : -1;
    }

    /**
     * Returns AdPauseDuration chrono delta time
     * @return the ad pause duration
     */
    public long getAdPauseDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().getPause().getDeltaTime(false)
                                  : -1;
    }

    /**
     * Returns total totalAdDuration chrono delta time
     * @return the total ad duration
     */
    public long getAdTotalDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().getTotal().getDeltaTime(false)
                                  : -1;
    }

    public Boolean isAdAudioEnabled() {
        return adsAdapter.getIsAudioEnabled();
    }

    public Boolean isAdSkippable() {
        return adsAdapter.getIsAdSkippable();
    }

    public Boolean isFullscreen() {
        return adsAdapter.getIsFullscreen();
    }

    /**
     * Returns household id
     * @return Household Id
     */
    public String getHouseholdId() {
        String val = null;

        if (getAdapter() != null) {
            try {
                val = getAdapter().getHouseholdId();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getHouseholdId");
                YouboraLog.error(ex);
            }
        }

        return val;
    }

    /**
     * Returns current device language in language-COUNTRYCODE format
     * @return Current language
     */
    public String getLanguage() {
        String locale = Locale.getDefault().toString();
        return locale.replace('_', '-');
    }

    /**
     * Returns the {@link RequestBuilder} instance that this Plugin is using
     * @return the {@link RequestBuilder} instance that this Plugin is using
     */
    public RequestBuilder getRequestBuilder() {
        return requestBuilder;
    }

    /**
     * Returns SmartSwitch config code
     * @return smartSwitch config code
     */
    public String getSmartSwitchConfigCode() {
        return options.getSmartSwitchConfigCode();
    }

    /**
     * Returns SmartSwitch group code
     * @return smartSwitch group code
     */
    public String getSmartSwitchGroupCode() {
        return options.getSmartSwitchGroupCode();
    }

    /**
     * Returns SmartSwitch contract code
     * @return smartSwitch contract code
     */
    public String getSmartSwitchContractCode() {
        return options.getSmartSwitchContractCode();
    }

    /**
     * Returns the deviceUUID from the device
     * @return deviceUUID
     */
    public String getDeviceId() {
        Context context = getApplicationContext();
        String deviceId = null;

        if (!getOptions().getDeviceIsAnonymous()) {
            if (options.getDeviceId() != null) {
                deviceId = options.getDeviceId();
            } else if (context != null) {
                InfinitySharedPreferencesManager infinityStorage =
                        new InfinitySharedPreferencesManager(context);

                if (infinityStorage.getDeviceUUID() == null)
                    infinityStorage.saveDeviceUUID(UUID.randomUUID().toString());

                deviceId = infinityStorage.getDeviceUUID();
            }
        }

        return deviceId;
    }

    public String getUserEmail() {
        return options.getUserEmail();
    }

    public String getContentPackage() { return options.getContentPackage(); }

    public String getContentSaga() {
        return options.getContentSaga();
    }

    public String getContentTvShow() {
        return options.getContentTvShow();
    }

    public String getContentSeason() {
        return options.getContentSeason();
    }

    public String getContentEpisodeTitle() {
        return options.getContentEpisodeTitle();
    }

    public String getContentChannel() {
        return options.getContentChannel();
    }

    public String getContentId() {
        return options.getContentId();
    }

    public String getContentImdbId() {
        return options.getContentImdbId();
    }

    public String getContentGracenoteId() {
        return options.getContentGracenoteId();
    }

    public String getContentType() {
        return options.getContentType();
    }

    public String getContentGenre() {
        return options.getContentGenre();
    }

    public String getContentLanguage() {
        return options.getContentLanguage();
    }

    public String getContentSubtitles() {
        return options.getContentSubtitles();
    }

    public String getContentContractedResolution() {
        return options.getContentContractedResolution();
    }

    public String getContentCost() {
        return options.getContentCost();
    }

    public String getContentPrice() {
        return options.getContentPrice();
    }

    public String getContentPlaybackType() {
        String val = options.getContentPlaybackType();

        if (getAdapter() != null && val == null) {
            try {
                if (options.isOffline())
                    val = "Offline";
                else if (getIsLiveOrNull() != null)
                    val = getIsLive() ? "Live" : "VoD";
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getContentPlaybackType");
                YouboraLog.error(ex);
            }
        }

        return val;
    }

    public String getContentDrm() {
        return options.getContentDrm();
    }

    public String getContentEncodingVideoCodec() {
        String val = options.getContentEncodingVideoCodec();

        if (val == null && adapter != null) val = adapter.getVideoCodec();

        return val;
    }

    public String getContentEncodingAudioCodec() {
        String val = options.getContentEncodingAudioCodec();

        if (val == null && adapter != null) val = adapter.getAudioCodec();

        return val;
    }

    public String getContentEncodingCodecSettings() {
        return YouboraUtil.stringifyBundle(options.getContentEncodingCodecSettings());
    }

    public String getContentEncodingCodecProfile() {
        return options.getContentEncodingCodecProfile();
    }

    public String getContentEncodingContainerFormat() {
        return options.getContentEncodingContainerFormat();
    }

    public String getNavContext() {
        String value = null;

        if (infinity != null) value = infinity.getNavContext();

        return value;
    }

    public String getAdInsertionType() {
        String value = null;

        if (adsAdapter != null) value = adsAdapter.getAdInsertionType();

        return value;
    }

    // Add listeners
    /**
     * Adds an Init listener
     * @param listener to add
     */
    public void addOnWillSendInitListener(WillSendRequestListener listener) {
        if (willSendInitListeners == null)
            willSendInitListeners = new ArrayList<>(1);

        willSendInitListeners.add(listener);
    }

    /**
     * Adds a Start listener
     * @param listener to add
     */
    public void addOnWillSendStartListener(WillSendRequestListener listener) {
        if (willSendStartListeners == null)
            willSendStartListeners = new ArrayList<>(1);

        willSendStartListeners.add(listener);
    }

    /**
     * Adds a Join listener
     * @param listener to add
     */
    public void addOnWillSendJoinListener(WillSendRequestListener listener) {
        if (willSendJoinListeners == null)
            willSendJoinListeners = new ArrayList<>(1);

        willSendJoinListeners.add(listener);
    }

    /**
     * Adds a Pause listener
     * @param listener to add
     */
    public void addOnWillSendPauseListener(WillSendRequestListener listener) {
        if (willSendPauseListeners == null)
            willSendPauseListeners = new ArrayList<>(1);

        willSendPauseListeners.add(listener);
    }

    /**
     * Adds a Resume listener
     * @param listener to add
     */
    public void addOnWillSendResumeListener(WillSendRequestListener listener) {
        if (willSendResumeListeners == null)
            willSendResumeListeners = new ArrayList<>(1);

        willSendResumeListeners.add(listener);
    }

    /**
     * Adds a Seek listener
     * @param listener to add
     */
    public void addOnWillSendSeekListener(WillSendRequestListener listener) {
        if (willSendSeekListeners == null)
            willSendSeekListeners = new ArrayList<>(1);

        willSendSeekListeners.add(listener);
    }

    /**
     * Adds a Buffer listener
     * @param listener to add
     */
    public void addOnWillSendBufferListener(WillSendRequestListener listener) {
        if (willSendBufferListeners == null)
            willSendBufferListeners = new ArrayList<>(1);

        willSendBufferListeners.add(listener);
    }

    /**
     * Adds a Error listener
     * @param listener to add
     */
    public void addOnWillSendErrorListener(WillSendRequestListener listener) {
        if (willSendErrorListeners == null)
            willSendErrorListeners = new ArrayList<>(1);

        willSendErrorListeners.add(listener);
    }

    /**
     * Adds a video event listener
     * @param listener to add
     */
    public void addOnWillSendVideoEventListener(WillSendRequestListener listener) {
        if (willSendVideoEventListeners == null)
            willSendVideoEventListeners = new ArrayList<>(1);
        willSendVideoEventListeners.add(listener);
    }

    /**
     * Adds a Stop listener
     * @param listener to add
     */
    public void addOnWillSendStopListener(WillSendRequestListener listener) {
        if (willSendStopListeners == null)
            willSendStopListeners = new ArrayList<>(1);

        willSendStopListeners.add(listener);
    }

    /**
     * Adds a Ping listener
     * @param listener to add
     */
    public void addOnWillSendPingListener(WillSendRequestListener listener) {
        if (willSendPingListeners == null)
            willSendPingListeners = new ArrayList<>(1);

        willSendPingListeners.add(listener);
    }

    /**
     * Adds an ad Start listener
     * @param listener to add
     */
    public void addOnWillSendAdStartListener(WillSendRequestListener listener) {
        if (willSendAdStartListeners == null)
            willSendAdStartListeners = new ArrayList<>(1);

        willSendAdStartListeners.add(listener);
    }

    /**
     * Adds an ad Join listener
     * @param listener to add
     */
    public void addOnWillSendAdJoinListener(WillSendRequestListener listener) {
        if (willSendAdJoinListeners == null)
            willSendAdJoinListeners = new ArrayList<>(1);

        willSendAdJoinListeners.add(listener);
    }

    /**
     * Adds an ad Pause listener
     * @param listener to add
     */
    public void addOnWillSendAdPauseListener(WillSendRequestListener listener) {
        if (willSendAdPauseListeners == null)
            willSendAdPauseListeners = new ArrayList<>(1);

        willSendAdPauseListeners.add(listener);
    }

    /**
     * Adds an ad Resume listener
     * @param listener to add
     */
    public void addOnWillSendAdResumeListener(WillSendRequestListener listener) {
        if (willSendAdResumeListeners == null)
            willSendAdResumeListeners = new ArrayList<>(1);

        willSendAdResumeListeners.add(listener);
    }

    /**
     * Adds an ad Buffer listener
     * @param listener to add
     */
    public void addOnWillSendAdBufferListener(WillSendRequestListener listener) {
        if (willSendAdBufferListeners == null)
            willSendAdBufferListeners = new ArrayList<>(1);

        willSendAdBufferListeners.add(listener);
    }

    /**
     * Adds an ad Stop listener
     * @param listener to add
     */
    public void addOnWillSendAdStopListener(WillSendRequestListener listener) {
        if (willSendAdStopListeners == null)
            willSendAdStopListeners = new ArrayList<>(1);

        willSendAdStopListeners.add(listener);
    }

    /**
     * Adds an ad Stop listener
     * @param listener to add
     */
    public void addOnWillSendAdClickListener(WillSendRequestListener listener) {
        if (willSendAdClickListeners == null)
            willSendAdClickListeners = new ArrayList<>(1);

        willSendAdClickListeners.add(listener);
    }

    /**
     * Adds an ad Init listener
     * @param listener to add
     */
    public void addOnWillSendAdInitListener(WillSendRequestListener listener) {
        if (willSendAdInitListeners == null)
            willSendAdInitListeners = new ArrayList<>(1);

        willSendAdInitListeners.add(listener);
    }

    /**
     * Adds an ad Error listener
     * @param listener to add
     */
    public void addOnWillSendAdErrorListener(WillSendRequestListener listener) {
        if (willSendAdErrorListeners == null)
            willSendAdErrorListeners = new ArrayList<>(1);

        willSendAdErrorListeners.add(listener);
    }

    /**
     * Adds an ad manifest listener.
     * @param listener to be added
     */
    public void addOnWillSendAdManifestListener(WillSendRequestListener listener) {
        if (willSendAdManifestListeners == null)
            willSendAdManifestListeners = new ArrayList<>(1);

        willSendAdManifestListeners.add(listener);
    }

    /**
     * Adds an ad break start.
     * @param listener to be added
     */
    public void addOnWillSendAdBreakStart(WillSendRequestListener listener) {
        if (willSendAdBreakStartListeners == null)
            willSendAdBreakStartListeners = new ArrayList<>(1);

        willSendAdBreakStartListeners.add(listener);
    }

    /**
     * Adds an ad break stop.
     * @param listener to be added
     */
    public void addOnWillSendAdBreakStop(WillSendRequestListener listener) {
        if (willSendAdBreakStopListeners == null)
            willSendAdBreakStopListeners = new ArrayList<>(1);

        willSendAdBreakStopListeners.add(listener);
    }

    /**
     * Adds an ad quartile.
     * @param listener to be added
     */
    public void addOnWillSendAdQuartile(WillSendRequestListener listener) {
        if (willSendAdQuartileListeners == null)
            willSendAdQuartileListeners = new ArrayList<>(1);

        willSendAdQuartileListeners.add(listener);
    }

    /**
     * Adds a session Start listener
     * @param listener to add
     */
    public void addOnWillSendSessionStartListener(WillSendRequestListener listener) {
        if (willSendSessionStartListeners == null)
            willSendSessionStartListeners = new ArrayList<>(1);

        willSendSessionStartListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionStopListener(WillSendRequestListener listener) {
        if (willSendSessionStopListeners == null)
            willSendSessionStopListeners = new ArrayList<>(1);

        willSendSessionStopListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionEventListener(WillSendRequestListener listener) {
        if (willSendSessionEventListeners == null)
            willSendSessionEventListeners = new ArrayList<>(1);

        willSendSessionEventListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionNavListener(WillSendRequestListener listener) {
        if (willSendSessionNavListeners == null)
            willSendSessionNavListeners = new ArrayList<>(1);

        willSendSessionNavListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionBeatListener(WillSendRequestListener listener) {
        if (willSendSessionBeatListeners == null)
            willSendSessionBeatListeners = new ArrayList<>(1);

        willSendSessionBeatListeners.add(listener);
    }

    /**
     * Adds an offline events request listener
     * @param listener to add
     */
    public void addOnWillSendOfflineEvents(WillSendRequestListener listener) {
        if (willSendOfflineEventsListeners == null)
            willSendOfflineEventsListeners = new ArrayList<>(1);

        willSendOfflineEventsListeners.add(listener);
    }

    // Remove listeners
    /**
     * Removes an Init listener
     * @param listener to remove
     */
    public void removeOnWillSendInitListener(WillSendRequestListener listener) {
        if (willSendInitListeners != null)
            willSendInitListeners.remove(listener);
    }

    // Remove listeners
    /**
     * Removes an ad Init listener
     * @param listener to remove
     */
    public void removeOnWillSendAdInitListener(WillSendRequestListener listener) {
        if (willSendAdInitListeners != null)
            willSendAdInitListeners.remove(listener);
    }

    /**
     * Removes a Start listener
     * @param listener to remove
     */
    public void removeOnWillSendStartListener(WillSendRequestListener listener) {
        if (willSendStartListeners != null)
            willSendStartListeners.remove(listener);
    }

    /**
     * Removes a Join listener
     * @param listener to remove
     */
    public void removeOnWillSendJoinListener(WillSendRequestListener listener) {
        if (willSendJoinListeners != null)
            willSendJoinListeners.remove(listener);
    }

    /**
     * Removes a Pause listener
     * @param listener to remove
     */
    public void removeOnWillSendPauseListener(WillSendRequestListener listener) {
        if (willSendPauseListeners != null)
            willSendPauseListeners.remove(listener);
    }

    /**
     * Removes a Resume listener
     * @param listener to remove
     */
    public void removeOnWillSendResumeListener(WillSendRequestListener listener) {
        if (willSendResumeListeners != null)
            willSendResumeListeners.remove(listener);
    }

    /**
     * Removes a Seek listener
     * @param listener to remove
     */
    public void removeOnWillSendSeekListener(WillSendRequestListener listener) {
        if (willSendSeekListeners != null)
            willSendSeekListeners.remove(listener);
    }

    /**
     * Removes a Buffer listener
     * @param listener to remove
     */
    public void removeOnWillSendBufferListener(WillSendRequestListener listener) {
        if (willSendBufferListeners != null)
            willSendBufferListeners.remove(listener);
    }

    /**
     * Removes an Error listener
     * @param listener to remove
     */
    public void removeOnWillSendErrorListener(WillSendRequestListener listener) {
        if (willSendErrorListeners != null)
            willSendErrorListeners.remove(listener);
    }

    /**
     * Removes a video event listener
     * @param listener to remove
     */
    public void removeOnWillSendVideoEventListener(WillSendRequestListener listener) {
        if (willSendVideoEventListeners != null)
            willSendVideoEventListeners.remove(listener);
    }

    /**
     * Removes a Stop listener
     * @param listener to remove
     */
    public void removeOnWillSendStopListener(WillSendRequestListener listener) {
        if (willSendStopListeners != null)
            willSendStopListeners.remove(listener);
    }

    /**
     * Removes a Ping listener
     * @param listener to remove
     */
    public void removeOnWillSendPingListener(WillSendRequestListener listener) {
        if (willSendPingListeners != null)
            willSendPingListeners.remove(listener);
    }

    /**
     * Removes an ad Start listener
     * @param listener to remove
     */
    public void removeOnWillSendAdStartListener(WillSendRequestListener listener) {
        if (willSendAdStartListeners != null)
            willSendAdStartListeners.remove(listener);
    }

    /**
     * Removes an ad Join listener
     * @param listener to remove
     */
    public void removeOnWillSendAdJoinListener(WillSendRequestListener listener) {
        if (willSendAdJoinListeners != null)
            willSendAdJoinListeners.remove(listener);
    }

    /**
     * Removes an ad Pause listener
     * @param listener to remove
     */
    public void removeOnWillSendAdPauseListener(WillSendRequestListener listener) {
        if (willSendAdPauseListeners != null)
            willSendAdPauseListeners.remove(listener);
    }

    /**
     * Removes an ad Resume listener
     * @param listener to remove
     */
    public void removeOnWillSendAdResumeListener(WillSendRequestListener listener) {
        if (willSendAdResumeListeners != null)
            willSendAdResumeListeners.remove(listener);
    }

    /**
     * Removes an ad Buffer listener
     * @param listener to remove
     */
    public void removeOnWillSendAdBufferListener(WillSendRequestListener listener) {
        if (willSendAdBufferListeners != null)
            willSendAdBufferListeners.remove(listener);
    }

    /**
     * Removes an ad Stop listener
     * @param listener to remove
     */
    public void removeOnWillSendAdStopListener(WillSendRequestListener listener) {
        if (willSendAdStopListeners != null)
            willSendAdStopListeners.remove(listener);
    }

    /**
     * Removes an ad Click listener
     * @param listener to remove
     */
    public void removeOnWillSendAdClick(WillSendRequestListener listener) {
        if (willSendAdClickListeners != null)
            willSendAdClickListeners.remove(listener);
    }

    /**
     * Removes an ad Error listener
     * @param listener to remove
     */
    public void removeOnWillSendAdError(WillSendRequestListener listener) {
        if (willSendAdErrorListeners != null)
            willSendAdErrorListeners.remove(listener);
    }

    /**
     * Removes an ad manifest listener.
     * @param listener to be removed
     */
    public void removeOnWillSendAdManifest(WillSendRequestListener listener) {
        if (willSendAdManifestListeners != null)
            willSendAdManifestListeners.remove(listener);
    }

    /**
     * Removes an ad break start.
     * @param listener to be removed
     */
    public void removeOnWillSendAdBreakStart(WillSendRequestListener listener) {
        if (willSendAdBreakStartListeners != null)
            willSendAdBreakStartListeners.remove(listener);
    }

    /**
     * Removes an ad break stop.
     * @param listener to be removed
     */
    public void removeOnWillSendAdBreakStop(WillSendRequestListener listener) {
        if (willSendAdBreakStopListeners != null)
            willSendAdBreakStopListeners.remove(listener);
    }

    /**
     * Removes an ad quartile.
     * @param listener to be removed
     */
    public void removeOnWillSendAdQuartile(WillSendRequestListener listener) {
        if (willSendAdQuartileListeners != null)
            willSendAdQuartileListeners.remove(listener);
    }

    /**
     * Removes a Session start listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionStart(WillSendRequestListener listener) {
        if (willSendSessionStartListeners != null)
            willSendSessionStartListeners.remove(listener);
    }

    /**
     * Removes a Session stop listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionStop(WillSendRequestListener listener) {
        if (willSendSessionStopListeners != null)
            willSendSessionStopListeners.remove(listener);
    }

    /**
     * Removes a Session nav listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionNav(WillSendRequestListener listener) {
        if (willSendSessionNavListeners != null)
            willSendSessionNavListeners.remove(listener);
    }

    /**
     * Removes a Session event listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionEvent(WillSendRequestListener listener) {
        if (willSendSessionEventListeners != null) {
            willSendSessionEventListeners.remove(listener);
        }
    }

    /**
     * Removes a Session beat listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionBeat(WillSendRequestListener listener) {
        if (willSendSessionBeatListeners != null)
            willSendSessionBeatListeners.remove(listener);
    }

    /**
     * Removes an Offline events request listener
     * @param listener to remove
     */
    public void removeOnWillSendOfflineEvents(WillSendRequestListener listener) {
        if (willSendOfflineEventsListeners != null)
            willSendOfflineEventsListeners.remove(listener);

    }

    /**
     * Saves the context
     * @param context the context
     */
    public void setApplicationContext(Context context) {
        this.context = context;
        if (context != null) dataSource = createEventDataSource();
    }

    /**
     * Gets the context
     * @return The application context.
     */
    public Context getApplicationContext() { return this.context; }

    // Content Adapter listener
    private PlayerAdapter.AdapterEventListener eventListener = new PlayerAdapter.ContentAdapterEventListener() {

        @Override
        public void onStart(Map<String, String> params) {
            startListener(params);
        }

        @Override
        public void onJoin(Map<String, String> params) {
            joinListener(params);
        }

        @Override
        public void onPause(Map<String, String> params) {
            pauseListener(params);
        }

        @Override
        public void onResume(Map<String, String> params) {
            resumeListener(params);
        }

        @Override
        public void onStop(Map<String, String> params) {
            stopListener(params);
        }

        @Override
        public void onBufferBegin(boolean convertFromBuffer, Map<String, String> params) {
            bufferBeginListener();
        }

        @Override
        public void onBufferEnd(Map<String, String> params) {
            bufferEndListener(params);
        }

        @Override
        public void onSeekBegin(boolean convertFromBuffer, Map<String, String> params) {
            seekBeginListener();
        }

        @Override
        public void onSeekEnd(Map<String, String> params) {
            seekEndListener(params);
        }

        @Override
        public void onError(Map<String, String> params) {
            errorListener(params);
        }

        @Override
        public void onVideoEvent(Map<String, String> params) { videoEventListener(params); }
    };

    // Ad Adapter listener
    private PlayerAdapter.AdapterEventListener adEventListener =  new AdAdapter.AdAdapterEventListener() {

        @Override
        public void onStart(Map<String, String> params) {
            adStartListener(params);
        }

        @Override
        public void onJoin(Map<String, String> params) {
            adJoinListener(params);
        }

        @Override
        public void onPause(Map<String, String> params) {
            adPauseListener(params);
        }

        @Override
        public void onResume(Map<String, String> params) {
            adResumeListener(params);
        }

        @Override
        public void onStop(Map<String, String> params) {
            adStopListener(params);
        }

        @Override
        public void onBufferBegin(boolean convertFromBuffer, Map<String, String> params) {
            adBufferBeginListener();
        }

        @Override
        public void onBufferEnd(Map<String, String> params) {
            adBufferEndListener(params);
        }

        @Override
        public void onClick(Map<String, String> params) {
            adClickListener(params);
        }

        @Override
        public void onAdInit(Map<String, String> params) {
            adInitListener(params);
        }

        @Override
        public void onError(Map<String, String> params) {
            adErrorListener(params);
        }

        @Override
        public void onQuartile(Map<String, String> params) { adQuartileListener(params); }

        @Override
        public void onManifest(Map<String, String> params) { adManifestListener(params); }

        @Override
        public void onAdBreakStart(Map<String, String> params) { adBreakStartListener(params); }

        @Override
        public void onAdBreakStop(Map<String, String> params) { adBreakStopListener(params); }
    };

    // Infinity
    private Infinity.InfinityEventListener infinityEventListener = new Infinity.InfinityEventListener() {

        @Override
        public void onSessionStart(String screenName, Map<String, String> dimensions) {
            sessionStartListener(screenName, dimensions);
        }

        @Override
        public void onSessionStop(Map<String, String> params) {
            sessionStopListener(params);
        }

        @Override
        public void onNav(String screenName) {
            sessionNavListener(screenName);
        }

        @Override
        public void onEvent(String eventName, Map<String, String> dimensions,
                Map<String, Double> values, Map<String, String> topLevelDimensions) {
            sessionEventListener(dimensions, values, eventName, topLevelDimensions);
        }
    };

    /**
     * Will send Request Interface.
     * This callback will be invoked just before an event is sent, to offer the chance to
     * modify the params that will be sent.
     */
    public interface WillSendRequestListener {

        /**
         * Called just before a {@link Request} is sent
         * @param serviceName the service name
         * @param plugin the {@link Plugin} that is calling this callback
         * @param params a Map of params that will be sent in the Request
         */
        void willSendRequest(String serviceName, Plugin plugin, Map<String, String> params);
        /**
         * Called just before a {@link Request} is sent
         * @param serviceName the service name
         * @param plugin the {@link Plugin} that is calling this callback
         * @param params an ArrayList of {@link JSONObject}
         */
        void willSendRequest(String serviceName, Plugin plugin, ArrayList<JSONObject> params);
    }

    public boolean isStarted() {
        return isStarted;
    }

    private boolean isExtraMetadataReady() {
        Bundle options = getOptions().toBundle();

        if (getOptions().getPendingMetadata() != null && getOptions().getWaitForMetadata()) {
            ArrayList<String> pendingMetadataKeys = getOptions().getPendingMetadata();
            ArrayList<String> pendingDataToRemove = new ArrayList<>();

            for (String pendingKey : pendingMetadataKeys) {
                if (options.get(pendingKey) == null)
                    return false;
                else
                    pendingDataToRemove.add(pendingKey);
            }

            removeNotPendingOptions(pendingDataToRemove);
        }

        return true;
    }

    private void removeNotPendingOptions(ArrayList<String> readyKeys) {
        ArrayList<String> pendingMetadataKeys = getOptions().getPendingMetadata();
        pendingMetadataKeys.removeAll(readyKeys);
    }

    private void startMetadataTimer() {
        if (getOptions().getPendingMetadata() != null && getOptions().getWaitForMetadata())
            metadataTimer.start();
    }
}
