package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.comm.Request
import java.net.HttpURLConnection

/**
 * Class that asynchronously gets the manifest content
 *
 * @author      Nice People at Work
 * @since       6.0
 */
open class ManifestParser(private val manifestHeaders: MutableMap<String, String>? = null) : Parser(manifestHeaders) {
    override fun parse(resource: String?, parentResource: String?, lastManifest: String?) {
        val request = createRequest(resource)
        manifestHeaders?.let { request.requestHeaders = it }
        request.maxRetries = 0
        request.addOnSuccessListener { _, response, _, _ ->
            this.lastManifest = response
        }

        request.addOnErrorListener(object: Request.RequestErrorListener {
            override fun onRequestError(connection: HttpURLConnection?) { done() }
            override fun onRequestRemovedFromQueue() {}
        })

        request.send()
    }

    override fun shouldExecute(lastManifest: String?): Boolean {
        return lastManifest == null
    }

    internal open fun createRequest(host: String?): Request {
        return Request(host, null)
    }
}