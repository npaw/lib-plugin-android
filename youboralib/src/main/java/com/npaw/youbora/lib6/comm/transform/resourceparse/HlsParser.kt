package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.YouboraLog.Companion.warn
import com.npaw.youbora.lib6.comm.Request
import com.npaw.youbora.lib6.comm.Request.RequestErrorListener
import com.npaw.youbora.lib6.plugin.Options

import java.net.HttpURLConnection
import java.util.*
import java.util.regex.Pattern

open class HlsParser(private val manifestHeaders: MutableMap<String, String>? = null) : Parser(manifestHeaders) {

    companion object {
        private const val REG_EXP_VIDEO_EXTENSION_PATTERN =
                "(\\S*?(\\.m3u8|\\.m3u|\\.ts|\\.mp4|\\.m4s|\\.cmfv)(?:\\?\\S*|\\n|\\r|$))"
    }

    private val regexPattern by lazy {
        Pattern.compile(REG_EXP_VIDEO_EXTENSION_PATTERN, Pattern.CASE_INSENSITIVE)
    }

    init {
        realResource = null
    }

    /**
     * Starts the HLS parsing from the given resource. The first (outside) call should set the
     * parentResource to null.
     * @param resource the resource url to start parsing
     * @param parentResource parent resource, usually null.
     */
    override fun parse(resource: String?, parentResource: String?, lastManifest: String?) {
        var varResource = resource
        val varParentResource = parentResource ?: resource ?: ""

        if (lastManifest != null) varResource = lastManifest //Since we already have the manifest let's use it instead of the resource

        varResource = varResource?.replace(",URI=", "\n")?.replace("\"", "")

        val matcher = regexPattern.matcher(varResource)

        if (matcher.find()) {
            val result = matcher.toMatchResult()
            var res = result.group(1)
            val extension = result.group(2)

            if (res != null && extension != null) {
                res = res.trim()
                val index = varParentResource.lastIndexOf('/')

                if (!res.lowercase(Locale.getDefault()).startsWith("http") && index >= 0) {
                    val a = res.toCharArray()

                    if (a[0] == '/' && a[1] != '/') {
                        var ind = varParentResource.indexOf('/') + 1
                        ind += varParentResource.substring(ind, varParentResource.length).indexOf('/') + 1
                        ind += varParentResource.substring(ind, varParentResource.length).indexOf('/')
                        res = varParentResource.substring(0, ind) + res
                    } else {
                        res = varParentResource.substring(0, index + 1) + res
                    }
                }

                val nextParentResource = res

                if (extension.endsWith("m3u8") || extension.endsWith("m3u")) {
                    val request = createRequest(res, null)
                    manifestHeaders?.let { request.requestHeaders = it }

                    request.addOnSuccessListener { _, response, _, _ ->
                        this.lastManifest = response
                        parse(response, nextParentResource, response)
                    }

                    request.addOnErrorListener(object : RequestErrorListener {
                        override fun onRequestError(connection: HttpURLConnection?) {
                            done()
                        }

                        override fun onRequestRemovedFromQueue() { }
                    })

                    request.send()
                } else {
                    transportFormat = when {
                        extension.endsWith("mp4") || extension.endsWith("m4s") -> {
                            Options.TransportFormat.MP4
                        }

                        extension.endsWith("ts") -> Options.TransportFormat.TS
                        extension.endsWith("cmfv") -> Options.TransportFormat.CMF
                        else -> null
                    }

                    realResource = res
                    done()
                }
            } else {
                realResource = res
                done()
            }
        } else {
            warn("Parse HLS Regex couldn't match.")
            done()
        }
    }

    override fun shouldExecute(lastManifest: String?): Boolean {
        return lastManifest != null && lastManifest.contains("#EXTM3U")
    }

    /**
     * Get the parsed resource. Will be null if parsing is not yet started and can be a partial
     * (an intermediate manifest) result if the parser is still running.
     * @return the parsed resource.
     */
    open fun getResource(): String? {
        return realResource
    }

    open fun createRequest(host: String?, service: String?): Request {
        return Request(host, service)
    }
}