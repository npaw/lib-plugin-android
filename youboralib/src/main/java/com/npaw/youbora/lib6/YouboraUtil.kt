package com.npaw.youbora.lib6

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonSyntaxException
import com.google.gson.stream.MalformedJsonException
import com.npaw.youbora.lib6.balancer.models.BalancerStats
import com.npaw.youbora.lib6.constants.Balancer.BALANCER_STATS_FILE
import com.npaw.youbora.lib6.extensions.toJson
import com.npaw.youbora.lib6.extensions.toMap
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.util.*
import java.util.regex.Pattern
import kotlin.math.abs

/**
 * An Utility class with static methods.
 * @author      Nice People at Work
 * @since       6.0
 */
open class YouboraUtil {

    companion object {

        private val gson = Gson()

        private val requestThread: HandlerThread by lazy {
            HandlerThread("YouboraRequestThread")
        }

        private val requestHandler: Handler by lazy {
            requestThread.start()
            Handler(requestThread.looper)
        }

        private val cdnThread: HandlerThread by lazy {
            HandlerThread("cdnPingThread")
        }

        private val _cdnHandler: Handler by lazy {
            cdnThread.start()
            Handler(cdnThread.looper)
        }

        /**
         * Pattern used to extract an URL's protocol.
         */
        private val stripProtocolPattern: Pattern by lazy {
            Pattern.compile("^(.*?://|//)", Pattern.CASE_INSENSITIVE)
        }

        @JvmStatic
        fun getHandler(): Handler { return requestHandler }

        @JvmStatic
        fun getCdnHandler(): Handler { return _cdnHandler }

        /**
         * Builds a string that represents the rendition.
         *
         * The returned string will have the following format: [width]x[height]@[bitrate].
         * If either the width or height are < 1, only the bitrate will be returned.
         * If bitrate is < 1, only the dimensions will be returned.
         * If bitrate is < 1, and there is no dimensions, a null will be returned.
         * The bitrate will also have one of the following suffixes depending on its
         * magnitude: bps, Kbps, Mbps
         *
         * @param width of the asset.
         * @param height of the asset.
         * @param bitrate (in the manifest) of the asset.
         * @return A string with the following format: [width]x[height]@[bitrate].
         */
        @JvmStatic
        @JvmOverloads
        fun buildRenditionString(width: Int, height: Int, bitrate: Double = 0.0): String {
            val sb = StringBuilder("")

            if (width > 0 && height > 0) {
                sb.append(width.toString()).append("x").append(height.toString())

                if (bitrate > 0) sb.append("@")
            }

            if (bitrate > 0) {
                when {
                    // Smaller than 1 Kbps
                    bitrate < 1e3 -> sb.append(String.format(Locale.US, "%.0fbps", bitrate))

                    // Smaller than 1 Mbps
                    bitrate < 1e6 -> {
                        sb.append(String.format(Locale.US, "%.0fKbps", bitrate / 1e3))
                    }

                    // Greater than 1 Mbps
                    else -> {
                        sb.append(String.format(Locale.US, "%.2fMbps", bitrate / 1e6))
                    }
                }
            }

            return sb.toString()
        }

        /**
         * Returns the JSON representation of a Map.
         * @param map to stringify
         * @return the JSON representation of a Map. If the map is null, null will be returned.
         */
        @JvmStatic fun stringifyMap(map: Map<String, *>?): String? {
            return map?.let { JSONObject(map).toString() }
        }

        /**
         * Returns the JSON representation of a Bundle.
         * @param b to stringify
         * @return the JSON representation of a Bundle. If the Bundle is null, null will be
         * returned.
         */
        @JvmStatic fun stringifyBundle(b: Bundle?): String? {
            return b?.toJson()?.toString()
        }

        /**
         * Returns the JSON representation of a List.
         * @param list to stringify
         * @return the JSON representation of a List. If the List is null, null will be returned.
         */
        @JvmStatic fun stringifyList(list: List<*>?): String? {
            return list?.let { JSONArray(it).toString() }
        }

        /**
         * Returns the Map representation of a Bundle.
         * @param b bundle to parse as Map
         * @return the Map representation of a Bundle. If the bundle is null or malformed, null will be returned.
         */
        @JvmStatic
        fun bundleToMap(b: Bundle?): MutableMap<String, String>? {
            return b?.toMap()
        }

        /**
         * Convert a Bundle to a JSONObject.
         * @param b to convert
         * @return JSONObject with the Bundle structure if Bundle is not null.
         */
        @Deprecated("Use .toJson Bundle's extension instead")
        @JvmStatic fun bundleToJSON(b: Bundle): JSONObject? {
            val json = JSONObject()

            b.keySet().filter {
                b.get(it) != null
            }.forEach { key ->
                var value = b.get(key)

                value?.let {
                    when {
                        // If the value is a Bundle, recursive call
                        it is Bundle -> value = it.toJson()
                        it is Map<*, *> -> value = JSONObject(it)
                        it is List<*> -> value = JSONArray(it)
                        it.javaClass.isArray -> {
                            // Use a map as wrapper, convert to json and extract json array
                            val m = HashMap<String, Any>().apply {
                                put("k", it)
                            }

                            value = JSONObject(m.toMap()).getJSONArray("k")
                        }
                    }
                }

                json.put(key, value)
            }

            return json
        }

        /**
         * Strip [protocol]:// from the beginning of the string.
         * @param host Url
         * @return stripped url
         */
        @JvmStatic fun stripProtocol(host: String): String? {
            return stripProtocolPattern.matcher(host).replaceFirst("")
        }

        /**
         * Adds specific protocol. ie: [http[s]:]//a-fds.youborafds01.com
         * @param url Domain of the service.
         * @param httpSecure If true will add https, if false http.
         * @return Return the complete service URL.
         */
        @JvmStatic fun addProtocol(url: String, httpSecure: Boolean): String {
            return if (httpSecure)
                "https://$url"
            else
                "http://$url"
        }

        /**
         * Return [number] if it's not null, [Double.MAX_VALUE], [Double.POSITIVE_INFINITY]
         * or [Double.NaN]. Otherwise, [defaultValue] will be returned.
         * @param number The number to be parsed
         * @param defaultValue Number to return if [number] is incorrect
         * @return number if it's a 'real' value, [defaultValue] otherwise
         */
        @JvmStatic fun parseNumber(number: Double?, defaultValue: Double?): Double? {
            return number?.let {
                val d = abs(it)

                it.takeIf {
                    d != Double.MAX_VALUE && !java.lang.Double.isInfinite(d) &&
                            !java.lang.Double.isNaN(d) && it >= 0
                }
            } ?: defaultValue
        }

        /**
         * Return [number] if it's not null, [Integer.MAX_VALUE] or [Integer.MIN_VALUE].
         * Otherwise, [defaultValue] will be returned.
         * @param number The number to be parsed
         * @param defaultValue Number to return if [number] is incorrect
         * @return number if it's a 'real' value, [defaultValue] otherwise
         */
        @JvmStatic fun parseNumber(number: Int?, defaultValue: Int?): Int? {
            return number?.let {
                it.takeIf { it != Integer.MAX_VALUE && it != Integer.MIN_VALUE }
            } ?: defaultValue
        }

        /**
         * Return [number] if it's not null, [Long.MAX_VALUE] or [Long.MIN_VALUE].
         * Otherwise, [defaultValue] will be returned.
         * @param number The number to be parsed
         * @param defaultValue Number to return if [number] is incorrect
         * @return number if it's a 'real' value, [defaultValue] otherwise
         */
        @JvmStatic fun parseNumber(number: Long?, defaultValue: Long?): Long? {
            return number?.let {
                it.takeIf { it != Long.MAX_VALUE && it != Long.MIN_VALUE }
            } ?: defaultValue
        }

        /**
         * Returns a params dictionary with the error values.
         *
         * @param msg Error Message
         * @param code Error code
         * @param errorMetadata additional error info
         * @param level Level of the error. Currently supports 'error' and 'fatal'
         * @return Built params
         */
        @JvmStatic fun buildErrorParams(code: String?, message: String?, errorMetadata: String?,
                                        level: String? = null): MutableMap<String, String> {
            var msg = message
            var c = code
            val params = HashMap<String, String>()

            val codeOk = c?.isNotEmpty() ?: false
            val msgOk = msg?.isNotEmpty() ?: false

            if (codeOk) {
                if (!msgOk) msg = c
            } else if (msgOk) {
                c = msg
            } else {
                msg = "PLAY_FAILURE"
                c = msg
            }

            c?.let { params["errorCode"] = it }
            msg?.let { params["errorMsg"] = it }

            errorMetadata?.takeIf { it.isNotEmpty() }?.apply { params["errorMetadata"] = this }
            level?.takeIf { it.isNotEmpty() }?.apply { params["errorLevel"] = this }

            return params
        }

        /**
         * Gets the application name (not package)
         * @return Application name
         * @param context the context
         */
        @JvmStatic fun getApplicationName(context: Context?): String {
            return context?.let {
                val applicationInfo = context.applicationInfo
                val stringId = applicationInfo.labelRes
                val nonLocalized = applicationInfo.nonLocalizedLabel?.toString() ?: "Unknown"

                if (stringId == 0)
                    nonLocalized
                else
                    context.getString(stringId)
            } ?: "Unknown"
        }

        /**
         * Returns the last method called in the stack trace within a specific package, or a
         * specified index, with beautified output in the format package.Class::Function:Line.
         *
         * This method obtains the current thread's stack trace and if stackTraceIndex is set to -1,
         * it filters out all stack trace elements that are not part of the 'com.npaw' package or
         * the package of the Plugin class. For elements within these packages, the package
         * name is stripped from the class name and the resulting string is returned in the format
         * package.Class::Function:Line. If no elements in the stack trace are part of the specified
         * packages, null is returned. If stackTraceIndex is not -1, it returns the specific stack
         * trace element at that index, also in the beautified format.
         *
         * This can be useful for logging and debugging, especially in cases where you want to know
         * what method triggered a certain event.
         *
         * @param stackTraceIndex The index of the stack trace element to return. If set to -1
         *   (default), the method returns the last method called within the specified packages. If
         *   not -1, it returns the stack trace element at that index.
         * @return The name of the last method called within the specified packages in the current
         *   thread's stack trace, or the stack trace element at the stackTraceIndex in the
         *   beautified format, or null if no such method or index exists.
         */
        @JvmOverloads
        fun getTriggeredEventTrace(stackTraceIndex: Int = -1): String? {
            val elements = Thread.currentThread().stackTrace
            val trace =
                if (stackTraceIndex == -1) {
                    val npawPackage = "com.npaw"
                    var npawPackageDEO = YouboraLog::class.java.`package`?.name?.toString()

                    npawPackageDEO =
                        npawPackageDEO
                            ?.lastIndexOf('.')
                            ?.takeIf { it != -1 }
                            ?.let {
                                npawPackageDEO?.substring(0, it).let { npawPackageDEOTmp ->
                                    npawPackageDEOTmp?.lastIndexOf ('.')
                                    ?.takeIf { it != -1 }
                                    ?.let {
                                        npawPackageDEOTmp.substring(0, it)
                                    }?: npawPackageDEOTmp
                                }
                            }?: npawPackageDEO
                    elements
                        .mapNotNull {
                            if (it.className.contains(npawPackage))
                                it.toString().replace(npawPackage, "").substring(1)
                            else {
                                /** Deobfuscation * */
                                if (npawPackageDEO != null &&
                                    it.className.contains(npawPackageDEO)
                                )
                                    it
                                else null
                            }
                        }
                        .lastOrNull()
                        ?.toString()
                } else elements.getOrNull(stackTraceIndex + 3)?.toString()
            return try {
                val pattern = Regex("""(.*)\.(.*)\((.*):(\d+)\)""")
                val matchResult = trace?.let { pattern.matchEntire(it) }
                matchResult?.let {
                    "${it.groupValues[1]}::${it.groupValues[2]}:${it.groupValues[4]}"
                }
                    ?: trace // If the pattern doesn't match, return the original string
            } catch (e: java.lang.Exception) {
                trace
            }
        }

        /**
         * Reads Balancer Stats json and parser it to [BalancerStats] object from local storage.
         * @return [BalancerStats] object or null if does not exists.
         * @param context the context
         */
        @JvmStatic
        fun readBalancerStatsFile(context: Context?): BalancerStats? {
            var balancerStats: BalancerStats? = null

            context?.let { c ->
                var json = ""
                try {
                    c.openFileInput(BALANCER_STATS_FILE).use { fileInputStream ->
                        InputStreamReader(fileInputStream).use { inputStreamReader ->
                            BufferedReader(inputStreamReader).use { bufferedReader ->
                                val stringBuilder = StringBuilder()
                                var line: String?
                                while (bufferedReader.readLine().also { line = it } != null) {
                                    stringBuilder.append(line)
                                }

                                json = stringBuilder.toString()

                                if (json.isNotEmpty()) {
                                    balancerStats = gson.fromJson(json, BalancerStats::class.java)
                                }

                                bufferedReader.close()
                            }
                            inputStreamReader.close()
                        }
                        fileInputStream.close()
                    }
                } catch (_: FileNotFoundException) {
                    // Ignore when there is not file to read
                } catch (e: MalformedJsonException) {
                    YouboraLog.warn("YouboraUtil/ MalformedJsonException: " + json)
                } catch (e: JsonSyntaxException) {
                    YouboraLog.warn("YouboraUtil/ JsonSyntaxException: " + json)
                } catch (_: Exception) {
                    YouboraLog.warn("YouboraUtil/ Exception: " + json)
                }
            }

            return balancerStats
        }

        @JvmStatic
        fun toJsonString(any: Any): String {
            return gson.toJson(any)
        }

        @JvmStatic
        fun toJsonTree(any: Any): JsonElement {
            return gson.toJsonTree(any)
        }
    }
}
