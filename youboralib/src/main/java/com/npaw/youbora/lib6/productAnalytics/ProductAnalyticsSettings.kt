package com.npaw.youbora.lib6.productAnalytics

class ProductAnalyticsSettings {
    var highlightContentAfter: Long = 1000
    var enableStateTracking: Boolean = false
    var activeStateTimeout: Long = 30000
    var activeStateDimension: Int = 9
}