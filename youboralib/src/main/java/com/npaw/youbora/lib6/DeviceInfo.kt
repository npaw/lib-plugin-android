package com.npaw.youbora.lib6

import android.app.UiModeManager
import android.content.Context
import android.content.res.Configuration
import org.json.JSONObject

/**
 * Utility class to obtain as much device information as possible, some values are set by default
 */
open class DeviceInfo {

    /**
     * Device model (e.g. MI 5), set by default using android.os.Build.MODEL
     */
    var deviceModel: String? = null
        private set

    /**
     * Device brand (e.g. Xiaomi), set by default using android.os.Build.BRAND
     */
    var deviceBrand: String? = null
        private set

    /**
     * Device type (e.g. pc, smartphone, stb, tv)
     */
    var deviceType: String? = null
        private set

    /**
     * Device name, internal identifier for the device
     */
    var deviceName: String? = null
        private set

    /**
     * Youbora's device code. If specified it will rewrite info gotten from user agent.
     * See a list of codes in [http://mapi.youbora.com:8081/devices](http://mapi.youbora.com:8081/devices).
     */
    var deviceCode: String? = null
        private set

    /**
     * Device OS name (e.g. Android)
     */
    var deviceOsName: String? = null
        private set

    /**
     * Device OS version (e.g. 8.1.0), set by default using android.os.Build.VERSION.RELEASE
     */
    var deviceOsVersion: String? = null
        private set

    /**
     * Device browser name, unset by default
     */
    var deviceBrowserName: String? = null
        private set

    /**
     * Device browser version, unset by default
     */
    var deviceBrowserVersion: String? = null
        private set

    /**
     * Device browser type, unset by default
     */
    var deviceBrowserType: String? = null
        private set

    /**
     * Device browser engine, unset by default
     */
    var deviceBrowserEngine: String? = null
        private set

    /**
     * This method maps all the device values to a JSON String
     * @return all device info as a JSON String
     */
    fun mapToJSONString(): String {
        val jsonObject = JSONObject()

        jsonObject.put("model", deviceModel)
        jsonObject.put("osVersion", deviceOsVersion)
        jsonObject.put("brand", deviceBrand)
        if (deviceType != null) jsonObject.put("deviceType", deviceType)
        if (deviceCode != null) jsonObject.put("deviceCode", deviceCode)
        if (deviceOsName != null) jsonObject.put("osName", deviceOsName)
        jsonObject.put("browserName", deviceBrowserName)
        jsonObject.put("browserVersion", deviceBrowserVersion)
        jsonObject.put("browserType", deviceBrowserType)
        jsonObject.put("browserEngine", deviceBrowserEngine)

        return jsonObject.toString()
    }

    /**
     * This Builder is used only for [DeviceInfo] class, since it has too many option parameters
     */
    class Builder {
        private var deviceModel: String? = android.os.Build.MODEL
        private var deviceBrand: String? = android.os.Build.BRAND
        private var deviceType: String? = null
        private var deviceName: String? = null
        private var deviceCode: String? = null
        private var deviceOsName: String? = null
        private var deviceOsVersion: String? = android.os.Build.VERSION.RELEASE
        private var deviceBrowserName: String = ""
        private var deviceBrowserVersion: String = ""
        private var deviceBrowserType: String = ""
        private var deviceBrowserEngine: String = ""

        fun setDeviceModel(deviceModel: String?) = apply {
            deviceModel?.let { this.deviceModel = it }
        }

        fun setDeviceBrand(deviceBrand: String?) = apply {
            deviceBrand?.let { this.deviceBrand = it }
        }

        fun setDeviceType(deviceType: String?, context: Context) = apply {
            this.deviceType = deviceType ?: detectDeviceType(context)
        }

        fun setDeviceName(deviceName: String?) = apply {
            deviceName?.let { this.deviceName = it }
        }

        fun setDeviceCode(deviceCode: String?) = apply {
            deviceCode?.let { this.deviceCode = it }
        }

        fun setDeviceOsName(deviceOsName: String?) = apply {
            deviceOsName?.let { this.deviceOsName = it }
        }

        fun setDeviceOsVersion(deviceOsVersion: String?) = apply {
            deviceOsVersion?.let { this.deviceOsVersion = it }
        }

        fun setDeviceBrowserName(deviceBrowserName: String) = apply {
            this.deviceBrowserName = deviceBrowserName
        }

        fun setDeviceBrowserVersion(deviceBrowserVersion: String) = apply {
            this.deviceBrowserVersion = deviceBrowserVersion
        }

        fun setDeviceBrowserType(deviceBrowserType: String) = apply {
            this.deviceBrowserType = deviceBrowserType
        }

        fun setDeviceBrowserEngine(deviceBrowserEngine: String) = apply {
            this.deviceBrowserEngine = deviceBrowserEngine
        }

        /**
         *  Determines if running on a Fire TV device or Android TV.
         *  @param context applicationContext
         *  @return String determing devcie type or **null** if running on a non-TV Device.
         */
        private fun detectDeviceType(context: Context) : String? {

            val amazonFireTVFeature: String = "amazon.hardware.fire_tv"
            val deviceModel: String = android.os.Build.MODEL
            val uiModeManager = context.getSystemService(Context.UI_MODE_SERVICE) as UiModeManager

            return when {
                deviceModel.startsWith("AFT") -> {
                    "Fire TV"
                }
                context.packageManager.hasSystemFeature(amazonFireTVFeature) -> {
                    "Fire TV"
                }
                uiModeManager.currentModeType == Configuration.UI_MODE_TYPE_TELEVISION -> {
                    "Android TV"
                }
                else -> {
                    null
                }
            }
        }

        fun build(): DeviceInfo {
            val deviceInfo = DeviceInfo()

            deviceInfo.deviceModel = this.deviceModel
            deviceInfo.deviceBrand = this.deviceBrand
            deviceInfo.deviceType = this.deviceType
            deviceInfo.deviceName = this.deviceName
            deviceInfo.deviceCode = this.deviceCode
            deviceInfo.deviceOsName = this.deviceOsName
            deviceInfo.deviceOsVersion = this.deviceOsVersion
            deviceInfo.deviceBrowserName = this.deviceBrowserName
            deviceInfo.deviceBrowserVersion = this.deviceBrowserVersion
            deviceInfo.deviceBrowserType = this.deviceBrowserType
            deviceInfo.deviceBrowserEngine = this.deviceBrowserEngine

            return deviceInfo
        }
    }
}
