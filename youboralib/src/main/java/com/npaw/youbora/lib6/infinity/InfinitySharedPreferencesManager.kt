package com.npaw.youbora.lib6.infinity

import android.content.Context

// Key names
private const val PREFERENCES_FILE_NAME = "youbora_infinity"
private const val PREFERENCES_SESSION_ID_KEY = "session_id"
private const val PREFERENCES_CONTEXT_KEY = "context_id"
private const val PREFERENCES_LAST_ACTIVE_KEY = "last_active_id"
private const val PREFERENCES_DEVICE_UUID = "device_uuid"

class InfinitySharedPreferencesManager(context: Context) : InfinityStorageContract {

    private val sharedPreferences = context
            .getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)

    override fun getSessionId(): String? { return getString(PREFERENCES_SESSION_ID_KEY) }
    override fun saveSessionId(sessionId: String) {
        saveString(PREFERENCES_SESSION_ID_KEY, sessionId)
    }

    override fun getContext(): String? { return getString(PREFERENCES_CONTEXT_KEY) }
    override fun saveContext(context: String) { saveString(PREFERENCES_CONTEXT_KEY, context) }

    override fun getLastActive(): Long { return getLong(PREFERENCES_LAST_ACTIVE_KEY) }
    override fun saveLastActive() {
        saveLong(PREFERENCES_LAST_ACTIVE_KEY, System.currentTimeMillis())
    }

    override fun getDeviceUUID(): String? { return getString(PREFERENCES_DEVICE_UUID) }
    override fun saveDeviceUUID(deviceUUID: String) {
        saveString(PREFERENCES_DEVICE_UUID, deviceUUID)
    }

    private fun getLong(key: String): Long { return sharedPreferences.getLong(key, -1L) }

    private fun getString(key: String): String? {
        return sharedPreferences.getString(key, null)
    }

    private fun saveLong(key: String, value: Long) {
        sharedPreferences.edit().putLong(key, value).apply()
    }

    private fun saveString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }
}
