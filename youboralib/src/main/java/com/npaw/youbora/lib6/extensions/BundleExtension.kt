package com.npaw.youbora.lib6.extensions

import android.os.Bundle

import org.json.JSONArray
import org.json.JSONObject

import java.util.HashMap

fun Bundle.putBoolean(key: String, value: Boolean?) {
    value?.let { putBoolean(key, it) }
}

fun Bundle.putDouble(key: String, value: Double?) {
    value?.let { putDouble(key, it) }
}

fun Bundle.putLong(key: String, value: Long?) {
    value?.let { putLong(key, it) }
}

fun Bundle.putInt(key: String, value: Int?) {
    value?.let { putInt(key, it) }
}

fun Bundle.putNotNullString(key: String, value: String?) {
    value?.let { putString(key, it) }
}

fun Bundle.putNotNullStringArrayList(key: String, value: ArrayList<String>?) {
    value?.let { putStringArrayList(key, it) }
}

fun Bundle.putNotNullIntegerArrayList(key: String, value: ArrayList<Int>?) {
    value?.let { putIntegerArrayList(key, it) }
}

fun Bundle.putNotNullBundle(key: String, value: Bundle?) {
    value?.let { putBundle(key, it) }
}

fun Bundle.toJson(): JSONObject? {
    val json = JSONObject()

    keySet().filter {
        get(it) != null
    }.forEach { key ->
        var value = get(key)

        value?.let {
            when {
                // If the value is a Bundle, recursive call
                it is Bundle -> value = it.toJson()
                it is Map<*, *> -> value = JSONObject(it)
                it is List<*> -> value = JSONArray(it)
                it.javaClass.isArray -> {
                    // Use a map as wrapper, convert to json and extract json array
                    val m = HashMap<String, Any>().apply {
                        put("k", it)
                    }

                    value = JSONObject(m.toMap()).getJSONArray("k")
                }
            }
        }

        json.put(key, value)
    }

    return json
}

fun Bundle.toMap(): MutableMap<String, String>? {
    val map: MutableMap<String, String> = HashMap()

    val ks: Set<String> = keySet()
    val iterator = ks.iterator()
    while (iterator.hasNext()) {
        val key = iterator.next()
        getString(key)?.let { map[key] = it }
    }

    return map
}