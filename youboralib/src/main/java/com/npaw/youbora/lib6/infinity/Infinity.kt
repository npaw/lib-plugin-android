package com.npaw.youbora.lib6.infinity

import android.content.Context

import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.comm.Communication
import com.npaw.youbora.lib6.comm.transform.ViewTransform
import com.npaw.youbora.lib6.flags.Flags
import com.npaw.youbora.lib6.plugin.Options
import com.npaw.youbora.lib6.plugin.Plugin

open class Infinity(val context: Context, var viewTransform: ViewTransform,
               infinityEventListener: InfinityEventListener, val options: Options) {

    private var infinityStorage: InfinityStorageContract? = null

    var communication: Communication? = null
        private set

    open val flags = Flags()

    // Listener list
    private var eventListeners = arrayListOf(infinityEventListener)

    fun getLastSent(): Long? { return infinityStorage?.getLastActive() }
    fun getNavContext(): String { return YouboraUtil.getApplicationName(context) }

    @JvmOverloads
    fun begin(screenName: String?, dimensions: MutableMap<String, String> = HashMap()) {
        if (!flags.isStarted) {
            flags.isStarted = true
            communication = Communication(options)
            communication?.addTransform(viewTransform)
            communication?.addTransform(TimestampLastSentTransform(context))
            fireSessionStart(screenName, dimensions)
        } else {
            fireNav(screenName)
        }
    }

    private fun fireSessionStart(screenName: String?, dimensions: MutableMap<String, String>) {
        infinityStorage = InfinitySharedPreferencesManager(context)
        // We use the timestamp as a unique id "locally"
        generateNewContext()
        eventListeners.forEach { it.onSessionStart(screenName, dimensions) }
    }

    fun fireNav(screenName: String?) { eventListeners.forEach { it.onNav(screenName) } }

    @JvmOverloads
    fun fireEvent(eventName: String?, dimensions: MutableMap<String, String> = HashMap(),
                  values: MutableMap<String, Double> = HashMap(),
                  topLevelDimensions: MutableMap<String, String> = HashMap()) {
        eventListeners.forEach { it.onEvent(eventName, dimensions, values, topLevelDimensions) }
    }

    private fun fireSessionStop(params: MutableMap<String, String>) {
        flags.reset()
        eventListeners.forEach { it.onSessionStop(params) }
    }

    @JvmOverloads
    fun end(params: MutableMap<String, String> = HashMap()) {
        if (flags.isStarted) { fireSessionStop(params) }
    }

    /**
     * Adds an event listener that will be invoked whenever a "fire*" method is called.
     * @param eventListener the listener to add
     */
    fun addEventListener(eventListener: InfinityEventListener) { eventListeners.add(eventListener) }

    /**
     * Remove an event listener
     * @param eventListener listener ot remove
     * @return whether the listener has been removed or not
     */
    fun removeEventListener(eventListener: InfinityEventListener): Boolean {
        return eventListeners.remove(eventListener)
    }

    private fun generateNewContext() {
        //For now we only will have one context that will be the app name
        infinityStorage?.saveContext(YouboraUtil.getApplicationName(context))
    }

    /**
     * Listener interface. The methods will be called whenever each corresponding event is fired.
     * These events are listened by the [Plugin] in order to send the corresponding request.
     * They are only used by Infinity class
     */
    interface InfinityEventListener {
        /**
         * Infinity detected session start event.
         * @param screenName name to be displayed in Youbora
         * @param dimensions map with all desired dimensions
         */
        fun onSessionStart(screenName: String?, dimensions: MutableMap<String, String>)

        /**
         * Infinity detected session stop event.
         * @param params params to add to the request
         */
        fun onSessionStop(params: MutableMap<String, String>)

        /**
         * Infinity detected session nav event.
         * @param screenName name to be displayed in Youbora
         */
        fun onNav(screenName: String?)

        /**
         * Infinity detected session nav event.
         * @param eventName name of the sent event
         * @param dimensions map with all desired dimensions
         * @param values map with all desired values
         */
        fun onEvent(eventName: String?, dimensions: MutableMap<String, String>,
                    values: MutableMap<String, Double>, topLevelDimensions: MutableMap<String, String>)
    }
}
