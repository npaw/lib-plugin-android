package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.comm.Request
import com.npaw.youbora.lib6.plugin.Options

import java.net.HttpURLConnection
import java.util.regex.Pattern

/**
 * Class to get the real resource behind a dash manifest.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
open class DashParser(private val manifestHeaders: MutableMap<String, String>? = null) : Parser(manifestHeaders) {
    override fun parse(resource: String?, parentResource: String?, lastManifest: String?) {
        if (lastManifest == null) {
            val request = createRequest(resource)
            manifestHeaders?.let { request.requestHeaders = it }
            request.maxRetries = 0
            request.addOnSuccessListener { _, response, _, _ ->
                this.lastManifest = response
                processManifest(resource, response)
            }

            request.addOnErrorListener(object: Request.RequestErrorListener {
                override fun onRequestError(connection: HttpURLConnection?) { done() }
                override fun onRequestRemovedFromQueue() {}
            })

            request.send()
        } else {
            processManifest(resource, lastManifest)
        }
    }

    private fun processManifest(resource: String?, response: String) {
        val matcher = Pattern.compile(".*<Location>(.+)</Location>.*").matcher(response)
        var location: String? = null
        if (matcher.find()) {
            location = matcher.toMatchResult().group(1)
        }

        if (location?.contentEquals(resource) == false) {
            parse(location, null, null)
        } else {
            parseFinalResource(resource, response)
        }
    }

    private fun parseFinalResource(resource: String?, response: String?) {
        response?.let {
            val baseUrl = Pattern.compile(".*<BaseURL>(.+)</BaseURL>.*").matcher(it)
            val segmentUrl = Pattern.compile(".*<SegmentURL.*media=\"([^\"]+)\".*").matcher(it)
            val segmentTemplate = Pattern.compile(".*<SegmentTemplate.*media=\"([^\"]+)\".*").matcher(it)

            getManifestMetadata(it)

            realResource = when {
                baseUrl.find() -> baseUrl.toMatchResult().group(1)
                segmentUrl.find() -> segmentUrl.toMatchResult().group(1)
                segmentTemplate.find() -> segmentTemplate.toMatchResult().group(1)
                else -> resource
            }
        }

        realResource = realResource?.takeIf { it.startsWith("http") } ?: resource

        this.lastManifest = response
        done()
    }

    private fun getManifestMetadata(manifest: String) {
        val adaptationSet = Pattern
                .compile(".*<AdaptationSet.*mimeType=\"video/([^\"]+)\".*").matcher(manifest)

        if (adaptationSet.find()) {
            transportFormat = when (adaptationSet.toMatchResult().group(1)) {
                "mp4", "m4s" -> Options.TransportFormat.MP4
                "mp2t" -> Options.TransportFormat.TS
                "cmfv" -> Options.TransportFormat.CMF
                else -> null
            }
        }
    }

    override fun shouldExecute(lastManifest: String?): Boolean {
        return lastManifest?.contains("<MPD") ?: false
    }

    internal open fun createRequest(host: String?): Request {
        return Request(host, null)
    }
}