package com.npaw.youbora.lib6.flags

open class AdFlags : BaseFlags() {

    open var isAdInitiated = false
    open var isAdBreakStarted = false

    override fun reset() {
        super.reset()
        isAdInitiated = false
    }
}