package com.npaw.youbora.lib6.adapter

import com.npaw.youbora.lib6.Chrono

/**
 * This class contains all the [Chrono]s related to view status.
 * Chronos measure time lapses between events.
 * ie: between start and join, between seek-begin and seek-end, etc.
 * Each plugin will have an instance of this class.
 * @author      Nice People at Work
 * @since       6.0
 */
open class PlaybackChronos {
    /** Chrono between start and joinTime.  */
    open var join = Chrono()

    /** Chrono between seek-begin and seek-end.  */
    open var seek = Chrono()

    /** Chrono between pause and resume.  */
    open var pause = Chrono()

    /** Chrono between buffer-begin and buffer-end.  */
    open var buffer = Chrono()

    /** Chrono for the totality of the view.  */
    open var total = Chrono()

    /** Chrono for the ad init time.  */
    open var adInit = Chrono()

    /** Chrono for the ad viewability */
    open var adViewability = Chrono()

    var adViewedPeriods = mutableListOf<Long>()

    /**
     * Reset flag values
     */
    fun reset() {
        join = Chrono()
        seek = Chrono()
        pause = Chrono()
        buffer = Chrono()
        total = Chrono()
        adInit = Chrono()
    }
}
