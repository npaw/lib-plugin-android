package com.npaw.youbora.lib6.persistence.entity;

/**
 * Created by Enrique on 27/12/2017.
 */

public class Event {

    private int uid;

    private String jsonEvents;

    private Long dateUpdate;

    private int offlineId;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Event(){}

    public Event(int uid, String jsonEvents, Long dateUpdate, int offlineId) {
        this.uid = uid;
        this.jsonEvents = jsonEvents;
        this.dateUpdate = dateUpdate;
        this.offlineId = offlineId;
    }

    public Event(String jsonEvents, Long dateUpdate, int offlineId) {
        this.jsonEvents = jsonEvents;
        this.dateUpdate = dateUpdate;
        this.offlineId = offlineId;
    }

    public String getJsonEvents() {
        return jsonEvents;
    }

    public void setJsonEvents(String jsonEvents) {
        this.jsonEvents = jsonEvents;
    }

    public Long getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Long dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public int getOfflineId() {
        return offlineId;
    }

    public void setOfflineId(int offlineId) {
        this.offlineId = offlineId;
    }
}
