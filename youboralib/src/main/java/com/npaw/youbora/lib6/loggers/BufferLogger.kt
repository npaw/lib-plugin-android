package com.npaw.youbora.lib6.loggers

import com.npaw.youbora.lib6.YouboraLog
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class BufferLogger : YouboraLog.YouboraLogger {

    private var bufferLogs : String = "";
    private var df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").apply {
        timeZone = TimeZone.getTimeZone("UTC");
    }

    @Synchronized
    override fun logYouboraMessage(message: String, logLevel: YouboraLog.Level) {
        bufferLogs+="${getInstant()} ${logLevel}: $message\n"
    }

    @Synchronized
    fun readBufferLogs(maxSize : Int = -1): String {
        var logs = ""
        var lastIndex = 0
        if(maxSize == -1 || bufferLogs.length <= maxSize ) {
            lastIndex = bufferLogs.length-1
        } else {
            lastIndex = bufferLogs.substring(0, maxSize).lastIndexOf("\n")
            if (lastIndex == -1) {
                lastIndex = bufferLogs.length-1
            }
        }

        if (lastIndex == bufferLogs.length-1) {
            logs = bufferLogs
            bufferLogs = ""
        } else {
            logs = bufferLogs.substring(0, lastIndex)
            bufferLogs = bufferLogs.substring(lastIndex+1)
        }

        return logs
    }
    @Synchronized
    fun size(): Int {
        return bufferLogs.length
    }

    private fun getInstant(): String {
        return df.format(Date())
    }
}