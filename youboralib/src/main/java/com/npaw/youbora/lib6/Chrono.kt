package com.npaw.youbora.lib6

/**
 * Utility class that provides chronometer like functionality.
 * Used to calculate the elapsed time between [start] and [stop] calls.
 * @author      Nice People at Work
 * @since       6.0
 */
open class Chrono {

    var startTime: Long? = null
    var stopTime: Long? = null
    var pauseTime: Long? = null

    /**
     * Offset to be added to deltaTime and stop (ms).
     */
    var offset = 0L

    /**
     * Reset the [Chrono] to its initial state.
     */
    open fun reset() {
        startTime = null
        stopTime = null
        pauseTime = null
        offset = 0L
    }

    /**
     * Starts this Chronometer.
     *
     * If the chronometer was already started, calling this method restarts it.
     */
    open fun start() {
        startTime = getNow()
        stopTime = null
        offset = 0L
    }

    fun pause() {
        pauseTime = getNow()
    }

    fun resume() {
        offset -= getNow() - (pauseTime ?: 0)
        pauseTime = null
    }

    /**
     * Stop this Chronometer.
     *
     * @return the total milliseconds that happened between the last call to [start] method and now.
     * If the chronometer was not started before then a -1 is returned.
     */
    open fun stop(): Long {
        pauseTime?.let { resume() }

        stopTime = getNow()

        return getDeltaTime()
    }

    /**
     * Stops the [Chrono].
     *
     * Use [getDeltaTime] with a false param in case you want to know the difference time without
     * stopping the chronometer.
     *
     * @return the total milliseconds that happened between the last call to [start] method and the
     * time that this chronometer has been stopped. If the chronometer was not started before then a
     * -1 is returned.
     */
    open fun getDeltaTime(): Long { return getDeltaTime(true) }

    /**
     * This method will stop the current chronometer depending on the value of the parameter
     * [stopIfNeeded].
     *
     * @param stopIfNeeded boolean value indicating whether this query should stop the chronometer
     * or not. If the chronometer has been already stopped this will have no effect.
     * @return the total milliseconds that happened between the last call to [start] method and the
     * time that this chronometer has been stopped. If the chronometer was not started before then
     * -1 is returned.
     */
    open fun getDeltaTime(stopIfNeeded: Boolean): Long {
        val now = getNow()

        return startTime?.let { startT ->
            if (stopIfNeeded && stopTime == null) stop()

            val tempOffset = pauseTime?.let { now - it } ?: 0
            val tempStop = stopTime ?: now

            offset - tempOffset + (tempStop - startT)
        } ?: -1
    }

    /**
     * Creates and returns a copy of the current Chrono.
     * @return a copy of the Chrono
     */
    fun copy(): Chrono {
        val c = Chrono()

        c.startTime = startTime
        c.stopTime = stopTime
        c.pauseTime = pauseTime
        c.offset = offset

        return c
    }

    companion object {

        /**
         * Gets a timestamp in milliseconds. This is NOT the same as [System.currentTimeMillis].
         * @return timestamp in milliseconds
         */
        @JvmStatic fun getNow(): Long { return System.nanoTime() / 1000000 }
    }
}