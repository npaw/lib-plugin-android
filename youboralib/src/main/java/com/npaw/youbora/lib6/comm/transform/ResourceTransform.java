package com.npaw.youbora.lib6.comm.transform;

import android.os.Handler;
import android.os.Looper;

import com.npaw.youbora.lib6.comm.transform.resourceparse.*;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.npaw.youbora.lib6.constants.Services.START;

/**
 * Parses resource urls to get transportstreams and CDN-related info.
 * @author      Nice People at Work
 * @since       6.0
 */
public class ResourceTransform extends Transform {

    private Plugin plugin;
    private boolean manifestEnabled;
    private boolean cdnEnabled;
    private Queue<String> cdnList;
    private Map<String, String> manifestHeaders;
    private ArrayList<String> cdnNameHeaders;
    private String nodeHeader;

    private String realResource;
    private String initResource;
    private String transportFormat;
    private String cdnName;
    private String cdnNodeHost;
    private String cdnNodeTypeString;
    private CdnTypeParser.Type cdnNodeType;

    private Runnable timeoutRunnable;
    private Handler timeoutHandler;

    /**
     * Constructor
     * @param plugin the plugin this ResourceTransform will use to get the info it needs
     */
    public ResourceTransform(Plugin plugin) {
        super();
        this.plugin = plugin;
        isBusy = false;
    }

    /**
     * Get the resource. If the transform is done, the real (parsed) resource will be returned
     * Otherwise the initial one is returned.
     * @return the initial or parsed resource
     */
    public String getResource() {
        if (plugin != null && plugin.getOptions() != null
                && plugin.getOptions().getContentResource() != null
                && !plugin.getOptions().isParseManifest()) {
            return plugin.getOptions().getContentResource();
        } else if (realResource != null) {
            return realResource;
        } else {
            return initResource;
        }
    }

    public String getTransportFormat() {
        return transportFormat;
    }

    /**
     * Get CDN name
     * @return the CDN name or null if unknown
     */
    public String getCdnName() {
        return cdnName;
    }

    /**
     * Get CDN node
     * @return the CDN node or null if unknown
     */
    public String getNodeHost() {
        return cdnNodeHost;
    }

    /**
     * Get CDN type string, as returned in the cdn header response
     * @return the CDN type string
     */
    public String getNodeTypeString() {
        return cdnNodeTypeString;
    }

    /**
     * Get CDN type, parsed from the type string
     * @return the CDN type
     */
    public String getNodeType() {
        if (cdnNodeType != null) {
            return Integer.toString(cdnNodeType.getValue());
        } else {
            return null;
        }
    }

    /**
     * Start the execution. Can be called more than once. If already running, it will be ignored,
     * if ended it will restart.
     * @param resource the original resource
     */
    public void init(String resource) {
        if (!isBusy) {
            isBusy = true;

            manifestEnabled = plugin.isParseManifest();
            cdnEnabled = plugin.isParseCdnNode();

            cdnList = new LinkedList<>(plugin.getParseCdnNodeList());
            manifestHeaders = plugin.getParseManifestAuth();
            cdnNameHeaders = plugin.getParseCdnNodeNameHeaderList();
            nodeHeader = plugin.getParseNodeHeader();

            if (nodeHeader != null) CdnParser.setBalancerNodeHeader(nodeHeader);
            if (cdnNameHeaders != null && cdnNameHeaders.size() > 0) CdnParser.setBalancerHeaderName(cdnNameHeaders);

            initResource = resource;

            setTimeout();

            if (manifestEnabled) {
                parseManifest();
            } else {
                parseCdn();
            }
        }
    }

    private void parseManifest() {
        parseManifest(null, null);
    }

    public void parseManifest(String lastManifest, String lastSrc) {
        //Be careful, order is important
        List<Parser> parsers = Arrays.asList(createLocationHeaderParser(manifestHeaders), createManifestParser(manifestHeaders),
                createDashParser(manifestHeaders), createHlsParser(manifestHeaders));
        String src = lastSrc == null ? initResource : lastSrc;
        parseManifest(lastManifest, src, parsers, null);
    }

    private void parseManifest(String lastManifest, String lastSrc, final List<Parser> parsers, final String format) {
        if (parsers != null && !parsers.isEmpty()) {
            final Parser currentParser = parsers.get(0);

            if (currentParser.shouldExecute(lastManifest)) {
                currentParser.addParserTransformListener(new Parser.ParserTransformListener() {
                    @Override
                    public void onParserTransformDone(@Nullable String parsedResource) {
                        String f = format;

                        if (f == null) {
                            f = currentParser.getTransportFormat();
                        }

                        String regex = "^(https?)://[^\\s/$.?#].[^\\s]*$";
                        Pattern pattern = java.util.regex.Pattern.compile(regex, java.util.regex.Pattern.CASE_INSENSITIVE);
                        Matcher matcher = pattern.matcher(parsedResource);

                        String srcToParse = matcher.matches() ? parsedResource : lastSrc;

                        parseManifest(currentParser.getLastManifest(), srcToParse, parsers.subList(1, parsers.size()), f);
                    }
                });
                currentParser.parse(lastSrc, null, lastManifest);
            } else {
                parseManifest(lastManifest, lastSrc, parsers.subList(1, parsers.size()), format);
            }
        } else {
            transportFormat = format;
            realResource = lastSrc;
            parseCdn();
        }
    }


    private void setTimeout() {
        // Abort operation after 3 seconds
        if (timeoutHandler == null) {
            timeoutHandler = createHandler();
        }

        if (timeoutRunnable == null) {
            timeoutRunnable = new Runnable() {
                @Override
                public void run() {
                    if (isBusy) {
                        done();
                        YouboraLog.warn("ResourceTransform has exceeded the maximum execution time (3s) and will be aborted.");
                    }
                }
            };
        }

        timeoutHandler.postDelayed(timeoutRunnable, (long) (3 * 1000));
    }

    Handler createHandler() {
        return Looper.myLooper() == null ? new Handler(Looper.getMainLooper()) : new Handler();
    }

    HlsParser createHlsParser(Map<String, String> manifestHeaders) {
        return new HlsParser(manifestHeaders);
    }


    DashParser createDashParser(Map<String, String> manifestHeaders) {
        return new DashParser(manifestHeaders);
    }

    LocationHeaderParser createLocationHeaderParser(Map<String, String> manifestHeaders) {
        return new LocationHeaderParser(manifestHeaders);
    }

    ManifestParser createManifestParser(Map<String, String> manifestHeaders) {
        return new ManifestParser(manifestHeaders);
    }

    private void parseCdn() {
        if (plugin != null && plugin.getOptions() != null && plugin.getOptions().isParseCdnSwitchHeader()) {
            CdnSwitch switchDetector = new CdnSwitch(plugin);
            switchDetector.addCdnTransformListener(new CdnSwitch.CdnTransformListener() {
                @Override
                public void onCdnTransformDone(@Nullable CdnSwitch cdnSwitch, @Nullable String cdn) {
                    cdnName = cdn;
                    done();
                }

                @Override
                public void onCdnTransformError(@Nullable CdnSwitch cdnSwitch) {
                    done();
                }
            });

            switchDetector.init();
        } else if (cdnEnabled && !cdnList.isEmpty()) {
            try {
                String cdn = cdnList.remove();

                if (getNodeHost() != null) {
                    done();
                }

                CdnParser cdnParser = createCdnParser(cdn);

                if (cdnParser == null) {
                    parseCdn();
                } else {
                    cdnParser.addCdnTransformListener(new CdnParser.CdnTransformListener() {
                        @Override
                        public void onCdnTransformDone(CdnParser cdnParser) {
                            cdnName = cdnParser.getCdnName();
                            cdnNodeHost = cdnParser.getNodeHost();
                            cdnNodeTypeString = cdnParser.getNodeTypeString();
                            cdnNodeType = cdnParser.getNodeType();

                            if (getNodeHost() != null) {
                                done();
                            } else {
                                parseCdn();
                            }
                        }
                    });

                    cdnParser.parse(getResource(), null);
                }
            } catch (NoSuchElementException e) {
                YouboraLog.error(e);
                done();
            }
        } else {
            done();
        }
    }

    CdnParser createCdnParser(String cdn) {
        return CdnParser.create(cdn);
    }

    /**
     * {@inheritDoc}
     *
     * Replaces the resource and / or Cdn info for the /start service.
     */
    @Override
    public void parse(Request request) {
        if (START.equals(request.getService())) {

            Map<String, String> lastSent = plugin.getRequestBuilder().getLastSent();

            request.setParam("transportFormat", getTransportFormat());
            lastSent.put("transportFormat", getTransportFormat());

            if (manifestEnabled) {
                request.setParam("parsedResource", getResource());
                lastSent.put("parsedResource", getResource());
            }

            if (cdnEnabled) {
                String cdn = (String) request.getParam("cdn");
                if (cdn == null) {
                    cdn = getCdnName();
                    request.setParam("cdn", cdn);
                }
                lastSent.put("cdn", cdn);

                request.setParam("nodeHost", getNodeHost());
                lastSent.put("nodeHost", getNodeHost());

                request.setParam("nodeType", getNodeType());
                lastSent.put("nodeType", getNodeType());

                request.setParam("nodeTypeString", getNodeTypeString());
                lastSent.put("nodeTypeString", getNodeTypeString());
            }
        }
    }
}
