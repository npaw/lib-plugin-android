package com.npaw.youbora.lib6.balancer.models

import com.google.gson.annotations.SerializedName

open class BalancerStats(
    @SerializedName("profileName")
    var profileName: String? = null,
    @SerializedName("cdnStats")
    var cdnStats: CdnLoaderStats? = null,
    @SerializedName("p2pStats")
    var p2pStats: P2PStats? = null,
    @SerializedName("segmentDuration")
    var segmentDuration: Long? = null,
    @SerializedName("version")
    val version: String? = null
)
