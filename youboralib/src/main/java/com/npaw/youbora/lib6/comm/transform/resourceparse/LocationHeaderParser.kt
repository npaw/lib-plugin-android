package com.npaw.youbora.lib6.comm.transform.resourceparse

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.comm.Request

import org.json.JSONException
import org.json.JSONObject

import java.net.HttpURLConnection

open class LocationHeaderParser(private val manifestHeaders: MutableMap<String, String>? = null) : Parser(manifestHeaders) {

    /**
     * Parses the API request url to find and locate the real resource.
     * @param resource url to start parsing
     */
    override fun parse(resource: String?, parentResource: String?, lastManifest: String?) {
        val request = createRequest(resource)
        manifestHeaders?.let { request.requestHeaders = it }
        request.maxRetries = 0
        request.addOnSuccessListener { connection, response, _, _ ->
            var url = connection.url.toString()

            try {
                url = JSONObject(response).getJSONObject("result").getString("url")
            } catch (e: JSONException) {
                YouboraLog.debug("Response isn't a JSON object")
            } finally {
                if (resource != null && url != resource) {
                    parse(url)
                } else {
                    val responseHeaders = connection.headerFields

                    this.realResource = responseHeaders["Location"]?.get(0) ?: resource
                    this.lastManifest = response

                    done()
                }
            }
        }

        request.addOnErrorListener(object: Request.RequestErrorListener {
            override fun onRequestError(connection: HttpURLConnection?) { done() }
            override fun onRequestRemovedFromQueue() {}
        })

        realResource = resource
        request.send()
    }

    internal open fun createRequest(host: String?): Request {
        return Request(host, null)
    }
}
