package com.npaw.youbora.lib6

import android.os.Handler
import android.os.Looper

import java.util.ArrayList

/**
 * An Utility class that provides timed events in a defined time interval.
 * @author      Nice People at Work
 * @since       6.0
 */
open class Timer @JvmOverloads constructor(callback: TimerEventListener, private var interval: Long, customHandler: Handler? = null) {

    /** List of [TimerEventListener].  */
    private var callbacks = ArrayList<TimerEventListener>(1)

    /** Chrono to inform the callback how much time has passed since the previous call.
     * Getter for chrono
     * @return the Chrono instance associated with this one
     */
    var chrono: Chrono = Chrono()

    /** Whether the Timer is running or not.
     * Getter for isRunning property.
     * @return true if the [Timer] is currently running
     */
    var isRunning = false
        private set

    /**
     * Runnable to post in the [handler].
     */
    private val tickRunnable = Runnable {
        val delta = chrono.getDeltaTime()

        val c = Chrono()
        c.start()

        callbacks.iterator().forEach { it.onTimerEvent(delta) }

        c.stop()
        setNextTick()
    }

    /**
     * [Handler] to post the delayed execution of the [callbacks].
     * This handler is created when the Timer is created, ensuring that the callback will be
     * executed on the same thread that created the Timer.
     * If [Timer] is instantiated from a [Thread] without a [Looper] the UI [Looper] is used instead.
     */
    private var handler: Handler

    init {
        handler = customHandler ?: Looper.myLooper()?.let { Handler(it) } ?: Handler(Looper.getMainLooper())
        addTimerCallback(callback)
    }

    constructor(callback: TimerEventListener) : this(callback, 5000)

    /**
     * Starts the timer.
     */
    open fun start() {
        if (!isRunning) {
            isRunning = true
            setNextTick()
            YouboraLog.notice("Timer started: every $interval ms")
        }
    }

    /**
     * Stops the timer.
     */
    open fun stop() {
        if (isRunning) {
            isRunning = false
            handler.removeCallbacks(tickRunnable)
        }
    }

    /**
     * Schedules the [handler] to run the [tickRunnable] in [interval] milliseconds.
     */
    private fun setNextTick() {
        if (isRunning) {
            chrono.start()
            handler.postDelayed(tickRunnable, interval)
        }
    }

    /**
     * Add a [TimerEventListener] to the callback list. Its [TimerEventListener.onTimerEvent]
     * will be called every [interval] seconds.
     * @param listener The [TimerEventListener] to add
     */
    fun addTimerCallback(listener: TimerEventListener) {
        callbacks.add(listener)
    }

    /**
     * Updates the interval at which the [TimerEventListener] is called.
     * @param interval new interval in milliseconds
     */
    fun setInterval(interval: Int) {
        this.interval = interval.toLong()
    }

    /**
     * Interface defined so any class willing to become a listener to Timer class events can add
     * custom behaviour.
     * @author      Nice People at Work
     * @since       6.0
     */
    interface TimerEventListener {
        /**
         * Method called periodically by the [Timer] class every [Timer.interval]
         * seconds.
         * @param delta a time delta (difference between two timestamps) in milliseconds.
         */
        fun onTimerEvent(delta: Long)
    }
}