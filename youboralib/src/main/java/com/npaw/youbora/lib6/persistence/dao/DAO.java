package com.npaw.youbora.lib6.persistence.dao;

import java.util.List;

public interface DAO<T> {
    Long insertNewElement(T element);

    Long insertElements(List<T> elements);

    T getElement(String[] whereColumns, String[] whereValues, String groupBy, String having,
                 String orderBy, String limit);

    List<T> getElements(String[] whereColumns, String[] whereValues, String groupBy, String having,
                        String orderBy, String limit);

    List<T> getAll();

    Integer deleteElement(String whereClause, String[] whereArgs);

    Integer deleteAll();
}
