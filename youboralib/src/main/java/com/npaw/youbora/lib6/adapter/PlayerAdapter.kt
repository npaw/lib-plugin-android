package com.npaw.youbora.lib6.adapter

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraUtil.Companion.stringifyMap
import com.npaw.youbora.lib6.extensions.concatTriggeredEventsAndJoinInParams
import com.npaw.youbora.lib6.extensions.printMap

open class PlayerAdapter<PlayerT>(player: PlayerT?) : BaseAdapter<PlayerT>(player) {

    /**
     * Override to return current playrate
     * @return the current play rate of the media (0 is paused, 1 is playing at normal speed)
     */
    open fun getPlayrate(): Double { return if (flags.isPaused) 0.0 else 1.0 }

    /**
     * Override to return Frames Per Second (FPS)
     * @return the current FPS of the media
     */
    open fun getFramesPerSecond(): Double? { return null }

    /**
     * Override to return dropped frames since start
     * @return the total dropped frames
     */
    open fun getDroppedFrames(): Int? { return null }

    /**
     * Override to return user bandwidth throughput
     * @return the current throughput in bits per second
     */
    open fun getThroughput(): Long? { return null }

    /**
     * Override to return program
     * @return the program. It may be program name, episode, etc.
     */
    open fun getProgram(): String? { return null }

    /**
     * Override to recurn true if live and false if VOD
     * @return whether the currently playing content is a live stream or not
     */
    open fun getIsLive(): Boolean? { return null }

    /**
     * Override to return player latency.
     * @return the current latency
     */
    open fun getLatency(): Double? { return null }

    /**
     * Override to return lost packets
     * @return the current packets being lost
     */
    open fun getPacketLoss(): Int? { return null }

    /**
     * Override to return sent packets
     * @return the current packets being sent
     */
    open fun getPacketSent(): Int? { return null }

    /**
     * Override to return a map with metrics
     * @return metrics
     */
    open fun getMetrics(): Map<*, *>? { return null }

    /**
     * Override to return household id
     * @return household player id
     */
    open fun getHouseholdId(): String? { return null }
    open fun getTotalBytes(): Long? { return null }
    open fun getUrlToParse(): String? { return null }

    open fun getCdnTraffic(): Long? { return null }
    open fun getP2PTraffic(): Long? { return null }
    open fun getAudioCodec(): String? { return null }
    open fun getVideoCodec(): String? { return null }
    open fun getUploadTraffic(): Long? { return null }
    open fun getIsP2PEnabled(): Boolean? { return null }

    /**
     * Emits event request.
     * @param eventName name of the event
     * @param dimensions key value map with dimensions
     * @param values key value map with values
     */
    @JvmOverloads
    open fun fireEvent(eventName: String = "", dimensions: MutableMap<String, String> = HashMap(),
                       values: MutableMap<String, Double> = HashMap(),
                       topLevelDimensions: MutableMap<String, String> = HashMap()) {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[PlayerAdapter:$adapterId] fireEvent isStarted:${flags.isStarted} eventName: $eventName dimensions: ${dimensions.printMap()} values: ${values.printMap()} topLevelDimensions: ${topLevelDimensions.printMap()}" )
        if (flags.isStarted) {
            eventListeners.iterator().forEach { event ->
                if (event is ContentAdapterEventListener) {
                    event.onVideoEvent(topLevelDimensions.apply {
                        put("name", eventName)
                        put("values", stringifyMap(values) ?: "{}")
                        put("dimensions", stringifyMap(dimensions) ?: "{}")
                    })
                }
            }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     * @param convertFromBuffer whether to convert an existing buffer into seek or not
     */
    @JvmOverloads
    open fun fireSeekBegin(convertFromBuffer: Boolean = true,
                           params: MutableMap<String, String> = java.util.HashMap()) {
        val noSeekConfig =  plugin?.let {(it.isLive && it.options.contentIsLiveNoSeek)}
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[PlayerAdapter:$adapterId] fireSeekBegin noSeekConfig:${noSeekConfig} flags.isJoined: ${flags.isJoined} flags.isSeeking: ${flags.isSeeking} flags.isBuffering: ${flags.isBuffering}" )

        if (noSeekConfig == true)
            return

        if (flags.isJoined && !flags.isSeeking) {
            if (flags.isBuffering) {
                if (convertFromBuffer) {
                    YouboraLog.notice("Converting current buffer to seek")

                    chronos.seek = chronos.buffer.copy()
                    chronos.buffer.reset()

                    triggeredEventsSeekMap.putAll(triggeredEventsBufferMap)
                    triggeredEventsBufferMap.clear()

                    flags.isBuffering = false
                } else {
                    return
                }
            } else {
                chronos.seek.start()
            }

            triggeredEventsSeekMap.concatTriggeredEventsAndJoinInParams(params)

            flags.isSeeking = true

            eventListeners.iterator().forEach { event ->
                if (event is ContentAdapterEventListener)
                    event.onSeekBegin(convertFromBuffer, params)
            }
        }
    }

    /**
     * Emits related event and set flags if current status is valid.
     * @param params Map of key:value pairs to add to the request.
     */
    @JvmOverloads
    open fun fireSeekEnd(params: MutableMap<String, String> = java.util.HashMap()) {
        val noSeekConfig =  plugin?.let {(it.isLive && it.options.contentIsLiveNoSeek)}
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[PlayerAdapter:$adapterId] fireSeekEnd noSeekConfig:${noSeekConfig} flags.isJoined: ${flags.isJoined} flags.isSeeking: ${flags.isSeeking}" )

        if (noSeekConfig == true) {
            triggeredEventsSeekMap.clear()
            return
        }

        if (flags.isJoined && flags.isSeeking) {
            flags.isSeeking = false
            chronos.seek.stop()
            monitor?.skipNextTick()

            triggeredEventsSeekMap.concatTriggeredEventsAndJoinInParams(params)
            val params = triggeredEventsSeekMap.toMutableMap()

            eventListeners.iterator().forEach { event ->
                if (event is ContentAdapterEventListener) event.onSeekEnd(params)
            }

            if (chronos.pause.getDeltaTime(false) > 0) {
                chronos.pause.resume();
            }
        }
        triggeredEventsSeekMap.clear()
    }

    open fun fireCast() {
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT,"[PlayerAdapter:$adapterId] fireCast" )
        fireStop(HashMap<String, String>().apply {
            put("casted", "true")
        })
    }

    interface ContentAdapterEventListener : AdapterEventListener {

        /**
         * Adapter detected a video event.
         * @param params to be added to the request
         */
        fun onVideoEvent(params: MutableMap<String, String>)

        /**
         * Adapter detected a seek begin event.
         * * @param convertFromBuffer whether the seek has been converted from a buffer or not
         * @param params to add to the request
         */
        fun onSeekBegin(convertFromBuffer: Boolean, params: MutableMap<String, String>)

        /**
         * Adapter detected a seek end event.
         * @param params to add to the request
         */
        fun onSeekEnd(params: MutableMap<String, String>)
    }
}