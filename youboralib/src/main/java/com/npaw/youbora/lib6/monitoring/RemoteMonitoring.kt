package com.npaw.youbora.lib6.monitoring

import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.Timer.TimerEventListener
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraLog.Companion.addLogger
import com.npaw.youbora.lib6.YouboraLog.Companion.error
import com.npaw.youbora.lib6.YouboraLog.Companion.hasLogger
import com.npaw.youbora.lib6.YouboraLog.Companion.removeLogger
import com.npaw.youbora.lib6.comm.Request
import com.npaw.youbora.lib6.comm.Request.RequestErrorListener
import com.npaw.youbora.lib6.comm.Request.RequestSuccessListener
import com.npaw.youbora.lib6.constants.RequestParams
import com.npaw.youbora.lib6.constants.Services.CONFIGURATION
import com.npaw.youbora.lib6.loggers.BufferLogger
import com.npaw.youbora.lib6.plugin.Plugin
import org.json.JSONException
import org.json.JSONObject
import java.net.HttpURLConnection

interface RemoteMonitoringListener {
    fun onSend(data: String)
}

open class RemoteMonitoring(val listener: RemoteMonitoringListener, private val plugin: Plugin) {

    private lateinit var remoteMonitoringTimer: Timer

    private val timeInterval = 5_000L

    private var deltaTime = 0L

    private val maxTime = timeInterval*12

    private val minBufferSize = 400

    private val maxBufferSize = 2000

    private lateinit var remoteLogger: BufferLogger

    var remoteMonitoringEnable: Boolean = false
        private set

    var stoppingRemoteMonitoringTimer: Boolean = false
        private set

    lateinit var remoteMonitoringInterceptorRequestSuccessListener: RequestSuccessListener
        private set

    lateinit var remoteMonitoringInterceptorRequestErrorListener: RequestErrorListener
        private set

    init {
        createRemoteMonitoring()
    }

    /**
     * Internal function for testing
     */
    protected open fun createRemoteMonitoringTimer(listener: TimerEventListener, interval: Long): Timer {
        return Timer(listener, interval)
    }

    /**
     * Internal function for testing
     */
    @Throws(JSONException::class)
    protected open fun createJSONFromString(string: String): JSONObject {
        return JSONObject(string)
    }

    /**
     * Create Remote Monitoring using fast data configuration
     */
    private fun createRemoteMonitoring() {
        registerRemoteLogger()
        remoteMonitoringTimer = createRemoteMonitoringTimer(object : TimerEventListener {
            override fun onTimerEvent(delta: Long) {
                if (remoteLogger.size() > minBufferSize || deltaTime >= maxTime) {
                    onSendMonitoring()
                    checkSwitchingOff()
                    deltaTime = 0
                } else {
                    deltaTime += timeInterval
                }
            }
        }, timeInterval)
        remoteMonitoringInterceptorRequestSuccessListener = RequestSuccessListener { connection, responseString, listenerParams, headers ->
            var responseString = responseString
            if (responseString == null || responseString.length == 0) {
                return@RequestSuccessListener
            }
            try {
                val outerJson = createJSONFromString(responseString)
                if (outerJson.has("enabledLogs")) {
                    val remoteDebug = outerJson.getBoolean("enabledLogs")
                    if (remoteDebug) {
                        configRemoteMonitoring(remoteDebug)
                        return@RequestSuccessListener
                    }
                }
            } catch (e: JSONException) {
                error("Internal Error remote monitoring.... JSONException")
            } catch (e: StringIndexOutOfBoundsException) {
                error("Internal Error remote monitoring... StringIndexOutOfBoundsException")
            }
            stopRemoteDebuggingTimer()
        }

        remoteMonitoringInterceptorRequestErrorListener = object : RequestErrorListener {
            override fun onRequestError(connection: HttpURLConnection?) {
                stopRemoteDebuggingTimer()
            }

            override fun onRequestRemovedFromQueue() {
                stopRemoteDebuggingTimer()
            }
        }
    }

    /**
     * Initialize Remote Monitoring using fast data configuration
     */
    fun initRemoteMonitoring() {
        requestConfiguration()
    }


    private fun requestConfiguration(){
        var params: MutableMap<String?, String?>? = mutableMapOf()
        val service = CONFIGURATION
        params = plugin.requestBuilder.buildParams(params, service).toMutableMap()
        if ("nicetest" == params.get(RequestParams.SYSTEM)) {
            stopRemoteDebuggingTimer()
            return
        }

        // Prepare request but don't send it yet
        val request = Request(plugin.host, service)
        val paramsObjectMap: Map<String, Any> = params.map { (key, value) ->
            key.toString() to value as Any
        }.toMap()
        request.params = paramsObjectMap
        request.addOnSuccessListener( remoteMonitoringInterceptorRequestSuccessListener )

        request.addOnErrorListener( remoteMonitoringInterceptorRequestErrorListener)
        request.send()
    }


    /**
     * Configure Remote Monitoring using configuration request
     */
    private fun configRemoteMonitoring(enable: Boolean) {
        setAvailability(enable)

        if (!remoteMonitoringEnable) {
            stopRemoteDebuggingTimer()
        } else {
            startRemoteMonitoringTimer()
        }
    }

    private fun setAvailability(enable: Boolean){
        remoteMonitoringEnable = enable
    }

    /**
     * Start Remote Monitoring Timer
     */
    fun startRemoteMonitoringTimer() {
        if(!remoteMonitoringEnable)
            return

        registerRemoteLogger()
        YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE)

        if (!remoteMonitoringTimer.isRunning && remoteMonitoringEnable)
            remoteMonitoringTimer.start()
        else if (remoteMonitoringTimer.isRunning && remoteMonitoringEnable && stoppingRemoteMonitoringTimer)
            stoppingRemoteMonitoringTimer = false
    }

    /**
     * Initialize Logger if is needed & Registering Logger in YouboraLog
     */
    private fun registerRemoteLogger() {
        if(!::remoteLogger.isInitialized)
            remoteLogger = BufferLogger()

        if (!hasLogger(remoteLogger))
            addLogger(remoteLogger)
    }

    /**
     * Unregistering Logger in YouboraLog
     */
    private fun unregisterRemoteLogger() {
        if (hasLogger(remoteLogger))
            removeLogger(remoteLogger)

        remoteLogger.readBufferLogs()

    }

    /**
     *  Delay switch off to send the last logs
     */
    private fun checkSwitchingOff(){
        if (stoppingRemoteMonitoringTimer) {
            unregisterRemoteLogger()

            remoteMonitoringTimer.stop()
            stoppingRemoteMonitoringTimer = false
        }
    }

    /**
     * Should be called for avoid infinity logs
     */
    fun stopRemoteDebuggingTimer() {
        if (remoteMonitoringTimer.isRunning)
            stoppingRemoteMonitoringTimer = true
        else if(!remoteMonitoringEnable && !stoppingRemoteMonitoringTimer)
            unregisterRemoteLogger()

    }


    /**
     *
     */
    private fun onSendMonitoring() {
        val data = remoteLogger.readBufferLogs(maxBufferSize)
        listener.onSend(data)

    }




}
