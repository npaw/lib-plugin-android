package com.npaw.youbora.lib6.infinity

import android.content.Context

import com.npaw.youbora.lib6.comm.Request
import com.npaw.youbora.lib6.comm.transform.Transform
import com.npaw.youbora.lib6.constants.Services.SESSION_START

class TimestampLastSentTransform(context: Context) : Transform() {

    private val infinityStorage: InfinityStorageContract

    init {
        infinityStorage = InfinitySharedPreferencesManager(context)
    }

    override fun parse(request: Request) { infinityStorage.saveLastActive() }

    override fun isBlocking(request: Request?): Boolean {
        request?.takeIf { isBusy && SESSION_START == it.service }?.let {
            isBusy = false
        }

        return isBusy
    }
}
