package com.npaw.youbora.lib6.infinity

interface InfinityStorageContract {
    fun getSessionId(): String?
    fun saveSessionId(sessionId: String)

    fun getContext(): String?
    fun saveContext(context: String)

    fun getLastActive(): Long
    fun saveLastActive()

    fun getDeviceUUID(): String?
    fun saveDeviceUUID(deviceUUID: String)
}