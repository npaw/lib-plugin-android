package com.npaw.youbora.lib6.balancer.models

import com.google.gson.annotations.SerializedName

data class FailedRequest(
    @SerializedName("error")
    var error: Int,
    @SerializedName("absent")
    var absent: Int,
    @SerializedName("timeout")
    var timeout: Int,
    @SerializedName("total")
    var total: Int
)
