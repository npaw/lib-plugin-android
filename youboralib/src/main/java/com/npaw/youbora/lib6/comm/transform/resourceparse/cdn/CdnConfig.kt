package com.npaw.youbora.lib6.comm.transform.resourceparse.cdn

import com.npaw.youbora.lib6.comm.Request

import java.util.*

/**
 * An instance of this class has all the info and logic needed in order to "match" against a
 * particular CDN and get its node host and type (hit or miss).
 *
 * @author      Nice People at Work
 * @since       6.0
 */
open class CdnConfig() {

    /**
     * CDN code
     * @return the code that represents this CDN
     */
    var code: String? = null
        private set

    var parsers = mutableListOf<CdnParsableResponseHeader>()
        private set

    var requestHeaders = mutableMapOf<String, String>()
        private set

    /**
     * Get the current [CdnTypeParser]
     * @return the [CdnTypeParser]
     */
    var typeParser: CdnTypeParser? = null
        private set

    /**
     * Get the current request method
     * @return requestMethod
     */
    /**
     * Request method to use when requesting for info on the CDN
     */
    var requestMethod = Request.METHOD_HEAD
        private set

    /**
     * Constructor
     * @param code the CDN code, one of the following [list](http://mapi.youbora.com:8081/cdns).
     */
    constructor(code: String?) : this() {
        this.code = code
    }

    /**
     * Sets the cdn code
     * @param code the code to set
     * @return itself to chain method calls
     */
    fun setCode(code: String?): CdnConfig {
        this.code = code
        return this
    }

    /**
     * Adds a [CdnParsableResponseHeader]
     * @param parser the [CdnParsableResponseHeader] to add
     * @return itself to chain method calls
     */
    fun addParser(parser: CdnParsableResponseHeader): CdnConfig {
        parsers.add(parser)
        return this
    }

    /**
     * Adds a request header key:value pair.
     * This headers will be added to the HEAD request used to get the CDN info. Some CDNs need
     * special headers to be set in a request in order to respond with the info we need.
     * @param key header name
     * @param value header value
     * @return itself to chain method calls
     */
    fun setRequestHeader(key: String, value: String): CdnConfig {
        requestHeaders[key] = value
        return this
    }

    /**
     * Set the [CdnTypeParser] that will be used to parse the cdn info. Once the http HEAD
     * request is performed, this parser will be invoked with the http response to parse it and
     * extract the CDN host and/or type.
     * @see CdnTypeParser
     *
     * @param typeParser instance of [CdnTypeParser]
     * @return itself to chain method calls
     */
    fun setTypeParser(typeParser: CdnTypeParser?): CdnConfig {
        this.typeParser = typeParser
        return this
    }

    /**
     * Sets the new request method
     * @param requestMethod the request's method
     * @return the CDN config
     */
    fun setRequestMethod(requestMethod: String): CdnConfig {
        this.requestMethod = requestMethod
        return this
    }
}