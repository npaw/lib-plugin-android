package com.npaw.youbora.lib6.productAnalytics

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.plugin.Options
import com.npaw.youbora.lib6.infinity.Infinity
import com.npaw.youbora.lib6.adapter.PlayerAdapter

import java.util.Timer
import java.util.TimerTask
import java.util.concurrent.atomic.AtomicReference

open class ProductAnalytics(private var options: Options, private var infinity: Infinity) {

    enum class EventTypes(val value: String) {
        userProfile("User Profile"),
        user("User"),
        navigation("Navigation"),
        attribution("Attribution"),
        section("Section"),
        contentPlayback("Content Playback"),
        search("Search"),
        externalApplication("External Application"),
        engagement("Engagement"),
        custom("Custom Event")
    }

    private var initialized: Boolean = false
    private var adapter: PlayerAdapter<Any>? = null
    private var productAnalyticsSettings: ProductAnalyticsSettings = ProductAnalyticsSettings()
    private var screenName: String = ""
    private var searchQuery: String = ""
    private var userState: ProductAnalyticsUserState? = null
    private var contentHighlightTimeout: TimerTask? = null
    private val pendingVideoEvents: MutableList<PendingVideoEvent> = mutableListOf()

    /**
     * Initializes product analytics
     * @param screenName
     * @param productAnalyticsSettings product analytics settings
     */

    fun initialize(screenName: String, productAnalyticsSettings: ProductAnalyticsSettings) {

        val defaultSettings = ProductAnalyticsSettings()

        this.screenName               = screenName
        this.productAnalyticsSettings = productAnalyticsSettings

        if (this.productAnalyticsSettings.highlightContentAfter < 1000 ) {
            YouboraLog.warn("Invalid highlightContentAfter value. Using default value instead.")
            this.productAnalyticsSettings.highlightContentAfter = defaultSettings.highlightContentAfter
        }

        if (this.productAnalyticsSettings.activeStateTimeout < 1000 ) {
            YouboraLog.warn("Invalid activeStateTimeout value. Using default value instead.")
            this.productAnalyticsSettings.activeStateTimeout = defaultSettings.activeStateTimeout
        }

        if (this.productAnalyticsSettings.activeStateDimension < 1 || this.productAnalyticsSettings.activeStateDimension > 20 ) {
            YouboraLog.warn("Invalid activeStateDimension value. Using default value instead.")
            this.productAnalyticsSettings.activeStateDimension = defaultSettings.activeStateDimension
        }

        this.initialized = true
        this.newSession()
    }

    /**
     * Update infinity instance
     */
    fun setInfinity(infinity: Infinity){
        this.infinity = infinity;
    }

    // ---------------------------------------------------------------------------------------------
    // ADAPTER
    // ---------------------------------------------------------------------------------------------

    /**
     * Track adapter start
     */

    fun adapterTrackStart() {
        if ( this.initialized ) {
            trackPlayerInteraction("start", mutableMapOf(), mutableMapOf(), true)
        }
    }

    /**
     * Execute after adapter is set to plugin
     */

    fun adapterAfterSet(adapter: PlayerAdapter<Any>) {

        if ( this.initialized ){
            this.adapter = adapter

            // Set active state

            if ( this.productAnalyticsSettings.enableStateTracking) {

                if (this.userState != null) {
                    YouboraLog.warn("userState is already initialized.")
                }

                this.userState = ProductAnalyticsUserState(this.productAnalyticsSettings.activeStateDimension, this.productAnalyticsSettings.activeStateTimeout, ::fireEventAdapter, AtomicReference(this.options))

            } else {
                this.userState = null
            }
        }
    }

    /**
     * Execute before removing adapter from plugin
     */

    fun adapterBeforeRemove() {
        this.userState?.dispose()
        this.userState = null
        this.adapter = null
        this.pendingVideoEvents.clear()
    }

    // ---------------------------------------------------------------------------------------------
    // SESSION
    // ---------------------------------------------------------------------------------------------

    /**
     * New user session
     * @return true if a new session has been started, false otherwise
     */

    fun newSession(): Boolean{

        val executed: Boolean

        if ( !this.initialized ){
            executed = false
            YouboraLog.warn("Cannot start a new session since Product Analytics is uninitialized.")
        } else {
            executed = true
            this.infinity.end()
            // TODO: do we have to pass screenName as an argument or use its current value?
            this.infinity.begin(this.screenName)
        }

        return executed
    }

    /**
     * Ends user session
     * @return true if the session has been successfully ended, false otherwise
     */

    fun endSession(): Boolean {

        val executed: Boolean

        if ( !this.initialized ){
            executed = false
            YouboraLog.warn("Cannot end session since Product Analytics is uninitialized.")
        } else {
            executed = true
            this.infinity.end()
        }

        return executed
    }

    // ---------------------------------------------------------------------------------------------
    // LOGIN / LOGOUT
    // ---------------------------------------------------------------------------------------------

    /**
     * Login successful
     * @param userId User identifier
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun loginSuccessful(userId: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        if ( !this.checkState("log in successfully") ){
            // Product Analytics is not ready for sending events
        } else if (userId.isBlank()) {
            YouboraLog.warn("Cannot log in successfully since userId is unset.")
        } else {
            // Send an event informing that we are closing the session because of a profile change

            this.loginSuccessfulEvent(userId, dimensions,  metrics)

            // Set the userId option and close + open a new session

            this.options.username = userId
            this.newSession()
        }
    }

    /**
     * Login successful
     * @param userId User identifier
     * @param profileId Profile identifier
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun loginSuccessful(userId: String, profileId: String, profileType: String? = null, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        if ( !this.checkState("log in successfully") ){
            // Product Analytics is not ready for sending events
        } else if (userId.isBlank()) {
            YouboraLog.warn("Cannot log in successfully since userId is unset.")
        } else if (profileId.isBlank()) {
            YouboraLog.warn("Cannot log in successfully since profileId is unset.")
        } else {
            // Send user login event

            this.loginSuccessfulEvent(userId, dimensions,  metrics)

            // Send profile selection event

            this.userProfileSelectedEvent(profileId, profileType, dimensions, metrics)

            // Set the userId option and close + open a new session

            this.options.username = userId
            this.options.userProfileId = profileId
            this.newSession()
        }
    }

    /**
     * Send login successful event
     * @param userId User identifier
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    private fun loginSuccessfulEvent(userId: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        val dimensionsInternal = mutableMapOf(
            "username" to userId
        )

        this.fireEventInternal(
            "User Login Successful",
            EventTypes.user,
            dimensionsInternal,
            dimensions,
            metrics)
    }

    /**
     * Login error
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun loginUnsuccessful(dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        if ( !this.checkState("track log in error") ){
            // Product Analytics is not ready for sending events
        } else {
            // Send an event informing that we are closing the session because of a profile change
            this.fireEventInternal(
     "User Login Unsuccessful",
                EventTypes.user,
                mutableMapOf(),
                dimensions,
                metrics)
        }
    }

    /**
     * Logout
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun logout(dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        if ( !this.checkState("log out") ){
            // Product Analytics is not ready for sending events
        } else {
            // Send an event informing that we are closing the session
            this.fireEventInternal(
     "User Logout",
                EventTypes.user,
                mutableMapOf(),
                dimensions,
                metrics)

            // Set the userId option and close + open a new session

            this.options.username = null
            this.options.userProfileId = null
            this.newSession()
        }
    }

    // ---------------------------------------------------------------------------------------------
    // PROFILE
    // ---------------------------------------------------------------------------------------------

    /**
     * Create user profile
     * @param profileId Profile identifier
     * @param profileType Profile type
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun userProfileCreated(profileId: String, profileType: String? = null, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        val dimensionsInternal: MutableMap<String, String>

        if ( !this.checkState("create user profile") ){
            // Product Analytics is not ready for sending events
        } else if (profileId.isBlank()) {
            YouboraLog.warn("Cannot create user profile since profileId is unset.")
        } else {
            dimensionsInternal = this.getUserProfileDimensions(profileId, profileType)
            this.fireEventInternal(
     "User Profile Created",
                EventTypes.userProfile,
                dimensionsInternal,
                dimensions,
                metrics)
        }
    }

    /**
     * Select user profile
     * @param profileId Profile identifier
     * @param profileType Profile type
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun userProfileSelected(profileId: String, profileType: String? = null, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        if ( !this.checkState("select user profile") ){
            // Product Analytics is not ready for sending events
        } else if (profileId.isBlank()) {
            YouboraLog.warn("Cannot select user profile since profileId is unset.")
        } else {
            // Send an event informing that we are closing the session because of a profile change

            this.userProfileSelectedEvent(profileId, profileType, dimensions, metrics)

            // Set the profileId option and close + open a new session

            this.options.userProfileId = profileId
            this.newSession()
        }
    }

    /**
     * Send user profile selected event
     * @param profileId Profile identifier
     * @param profileType Profile type
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    private fun userProfileSelectedEvent(profileId: String, profileType: String? = null, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()){
        val dimensionsInternal: MutableMap<String, String> = this.getUserProfileDimensions(profileId, profileType)

        this.fireEventInternal(
                "User Profile Selected",
                EventTypes.userProfile,
                dimensionsInternal,
                dimensions,
                metrics)
    }


    /**
     * Delete user profile
     * @param profileId Profile identifier
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun userProfileDeleted(profileId: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        val dimensionsInternal: MutableMap<String, String>

        if ( !this.checkState("delete user profile") ){
            // Product Analytics is not ready for sending events
        } else if (profileId.isBlank()) {
            YouboraLog.warn("Cannot delete user profile since profileId is unset.")
        } else {
            dimensionsInternal = this.getUserProfileDimensions(profileId, null)
            this.fireEventInternal(
     "User Profile Deleted",
                EventTypes.userProfile,
                dimensionsInternal,
                dimensions,
                metrics)
        }
    }

    /**
     * Get user profile dimensions
     * @param {*} profileId
     * @param {*} profileType
     * @returns
     * @private
     */

    private fun getUserProfileDimensions(profileId: String, profileType: String? = null): MutableMap<String, String>{
        val dimensions: MutableMap<String, String> = mutableMapOf(
            "profileId" to profileId
        )

        if ( profileType !== null ) {
            dimensions["profileType"] = profileType
        }

        return dimensions
    }

    // ---------------------------------------------------------------------------------------------
    // NAVIGATION
    // ---------------------------------------------------------------------------------------------

    /**
     * Tracks navigation
     * @param screenName The unique name to identify a page of the application.
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackNavByName(screenName: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {

        if ( !this.checkState("track navigation") ){
            // Product Analytics is not ready for sending events
        } else if (screenName.isBlank()) {
            YouboraLog.warn("Cannot track navigation since page has not been supplied.")
        } else {
            this.screenName = screenName

            this.fireEventInternal(
     "Navigation $screenName",
                EventTypes.navigation,
                mutableMapOf(
                    "route" to "",
                    "routeDomain" to "",
                    "fullRoute" to ""),
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // ATTRIBUTION
    // ---------------------------------------------------------------------------------------------

    /**
     * Tracks attribution
     * @param utmSource The UTM Source parameter. It is commonly used to identify a search engine, newsletter, or other source (i.e., Google, Facebook, etc.).
     * @param utmMedium The UTM Medium parameter. It is commonly used to identify a medium such as email or cost-per-click (cpc).
     * @param utmCampaign The UTM Campaign parameter. It is commonly used for campaign analysis to identify a specific product promotion or strategic campaign (i.e., spring sale).
     * @param utmTerm The UTM Term parameter. It is commonly used with paid search to supply the keywords for ads (i.e., Customer, NonBuyer, etc.).
     * @param utmContent The UTM Content parameter. It is commonly used for A/B testing and content-targeted ads to differentiate ads or links that point to the same URL (i.e., Banner1, Banner2, etc.)
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackAttribution(utmSource: String, utmMedium: String? = null, utmCampaign: String? = null, utmTerm: String? = null, utmContent: String? = null, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {

        val utmParams : MutableMap<String, String> = mutableMapOf()

        if ( utmSource   != "" ) utmParams["utmSource"] = utmSource
        if ( utmMedium   != null && utmMedium   != "" ) utmParams["utmMedium"]   = utmMedium
        if ( utmCampaign != null && utmCampaign != "" ) utmParams["utmCampaign"] = utmCampaign
        if ( utmTerm     != null && utmTerm     != "" ) utmParams["utmTerm"]     = utmTerm
        if ( utmContent  != null && utmContent  != "" ) utmParams["utmContent"]  = utmContent

        // Track attribution

        if ( !this.checkState("track attribution") ){
            // Product Analytics is not ready for sending events
        } else if (utmParams.isNotEmpty()) {
            val dimensionsInternal: MutableMap<String, String> = mutableMapOf(
                "url" to ""
            )
            dimensionsInternal.putAll(utmParams)

            this.fireEventInternal(
     "Attribution",
                EventTypes.attribution,
                dimensionsInternal,
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // SECTION
    // ---------------------------------------------------------------------------------------------

    /**
     * Section goes into viewport.
     * @param section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
     * @param sectionOrder The section order within the page.
     * @param [dimensions] Dimensions to track
     * @param [metrics] Metrics to track
     */

    @JvmOverloads
    fun trackSectionVisible(section: String, sectionOrder: Int, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        if ( !this.checkState("track section visible") ){
            // Product Analytics is not ready for sending events
        } else if (section == "") {
            YouboraLog.warn("Cannot track section visible since no section has been supplied.")
        } else if (sectionOrder < 1) {
            YouboraLog.warn("Cannot track section visible since sectionOrder is invalid.")
        } else {
            this.fireEventInternal(
     "Section Visible",
                EventTypes.section,
                mutableMapOf(
                    "section" to section,
                    "sectionOrder" to sectionOrder.toString()
                ),
                dimensions,
                metrics)
        }
    }

    /**
     * Section goes out of viewport.
     * @param section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
     * @param sectionOrder The section order within the page.
     * @param [dimensions] Dimensions to track
     * @param [metrics] Metrics to track
     */

    @JvmOverloads
    fun trackSectionHidden(section: String, sectionOrder: Int, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        if ( !this.checkState("track section hidden") ){
            // Product Analytics is not ready for sending events
        } else if (section == "") {
            YouboraLog.warn("Cannot track section hidden since no section has been supplied.")
        } else if (sectionOrder < 1) {
            YouboraLog.warn("Cannot track section hidden since sectionOrder is invalid.")
        } else {
            this.fireEventInternal(
     "Section Hidden",
                EventTypes.section,
                mutableMapOf(
                    "section" to section,
                    "sectionOrder" to sectionOrder.toString()
                ),
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // CONTENT
    // ---------------------------------------------------------------------------------------------

    /**
     * Sends a content highlight event if content is focused during, at least, highlightContentAfter ms.
     * @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
     * @param {integer} sectionOrder The section order within the page.
     * @param {integer} column Used to indicate the column number where content is placed in a grid layout The first column is number 1.
     * @param {integer} row Used to indicate the row number where content is placed in a grid layout. The first row is number 1. In the case of a horizontal list instead of a grid, the row parameter should be set to 1.
     * @param {string} contentID The unique content identifier of the content linked.
     * @param {Object} [dimensions] Dimensions to track
     * @param {Object} [metrics] Metrics to track
     */

    @JvmOverloads
    fun contentFocusIn(section: String, sectionOrder: Int, column: Int, row: Int, contentID: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        this.contentFocusOut()

        if ( !this.checkState("track content highlight") ){
            // Product Analytics is not ready for sending events
        } else if (section == "") {
            YouboraLog.warn("Cannot track content highlight since no section has been supplied.")
        } else if (sectionOrder < 1) {
            YouboraLog.warn("Cannot track content highlight since sectionOrder is invalid.")
        } else if (column < 1) {
            YouboraLog.warn("Cannot track content highlight since column is invalid.")
        } else if (row < 1 ) {
            YouboraLog.warn("Cannot track content highlight since row is invalid.")
        } else if (contentID == "") {
            YouboraLog.warn("Cannot track content highlight since no contentID has been supplied.")
        } else {

            this.contentHighlightTimeout = object : TimerTask() {
                override fun run() {
                    this@ProductAnalytics.trackContentHighlight(section, sectionOrder, column, row, contentID, dimensions, metrics)
                }
            }

            Timer().schedule(this.contentHighlightTimeout, this.productAnalyticsSettings.highlightContentAfter)
        }
    }

    /**
      * Content loses focus
      */

    fun contentFocusOut() {
        this.contentHighlightTimeout?.cancel()
        this.contentHighlightTimeout = null
    }

    /**
     * Tracks content highlights.
     * @param {string} section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
     * @param {integer} sectionOrder The section order within the page.
     * @param {integer} column Used to indicate the column number where content is placed in a grid layout The first column is number 1.
     * @param {integer} row Used to indicate the row number where content is placed in a grid layout. The first row is number 1. In the case of a horizontal list instead of a grid, the row parameter should be set to 1.
     * @param {string} contentID The unique content identifier of the content linked.
     * @param {Object} [dimensions] Dimensions to track
     * @param {Object} [metrics] Metrics to track
     */
    private fun trackContentHighlight(section: String, sectionOrder: Int, column: Int, row: Int, contentID: String, dimensions: MutableMap<String, String>?, metrics: MutableMap<String, Double>) {

        this.fireEventInternal(
 "Section Content Highlight",
            EventTypes.section,
            mutableMapOf(
                "section" to section,
                "sectionOrder" to sectionOrder.toString(),
                "column" to column.toString(),
                "row" to row.toString(),
                "contentId" to contentID
            ),
            dimensions,
            metrics)
    }

    /**
     * Tracks the location of user clicks.
     * @param section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
     * @param sectionOrder The section order within the page.
     * @param column Used to indicate the column number where content is placed in a grid layout The first column is number 1.
     * @param row Used to indicate the row number where content is placed in a grid layout. The first row is number 1. In the case of a horizontal list instead of a grid, the row parameter should be set to 1.
     * @param contentID The unique content identifier of the content linked.
     * @param [dimensions] Dimensions to track
     * @param [metrics] Metrics to track
     */

    @JvmOverloads
    fun trackContentClick(section: String, sectionOrder: Int, column: Int, row: Int, contentID: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track content click") ){
            // Product Analytics is not ready for sending events
        } else if (section == "") {
            YouboraLog.warn("Cannot track content click since no section has been supplied.")
        } else if (sectionOrder < 1) {
            YouboraLog.warn("Cannot track content click since sectionOrder is invalid.")
        } else if (column < 1) {
            YouboraLog.warn("Cannot track content click since column is invalid.")
        } else if (row < 1 ) {
            YouboraLog.warn("Cannot track content click since row is invalid.")
        } else if (contentID == "") {
            YouboraLog.warn("Cannot track content click since no contentID has been supplied.")
        } else {
            this.fireEventInternal(
     "Section Content Click",
                EventTypes.section,
                hashMapOf(
                    "section" to section,
                    "sectionOrder" to sectionOrder.toString(),
                    "column" to column.toString(),
                    "row" to row.toString(),
                    "contentId" to contentID
                ),
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // CONTENT PLAYBACK
    // ---------------------------------------------------------------------------------------------

    /**
     * Tracks when a content starts playing be it automatically or through a user interaction.
     * @param contentID The unique content identifier of the content being played.
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackPlay(contentID: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        val eventName: String = "Content Play"
        val startEvent: Boolean = false
        if ( !this.checkState("track play") ){
            // Product Analytics is not ready for sending events
        } else if (contentID == "") {
            YouboraLog.warn("Cannot track play since no contentID has been supplied.")
        } else if ( this.adapter != null && !this.adapter!!.flags.isStarted ) {
            this.pendingVideoEvents.add(PendingVideoEvent(eventName, contentID, dimensions, metrics, startEvent))
        } else {
            this.trackPlayerEventsPending()
            this.trackPlayerEvent(eventName, contentID, dimensions, metrics, startEvent)
        }
    }

    /**
     * Tracks content watching events.
     * TODO: add (2nd) argument to tell whether user state must be updated or not

     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     * @param startEvent Internal param informing that current interaction is responsible of first player start
     */

    @JvmOverloads
    fun trackPlayerInteraction(eventName: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf(), startEvent: Boolean = false) {
        val eventNameFull: String = "Content Play $eventName"
        val contentID: String? = null
        if ( !this.checkState("track player interaction") ){
            // Product Analytics is not ready for sending events
        } else if (eventName == "") {
            YouboraLog.warn("Cannot track player interaction since no interaction name has been supplied.")
        } else if ( this.adapter != null && !this.adapter!!.flags.isStarted ) {
            this.pendingVideoEvents.add(PendingVideoEvent(eventNameFull, contentID, dimensions, metrics, startEvent))
        } else {
            this.trackPlayerEventsPending()
            this.trackPlayerEvent(eventNameFull, contentID, dimensions, metrics, startEvent)
        }
    }

    /**
     * Track player pending events
     */

    private fun trackPlayerEventsPending() {
        this.pendingVideoEvents.forEach{
            event ->
            this.trackPlayerEvent(event.eventName, event.contentId, event.dimensions, event.metrics, event.startEvent)
        }

        this.pendingVideoEvents.clear()
    }

    /**
     * Track player event
     * @param eventName The name of the interaction (i.e., Pause, Seek, Skip Intro, Skip Ads, Switch Language, etc.).
     * @param contentID The unique content identifier of the content being played.
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     * @param startEvent Internal param informing that current interaction is responsible of first player start
     */
    private fun trackPlayerEvent(eventName: String, contentID: String?, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf(), startEvent: Boolean = false) {
        val dimensionsInternal: MutableMap<String, String> = hashMapOf(
        )

        if ( contentID != null ){
            dimensionsInternal["contentId"] = contentID
        }

        this.fireEventAdapter(
            eventName,
            dimensionsInternal,
            dimensions,
            metrics)

        if ( !startEvent ){
            this.userState?.setActive(eventName)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // CONTENT SEARCH
    // ---------------------------------------------------------------------------------------------

    /**
     * Tracks search query events.
     * @param searchQuery The search term entered by the user.
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackSearchQuery(searchQuery: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track search query") ){
            // Product Analytics is not ready for sending events
        } else if (searchQuery == "") {
            YouboraLog.warn("Cannot track search query since no searchQuery has been supplied.")
        } else {
            this.searchQuery = searchQuery
            this.fireEventInternal(
     "Search Query",
                EventTypes.search,
                hashMapOf(
                    "query" to this.searchQuery
                ),
                dimensions,
                metrics)
        }
    }

    /**
     * Tracks search result events.
     * @param resultCount The number of search results returned by a search query.
     * @param searchQuery The search term entered by the user.
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackSearchResult(resultCount: Int, searchQuery: String? = null, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track search result") ){
            // Product Analytics is not ready for sending events
        } else if (resultCount < 0) {
            YouboraLog.warn("Cannot track search result since resultCount is invalid.")
        } else {
            val query: String = if (searchQuery != null && searchQuery != "" ){
                searchQuery
            } else {
                this.searchQuery
            }

            this.fireEventInternal(
     "Search Results",
                EventTypes.search,
                mutableMapOf(
                    "query" to query,
                    "resultCount" to resultCount.toString()
                ),
                dimensions,
                metrics)
        }
    }

    /**
     * Tracks user interactions with search results.
     * @param section The section title. It is commonly used to indicate the section title presented over a grid layout (e.g. Recommended Movies, Continue Watching, etc).
     * @param sectionOrder The section order within the page.
     * @param column The content placement column. It is commonly used to indicate the column number where content is placed in a grid layout (i.e.1, 2, etc..).
     * @param row The content placement row. It is commonly used to indicate the row number where content is placed in a grid layout (i.e.1, 2, etc..).
     * @param contentID The content identifier. It is used for internal content unequivocally identification (i.e., AAA000111222).
     * @param searchQuery The search term entered by the user.
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackSearchClick(section: String, sectionOrder: Int, column: Int, row: Int, contentID: String, searchQuery: String? = null, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track search click") ){
            // Product Analytics is not ready for sending events
        } else if (column < 1) {
            YouboraLog.warn("Cannot track search click since column is invalid.")
        } else if (row < 1) {
            YouboraLog.warn("Cannot track search click since row is invalid.")
        } else if (contentID == "") {
            YouboraLog.warn("Cannot track search click since no contentID has been supplied.")
        } else {
            val query: String = if (searchQuery != null && searchQuery != "" ){
                searchQuery
            } else {
                this.searchQuery
            }

            val sectionParam: String = if ( section == "" ){
                "Search"
            } else {
                section
            }

            val sectionOrderParam: Int = if ( sectionOrder < 1 ){
                1
            } else {
                sectionOrder
            }

            this.fireEventInternal(
     "Search Result Click",
                EventTypes.search,
                mutableMapOf(
                    "query" to query,
                    "section" to sectionParam,
                    "sectionOrder" to sectionOrderParam.toString(),
                    "column" to column.toString(),
                    "row" to row.toString(),
                    "contentId" to contentID
                ),
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // EXTERNAL APPLICATIONS
    // ---------------------------------------------------------------------------------------------

    /**
     * Tracks external app start events.
     * @param appName The name of the application being used to deliver the content to the end-user (i.e., Netflix).
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackExternalAppLaunch(appName: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track external application launch") ){
            // Product Analytics is not ready for sending events
        } else if (appName == "") {
            YouboraLog.warn("Cannot track external application launch since no appName has been supplied.")
        } else {
            this.fireEventInternal(
     "External Application Launch",
                EventTypes.externalApplication,
                hashMapOf(
                    "appName" to appName
                ),
                dimensions,
                metrics)
        }
    }

    /**
     * Tracks external app stop events.
     * @param appName The name of the application being used to deliver the content to the end-user (i.e., Netflix).
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackExternalAppExit(appName: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track external application exit") ){
            // Product Analytics is not ready for sending events
        } else if (appName == "") {
            YouboraLog.warn("Cannot track external application exit since no appName has been supplied.")
        } else {
            this.fireEventInternal(
     "External Application Exit",
                EventTypes.externalApplication,
                hashMapOf(
                    "appName" to appName
                ),
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // ENGAGEMENT
    // ---------------------------------------------------------------------------------------------

    /**
     * Tracks engagement events.
     * @param eventName The name of the engagement event (i.e., Share, Save, Rate, etc.).
     * @param contentID The unique content identifier of the content the user is engaging with.
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackEngagementEvent(eventName: String, contentID: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track engagement event") ){
            // Product Analytics is not ready for sending events
        } else if (eventName == "") {
            YouboraLog.warn("Cannot track engagement event since no eventName has been supplied.")
        } else if (contentID == "") {
            YouboraLog.warn("Cannot track engagement event since no contentID has been supplied.")
        } else {
            this.fireEventInternal(
     "Engagement $eventName",
                EventTypes.engagement,
                hashMapOf(
                    "contentId" to contentID
                ),
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // CUSTOM EVENT
    // ---------------------------------------------------------------------------------------------

    /**
     * Track custom event
     * @param eventName Name of the event to track
     * @param dimensions Dimensions to track
     * @param metrics Metrics to track
     */

    @JvmOverloads
    fun trackEvent(eventName: String, dimensions: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = mutableMapOf()) {
        if ( !this.checkState("track custom event") ){
            // Product Analytics is not ready for sending events
        } else if (eventName === "") {
            YouboraLog.warn("Cannot track custom event since no eventName has been supplied.")
        } else {
            this.fireEventInternal(
     "Custom $eventName",
                EventTypes.custom,
                hashMapOf(),
                dimensions,
                metrics)
        }
    }

    // ---------------------------------------------------------------------------------------------
    // HELPERS
    // ---------------------------------------------------------------------------------------------

    /**
     * Fires an event
     * @param eventName Name of the event to be fired
     * @param eventType Type of the event being tracked
     * @param dimensionsInternal Dimensions supplied by user
     * @param dimensionsUser Specific event dimensions
     * @param metrics Metrics to track
     */
    private fun fireEventInternal(eventName: String, eventType: EventTypes, dimensionsInternal: MutableMap<String, String>? = null, dimensionsUser: MutableMap<String, String>? = null, metrics: MutableMap<String, Double> = HashMap()) {
        val dimensions = buildDimensions(eventType, dimensionsInternal, dimensionsUser)
        var dimensionsCustom: MutableMap<String, String> = mutableMapOf()
        var dimensionsTop: MutableMap<String, String> = mutableMapOf()

        YouboraLog.notice(eventName)

        if ( dimensions.containsKey("custom") ) {
            dimensionsCustom = dimensions["custom"]!!
        }

        if ( dimensions.containsKey("top") ) {
            dimensionsTop = dimensions["top"]!!
        }
        
        // Track event

        this.infinity.fireEvent(eventName, dimensionsCustom, metrics, dimensionsTop)
    }

    /**
     * Fires an adapter event (in case it is available)
     * @param eventName Event name
     * @param dimensionsInternal Dimensions supplied by user
     * @param dimensionsUser Specific event dimensions
     * @param metrics Metrics to track
     */
    private fun fireEventAdapter(eventName: String, dimensionsInternal: MutableMap<String, String>?, dimensionsUser: MutableMap<String, String>?, metrics: MutableMap<String, Double> = HashMap()) {

        val dimensions = buildDimensions(EventTypes.contentPlayback, dimensionsInternal, dimensionsUser)
        val adapterRef = this.adapter

        YouboraLog.notice(eventName)

        val dimensionsCustom: MutableMap<String, String> = if ( dimensions.containsKey("custom") ) {
            dimensions["custom"]!!
        } else {
            mutableMapOf()
        }

        val dimensionsTopLevel: MutableMap<String, String> = if ( dimensions.containsKey("top") ) {
            dimensions["top"]!!
        } else {
            mutableMapOf()
        }

        if ( adapterRef == null) {
            YouboraLog.warn("Adapter event cannot be fired since adapter is unavailable.")
        } else {
            adapterRef.fireEvent(eventName, dimensionsCustom, metrics, dimensionsTopLevel)
        }
    }

    /**
     * Builds a list of top level and custom dimensions
     * @param dimensionsInternal Object containing a list of internal dimensions
     * @param dimensionsUser Object containing a list of custom dimensions
     */
    private fun buildDimensions(eventType: EventTypes, dimensionsInternal: MutableMap<String, String>?, dimensionsUser: MutableMap<String, String>?): Map<String, MutableMap<String, String>> {
        val dimensionsCustom: MutableMap<String, String> = mutableMapOf("page" to this.screenName)

        if (dimensionsInternal != null) {
            dimensionsCustom.putAll(dimensionsInternal)
        }

        if (dimensionsUser != null) {
            dimensionsCustom.putAll(dimensionsUser)
        }

        dimensionsCustom.putAll(mutableMapOf(
            "eventSource" to "Product Analytics",
            "eventType" to eventType.value
        ))

        val topKeys = arrayOf("contentid", "contentId", "contentID", "utmSource", "utmMedium", "utmCampaign", "utmTerm", "utmContent", "profileId", "profile_id", "username")
        val topKeysDelete = arrayOf("contentid", "contentId", "contentID", "profileId", "profile_id", "username")

        val dimensionsTopLevel: MutableMap<String, String> = mutableMapOf()

        for (key in topKeys) {
            if (dimensionsCustom.containsKey(key)) {
                dimensionsTopLevel[key] = dimensionsCustom[key]!!
            }
        }

        for (key in topKeysDelete) {
            dimensionsCustom.remove(key)
        }

        return mutableMapOf(
            "custom" to dimensionsCustom,
            "top" to dimensionsTopLevel
        )
    }

    /**
     * Check state before sending an event
     */

    private fun checkState(message: String): Boolean{
        val valid: Boolean

        if ( !this.initialized ) {
            valid = false;
            YouboraLog.warn("Cannot $message since Product Analytics is uninitialized.")
        } else if ( !this.infinity.flags.isStarted ){
            valid = false;
            YouboraLog.warn("Cannot $message since session is closed ")
        } else {
            valid = true;
        }

        return valid;
    }

    /**
     * Pending Video Event
     */
    private class PendingVideoEvent(
        val eventName: String,
        val contentId: String?,
        val dimensions: MutableMap<String, String>?,
        val metrics: MutableMap<String, Double>,
        val startEvent: Boolean
    ){
    }

}
