package com.npaw.youbora.lib6.flags

open class Flags {

    open var isStarted = false

    open fun reset() { isStarted = false }
}