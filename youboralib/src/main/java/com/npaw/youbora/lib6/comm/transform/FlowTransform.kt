package com.npaw.youbora.lib6.comm.transform

import com.npaw.youbora.lib6.comm.Request
import com.npaw.youbora.lib6.constants.Services.ERROR
import com.npaw.youbora.lib6.constants.Services.INIT
import com.npaw.youbora.lib6.constants.Services.OFFLINE_EVENTS
import com.npaw.youbora.lib6.constants.Services.START

/**
 * This transform ensures that no requests will be sent before an /init or /start request.
 * As these are the two possible first requests that the API expects for a view.
 * [API docs](http://developer.nicepeopleatwork.com/apidocs/swagger/html/?module=nqs7).
 * @author      Nice People at Work
 * @since       6.0
 */
open class FlowTransform : Transform() {

    companion object {
        private val EXPECTED_SERVICES = listOf(INIT, START, OFFLINE_EVENTS)
    }

    override fun parse(request: Request?) {
        // This Transform does not need to alter the Requests, so do nothing here.
    }

    /**
     * {@inheritDoc}
     *
     * Blocks all the requests until an /init or a /start is found.
     * The only exception to this logic is the /error, that can be sent at any time
     */
    override fun isBlocking(request: Request?): Boolean {
        if (isBusy && request != null) {
            if (EXPECTED_SERVICES.contains(request.service)) {
                isBusy = false
            } else if (ERROR == request.service) {
                // If it's an error we make an exception and bypass the blocking
                return false
            }
        }

        return super.isBlocking(request)
    }
}