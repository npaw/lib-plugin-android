package com.npaw.youbora.lib6.persistence.dao;

import static com.npaw.youbora.lib6.persistence.OfflineContract.OfflineEntry.TABLE_NAME;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.persistence.OfflineContract;
import com.npaw.youbora.lib6.persistence.entity.Event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Enrique on 25/01/2018.
 */

public class EventDAO implements DAO<Event>{
    private final SQLiteOpenHelper dbHelper;

    public EventDAO(SQLiteOpenHelper helper) {
        this.dbHelper = helper;
    }

    @Override
    synchronized public Long insertNewElement(Event event){
        List<Event> events = new ArrayList<>(1);
        events.add(event);
        return insertElements(events);
    }

    @Override
    synchronized public Long insertElements(List<Event> elements) {
        SQLiteDatabase db = getWritableDatabase();
        long lastId = -1L;

        if(db == null)
            return -1L;

        ContentValues values;

        for (Event event : elements) {
            values = new ContentValues();

            values.put(OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS, event.getJsonEvents());
            values.put(OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE, System.currentTimeMillis());
            values.put(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID, event.getOfflineId());

            lastId = db.insert(OfflineContract.OfflineEntry.TABLE_NAME, null, values);
        }
        return lastId;
    }

    @Override
    public Event getElement(String[] whereColumns, String[] whereValues, String groupBy, String having,
                            String orderBy, String limit) {
        List<Event> events = getElements(whereColumns, whereValues, groupBy, having, orderBy, limit);

        if (events != null && events.size() != 0) {
            if (events.size() != 1) {
                YouboraLog.error(
                        String.format("More than one event was found for %s with value %s, only first will be returned"
                                , Arrays.toString(whereColumns), Arrays.toString(whereValues))
                );
            }
            return events.get(0);
        }
        return null;
    }

    @Override
    public List<Event> getElements(String[] whereColumns, String[] whereValues, String groupBy, String having,
                                   String orderBy, String limit) {
        List<Event> events = new ArrayList<>();

        Cursor cursor = null;

        try {
            SQLiteDatabase db = getReadableDatabase();

            if (db == null) {
                return events;
            }

            String[] columns = getAllColumns();
            cursor = db.query(TABLE_NAME, columns, formatWhereString(whereColumns), whereValues, groupBy, having, orderBy, limit);

            if (cursor.moveToFirst()){
                do {
                    events.add(generateEvent(cursor));
                } while (cursor.moveToNext());
            }
        }
        catch ( Exception e ) {
            YouboraLog.error(e);
        } finally {
            if(cursor != null) cursor.close();
        }

        return events;
    }

    @Override
    public List<Event> getAll(){

        List<Event> events = new ArrayList<>();

        Cursor cursor = null;

        try {
            SQLiteDatabase db = getReadableDatabase();

            if (db == null) {
                return events;
            }

            String[] columns = getAllColumns();
            cursor = db.query(TABLE_NAME, columns, null, null, null, null, null);

            if (cursor.moveToFirst()){
                do {
                    events.add(generateEvent(cursor));
                } while (cursor.moveToNext());
            }
        }
        catch ( Exception e ) {
            YouboraLog.error(e);
        } finally {
            if(cursor != null) cursor.close();
        }

        return events;
    }

    @Override
    synchronized public Integer deleteElement(String whereClause, String[] whereArgs){

        SQLiteDatabase db = getWritableDatabase();

        if (db == null)
            return 0;

        return db.delete(OfflineContract.OfflineEntry.TABLE_NAME
                , whereClause
                , whereArgs);
    }

    @Override
    synchronized public Integer deleteAll() {
        SQLiteDatabase db = getWritableDatabase();

        if (db == null)
            return 0;

        return db.delete(OfflineContract.OfflineEntry.TABLE_NAME
                , null
                , null);
    }

    synchronized public SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase db = null;

        try{
            db =  dbHelper.getWritableDatabase();
        } catch(Exception ex) {
            YouboraLog.error("Could not open database");
            YouboraLog.error(ex);
        }

        return db;
    }

    synchronized public SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase db = null;

        try{
            db =  dbHelper.getReadableDatabase();
        } catch(Exception ex) {
            YouboraLog.error("Could not open database");
            YouboraLog.error(ex);
        }

        return db;
    }

    private String[] getAllColumns(){
        return new String[] {
                OfflineContract.OfflineEntry.COLUMN_NAME_UID,
                OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS,
                OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE,
                OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID
        };
    }

    private String formatWhereString(String[] args) {
        StringBuilder argsString = new StringBuilder();
        if (args == null) {
            return argsString.toString();
        }

        for (String arg : args) {
            argsString.append(arg).append(" = ?,");
        }

        return argsString.length() == 0 ? argsString.toString() :
                argsString.substring(0, argsString.length() - 1);
    }

    public SQLiteOpenHelper getDatabaseInstance() {
        return dbHelper;
    }

    private Event generateEvent(Cursor cursor){

        //Init variables
        int eventId = 0;
        String eventEvents = "";
        long updated = 0;
        int offlineId = 0;

        try{
            eventId = cursor.getInt(
                    cursor.getColumnIndexOrThrow(OfflineContract.OfflineEntry.COLUMN_NAME_UID));

            eventEvents = cursor.getString(
                    cursor.getColumnIndexOrThrow(OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS));

            updated = cursor.getLong(
                    cursor.getColumnIndexOrThrow(OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE));

            offlineId = cursor.getInt(
                    cursor.getColumnIndexOrThrow(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID));
        }catch(Exception ex){
            YouboraLog.error(ex);
        }

        return new Event(eventId, eventEvents, updated, offlineId);
    }
}
