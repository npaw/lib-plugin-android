package com.npaw.youbora.lib6.adapter

import com.npaw.youbora.lib6.Chrono
import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.Timer.TimerEventListener

import kotlin.math.abs

/**
 * This class periodically checks the player's playhead in order to infer buffer and/or seek events.
 *
 * The [PlayheadMonitor] is bounded to a [PlayerAdapter] and fires its buffer and seek
 * start and end methods.
 *
 * In order to use this feature, [PlayerAdapter.monitorPlayhead] should
 * be called from the Adapter's overridden constructor.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
open class PlayheadMonitor(val adapter: BaseAdapter<*>, type: Int, var interval: Int) {

    companion object {
        // Monitor types
        /**
         * Monitor nothing.
         */
        const val TYPE_NONE = 0

        /**
         * Monitor buffer.
         */
        const val TYPE_BUFFER = 1

        /**
         * Monitor seek.
         */
        const val TYPE_SEEK = 2

        private const val BUFFER_THRESHOLD_RATIO = 0.5
        private const val SEEK_THRESHOLD_RATIO = 2.0
    }

    private var timer: Timer? = null
    private var lastPlayhead = 0.0
    private val chrono = createChrono()
    private val bufferEnabled: Boolean
    private val seekEnabled: Boolean

    /**
     * Get current monitor state
     * @return current [PlayheadMonitor] state
     */
    var isRunning = false
        private set

    /**
     * Constructor
     *
     * @param adapter the adapter from where to watch the playhead
     * @param type type of monitoring desired, can be TYPE_BUFFER, TYPE_SEEK or both; TYPE_BUFFER |
     * TYPE_SEEK
     * @param interval the interval between playhead checks. If this is 0 or negative, 800 ms will
     * be used instead.
     */
    init {
        seekEnabled = type and TYPE_SEEK == TYPE_SEEK && adapter is PlayerAdapter<*>
        bufferEnabled = type and TYPE_BUFFER == TYPE_BUFFER
        interval = if (interval > 0) interval else 800 // 800 ms by default

        if (interval > 0) {
            timer = createTimer(object : TimerEventListener {
                override fun onTimerEvent(delta: Long) {
                    progress()
                }
            }, interval.toLong())
        }
    }

    /**
     * Start monitoring playhead
     */
    open fun start() {
        if (!cannotBeUsed()) {
            timer?.start()
            isRunning = true
        }
    }

    private fun cannotBeUsed(): Boolean {
        val plugin = adapter.plugin
        val isLive = plugin?.isLive ?: false
        val contentIsLiveNoMonitor = plugin?.options?.contentIsLiveNoMonitor ?: false

        return isLive && contentIsLiveNoMonitor
    }

    /**
     * Stop monitoring playhead
     */
    open fun stop() {
        timer?.stop()
        isRunning = false
    }

    /**
     * Used to ignore the immediate next check after this call. Used when the player resumes
     * playback after a pause or a seek.
     */
    open fun skipNextTick() {
        lastPlayhead = 0.0
    }

    /**
     * Get the playhead from the [PlayerAdapter]
     * @return the Adapter's current playhead.
     */
    protected val playhead: Double?
        get() = adapter.getPlayhead()

    /**
     * Call this method at every tick of timeupdate/progress.
     * If you defined an interval, do not fire this method manually.
     */
    open fun progress() {
        // Reset timer
        val deltaTime = chrono.stop()
        chrono.start()

        // Define thresholds
        val bufferThreshold = deltaTime * BUFFER_THRESHOLD_RATIO
        val seekThreshold = deltaTime * SEEK_THRESHOLD_RATIO

        // Calculate diff playhead
        val currentPlayhead = playhead ?: 0.0
        val diffPlayhead = abs(lastPlayhead - currentPlayhead) * 1000 // Seconds -> millis

        if (diffPlayhead < bufferThreshold) {
            // Playhead is stalling: buffer
            if (bufferEnabled && lastPlayhead > 0 && !adapter.flags.isPaused &&
                    !adapter.flags.isSeeking) {
                adapter.fireBufferBegin(false, mutableMapOf("triggeredEvents" to "PlayHeadMonitor::progress()::diffPlayhead < bufferThreshold")) // don't convert from seek
            }
        } else if (diffPlayhead > seekThreshold) {
            // Playhead has jumped: seek
            if (seekEnabled && lastPlayhead > 0) {
                (adapter as PlayerAdapter).fireSeekBegin(true, mutableMapOf("triggeredEvents" to "PlayHeadMonitor::progress()::diffPlayhead > seekThreshold")) // convert from buffer to seek
            }
        } else {
            // Healthy: close buffers and seeks
            if (seekEnabled && adapter.flags.isSeeking) {
                (adapter as PlayerAdapter).fireSeekEnd(mutableMapOf("triggeredEvents" to "PlayHeadMonitor::progress()"))
            } else if (bufferEnabled && adapter.flags.isBuffering) {
                adapter.fireBufferEnd(mutableMapOf("triggeredEvents" to "PlayHeadMonitor::progress()"))
            }
        }

        // Update playhead
        lastPlayhead = currentPlayhead
    }

    open fun createChrono(): Chrono {
        return Chrono()
    }

    open fun createTimer(listener: TimerEventListener, interval: Long): Timer? {
        return Timer(listener, interval)
    }
}