package com.npaw.youbora.lib6.persistence.datasource;

/**
 * Successful query listener.
 */
public interface QuerySuccessListener<T> {
    /**
     * Callback invoked when a query from {@link com.npaw.youbora.lib6.persistence.dao.EventDAO} has been successful.
     *
     * @param queryResult expected query return value
     */
    void onQueryResolved(T queryResult);
}