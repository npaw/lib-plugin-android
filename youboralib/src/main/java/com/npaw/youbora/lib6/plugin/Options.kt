package com.npaw.youbora.lib6.plugin

import android.os.Bundle

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.*
import com.npaw.youbora.lib6.extensions.*
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.DASH
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.DVB
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.DVBC
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.DVBT
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.DVBT2
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.HDS
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.HLS
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.MSS
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.MULTICAST
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.RTMP
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.RTP
import com.npaw.youbora.lib6.plugin.Options.StreamingProtocol.RTSP
import com.npaw.youbora.lib6.plugin.Options.TransportFormat.CMF
import com.npaw.youbora.lib6.plugin.Options.TransportFormat.MP4
import com.npaw.youbora.lib6.plugin.Options.TransportFormat.TS
import java.lang.IllegalArgumentException

import java.util.*

import kotlin.collections.ArrayList

/**
 * This class stores all the Youbora configuration settings.
 * Any value specified in this class, if set, will override the info the plugin is able to get on
 * its own.
 *
 * The only **required** option is the [accountCode].
 * @author      Nice People at Work
 * @since       6.0
 */
open class Options() {

    /**
     * NicePeopleAtWork account code that indicates the customer account.
     */
    open var accountCode: String? = "nicetest"

    // Ad options

    /**
     * Set to positive integer array indicating the position (playhead in seconds) of the ad breaks,
     * provided by the ad server.
     */
    open var adBreaksTime: ArrayList<Int>? = null

    open var adCampaign: String? = null
    open var adCreativeId: String? = null

    /**
     * Set to [Int] positive value indicating how many breaks will be shown for the active view,
     * value expected by the customer (the number of breaks requested to the server).
     *
     * If [adExpectedPattern] is set, [adExpectedBreaks] can be calculated automatically, so there
     * is no need to set any value.
     */
    open var adExpectedBreaks: Int? = null

    /**
     * Set to positive integer array indicating how many ads will be shown for each break,
     * value expected by the customer (the number of ads requested to the server for each break).
     *
     * The [Bundle] keys that should be used are: "pre", "mid" and "post". The type of their value
     * should be ArrayList<Int>.
     */
    @Transient
    open var adExpectedPattern: Bundle? = null

    /**
     * Set to [Int] positive value indicating how many breaks shown for the active view,
     * provided by the ad server.
     */
    open var adGivenBreaks: Int? = null

    /**
     * If true, youbora blocks ad events and calculates jointime ignoring ad time.
     */
    open var adIgnore = false

    /**
     * [Bundle] containing mixed extra information about the ads like: director, parental rating,
     * device info or the audio channels.
     */
    @Transient
    open var adMetadata: Bundle? = Bundle()

    open var adProvider: String? = null
    open var adResource: String? = null
    open var adTitle: String? = null

    /**
     * Set to integer positive value indicating how many ads
     * will be shown as post-rolls if they do it after content player triggers stop event.
     */
    @Deprecated("This option will be removed in future releases")
    open var adsAfterStop = 0

    // App options

    /**
     * Name of the app.
     */
    open var appName: String? = null

    /**
     * Version of the app.
     */
    open var appReleaseVersion: String? = null

    /**
     * Optional auth token to validate all the requests.
     */
    open var authToken: String? = null

    /**
     * Optional auth type. Used if authToken is set.
     * "Bearer" by default.
     */
    open var authType = "Bearer"

    // Content options

    /**
     * Bitrate of the content in bits per second.
     */
    open var contentBitrate: Long? = null

    /**
     * Codename of the CDN where the content is streaming from.
     * See a list of codes in
     * <a href="http://mapi.youbora.com:8081/cdns">http://mapi.youbora.com:8081/cdns</a>.
     */
    open var contentCdn: String? = null

    open var contentCdnNode: String? = null
    open var contentCdnType: String? = null
    open var contentChannel: String? = null
    open var contentContractedResolution: String? = null
    open var contentCost: String? = null
    open var contentDrm: String? = null

    /**
     * Duration of the media <b>in seconds</b>.
     */
    open var contentDuration: Double? = null

    /**
     * Video segment length in <b>in milliseconds</b>.
     */
    open var contentSegmentDuration: Long? = null

    open var contentEncodingAudioCodec: String? = null
    open var contentEncodingCodecProfile: String? = null
    open var contentEncodingCodecSettings: Bundle? = null
    open var contentEncodingContainerFormat: String? = null
    open var contentEncodingVideoCodec: String? = null
    open var contentEpisodeTitle: String? = null

    /**
     * Frames per second of the media being played.
     */
    open var contentFps: Double? = null

    open var contentGenre: String? = null
    open var contentGracenoteId: String? = null
    open var contentId: String? = null
    open var contentImdbId: String? = null

    /**
     * true if the content is Live. false if VOD.
     */
    open var contentIsLive: Boolean? = null

    /**
     * Disable seeks if content is live. Only applies if content is live, for VOD gets ignored.
     */
    open var contentIsLiveNoSeek = false

    /** Disables the monitor if content is live **/
    open var contentIsLiveNoMonitor = false

    open var contentLanguage: String? = null

    /**
     * [Bundle] containing mixed extra information about the content like: director,
     * parental rating, device info or the audio channels.
     */
    @Transient
    open var contentMetadata: Bundle? = Bundle()

    /**
     * [Bundle] containing the metric name as String and the numeric value as the value.
     * It accepts float, double, int, long and String as values, any other type will get ignored.
     */
    @Transient
    open var contentMetrics: Bundle? = Bundle()

    open var contentPackage: String? = null
    open var contentPlaybackType: String? = null
    open var contentPrice: String? = null

    /**
     * Name or value of the current rendition (quality) of the content.
     */
    open var contentRendition: String? = null

    /**
     * URL/path of the current media resource.
     */
    open var contentResource: String? = null

    open var contentSaga: String? = null
    open var contentSeason: String? = null

    /**
     * Resource streaming protocol
     * Accepted values for this option are: HDS, HLS, MSS, DASH, RTMP, RTP, RTSP, MULTICAST, DVB, DVB-C, DVB-T, DVB-T2.
     */
    open var contentStreamingProtocol: String? = null
        get() {
           val streamingProtocols = listOf(HDS, HLS, MSS, DASH, RTMP, RTP, RTSP, MULTICAST, DVB, DVBC, DVBT, DVBT2)

           return field?.let { f ->
               streamingProtocols.firstOrNull { it == f }.also {
                   if (it == null) YouboraLog.warn("contentStreamingProtocol has a not valid value")
               }
           }
        }

        set(value) { field = value?.uppercase(Locale.getDefault()) }

    object StreamingProtocol {
        const val HDS = "HDS"
        const val HLS = "HLS"
        const val MSS = "MSS"
        const val DASH = "DASH"
        const val RTMP = "RTMP"
        const val RTP = "RTP"
        const val RTSP = "RTSP"
        const val MULTICAST = "MULTICAST"
        const val DVB = "DVB"
        const val DVBC = "DVB-C"
        const val DVBT = "DVB-T"
        const val DVBT2 = "DVB-T2"
    }

    open var contentSubtitles: String? = null

    /**
     * Throughput of the client bandwidth in bits per second.
     */
    open var contentThroughput: Long? = null

    /**
     * Title of the media.
     */
    open var contentTitle: String? = null

    open var contentTotalBytes: Long? = null
    open var contentSendTotalBytes = false

    /**
     * Custom unique code to identify the view.
     */
    open var contentTransactionCode: String? = null

    open var contentTvShow: String? = null
    open var contentType: String? = null

    // Custom dimensions
    open var contentCustomDimensions = Bundle()
    open var contentCustomDimension1: String? = null
    open var contentCustomDimension2: String? = null
    open var contentCustomDimension3: String? = null
    open var contentCustomDimension4: String? = null
    open var contentCustomDimension5: String? = null
    open var contentCustomDimension6: String? = null
    open var contentCustomDimension7: String? = null
    open var contentCustomDimension8: String? = null
    open var contentCustomDimension9: String? = null
    open var contentCustomDimension10: String? = null
    open var contentCustomDimension11: String? = null
    open var contentCustomDimension12: String? = null
    open var contentCustomDimension13: String? = null
    open var contentCustomDimension14: String? = null
    open var contentCustomDimension15: String? = null
    open var contentCustomDimension16: String? = null
    open var contentCustomDimension17: String? = null
    open var contentCustomDimension18: String? = null
    open var contentCustomDimension19: String? = null
    open var contentCustomDimension20: String? = null

    open var adCustomDimension1: String? = null
    open var adCustomDimension2: String? = null
    open var adCustomDimension3: String? = null
    open var adCustomDimension4: String? = null
    open var adCustomDimension5: String? = null
    open var adCustomDimension6: String? = null
    open var adCustomDimension7: String? = null
    open var adCustomDimension8: String? = null
    open var adCustomDimension9: String? = null
    open var adCustomDimension10: String? = null

    // Device options

    /**
     * What will be displayed as the device brand on Youbora
     * (provided by default with android.os.Build.BRAND if not set).
     */
    open var deviceBrand: String? = null

    /**
     * Youbora's device code. If specified it will rewrite info gotten from user agent.
     * See a list of codes in
     * <a href="http://mapi.youbora.com:8081/devices">http://mapi.youbora.com:8081/devices</a>.
     */
    open var deviceCode: String? = null

    /**
     * Unique identifyer of the device. If set it will consider the value as the device id.
     * By default the plugin tries to generate a unique id.
     * It wont be sent if [deviceIsAnonymous] is set to true.
     */
    open var deviceId: String? = null

    /**
     * If enabled, the deviceUUID won't be reported.
     */
    open var deviceIsAnonymous = false

    /**
     * What will be displayed as the device model on Youbora
     * (provided by default with android.os.Build.MODEL if not set).
     */
    open var deviceModel: String? = null

    /**
     * OS name that will be displayed on Youbora.
     */
    open var deviceOsName: String? = null

    /**
     * OS version that will be displayed on Youbora
     * (provided by default with android.os.Build.VERSION.RELEASE if not set).
     */
    open var deviceOsVersion: String? = null

    /**
     * What will be displayed as the device type on Youbora (pc, smartphone, stb, tv, etc.).
     */
    open var deviceType: String? = null

    /**
     * If it has elements on it, all the errors matching this code wont be reported
     */
    open var errorsToIgnore: Array<String>? = null

    /**
     * If it has elements on it, all the errors matching this code will fire the stop event to end
     * the view.
     */
    open var fatalErrors: Array<String>? = null

    /**
     * If it has elements on it, all the errors matching this code won't fire a stop event to end
     * the view
     */
    open var nonFatalErrors: Array<String>? = null

    /**
     * List of experiment ids to use with SmartUsers.
     */
    @Deprecated("This option will be removed in future releases")
    open var experimentIds: ArrayList<String>? = ArrayList()

    /**
     * Set to [Int] positive value indicating how many ads will be shown for the active break,
     * provided by the ad server.
     */
    open var givenAds: Int? = null

    /**
     * Host of the Fastdata service.
     */
    open var host: String? = "lma.npaw.com"

    /**
     * The method that will be used for plugin requests, default: [Method.GET].
     *
     * Accepted values for this option are: [Method.POST], [Method.GET].
     */
    open var method = Method.GET

    enum class Method {
        POST, GET
    }

    // Boolean options

    /**
     * Stop the plugin automatically when the user goes to background.
     */
    open var isAutoDetectBackground = true

    /**
     * Flag to send the start by the adapter or not.
     */
    open var isAutoStart = true

    /**
     * If enabled the plugin won't send NQS requests.
     * Default: true.
     */
    open var isEnabled = true

    /**
     * Option to always force init.
     */
    open var isForceInit = false

    /**
     * Define the security of NQS calls.
     * If true it will use "https://".
     * If false it will use "http://".
     * Default: true.
     */
    open var isHttpSecure = true

    /**
     * If true the plugin will store the events and send them later when there's connection.
     */
    open var isOffline = false

    /**
     * If enabled the plugin instance will be informed that the current view/session is
     * being affected by an Ad Blocker.
     * Default: null.
     */
    open var isAdBlockerDetected : Boolean? = null

    /**
     * If true the plugin will query the CDN to retrieve the node name.
     * It might slow performance down.
     * Default: false.
     */
    open var isParseCdnNode = false

    /**
     * Parses a video chunk or manifest every x seconds to read the x-cdn header and report it.
     */
    open var isParseCdnSwitchHeader = false

    /**
     * Returns a boolean to indicate if plugin should ignore send pauseDuration in a small pause events
     **/
    open var ignorePauseSmallEvents = true

    /** String to send on start events to link views with previous session events */
    open var linkedViewId: String? = null

    /**
     * If parse.cdnSwitchHeader enabled, the time between new requests.
     */
    open var parseCdnTTL = 60

    /**
     * If true the plugin will parse DASH files as a resource.
     * Default: false.
     */
    @Deprecated("This option will be removed in future releases, please use isParseManifest instead")
    open var isParseDash = false
        set(value) {
            if (value) isParseManifest = value
            field = value
        }

    /**
     * If true the plugin will parse HLS files to use the first .ts file found as resource.
     * It might slow performance down.
     * Default: false.
     */
    @Deprecated("This option will be removed in future releases, please use isParseManifest instead")
    open var isParseHls = false
        set(value) {
            if (value) isParseManifest = value
            field = value
        }

    /**
     * If true the plugin will parse the resource trying to find out the real given resource
     * instead of the API url.
     * It might slow performance down.
     * Default: false
     */
    @Deprecated("This option will be removed in future releases, please use isParseManifest instead")
    open var isParseLocationHeader = false
        set(value) {
            if (value) isParseManifest = value
            field = value
        }

    /**
     * If true all "content" parsers will be used, HLS or DASH and location.
     * the plugin will parse the resource trying to find out the real given resource
     * instead of the API url.
     * If parsing HLS the plugin will parse files to use the first .ts file found as resource.
     * If parsing DASH the plugin will parse files as a resource.
     * Default: false.
     */
    open var isParseManifest = false

    // Network options

    /**
     * See a list of codes in <a href="http://mapi.youbora.com:8081/connectionTypes">
     * http://mapi.youbora.com:8081/connectionTypes</a>.
     */
    open var networkConnectionType: String? = null

    /**
     * IP of the viewer/user, e.g. "48.15.16.23".
     */
    open var networkIP: String? = null

    /**
     * Name of the internet service provider of the viewer/user.
     */
    open var networkIsp: String? = null

    // Parse options

    /**
     * If [isParseManifest] enabled, it adds extra headers [Bundle] to the request of the content.
     * Use this if for example, the player needs to include authentication headers to request the content,
     * so the plugin needs it to access to the manifest files too.
     */
    @Transient
    open var parseManifestAuth: Bundle? = Bundle()

    /**
     * If defined, resource parse will try to fetch the CDN code from the custom header defined
     * by this property, e.g. "x-cdn-forward".
     */
    open var parseCdnNameHeader: String? = "x-cdn-forward"
    set(value) {
        value?.let {
            parseCdnNameHeaderList = arrayListOf(value)
        }?:kotlin.run {
            parseCdnNameHeaderList = null
        }
        field = value
    }

    /**
     * If defined, resource parse will try to fetch the CDN code from the custom headers defined
     * by this property,
     * Default: ["x-cdn-forward"].
     */
    open var parseCdnNameHeaderList: ArrayList<String>? = arrayListOf("x-cdn-forward")

    /**
     * If defined, resource parse will try to fetch the CDN node name from the custom header defined
     * by this property. ie: 'x-node'
     */
    open var parseNodeHeader: String? = null

    /**
     * List of CDN names to parse. This is only used when {@link #parseCdnNode} is enabled.
     * Order is respected when trying to match against a CDN.
     * Default: ["Akamai", "Cloudfront", "Level3", "Fastly", "Highwinds"].
     */
    open var parseCdnNodeList: ArrayList<String>? = arrayListOf(
            CDN_NAME_AKAMAI,
            CDN_NAME_CLOUDFRONT,
            CDN_NAME_LEVEL3,
            CDN_NAME_FASTLY,
            CDN_NAME_HIGHWINDS,
            CDN_NAME_TELEFONICA,
            CDN_NAME_AMAZON,
            CDN_NAME_EDGECAST,
            CDN_NAME_NOS
    )

    /**
     * Secondary title of the media. This could be program name, season, episode, etc.
     */
    open var program: String? = null

    @Transient
    open var sessionMetrics: Bundle? = Bundle()

    open var smartSwitchConfigCode: String? = null
    open var smartSwitchGroupCode: String? = null
    open var smartSwitchContractCode: String? = null

    /** Accepted values for this option are: TS, MP4 & CMF. */
    open var transportFormat: String? = null
        get() {
            val transportFormat = listOf(TS, MP4, CMF)

            return field?.let { f ->
                transportFormat.firstOrNull { it == f }.also {
                    if (it == null) YouboraLog.warn("transportFormat has a not valid value")
                }
            }
        }

        set(value) { field = value?.uppercase(Locale.getDefault()) }

    object TransportFormat {
        const val TS = "TS"
        const val MP4 = "MP4"
        const val CMF = "CMF"
    }

    open var urlToParse: String? = null

    /**
     * Display's Extended Display Identification Data (EDID) to parse.
     */
    open var deviceEDID : String? = null

    open fun setDeviceEDID(value : ByteArray) { deviceEDID = value.contentToString() }


    // User options

    /**
     * User ID value inside your system.
     */
    open var username: String? = null

    open var userEmail: String? = null

    /**
     * Anonymous user ID value inside your system.
     */
    open var userAnonymousId: String? = null

    /**
     * User type (e.g. Premium, Free, Unregistered, Registered).
     */
    open var userType: String? = null

    /**
     * Option to obfuscate the IP.
     */
    open var userObfuscateIp = false

    /**
    * Privacy protocol to be used, nothing by default.
    * Possible values are "optin" and "optout"
    */
    open var userPrivacyProtocol: String? = null

    /**
     * User profile identifier
     */
    open var userProfileId: String? = null

    /**
     * Enabling this option enables the posibility of getting the /start request later on the view,
     * making the flow go as follows: /init is sent when the player starts to load content,
     * then when the playback starts /joinTime event will be sent, but with the difference of no
     * /start request, instead it will be delayed until all the option keys from
     * {@link #pendingMetadata} are not <b>null</b>, this is very important, since an empty string
     * is considered a not null and therefore is a valid value.
     */
    open var waitForMetadata = false

    /**
     * Set option keys you want to wait for metadata, in order to work {@link #waitForMetadata}
     * must be set to true.
     * You need to create an {@link @ArrayList} with all the options you want to make the start
     * be hold on.
     * You can find all the keys with the following format: Options.KEY_{OPTION_NAME} where option
     * name is the same one as the option itself.
     *
     * Find below an example:
     *
     * ArrayList optionsToWait = new ArrayList<String>()
     * optionsToWait.add(KEY_CONTENT_TITLE)
     * optionsToWait.add(KEY_CONTENT_CUSTOM_DIMENSION_1)
     * options.setPendingMetada(optionsToWait)
     *
     * The code above prevent the /start request unless {@link #setContentTitle} and
     * {@link #setCustomDimension1}
     * stop being null
     * (you can call the design setter for each property when information is available).
     *
     */
    open var pendingMetadata: ArrayList<String>? = ArrayList()

    // Keys for Bundle
    companion object {
        const val KEY_ACCOUNT_CODE = "accountCode"

        const val KEY_AD_BREAKS_TIME = "ad.breaksTime"
        const val KEY_AD_CAMPAIGN = "ad.campaign"
        const val KEY_AD_CREATIVE_ID = "ad.creativeId"
        const val KEY_AD_EXPECTED_BREAKS = "ad.expectedBreaks"
        const val KEY_AD_EXPECTED_PATTERN = "ad.expectedPattern"
        const val KEY_AD_GIVEN_BREAKS = "ad.givenBreaks"
        const val KEY_AD_IGNORE = "ad.ignore"
        const val KEY_AD_METADATA = "ad.metadata"
        const val KEY_AD_PROVIDER = "ad.provider"
        const val KEY_AD_RESOURCE = "ad.resource"
        const val KEY_AD_TITLE = "ad.title"
        const val KEY_ADS_AFTERSTOP = "ad.afterStop"

        const val KEY_APP_NAME = "app.name"
        const val KEY_APP_RELEASE_VERSION = "app.releaseVersion"

        const val KEY_AUTH_TOKEN = "authToken"
        const val KEY_AUTH_TYPE = "authType"

        const val KEY_AUTOSTART = "autoStart"
        const val KEY_BACKGROUND = "autoDetectBackground"

        const val KEY_CONTENT_BITRATE = "content.bitrate"
        const val KEY_CONTENT_CDN = "content.cdn"
        const val KEY_CONTENT_CDN_NODE = "content.cdnNode"
        const val KEY_CONTENT_CDN_TYPE = "content.cdnType"
        const val KEY_CONTENT_CHANNEL = "content.channel"
        const val KEY_CONTENT_CONTRACTED_RESOLUTION = "content.contractedResolution"
        const val KEY_CONTENT_COST = "content.cost"
        const val KEY_CONTENT_DRM = "content.drm"
        const val KEY_CONTENT_DURATION = "content.duration"
        const val KEY_CONTENT_SEGMENT_DURATION = "content.segmentDuration"
        const val KEY_CONTENT_ENCODING_AUDIO_CODEC = "content.encoding.audioCodec"
        const val KEY_CONTENT_ENCODING_CODEC_PROFILE = "content.encoding.codecProfile"
        const val KEY_CONTENT_ENCODING_CODEC_SETTINGS = "content.encoding.codecSettings"
        const val KEY_CONTENT_ENCODING_CONTAINER_FORMAT = "content.encoding.containerFormat"
        const val KEY_CONTENT_ENCODING_VIDEO_CODEC = "content.encoding.videoCodec"
        const val KEY_CONTENT_EPISODE_TITLE = "content.episodeTitle"
        const val KEY_CONTENT_FPS = "content.fps"
        const val KEY_CONTENT_GENRE = "content.genre"
        const val KEY_CONTENT_GRACENOTE_ID = "content.gracenoteId"
        const val KEY_CONTENT_ID = "content.id"
        const val KEY_CONTENT_IMDB_ID = "content.imdbId"
        const val KEY_CONTENT_IS_LIVE = "content.isLive"
        const val KEY_CONTENT_IS_LIVE_NO_SEEK = "content.isLive.noSeek"
        const val KEY_CONTENT_IS_LIVE_NO_MONITOR = "content.isLive.noMonitor"
        const val KEY_CONTENT_LANGUAGE = "content.language"
        const val KEY_CONTENT_METADATA = "content.metadata"
        const val KEY_CONTENT_METRICS = "content.metrics"
        const val KEY_CONTENT_PACKAGE = "content.package"
        const val KEY_CONTENT_PLAYBACK_TYPE = "content.playbackType"
        const val KEY_CONTENT_PRICE = "content.price"
        const val KEY_CONTENT_PROGRAM = "content.program"
        const val KEY_CONTENT_RENDITION = "content.rendition"
        const val KEY_CONTENT_RESOURCE = "content.resource"
        const val KEY_CONTENT_SAGA = "content.saga"
        const val KEY_CONTENT_SEASON = "content.season"
        const val KEY_CONTENT_STREAMING_PROTOCOL = "content.streamingProtocol"
        const val KEY_CONTENT_SUBTITLES = "content.subtitles"
        const val KEY_CONTENT_THROUGHPUT = "content.throughput"
        const val KEY_CONTENT_TITLE = "content.title"
        const val KEY_CONTENT_TOTAL_BYTES = "content.totalBytes"
        const val KEY_CONTENT_SEND_TOTAL_BYTES = "content.sendTotalBytes"
        const val KEY_CONTENT_TRANSACTION_CODE = "content.transactionCode"
        const val KEY_CONTENT_TV_SHOW = "content.tvShow"
        const val KEY_CONTENT_TYPE = "content.type"

        const val KEY_CUSTOM_DIMENSIONS = "content.customDimensions"
        const val KEY_CUSTOM_DIMENSION_1 = "content.customDimension.1"
        const val KEY_CUSTOM_DIMENSION_2 = "content.customDimension.2"
        const val KEY_CUSTOM_DIMENSION_3 = "content.customDimension.3"
        const val KEY_CUSTOM_DIMENSION_4 = "content.customDimension.4"
        const val KEY_CUSTOM_DIMENSION_5 = "content.customDimension.5"
        const val KEY_CUSTOM_DIMENSION_6 = "content.customDimension.6"
        const val KEY_CUSTOM_DIMENSION_7 = "content.customDimension.7"
        const val KEY_CUSTOM_DIMENSION_8 = "content.customDimension.8"
        const val KEY_CUSTOM_DIMENSION_9 = "content.customDimension.9"
        const val KEY_CUSTOM_DIMENSION_10 = "content.customDimension.10"
        const val KEY_CUSTOM_DIMENSION_11 = "content.customDimension.11"
        const val KEY_CUSTOM_DIMENSION_12 = "content.customDimension.12"
        const val KEY_CUSTOM_DIMENSION_13 = "content.customDimension.13"
        const val KEY_CUSTOM_DIMENSION_14 = "content.customDimension.14"
        const val KEY_CUSTOM_DIMENSION_15 = "content.customDimension.15"
        const val KEY_CUSTOM_DIMENSION_16 = "content.customDimension.16"
        const val KEY_CUSTOM_DIMENSION_17 = "content.customDimension.17"
        const val KEY_CUSTOM_DIMENSION_18 = "content.customDimension.18"
        const val KEY_CUSTOM_DIMENSION_19 = "content.customDimension.19"
        const val KEY_CUSTOM_DIMENSION_20 = "content.customDimension.20"
        const val KEY_AD_CUSTOM_DIMENSION_1 = "ad.customDimension.1"
        const val KEY_AD_CUSTOM_DIMENSION_2 = "ad.customDimension.2"
        const val KEY_AD_CUSTOM_DIMENSION_3 = "ad.customDimension.3"
        const val KEY_AD_CUSTOM_DIMENSION_4 = "ad.customDimension.4"
        const val KEY_AD_CUSTOM_DIMENSION_5 = "ad.customDimension.5"
        const val KEY_AD_CUSTOM_DIMENSION_6 = "ad.customDimension.6"
        const val KEY_AD_CUSTOM_DIMENSION_7 = "ad.customDimension.7"
        const val KEY_AD_CUSTOM_DIMENSION_8 = "ad.customDimension.8"
        const val KEY_AD_CUSTOM_DIMENSION_9 = "ad.customDimension.9"
        const val KEY_AD_CUSTOM_DIMENSION_10 = "ad.customDimension.10"

        const val KEY_DEVICE_BRAND = "device.brand"
        const val KEY_DEVICE_CODE = "device.code"
        const val KEY_DEVICE_ID = "device.id"
        const val KEY_DEVICE_IS_ANONYMOUS = "device.isAnonymous"
        const val KEY_DEVICE_MODEL = "device.model"
        const val KEY_DEVICE_OS_NAME = "device.osName"
        const val KEY_DEVICE_OS_VERSION = "device.osVersion"
        const val KEY_DEVICE_TYPE = "device.type"
        const val KEY_DEVICE_EDID = "device.edid"

        const val KEY_ENABLED = "enabled"
        const val KEY_ERRORS_IGNORE = "errors.ignore"
        const val KEY_ERRORS_FATAL = "errors.fatal"
        const val KEY_ERRORS_NON_FATAL = "errors.nonFatal"
        const val KEY_EXPERIMENT_IDS = "experiments"
        const val KEY_FORCEINIT = "forceInit"
        const val KEY_GIVEN_ADS = "ad.givenAds"
        const val KEY_HOST = "host"
        const val KEY_METHOD = "method"
        const val KEY_HTTP_SECURE = "httpSecure"
        const val LINKED_VIEW_ID = "linkedViewId"

        const val KEY_NETWORK_CONNECTION_TYPE = "network.connectionType"
        const val KEY_NETWORK_IP = "network.ip"
        const val KEY_NETWORK_ISP = "network.isp"

        const val KEY_OFFLINE = "offline"

        const val KEY_AD_BLOCKER_DETECTED = "ad.blockerDetected"

        const val KEY_PARSE_MANIFEST_AUTH = "parse.manifest.auth"
        const val KEY_PARSE_CDN_NAME_HEADER = "parse.cdnNameHeader"
        const val KEY_PARSE_CDN_NAME_HEADER_LIST = "parse.cdnNameHeader.list"
        const val KEY_PARSE_CDN_NODE_HEADER = "parse.cdnNodeHeader"
        const val KEY_PARSE_CDN_NODE = "parse.cdnNode"
        const val KEY_PARSE_CDN_NODE_LIST = "parse.cdnNode.list"
        const val KEY_PARSE_CDN_SWITCH_HEADER = "parse.cdnSwitchHeader"
        const val KEY_PARSE_CDN_TTL = "parse.cdnTTL"
        const val KEY_PARSE_DASH = "parse.dash"
        const val KEY_PARSE_HLS = "parse.hls"
        const val KEY_PARSE_LOCATION_HEADER = "parse.locationHeader"
        const val KEY_PARSE_MANIFEST = "parse.manifest"

        const val KEY_SESSION_METRICS = "session.metrics"

        const val KEY_SS_CONFIG_CODE = "smartswitch.configCode"
        const val KEY_SS_GROUP_CODE = "smartswitch.groupCode"
        const val KEY_SS_CONTRACT_CODE = "smartswitch.contractCode"

        const val KEY_TRANSPORT_FORMAT = "content.transportFormat"

        const val KEY_URL_TO_PARSE = "urlToParse"

        const val KEY_USERNAME = "username"
        const val KEY_USER_EMAIL = "user.email"
        const val KEY_USER_ANONYMOUS_ID = "user.anonymousId"
        const val KEY_USER_TYPE = "user.type"
        const val KEY_USER_OBFUSCATE_IP = "user.obfuscateIp"
        const val KEY_PRIVACY_PROTOCOL = "user.privacyProtocol"
        const val KEY_USER_PROFILE_ID = "user.profileId"

        const val KEY_WAIT_METADATA = "waitForMetadata"
        const val KEY_PENDING_METADATA = "pendingMetadata"
    }

    /**
     * Constructor.
     * It will populate the fields by reading the values form a [Bundle].
     * @param b [Bundle] where to read the values from
     */
    constructor(b: Bundle) : this() {
        if (b.containsKey(KEY_ACCOUNT_CODE)) accountCode = b.getString(KEY_ACCOUNT_CODE)

        if (b.containsKey(KEY_AD_BREAKS_TIME))
            adBreaksTime = b.getIntegerArrayList(KEY_AD_BREAKS_TIME)
        if (b.containsKey(KEY_AD_CAMPAIGN)) adCampaign = b.getString(KEY_AD_CAMPAIGN)
        if (b.containsKey(KEY_AD_CREATIVE_ID)) adCreativeId = b.getString(KEY_AD_CREATIVE_ID)
        if (b.containsKey(KEY_AD_EXPECTED_BREAKS))
            adExpectedBreaks = b.getInt(KEY_AD_EXPECTED_BREAKS)
        if (b.containsKey(KEY_AD_EXPECTED_PATTERN))
            adExpectedPattern = b.getBundle(KEY_AD_EXPECTED_PATTERN)
        if (b.containsKey(KEY_AD_GIVEN_BREAKS)) adGivenBreaks = b.getInt(KEY_AD_GIVEN_BREAKS)
        if (b.containsKey(KEY_AD_IGNORE)) adIgnore = b.getBoolean(KEY_AD_IGNORE)
        if (b.containsKey(KEY_AD_METADATA)) adMetadata = b.getBundle(KEY_AD_METADATA)
        if (b.containsKey(KEY_AD_PROVIDER)) adProvider = b.getString(KEY_AD_PROVIDER)
        if (b.containsKey(KEY_AD_RESOURCE)) adResource = b.getString(KEY_AD_RESOURCE)
        if (b.containsKey(KEY_AD_TITLE)) adTitle = b.getString(KEY_AD_TITLE)
        if (b.containsKey(KEY_ADS_AFTERSTOP)) adsAfterStop = b.getInt(KEY_ADS_AFTERSTOP)

        if (b.containsKey(KEY_APP_NAME)) appName = b.getString(KEY_APP_NAME)
        if (b.containsKey(KEY_APP_RELEASE_VERSION))
            appReleaseVersion = b.getString(KEY_APP_RELEASE_VERSION)

        if (b.containsKey(KEY_AUTH_TOKEN)) authToken = b.getString(KEY_AUTH_TOKEN)
        b.getString(KEY_AUTH_TYPE)?.let { authType = it }

        if (b.containsKey(KEY_AUTOSTART)) isAutoStart = b.getBoolean(KEY_AUTOSTART)
        if (b.containsKey(KEY_BACKGROUND)) isAutoDetectBackground = b.getBoolean(KEY_BACKGROUND)

        if (b.containsKey(KEY_AD_BLOCKER_DETECTED)) isAdBlockerDetected = b.getBoolean(KEY_AD_BLOCKER_DETECTED)

        if (b.containsKey(KEY_CONTENT_BITRATE)) contentBitrate = b.getLong(KEY_CONTENT_BITRATE)
        if (b.containsKey(KEY_CONTENT_CDN)) contentCdn = b.getString(KEY_CONTENT_CDN)
        if (b.containsKey(KEY_CONTENT_CDN_NODE)) contentCdnNode = b.getString(KEY_CONTENT_CDN_NODE)
        if (b.containsKey(KEY_CONTENT_CDN_TYPE)) contentCdnType = b.getString(KEY_CONTENT_CDN_TYPE)
        if (b.containsKey(KEY_CONTENT_CHANNEL)) contentChannel = b.getString(KEY_CONTENT_CHANNEL)
        if (b.containsKey(KEY_CONTENT_CONTRACTED_RESOLUTION))
            contentContractedResolution = b.getString(KEY_CONTENT_CONTRACTED_RESOLUTION)
        if (b.containsKey(KEY_CONTENT_COST)) contentCost = b.getString(KEY_CONTENT_COST)
        if (b.containsKey(KEY_CONTENT_DRM)) contentDrm = b.getString(KEY_CONTENT_DRM)
        if (b.containsKey(KEY_CONTENT_DURATION)) contentDuration = b.getDouble(KEY_CONTENT_DURATION)
        if (b.containsKey(KEY_CONTENT_SEGMENT_DURATION)) contentSegmentDuration = b.getLong(KEY_CONTENT_SEGMENT_DURATION)
        if (b.containsKey(KEY_CONTENT_ENCODING_AUDIO_CODEC))
            contentEncodingAudioCodec = b.getString(KEY_CONTENT_ENCODING_AUDIO_CODEC)
        if (b.containsKey(KEY_CONTENT_ENCODING_CODEC_PROFILE))
            contentEncodingCodecProfile = b.getString(KEY_CONTENT_ENCODING_CODEC_PROFILE)
        if (b.containsKey(KEY_CONTENT_ENCODING_CODEC_SETTINGS))
            contentEncodingCodecSettings = b.getBundle(KEY_CONTENT_ENCODING_CODEC_SETTINGS)
        if (b.containsKey(KEY_CONTENT_ENCODING_CONTAINER_FORMAT))
            contentEncodingContainerFormat = b.getString(KEY_CONTENT_ENCODING_CONTAINER_FORMAT)
        if (b.containsKey(KEY_CONTENT_ENCODING_VIDEO_CODEC))
            contentEncodingVideoCodec = b.getString(KEY_CONTENT_ENCODING_VIDEO_CODEC)
        if (b.containsKey(KEY_CONTENT_EPISODE_TITLE))
            contentEpisodeTitle = b.getString(KEY_CONTENT_EPISODE_TITLE)
        if (b.containsKey(KEY_CONTENT_FPS)) contentFps = b.getDouble(KEY_CONTENT_FPS)
        if (b.containsKey(KEY_CONTENT_GENRE)) contentGenre = b.getString(KEY_CONTENT_GENRE)
        if (b.containsKey(KEY_CONTENT_GRACENOTE_ID))
            contentGracenoteId = b.getString(KEY_CONTENT_GRACENOTE_ID)
        if (b.containsKey(KEY_CONTENT_ID)) contentId = b.getString(KEY_CONTENT_ID)
        if (b.containsKey(KEY_CONTENT_IMDB_ID)) contentImdbId = b.getString(KEY_CONTENT_IMDB_ID)
        if (b.containsKey(KEY_CONTENT_IS_LIVE)) contentIsLive = b.getBoolean(KEY_CONTENT_IS_LIVE)
        if (b.containsKey(KEY_CONTENT_IS_LIVE_NO_SEEK))
            contentIsLiveNoSeek = b.getBoolean(KEY_CONTENT_IS_LIVE_NO_SEEK)
        if (b.containsKey(KEY_CONTENT_IS_LIVE_NO_MONITOR))
            contentIsLiveNoMonitor = b.getBoolean(KEY_CONTENT_IS_LIVE_NO_MONITOR)
        if (b.containsKey(KEY_CONTENT_LANGUAGE)) contentLanguage = b.getString(KEY_CONTENT_LANGUAGE)
        if (b.containsKey(KEY_CONTENT_METADATA)) contentMetadata = b.getBundle(KEY_CONTENT_METADATA)
        if (b.containsKey(KEY_CONTENT_METRICS)) contentMetrics = b.getBundle(KEY_CONTENT_METRICS)
        if (b.containsKey(KEY_CONTENT_PACKAGE)) contentPackage = b.getString(KEY_CONTENT_PACKAGE)
        if (b.containsKey(KEY_CONTENT_PLAYBACK_TYPE))
            contentPlaybackType = b.getString(KEY_CONTENT_PLAYBACK_TYPE)
        if (b.containsKey(KEY_CONTENT_PRICE)) contentPrice = b.getString(KEY_CONTENT_PRICE)
        if (b.containsKey(KEY_CONTENT_PROGRAM)) program = b.getString(KEY_CONTENT_PROGRAM)
        if (b.containsKey(KEY_CONTENT_RENDITION))
            contentRendition = b.getString(KEY_CONTENT_RENDITION)
        if (b.containsKey(KEY_CONTENT_RESOURCE)) contentResource = b.getString(KEY_CONTENT_RESOURCE)
        if (b.containsKey(KEY_CONTENT_SAGA)) contentSaga = b.getString(KEY_CONTENT_SAGA)
        if (b.containsKey(KEY_CONTENT_SEASON)) contentSeason = b.getString(KEY_CONTENT_SEASON)
        if (b.containsKey(KEY_CONTENT_STREAMING_PROTOCOL))
            contentStreamingProtocol = b.getString(KEY_CONTENT_STREAMING_PROTOCOL)
        if (b.containsKey(KEY_CONTENT_SUBTITLES))
            contentSubtitles = b.getString(KEY_CONTENT_SUBTITLES)
        if (b.containsKey(KEY_CONTENT_THROUGHPUT))
            contentThroughput = b.getLong(KEY_CONTENT_THROUGHPUT)
        if (b.containsKey(KEY_CONTENT_TITLE)) contentTitle = b.getString(KEY_CONTENT_TITLE)
        if (b.containsKey(KEY_CONTENT_TOTAL_BYTES)) contentTotalBytes = b.getLong(KEY_CONTENT_TOTAL_BYTES)
        if (b.containsKey(KEY_CONTENT_SEND_TOTAL_BYTES)) contentSendTotalBytes = b.getBoolean(KEY_CONTENT_SEND_TOTAL_BYTES)
        if (b.containsKey(KEY_CONTENT_TRANSACTION_CODE))
            contentTransactionCode = b.getString(KEY_CONTENT_TRANSACTION_CODE)
        if (b.containsKey(KEY_CONTENT_TV_SHOW)) contentTvShow = b.getString(KEY_CONTENT_TV_SHOW)
        if (b.containsKey(KEY_CONTENT_TYPE)) contentType = b.getString(KEY_CONTENT_TYPE)

        b.getBundle(KEY_CUSTOM_DIMENSIONS)?.let { contentCustomDimensions = it }

        if (b.containsKey(KEY_CUSTOM_DIMENSION_1))
            contentCustomDimension1 = b.getString(KEY_CUSTOM_DIMENSION_1)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_2))
            contentCustomDimension2 = b.getString(KEY_CUSTOM_DIMENSION_2)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_3))
            contentCustomDimension3 = b.getString(KEY_CUSTOM_DIMENSION_3)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_4))
            contentCustomDimension4 = b.getString(KEY_CUSTOM_DIMENSION_4)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_5))
            contentCustomDimension5 = b.getString(KEY_CUSTOM_DIMENSION_5)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_6))
            contentCustomDimension6 = b.getString(KEY_CUSTOM_DIMENSION_6)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_7))
            contentCustomDimension7 = b.getString(KEY_CUSTOM_DIMENSION_7)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_8))
            contentCustomDimension8 = b.getString(KEY_CUSTOM_DIMENSION_8)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_9))
            contentCustomDimension9 = b.getString(KEY_CUSTOM_DIMENSION_9)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_10))
            contentCustomDimension10 = b.getString(KEY_CUSTOM_DIMENSION_10)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_11))
            contentCustomDimension11 = b.getString(KEY_CUSTOM_DIMENSION_11)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_12))
            contentCustomDimension12 = b.getString(KEY_CUSTOM_DIMENSION_12)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_13))
            contentCustomDimension13 = b.getString(KEY_CUSTOM_DIMENSION_13)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_14))
            contentCustomDimension14 = b.getString(KEY_CUSTOM_DIMENSION_14)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_15))
            contentCustomDimension15 = b.getString(KEY_CUSTOM_DIMENSION_15)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_16))
            contentCustomDimension16 = b.getString(KEY_CUSTOM_DIMENSION_16)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_17))
            contentCustomDimension17 = b.getString(KEY_CUSTOM_DIMENSION_17)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_18))
            contentCustomDimension18 = b.getString(KEY_CUSTOM_DIMENSION_18)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_19))
            contentCustomDimension19 = b.getString(KEY_CUSTOM_DIMENSION_19)
        if (b.containsKey(KEY_CUSTOM_DIMENSION_20))
            contentCustomDimension20 = b.getString(KEY_CUSTOM_DIMENSION_20)

        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_1))
            adCustomDimension1 = b.getString(KEY_AD_CUSTOM_DIMENSION_1)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_2))
            adCustomDimension2 = b.getString(KEY_AD_CUSTOM_DIMENSION_2)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_3))
            adCustomDimension3 = b.getString(KEY_AD_CUSTOM_DIMENSION_3)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_4))
            adCustomDimension4 = b.getString(KEY_AD_CUSTOM_DIMENSION_4)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_5))
            adCustomDimension5 = b.getString(KEY_AD_CUSTOM_DIMENSION_5)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_6))
            adCustomDimension6 = b.getString(KEY_AD_CUSTOM_DIMENSION_6)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_7))
            adCustomDimension7 = b.getString(KEY_AD_CUSTOM_DIMENSION_7)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_8))
            adCustomDimension8 = b.getString(KEY_AD_CUSTOM_DIMENSION_8)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_9))
            adCustomDimension9 = b.getString(KEY_AD_CUSTOM_DIMENSION_9)
        if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_10))
            adCustomDimension10 = b.getString(KEY_AD_CUSTOM_DIMENSION_10)

        if (b.containsKey(KEY_DEVICE_BRAND)) deviceBrand = b.getString(KEY_DEVICE_BRAND)
        if (b.containsKey(KEY_DEVICE_CODE)) deviceCode = b.getString(KEY_DEVICE_CODE)
        if (b.containsKey(KEY_DEVICE_ID)) deviceId = b.getString(KEY_DEVICE_ID)
        if (b.containsKey(KEY_DEVICE_IS_ANONYMOUS))
            deviceIsAnonymous = b.getBoolean(KEY_DEVICE_IS_ANONYMOUS)
        if (b.containsKey(KEY_DEVICE_MODEL)) deviceModel = b.getString(KEY_DEVICE_MODEL)
        if (b.containsKey(KEY_DEVICE_OS_NAME)) deviceOsName = b.getString(KEY_DEVICE_OS_NAME)
        if (b.containsKey(KEY_DEVICE_OS_VERSION))
            deviceOsVersion = b.getString(KEY_DEVICE_OS_VERSION)
        if (b.containsKey(KEY_DEVICE_TYPE)) deviceType = b.getString(KEY_DEVICE_TYPE)

        if (b.containsKey(KEY_ENABLED)) isEnabled = b.getBoolean(KEY_ENABLED)
        if (b.containsKey(KEY_EXPERIMENT_IDS))
            experimentIds = b.getStringArrayList(KEY_EXPERIMENT_IDS)
        if (b.containsKey(KEY_ERRORS_IGNORE)) errorsToIgnore = b.getStringArray(KEY_ERRORS_IGNORE)
        if (b.containsKey(KEY_ERRORS_FATAL)) fatalErrors = b.getStringArray(KEY_ERRORS_FATAL)
        if (b.containsKey(KEY_ERRORS_NON_FATAL)) nonFatalErrors = b.getStringArray(KEY_ERRORS_NON_FATAL)

        if (b.containsKey(KEY_FORCEINIT)) isForceInit = b.getBoolean(KEY_FORCEINIT)
        if (b.containsKey(KEY_GIVEN_ADS)) givenAds = b.getInt(KEY_GIVEN_ADS)
        if (b.containsKey(KEY_HOST)) host = b.getString(KEY_HOST)
        if (b.containsKey(KEY_METHOD)) method = try {Method.valueOf(b.getString(KEY_METHOD) ?: "GET")} catch (e: IllegalArgumentException) { Method.GET }
        if (b.containsKey(KEY_HTTP_SECURE)) isHttpSecure = b.getBoolean(KEY_HTTP_SECURE)
        if (b.containsKey(LINKED_VIEW_ID)) linkedViewId = b.getString(LINKED_VIEW_ID)

        if (b.containsKey(KEY_NETWORK_CONNECTION_TYPE))
            networkConnectionType = b.getString(KEY_NETWORK_CONNECTION_TYPE)
        if (b.containsKey(KEY_NETWORK_IP)) networkIP = b.getString(KEY_NETWORK_IP)
        if (b.containsKey(KEY_NETWORK_ISP)) networkIsp = b.getString(KEY_NETWORK_ISP)

        if (b.containsKey(KEY_OFFLINE)) isOffline = b.getBoolean(KEY_OFFLINE)

        if (b.containsKey(KEY_PARSE_MANIFEST_AUTH)) parseManifestAuth = b.getBundle(KEY_PARSE_MANIFEST_AUTH)
        if (b.containsKey(KEY_PARSE_CDN_NAME_HEADER))
            parseCdnNameHeader = b.getString(KEY_PARSE_CDN_NAME_HEADER)
        if (b.containsKey(KEY_PARSE_CDN_NAME_HEADER_LIST))
            parseCdnNameHeaderList = b.getStringArrayList(KEY_PARSE_CDN_NAME_HEADER_LIST)
        if (b.containsKey(KEY_PARSE_CDN_NODE_HEADER))
            parseNodeHeader = b.getString(KEY_PARSE_CDN_NODE_HEADER)
        if (b.containsKey(KEY_PARSE_CDN_NODE)) isParseCdnNode = b.getBoolean(KEY_PARSE_CDN_NODE)
        if (b.containsKey(KEY_PARSE_CDN_NODE_LIST))
            parseCdnNodeList = b.getStringArrayList(KEY_PARSE_CDN_NODE_LIST)
        if (b.containsKey(KEY_PARSE_CDN_SWITCH_HEADER)) isParseCdnSwitchHeader = b.getBoolean(KEY_PARSE_CDN_SWITCH_HEADER)
        if (b.containsKey(KEY_PARSE_CDN_TTL)) parseCdnTTL = b.getInt(KEY_PARSE_CDN_TTL)
        if (b.containsKey(KEY_PARSE_DASH)) isParseDash = b.getBoolean(KEY_PARSE_DASH)
        if (b.containsKey(KEY_PARSE_HLS)) isParseHls = b.getBoolean(KEY_PARSE_HLS)
        if (b.containsKey(KEY_PARSE_LOCATION_HEADER))
            isParseLocationHeader = b.getBoolean(KEY_PARSE_LOCATION_HEADER)
        if (b.containsKey(KEY_PARSE_MANIFEST)) isParseManifest = b.getBoolean(KEY_PARSE_MANIFEST)

        if (b.containsKey(KEY_SESSION_METRICS)) sessionMetrics = b.getBundle(KEY_SESSION_METRICS)

        if (b.containsKey(KEY_SS_CONFIG_CODE))
            smartSwitchConfigCode = b.getString(KEY_SS_CONFIG_CODE)
        if (b.containsKey(KEY_SS_GROUP_CODE)) smartSwitchGroupCode = b.getString(KEY_SS_GROUP_CODE)
        if (b.containsKey(KEY_SS_CONTRACT_CODE))
            smartSwitchContractCode = b.getString(KEY_SS_CONTRACT_CODE)

        if (b.containsKey(KEY_TRANSPORT_FORMAT)) transportFormat = b.getString(KEY_TRANSPORT_FORMAT)

        if (b.containsKey(KEY_URL_TO_PARSE)) urlToParse = b.getString(KEY_URL_TO_PARSE)

        if (b.containsKey(KEY_DEVICE_EDID)) deviceEDID = b.getString(KEY_DEVICE_EDID)

        if (b.containsKey(KEY_USERNAME)) username = b.getString(KEY_USERNAME)
        if (b.containsKey(KEY_USER_EMAIL)) userEmail = b.getString(KEY_USER_EMAIL)
        if (b.containsKey(KEY_USER_ANONYMOUS_ID))
            userAnonymousId = b.getString(KEY_USER_ANONYMOUS_ID)
        if (b.containsKey(KEY_USER_TYPE)) userType = b.getString(KEY_USER_TYPE)
        if (b.containsKey(KEY_USER_OBFUSCATE_IP))
            userObfuscateIp = b.getBoolean(KEY_USER_OBFUSCATE_IP)
        if (b.containsKey(KEY_PRIVACY_PROTOCOL)) userPrivacyProtocol = b.getString(KEY_PRIVACY_PROTOCOL)
        if (b.containsKey(KEY_USER_PROFILE_ID)) userProfileId = b.getString(KEY_USER_PROFILE_ID)

        if (b.containsKey(KEY_WAIT_METADATA)) waitForMetadata = b.getBoolean(KEY_WAIT_METADATA)
        if (b.containsKey(KEY_PENDING_METADATA))
            pendingMetadata = b.getStringArrayList(KEY_PENDING_METADATA)
    }

    /**
     * Convert this [Options] instance into a [Bundle] representation.
     *
     * To get the [Options] object from a bundle use the Options(Bundle) constructor.
     * This can be useful to carry the options in an Intent.
     * @return A [Bundle] with the Youbora [Options].
     */
    fun toBundle(): Bundle {
        val b = Bundle()

        b.putNotNullString(KEY_ACCOUNT_CODE, accountCode)

        b.putNotNullIntegerArrayList(KEY_AD_BREAKS_TIME, adBreaksTime)
        b.putNotNullString(KEY_AD_CAMPAIGN, adCampaign)
        b.putNotNullString(KEY_AD_CREATIVE_ID, adCreativeId)
        b.putInt(KEY_AD_EXPECTED_BREAKS, adExpectedBreaks)
        b.putNotNullBundle(KEY_AD_EXPECTED_PATTERN, adExpectedPattern)
        b.putInt(KEY_AD_GIVEN_BREAKS, adGivenBreaks)
        b.putBoolean(KEY_AD_IGNORE, adIgnore)
        b.putNotNullBundle(KEY_AD_METADATA, adMetadata)
        b.putNotNullString(KEY_AD_PROVIDER, adProvider)
        b.putNotNullString(KEY_AD_RESOURCE, adResource)
        b.putNotNullString(KEY_AD_TITLE, adTitle)
        b.putInt(KEY_ADS_AFTERSTOP, adsAfterStop)

        b.putNotNullString(KEY_AUTH_TOKEN, authToken)
        b.putNotNullString(KEY_AUTH_TYPE, authType)

        b.putNotNullString(KEY_APP_NAME, appName)
        b.putNotNullString(KEY_APP_RELEASE_VERSION, appReleaseVersion)

        b.putBoolean(KEY_AUTOSTART, isAutoStart)
        b.putBoolean(KEY_BACKGROUND, isAutoDetectBackground)

        b.putBoolean(KEY_AD_BLOCKER_DETECTED, isAdBlockerDetected)

        b.putLong(KEY_CONTENT_BITRATE, contentBitrate)
        b.putNotNullString(KEY_CONTENT_CDN, contentCdn)
        b.putNotNullString(KEY_CONTENT_CDN_NODE, contentCdnNode)
        b.putNotNullString(KEY_CONTENT_CDN_TYPE, contentCdnType)
        b.putNotNullString(KEY_CONTENT_CHANNEL, contentChannel)
        b.putNotNullString(KEY_CONTENT_CONTRACTED_RESOLUTION, contentContractedResolution)
        b.putNotNullString(KEY_CONTENT_COST, contentCost)
        b.putNotNullString(KEY_CONTENT_DRM, contentDrm)
        b.putDouble(KEY_CONTENT_DURATION, contentDuration)
        b.putLong(KEY_CONTENT_SEGMENT_DURATION, contentSegmentDuration)
        b.putNotNullString(KEY_CONTENT_ENCODING_AUDIO_CODEC, contentEncodingAudioCodec)
        b.putNotNullString(KEY_CONTENT_ENCODING_CODEC_PROFILE, contentEncodingCodecProfile)
        b.putNotNullBundle(KEY_CONTENT_ENCODING_CODEC_SETTINGS, contentEncodingCodecSettings)
        b.putNotNullString(KEY_CONTENT_ENCODING_CONTAINER_FORMAT, contentEncodingContainerFormat)
        b.putNotNullString(KEY_CONTENT_ENCODING_VIDEO_CODEC, contentEncodingVideoCodec)
        b.putNotNullString(KEY_CONTENT_EPISODE_TITLE, contentEpisodeTitle)
        b.putDouble(KEY_CONTENT_FPS, contentFps)
        b.putNotNullString(KEY_CONTENT_GENRE, contentGenre)
        b.putNotNullString(KEY_CONTENT_GRACENOTE_ID, contentGracenoteId)
        b.putNotNullString(KEY_CONTENT_ID, contentId)
        b.putNotNullString(KEY_CONTENT_IMDB_ID, contentImdbId)
        b.putBoolean(KEY_CONTENT_IS_LIVE, contentIsLive)
        b.putBoolean(KEY_CONTENT_IS_LIVE_NO_SEEK, contentIsLiveNoSeek)
        b.putBoolean(KEY_CONTENT_IS_LIVE_NO_MONITOR, contentIsLiveNoMonitor)
        b.putNotNullString(KEY_CONTENT_LANGUAGE, contentLanguage)
        b.putNotNullBundle(KEY_CONTENT_METADATA, contentMetadata)
        b.putNotNullBundle(KEY_CONTENT_METRICS, contentMetrics)
        b.putNotNullString(KEY_CONTENT_PACKAGE, contentPackage)
        b.putNotNullString(KEY_CONTENT_PLAYBACK_TYPE, contentPlaybackType)
        b.putNotNullString(KEY_CONTENT_PRICE, contentPrice)
        b.putNotNullString(KEY_CONTENT_PROGRAM, program)
        b.putNotNullString(KEY_CONTENT_RENDITION, contentRendition)
        b.putNotNullString(KEY_CONTENT_RESOURCE, contentResource)
        b.putNotNullString(KEY_CONTENT_SAGA, contentSaga)
        b.putNotNullString(KEY_CONTENT_SEASON, contentSeason)
        b.putNotNullString(KEY_CONTENT_STREAMING_PROTOCOL, contentStreamingProtocol)
        b.putNotNullString(KEY_CONTENT_SUBTITLES, contentSubtitles)
        b.putLong(KEY_CONTENT_THROUGHPUT, contentThroughput)
        b.putNotNullString(KEY_CONTENT_TITLE, contentTitle)
        b.putLong(KEY_CONTENT_TOTAL_BYTES, contentTotalBytes)
        b.putBoolean(KEY_CONTENT_SEND_TOTAL_BYTES, contentSendTotalBytes)
        b.putNotNullString(KEY_CONTENT_TRANSACTION_CODE, contentTransactionCode)
        b.putNotNullString(KEY_CONTENT_TV_SHOW, contentTvShow)
        b.putNotNullString(KEY_CONTENT_TYPE, contentType)

        b.putBundle(KEY_CUSTOM_DIMENSIONS, contentCustomDimensions)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_1, contentCustomDimension1)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_2, contentCustomDimension2)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_3, contentCustomDimension3)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_4, contentCustomDimension4)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_5, contentCustomDimension5)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_6, contentCustomDimension6)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_7, contentCustomDimension7)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_8, contentCustomDimension8)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_9, contentCustomDimension9)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_10, contentCustomDimension10)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_11, contentCustomDimension11)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_12, contentCustomDimension12)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_13, contentCustomDimension13)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_14, contentCustomDimension14)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_15, contentCustomDimension15)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_16, contentCustomDimension16)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_17, contentCustomDimension17)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_18, contentCustomDimension18)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_19, contentCustomDimension19)
        b.putNotNullString(KEY_CUSTOM_DIMENSION_20, contentCustomDimension20)

        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_1, adCustomDimension1)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_2, adCustomDimension2)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_3, adCustomDimension3)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_4, adCustomDimension4)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_5, adCustomDimension5)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_6, adCustomDimension6)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_7, adCustomDimension7)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_8, adCustomDimension8)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_9, adCustomDimension9)
        b.putNotNullString(KEY_AD_CUSTOM_DIMENSION_10, adCustomDimension10)

        b.putNotNullString(KEY_DEVICE_BRAND, deviceBrand)
        b.putNotNullString(KEY_DEVICE_CODE, deviceCode)
        b.putNotNullString(KEY_DEVICE_ID, deviceId)
        b.putBoolean(KEY_DEVICE_IS_ANONYMOUS, deviceIsAnonymous)
        b.putNotNullString(KEY_DEVICE_MODEL, deviceModel)
        b.putNotNullString(KEY_DEVICE_OS_NAME, deviceOsName)
        b.putNotNullString(KEY_DEVICE_OS_VERSION, deviceOsVersion)
        b.putNotNullString(KEY_DEVICE_TYPE, deviceType)

        b.putBoolean(KEY_ENABLED, isEnabled)
        b.putNotNullStringArrayList(KEY_EXPERIMENT_IDS, experimentIds)
        b.putStringArray(KEY_ERRORS_IGNORE, errorsToIgnore)
        b.putStringArray(KEY_ERRORS_FATAL, fatalErrors)
        b.putStringArray(KEY_ERRORS_NON_FATAL, nonFatalErrors)

        b.putBoolean(KEY_FORCEINIT, isForceInit)
        b.putInt(KEY_GIVEN_ADS, givenAds)
        b.putNotNullString(KEY_HOST, host)
        b.putNotNullString(KEY_METHOD, method.name)
        b.putBoolean(KEY_HTTP_SECURE, isHttpSecure)
        b.putNotNullString(LINKED_VIEW_ID, linkedViewId)

        b.putNotNullString(KEY_NETWORK_IP, networkIP)
        b.putNotNullString(KEY_NETWORK_ISP, networkIsp)
        b.putNotNullString(KEY_NETWORK_CONNECTION_TYPE, networkConnectionType)

        b.putBoolean(KEY_OFFLINE, isOffline)

        b.putNotNullBundle(KEY_PARSE_MANIFEST_AUTH, parseManifestAuth)
        b.putNotNullString(KEY_PARSE_CDN_NAME_HEADER, parseCdnNameHeader)
        b.putNotNullStringArrayList(KEY_PARSE_CDN_NAME_HEADER_LIST, parseCdnNameHeaderList)
        b.putNotNullString(KEY_PARSE_CDN_NODE_HEADER, parseNodeHeader)
        b.putBoolean(KEY_PARSE_CDN_NODE, isParseCdnNode)
        b.putNotNullStringArrayList(KEY_PARSE_CDN_NODE_LIST, parseCdnNodeList)
        b.putBoolean(KEY_PARSE_CDN_SWITCH_HEADER, isParseCdnSwitchHeader)
        b.putInt(KEY_PARSE_CDN_TTL, parseCdnTTL)
        b.putBoolean(KEY_PARSE_DASH, isParseDash)
        b.putBoolean(KEY_PARSE_HLS, isParseHls)
        b.putBoolean(KEY_PARSE_LOCATION_HEADER, isParseLocationHeader)
        b.putBoolean(KEY_PARSE_MANIFEST, isParseManifest)

        b.putNotNullBundle(KEY_SESSION_METRICS, sessionMetrics)

        b.putNotNullString(KEY_SS_CONFIG_CODE, smartSwitchConfigCode)
        b.putNotNullString(KEY_SS_GROUP_CODE, smartSwitchGroupCode)
        b.putNotNullString(KEY_SS_CONTRACT_CODE, smartSwitchContractCode)

        b.putNotNullString(KEY_TRANSPORT_FORMAT, transportFormat)

        b.putNotNullString(KEY_URL_TO_PARSE, urlToParse)

        b.putNotNullString(KEY_DEVICE_EDID, deviceEDID)

        b.putNotNullString(KEY_USERNAME, username)
        b.putNotNullString(KEY_USER_EMAIL, userEmail)
        b.putNotNullString(KEY_USER_ANONYMOUS_ID, userAnonymousId)
        b.putNotNullString(KEY_USER_TYPE, userType)
        b.putBoolean(KEY_USER_OBFUSCATE_IP, userObfuscateIp)
        b.putNotNullString(KEY_PRIVACY_PROTOCOL, userPrivacyProtocol)
        b.putNotNullString(KEY_USER_PROFILE_ID, userProfileId)

        b.putBoolean(KEY_WAIT_METADATA, waitForMetadata)
        b.putNotNullStringArrayList(KEY_PENDING_METADATA, pendingMetadata)

        return b
    }
}
