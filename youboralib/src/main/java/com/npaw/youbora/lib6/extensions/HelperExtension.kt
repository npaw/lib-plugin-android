package com.npaw.youbora.lib6.extensions

class HelperExtension {
    companion object {
        fun <K,V> printMap(map: Map<K,V>?): String? {
            return map?.printMap()
        }
    }
}
fun <K,V> Map<K,V>.printMap():String = this.map { "${it.key}: ${it.value}" }.joinToString(", ")