package com.npaw.youbora.lib6.productAnalytics
import com.npaw.youbora.lib6.plugin.Options
import com.npaw.youbora.lib6.YouboraLog
import java.util.Timer
import java.util.TimerTask
import java.util.concurrent.atomic.AtomicReference

class ProductAnalyticsUserState(
    activeStateDimension: Int, activeStateTimeout: Long,
    private val fireEventAdapter: (eventName: String, dimensionsInternal: MutableMap<String, String>?, dimensionsUser: MutableMap<String, String>?, metrics: MutableMap<String, Double>) -> Unit,
    private val options: AtomicReference<Options>
){
    private var state: States = States.passive
    private val dimension: String = "contentCustomDimension$activeStateDimension"
    private var timerId: TimerTask? = null
    private val timerInterval: Long = activeStateTimeout

    enum class States(val state: String) {
        active("active"),
        passive("passive")
    }
    /*
    We must set state (customDimension) at the very beginning. Otherwise, customDimension will have "unknown" value during the time
    between page load and player first start.
    */

    init {
        // Set initial passive state
        this.storeState(States.passive)
    }

    /**
     * Release resources
     */
    fun dispose() {
        this.timerStop();
    }

    /**
     * Set active state
     * @param {string} eventName
     * @param {boolean} playerStarted
     */

    fun setActive(eventName: String) {
        val state: States = States.active

        if (this.state !== state) {
            this.fireEvent(state, eventName)
            this.storeState(state)
        }

        timerStart()
    }

    /**
     * Fire switch state event
     * @private
     */

    private fun fireEvent(state: States, eventName: String) {

        YouboraLog.notice("User changing from state $this.state to $state")

        val dimensions: MutableMap<String, String> = mutableMapOf(
            "newState" to state.toString(),
            "triggerEvent" to eventName,
            "stateFromTo" to "${this.state} to $state"
        )

        // Fire the event

        when (state) {
            States.active  -> this.fireEventAdapter("Content Playback State Switch to Active", dimensions, mutableMapOf(), mutableMapOf())
            States.passive -> this.fireEventAdapter("Content Playback State Switch to Passive", dimensions, mutableMapOf(), mutableMapOf())
        }
    }

    /**
     * Store state
     * @param {string} state
     * @private
     */

    private fun storeState(state: States) {
        this.state = state

        try {
            val field = this.options.get().javaClass.getDeclaredField(this.dimension)
            field.isAccessible = true
            field.set(this.options.get(), state.toString())
        } catch (e: NoSuchFieldException) {
            YouboraLog.warn("Invalid attribute name: ${this.dimension}")
        } catch (e: IllegalAccessException) {
            YouboraLog.warn("Failed to set value for attribute: ${this.dimension}")
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------------
    // TIMER MANAGEMENT
    // ------------------------------------------------------------------------------------------------------------------------------

    /**
     * Starts interaction monitor timer
     * @private
     */

    private fun timerStart(){
        val state: States = States.passive

        this.timerStop()
        this.timerId = object : TimerTask() {
            override fun run() {
                this@ProductAnalyticsUserState.fireEvent(state, "timer")
                this@ProductAnalyticsUserState.storeState(state)
            }
        }

        Timer().schedule(this.timerId, this.timerInterval)
    }

    /**
     * Stops interaction monitor timer
     * @private
     */

    private fun timerStop() {
        this.timerId?.cancel()
    }
}
