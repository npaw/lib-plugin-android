# Youboralib Android
[![build](https://img.shields.io/bitbucket/pipelines/npaw/lib-plugin-android.svg)](https://bitbucket.org/npaw/lib-plugin-android/)
[![codecov](https://codecov.io/bb/npaw/lib-plugin-android/branch/master/graph/badge.svg)](https://codecov.io/bb/npaw/lib-plugin-android)

## Documentation, installation & usage

Add this to your build.gradle:

```groovy
repositories {
    ...
    maven { url  "https://npaw.jfrog.io/artifactory/youbora/" }
    ...
}

dependencies {
    ...
    implementation "com.nicepeopleatwork:youboralib:6.7.+"
    ...
}
```

Also please refer to [Developer Portal](http://developer.nicepeopleatwork.com).

## I need help!
If you find a bug, have a suggestion or need assistance, please send an e-mail to <support@nicepeopleatwork.com>.
