## [6.8.32] - 2024-10-08
### Added
- [PROD-982]
  - Product Analytics: added `loginSuccessful`, `loginUnsuccessful` and `logout` methods. Also added `userProfileCreated`, `userProfileSelected` and `userProfileDeleted`.
  - `profileId` is sent on `infinity/session/start`, `infinity/session/nav`, `/data`, `/init`, `/start` and `/error` events (the same events where `username` is included).
### Modified
- Product Analytics: `trackNavByName` no longer starts a session or sends a session/nav event.
- Product Analytics: `initialize` starts a session.
- Product Analytics: tracking methods fail when called within a closed session.
- Plugin: product analytics is no longer destroyed after closing the session.

## [6.8.31] - 2024-07-11
### Modified
- [PROD-921] Product Analytics: rename `trackNavigation` as `trackNavByName`.
- [PROD-929] Product Analytics: add `section` and `sectionOrder` arguments to `trackSearchClick`.
- [PROD-935] Product Analytics: user state is `passive` on video start.

## [6.8.30] - 2024-05-09
### Added
- [PROD-899] Product Analytics: add setUserProfile method to allow collecting profile identifiers.

## [6.8.29] - 2024-04-17
### Added
- Product Analytics: add sectionOrder argument to section related tracking methods.
### Fixed
- Product Analytics: ensure initialize method is called before processing any request.

## [6.8.28] - 2024-03-19
### Fixed
- Removed Objects.requireNonNull due to it causing crashes.
### Modified
- Improved nullability handling in RemoteMonitoring.

## [6.8.27] - 2024-02-05
### Added
- Product Analytics initial version.

## [6.8.26] - 2023-12-19
### Fixed
- Assure that the Parsed Resource is a valid URL.

## [6.8.25] - 2023-11-21
### Fixed
- Resolve OOM error on the CDN Handler not being freed when the plugin was destroyed.

## [6.8.24] - 2023-10-09
### Fixed
- Resolved ConcurrentModificationException by switching to CopyOnWriteArrayList for thread-safe logger modifications on YouboraLog.

## [6.8.23] - 2023-09-14
### Fixed
- Fixed crashing issue during logging operation. We have updated our code to prevent simultaneous reads and modifications to the same data source, ensuring the app's stability during concurrent operations.
- Fixed code style warning.

## [6.8.22] - 2023-08-21
### Changed
- Remove calling `fireInit()` on the `adStopListener()` because it was creating wrong join times.

## [6.8.21] - 2023-07-26
### Fixed
- Fixed accessing exoplayer method with wrong thread

## [6.8.20] - 2023-07-25
### Fixed
- Use specific HandlerThread for accessing cdn balancer information from file

## [6.8.19] - 2023-07-10
### Added
- Add flag 'ignorePauseSmallEvents' (true by default) on options to avoid ignore, or not, small events (<= 50ms) (some players, when are on buffer or seek, are reporting pause and resume events).
### Modified
- On seek/buffer begins, pause "pause chronos" instead of reset it, to report pause values correctly in case if user is pausing the content, and after that, do a seek
### Fixed
- Prevent app from crashing by using local `BaseFlag` variables.

## [6.8.18] - 2023-06-26
### Added
- Added Entities to events: Buffer, Error, Stop
- Added `CONTENT_ID` to Entities
### Modified
- Beautified TriggeredEvents

## [6.8.17] - 2023-05-23
### Added
- Changed fastdata url to lma
- Use json format (instead of jsonp) in fastdata response
- Updated CdnBalancerStats, improved flow

## [6.8.16] - 2023-05-12
### Added
- Added @SerializedName annotation to BalancerStats to avoid proguard issues

## [6.8.15] - 2023-05-12
### Added
- Added STATE_REJECT to the Transformer.
- Implemented STATE_REJECT in ViewTransformer to prevent sending a request when fast data is missing.
- Limited MAX_RESPONSE_SIZE to 1,048,576 bytes (1MB) in Requests.

## [6.8.14] - 2023-05-02
### Fixed
- Set `isHttpSecure()` to `true` as default.
- Fix the package name retriever

## [6.8.13] - 2023-04-14
### Fixed
- Fixed cdn balancer stats refreshing

## [6.8.12] - 2023-04-13
### Fixed
- Fixed comparing cdnStats with previous state and report cdn name and provider correctly

## [6.8.11] - 2023-03-16
### Added
- Remote monitoring using /configuration endpoint
- Added Triggered Events

## [6.8.10] - 2023-03-16
### Added
- `parseCdnNameHeaderList` option to be able to fetch the CDN code from a custom header names list

## [6.8.9] - 2023-03-09
### Added
- LibVersion param
- ConnectTimeout to Request
### Fixed
- ViewCode will be added before adding a request to queue when not offline and not coming from offline
- Fixed getAdViewability & getAdViewedDuration
- sendResume will always send RESUME when called

## [6.8.9] - 2023-03-09
### Added
- LibVersion param
- ConnectTimeout to Request
### Fixed
- ViewCode will be added before adding a request to queue when not offline and not coming from offline
- Fixed getAdViewability & getAdViewedDuration
- sendResume will always send RESUME when called

## [6.8.8] - 2023-03-08
### Fixed
- Added cdn headers case-insensitive

## [6.8.7] - 2023-03-01
### Added
- Streaming Protocol support: DVB-C, DVB-T, DVB-T2, MULTICAST
### Modified
- Ignore errors and fireFatal list will check equals when the code is a number
### Fixed
- Aligning ping entities. Added: TITLE

## [6.8.6] - 2023-02-28
### Added
- Balancer version on each cdn ping request
### Modified
- Cdn pings request using RequestBuilder

## [6.8.5] - 2023-02-14
### Fixed
-  setActivity creates sessionActivity which will be managed by the plugin internally after first set.

## [6.8.4] - 2022-11-04
### Fixed
- Capture Exception in cdn balancer stats and other small changes

## [6.8.3] - 2022-10-21
### Fixed
- Cursor should be freed up after use with #close().
- Require the database to be opened read-only when querying by calling #getReadableDatabase().
- Avoid calling #getIsLive() from `OFFLINE_EVENTS` since it is not needed and could produce thread exceptions.
- Fix pipelines.
### Modified
- Upgrade database to version `2` to reflect changes.
- Upgrade `compileSdkVersion` to `32`.
- Upgrade `build.gradle` to `7.2.1`.
- Upgrade `mockito` to `4.0.0`.
- Upgrade `robolectric` to `4.8.1`.
- Upgrade `okhttp3:mockwebserver` to `4.9.2`.
- Refactor SqlLite database Unit tests.
### Added
- Support send requests with `POST` method, and parameters on the request `body`.

## [6.8.2] - 2022-10-14
### Fixed
- Latest cdn balancer stats version.

## [6.8.1] - 2022-09-09
### Fixed
- Fixed incorrect usage of HandlerThread and use same SQLiteDatabase instance always.
- Fixed possible crash in offline db select.

## [6.8.0] - 2022-08-05
### Modified
- Upgrade minor version since NPAW's CDN balancer support was added as a new feature. 

## [6.7.72] - 2022-07-29
### Added
- Support for NPAW's CDN balancer / active switching / P2P client analytics reporting: cdnBalancerResponseUUID, segmentDuration, global CDN and P2P data, and specific CDN use stats.

## [6.7.71] - 2022-07-14
### Fixed
- When parsing a DASH manifest file avoid parsing `location` value if is the same as the original resource.

## [6.7.70] - 2022-07-11
### Added
- `userPrivacyProtocol` option.

## [6.7.69] - 2022-07-04
### Added
- `parse.manifest.auth` option.

## [6.7.68] - 2022-05-26
### Fixed
- reset `breakNumber` when content stops.

## [6.7.67] - 2022-04-29
### Added
- `parse.cdnNodeHeader` option, to parse from additional requests the host name providing content when using a custom balancer.

## [6.7.66] - 2022-04-26
### Fixed
- Update Request's `parsedResource` param after `ResourceTransform` is done().

## [6.7.65] - 2022-04-20
### Fixed
- Set a read timeout to `HttpURLConnection` when expecting a response form a request.

## [6.7.64] - 2022-04-06
### Fixed
- Resume being ignored during playback.

## [6.7.63] - 2022-03-25
### Added
- Added `pauseJoinDuration()` & `resumeJoinDuration()` to `Plugin`.

## [6.7.62] - 2022-03-03
### Added
- Added `content.package` option back.

## [6.7.61] - 2022-03-01
### Fixed
- Resume being sent in the middle of a break.

## [6.7.60] - 2022-02-18
### Added
- `fireAdBreakStop()` triggered when `removeAdsAdapter()` called.
### Fixed
- fix join after after adStop.

## [6.7.59] - 2022-02-07
### Added
- Auth token.
### Changed
- Modified `ContentPlaybackType` default value to null.

## [6.7.58] - 2022-02-04
### Added
- `pause()` & `resume()` to `Chrono`.

## [6.7.57] - 2022-01-25
### Added
- Language to session start event.
### Fixed
- Language being sent on entities.

## [6.7.56] - 2022-01-05
### Added
- Added feature to detect if running in a Fire TV device.
- Added `NosOtt` CDN to CdnParser.
### Removed
- Remove `content.package` option.

## [6.7.55] - 2021-12-21
### Fixed
- Init triggered when ad starts, conditioned by not being a post-roll.

## [6.7.54] - 2021-12-17
### Added
- Added `adNumberInBreak` parameter in ad events.
- Added feature to detect if running in an Android TV device.
### Fixed
- Parse `deviceEDID` option value from byteArray as String HEX.

## [6.7.53] - 2021-11-22
### Added
- Additional case for `LocationHeaderParser`.

## [6.7.52] - 2021-11-15
### Added
- `deviceEDID` option.
### Changed
- `playhead` won't be sent when live.

## [6.7.51] - 2021-10-28
### Fixed
- Relative URL being parsed on DASH.

## [6.7.50] - 2021-10-22
### Fixed
- NodeType reporting contentType instead of contentCdnType.
- Parsed resource reported as media resource.

## [6.7.49] - 2021-10-18
### Fixed
- Null check added to getParsedResource().

## [6.7.48] - 2021-10-14
### Fixed
- Activity lifecycle listener behaviour.

## [6.7.47] - 2021-10-04
### Added
- Include 'player' parameter on adError event.
- Report parsedResource if parse.manifest is not enabled but the adapter can return something in the method getURLToParse().

## [6.7.46] - 2021-10-01
### Added
- isAdBlockerDetected option.
### Fixed
- Options' constants modified to match JS ones.

## [6.7.45] - 2021-09-15
### Fixed
- Start resetting comm when last service was an error.

## [6.7.44] - 2021-09-10
### Added
- contentIsLiveNoMonitor option.
### Fixed
- Price not being reported on start.

## [6.7.43] - 2021-08-30
### Added
- contentCustomDimensions option.

## [6.7.42] - 2021-08-13
### Added
- New LocationHeaderParser case.

## [6.7.41] - 2021-08-09
### Fixed
- Session events not waiting for session start.

## [6.7.40] - 2021-07-05
### Added
- Single thread for requests.

## [6.7.39] - 2021-07-02
### Added
- Obfuscate ip reported on session start.
- HLS strange case supported.

## [6.7.38] - 2021-06-21
### Added
- Error now fires init.

## [6.7.37] - 2021-06-08
### Added
- Akamai headers.

## [6.7.36] - 2021-06-01
### Fixed
- Possible memory leaks on activity and adapter.

## [6.7.35] - 2021-05-18
### Fixed
- Parse CDN.

## [6.7.34] - 2021-05-13
### Fixed
- Null pointer exception.

## [6.7.33] - 2021-05-12
### Added
- Ad insertion type.
- Ad viewability.

## [6.7.32] - 2021-03-23
### Added
- CMF transport format.

## [6.7.31] - 2021-03-05
### Added
- Metrics sent on stop and pings.
- Session metrics sent on session stop.

## [6.7.30] - 2021-02-26
### Modified
- Deployment platform moved from Bintray to JFrog.

## [6.7.29] - 2021-02-22
### Added
- fireInit() triggered when ad starts.

## [6.7.28] - 2021-02-15
### Modified
- Nullable player supported now for PlayerAdapter.

## [6.7.27] - 2021-02-11
### Added
- LinkedViewId option added.
- Infinity and PlayerAdapter's fireEvent(eventName, dimensions, values, topLevelDimensions).

## [6.7.26] - 2021-01-28
### Fixed
- contentStreamingProtocol and transportFormat logs spamming when no value has been set.

## [6.7.25] - 2021-01-21
### Added
- Edgecast cdn.

## [6.7.24] - 2020-12-17
### Modified
- Error options don't need to match the whole error code anymore.

## [6.7.23] - 2020-12-16
### Fixed
- NoSuchElementException.

## [6.7.22] - 2020-12-11
### Added
- New params to session start.

## [6.7.21] - 2020-11-30
### Added
- DeviceId option.

## [6.7.20] - 2020-10-28
### Fixed
- Playrate bug when 0.
- Crash when connection null on HlsParser's onRequestError listener.

## [6.7.19] - 2020-10-23
### Fixed
- Negative values restricted.

## [6.7.18] - 2020-10-16
### Added
- CdnSwitch.
- Latency will be reported just for live.
- Default values to fastDataConfig.

## [6.7.17] - 2020-10-06
### Modified
- Plugin's fire(Fatal)Error(String msg, String code, String errorMetadata, Exception ex) -> fire(Fatal)Error(String code, String msg, String errorMetadata, Exception ex)
### Fixed
- Plugin's fire(Fatal)Error method was swapping internally msg and code order.

## [6.7.16] - 2020-09-28
### Added
- Error options.
### Fixed
- Offline mode crashing when no fastData is received when initializing the plugin.

## [6.7.15] - 2020-09-17
### Fixed
- Crash when setting fastDataConfig not null but with null fields.

## [6.7.14] - 2020-08-04
### Removed
- fireInit() triggered on adStartListener.

## [6.7.13] - 2020-07-20
### Added
- Constants names for the Bundle.
- HlsParser improved.
- Playhead to error request.
- TransportFormat full funcionality.

## [6.7.12] - 2020-07-08
### Added
- External viewcode support to link session and views from different plugins

## [6.7.11] - 2020-07-01
### Added
- TransportFormat option.
- urlToParse.
### Modified
- StreamingProtocol just accepts fixed values again.
### Migrated to Kotlin
- FlowTransform.
- HlsParser.
### Removed
- DeviceUUID generated with sensible data from the device.

## [6.7.10] - 2020-06-11
### Migrated to Kotlin
- PlayheadMonitor.
- PlaybackChronos.
- CdnConfig.

## [6.7.9] - 2020-06-02
### Improved
- Passing the activity might lead to a memory leak, now when removeAdapter is called callbacks are cleared.

## [6.7.8] -2020-05-22
### Fixed
- Beat right after nav with wrong diffTime.

## [6.7.7] - 2020-05-06
### Fixed
- Session code now changes when you 'reset' the session.
### Removed
- Init sent before error.

## [6.7.6] - 2020-05-04
### Added
- Support for total bytes.
### Fixed
- Timestamp removed from sessionRoot and parentId.

## [6.7.5] - 2020-04-22
### Fixed
- Content metrics are now sent with the proper format.

## [6.7.4] - 2020-04-20
### Added
- parentId to init, start and error request.
### Deprecated
- Plugin(Options, Activity) constructor.

## [6.7.3] - 2020-03-03
### Modified
- Version's -android suffix replaced by -Android.
### Fixed
- -android suffix duplicated when getVersion() not overridden.
- Crash when using deprecated Plugin's constructor.

## [6.7.2] - 2020-02-27
### Added 
- buildRenditionString(width, height) overload.
- -android suffix to plugin and adapter version.
### Fixed
- protected modifier removed from BaseAdapter's fireJoin method.

## [6.7.1] - 2020-02-19
### Added
- getAudioCodec() & getVideoCodec() PlayerAdapter's methods.
### Fixed
- isAdBreakStarted flag resetting when it should not.

## [6.7.0] - 2020-02-13
### Added
- Fire init when an error is triggered.
- Replace adPosition and breakPosition for position.

### Refactored
- PlayerAdapter and AdsAdapter.

### Removed
- fireEvent(Map, Map, String) Infinity's method.
- Plugin(Options, PlayerAdapter) constructor.
- Plugin(Options, PlayerAdapter, Context) constructor.
- Plugin(Options, PlayerAdapter, Activity) constructor.

## [6.6.7] - 2020-01-23
### Fixed
- Crash when using ManifestParser and a wrong resource.

## [6.6.6] - 2020-01-17
### Fixed
- Possible concurrent modification exception when r/w the requests object.

## [6.6.5] - 2020-01-14
### Refactored
- Offline playback tracking.
- Parser transforms unified.
### Fixed
- File descriptors not closing.
### Removed
- Join and adJoin deprecated parameters.

## [6.6.4] - 2019-12-20
### Added 
- Amazon CDN support.
- Possibility to put any String in contentStreamingProtocol option.
- StreamingProtocol object with constants.
- Infinity method overloads.
### Modified 
- contentIsLiveNoSeek default value set to false and type to primitive boolean.

## [6.6.3] - 2019-12-09
### Fixed 
- Metadata timer not stopping when it should.

## [6.6.2] - 2019-12-05
### Added
- Possibility to initiate the Plugin and Adapter from a Java Thread.
### Fixed
- Crash when using deprecated constructor Plugin(Options).
### Migrated to Kotlin
- Chrono.
- RequestBuilder.

## [6.6.1] - 2019-11-26
### Added
- Parse DASH support.

## [6.6.0] - 2019-11-21
### Added
- Infinity refactor.
### Fixed
- Offline viewCode doesn't force nextView() anymore.
### Deprecated
- Plugin's method getInfinity(Context).
- adsAfterStop & experimentIds option.
### Removed
- PlayerAdapter's method and event fireAllAdsCompleted().
- anonymousId, anonymousUser, networkObfuscateIp & customDimensions options.

## [6.5.7] - 2019-11-08
### Added
- fireManifest(ManifestError, String) PlayerAdapter's method.
### Fixed
- Crash when null context is passed to YouboraUtil.getApplicationName(context).
- Offline events now use timestamp instead of view index.
- Manifest event will be sent even if the adapter does not exist.
### Deprecated
- fireManifest(String, String) PlayerAdapter's method.

## [6.5.6] - 2019-11-04
### Added
- fireError(), fireError(code), fireError(message, code) PlayerAdapter's methods.
### Migrated to Kotlin
- YouboraLog.
- DeviceInfo.
- Constants.
- Timer.
- YouboraUtil.

## [6.5.5] - 2019-10-10
### Improved
- Waiting for metadata separated from ping timer.
### Fixed
- Crash when trying to generate the deviceUUID.

## [6.5.4] - 2019-09-03
### Fixed
- contentPlaybackType not working as expected.

## [6.5.3] - 2019-08-02
### Fixed
- adError wasn't increasing breakNumber & adNumber.

## [6.5.2] - 2019-07-17
### Added
- adPauseDuration param to pings.
### Fixed
- Null checks added to Options Bundle constructor and toBundle() method.

## [6.5.1] - 2019-07-02
### Added
- fireEvent(String, Map, Map) Infinity's method.
- contentCustomDimensions sent on sessionStart.
- nodeHost & nodeType options.
- deviceIsAnonymous option to disable the deviceUUID.
### Fixed
- Not typed parameters for PlayerAdapter's method fireEvent(String, Map, Map).
### Removed
- deviceCode from RequestBuilder.
### Deprecated
- fireEvent(Map, Map, String) Infinity's method.

## [6.5.0] - 2019-06-19
### Added
- Ads rework.
- adProvider option & getAdProvider PlayerAdapter's method.
### Changed
- Fingerprint replaced by deviceUUID.
- isInfinity won't be sent on data request anymore.
- pingEntities values modified.
### Fixed
- fireEvent not firing anymore if the view has not been started.
### Removed
- ActiveSessions.
- title2 & contentTitle2.
- Extraparams1-20 & adExtraparams1-10.
### Deprecated
- Plugin(Options, PlayerAdapter) constructor.
- Plugin(Options, PlayerAdapter, Context) constructor.
- Plugin(Options, PlayerAdapter, Activity) constructor.

## [6.4.7] - 2019-06-17
### Fixed
- Infinity was not resetting the sessionRoot.

## [6.4.6] - 2019-06-03
### Changed
- All P2P metrics return types are now Long instead of Integer.

## [6.4.5] - 2019-05-28
### Fixed
- Plugin's method getContentGraceNoteId() renamed to getContentGracenoteId().
- Plugin's method getContentEncodingCodecFormat() renamed to getContentEncodingContainerFormat().
### Deprecated
- PlayerAdapter's method and event fireAllAdsCompleted().

## [6.4.4] - 2019-05-17
### Fixed
- contentIsLiveNoSeek option restricting also VoD seeks.

## [6.4.3] - 2019-05-09
### Added
- Now adDuration will be sent on adStart events.
- Default host option value changed to a-fds.youborafds01.com.
- LocationHeaderParser feature.

## [6.4.2] - 2019-04-25
### Fixed
- Now smartSwitchConfigCode, smartSwitchGroupCode, smartSwitchContractCode, experimentIds and 
userEmail should be reported correctly.

## [6.4.1] - 2019-04-10
### Added 
- Options class migrated to Kotlin.
### Deprecated
- getAnonymousUser(), setAnonymousUser(String), getAnonymousId(), setAnonymousId(String).

## [6.4.0] - 2019-04-03
### Added
- Many options.
- fireEvent(eventName, dimensions, values) method added.
- Now it is possible to add content and session metrics.
- Now it is possible to delay the start event (and therefore have all the metadata ready) and 
have correct joinTime.
### Fixed
- infinity/event endpoint replaced by infinity/session/event.
- sessionId won't be sent in video events anymore.
- code won't be sent in session events anymore.
### Deprecated
- getIsInfinity(), setIsInfinity(boolean).
- getNetworkObfuscateIp(), setNetworkObfuscateIp(boolean).
- getAdsAfterStop(), setAdsAfterStop(int).
- customDimensions getters and setters.

## [6.3.9] - 2019-02-20
### Added
- sessionRoot will be sent (again) for every request.
### Deprecated
- Constructor Plugin(Options).

## [6.3.8] - 2019-02-13
### Added
- Fingerprint parameter.
### Fixed
- Akamai CDN parse should now work as expected.
- fireStop will not be sent anymore when the adapter is not started.
- Infinity url parameters will not be sent anymore if isInfinity is false.

## [6.3.7] - 2019-02-06
### Added
- accountCode has been added to every request.
- appName and appReleaseVersion added as an option.
- TELEFO CDN added.
- title2 renamed to program.
- extraparams renamed to customDimensions.
- adExtraparams renamed to adCustomDimensions.

## [6.3.6] - 2019-01-28
### Added
 - Now adInit is sent automatically when some information cannot be retrieved.

## [6.3.5] - 2019-01-21
### Fix 
 - Fixed communication resetting in case of an error event before /start request.

## [6.3.4] - 2019-01-14
### Added 
 - Add pluginInfo param.
 - isActivityStopped property added.

## [6.3.3] - 2018-12-27
### Added 
 - Now almost every device dimensions it's customizable through options.
 - Last year deploy. HAPPY 2019!!!

## [6.3.2] - 2018-12-20
### Fixed 
 - Fix auto background with ads and init.

## [6.3.1] - 2018-12-18
### Fixed 
 - In case of joinTime firing too early in offline mode it gets queued until start is sent.

## [6.3.0] - 2018-11-29
### Added
 - Now init is sent automatically when some information cannot be retrieved.
### Fixed 
 - Now the autoDetectBackground option is set to true by default.

## [6.2.11] - 2018-10-25
### Added
 - Add Smart Switch options.
### Fixed
 - Depending on the application architecture the offline access to SQLite DB may crash, that is 
 fixed.
 - Now P2P params are added to the RequestBuilder.
 
## [6.2.10] - 2018-10-09
### Improved
 - Errors doesn't change viewcode if they are sent right after stop (500ms or less).
### Fixed
 - Wrong mediaResource if ParseHLS and contentResource option were active at the same time.
 - Timer setNextTick() being call when shouldn't.
 - Crash when not closing local DB.
 - Make local DB singleton synchronized to avoid exceptions.

## [6.2.9] - 2018-09-12
### Fixed
- AnonymousUser now is sent correctly.

## [6.2.8] - 2018-09-06
### Fixed
- Crash when enabling offline mode.
- Now offline stored events will be sent even if the adapter has not been started.

## [6.2.7] - 2018-08-29
### Added
 - Get activity set as default in case of no onStart.

## [6.2.6] - 2018-08-29
### Fixed
 - Fix background detection by checking activities.

## [6.2.5] - 2018-08-28
### Changed
 - Changed Infinity event endpoint.
### Fixed
 - Fix background detection.

## [6.2.4] - 2018-08-27
### Fixed
 - Fix expiration time.

## [6.2.3] - 2018-08-27
### Fixed
 - isInfinity option.

## [6.2.2] - 2018-08-24
### Fixed
 - Fix a crash when not providing context.
 - Fix dimensions name for event.

## [6.2.1] - 2018-08-23
### Fixed
 - Wrong parameter name for dimensions.

## [6.2.0] - 2018-08-22
### Added
 - Infinity.
 - Option to anonymous user.

## [6.1.8] - 2018-07-10
### Added
 - Now the username will be included in the data of the request, but just if it isn't null.
 - Three methods: fireClick(String url), fireCasted(), fireSkipped().
### Fixed
 - Now if the adapter is not changed and an stop is send the adNumber will reset properly.
 - Now the resource set on the options won't be overwritten by the resource of the video being 
 played.
 - Now when fireStop() is called from the Plugin's class, it will check if the adapter is not null. 
 If it isn't, it will call the adapter's fireStop().
 - If the adDuration is null, it won't be sent on the adStart anymore.
### Removed
 - Nqs6Transform usages commented.
  
## [6.1.7] - 2018-05-04
### Added
 - HouseholdId param.

## [6.1.6] - 2018-04-27
### Added
 - Possibility to hide IP Address.
 - New parameters: latency, packetLoss, packetSend.
 - Option to disable seeks in case of live.

## [6.1.5] - 2018-04-16
### Fixed
 - Wrong pingTime if sending offline events just before starting offline playback.

## [6.1.4] - 2018-03-23
### Added
 - Experiment ids for smart users.
### Fixed
 - No exception raised if adapter.getDuration() returns null.
 
## [6.1.3] - 2018-03-08
### Added
 - Now there are ten extraparams (total twenty).

## [6.1.2] - 2018-02-21
### Fixed
 - Now if you fire an error without init or start the view number will increase.

## [6.1.1] - 2018-02-19
### Improved
 - If is live content duration will be reported as 0.
### Fixed
 - Error serverity is not send anymore (adapters can send it anyway).
 - No crash if an ads adapter doesn't implement getPosition() method.

## [6.1.0] - 2018-01-30
### Added
 - Streaming protocol option now is "sendable".
 - User Type option.
 - adExtraparams.
### Removed
 - Now Room is no longer a dependency.

## [6.1.0-beta2] - 2018-01-15
### Fixed
 - Now events are properly removed when they have been send.
 - New event send system.

## [6.1.0-beta] - 2018-01-05
### Refactored
 - Offline mode refactored, now SQLite is used instead of SharedPrefs.

## [6.0.10] - 2018-01-02
### Added
 - Streaming protocol option.
 - Test cases.

## [6.0.9] - 2017-12-22
### Fixed
 - No data petition in case of offline mode.
 - Pause duration properly reported in case of stopping while paused.
 - Fire fatal error improved.

## [6.0.8] - 2017-12-15
### Added
 - removeAdapter and removeAdsAdapter stopping pings is optional.
 - Add AdError specific methods.
 - Add playhead.
 - Add timemark for debug purposes.
### Fixed
 - If no adInit join starts.
 - In case of stop while paused now pauseDuration is send too.
 - Only stops if init.
## Removed
 - Send ALWAYS ads stop if needed with player stop.

## [6.0.8-beta8] - 2017-12-04
### Added
 - Callback to call when ads have ended.
 - Timemark on all requests.
### Fixed
 - Null check chronos.

## [6.0.8-beta7] - 2017-11-29
### Added
 - FireEnd method.
### Fixed
 - FireEnd with params.
 - adPlayhead and playhead doesn't need adapter.

## [6.0.8-beta6] - 2017-11-28
### Added
 - FireFatalError at plugin.
 - Check for playback has really ended.
### Fixed
 - Wrong join time with adInit.

## [6.0.8-beta5] - 2017-11-24
### Added
 - FireStop plugin.
 - Check playhead for ad position.

## [6.0.8-beta4] - 2017-11-24
### Added
 - FireFatalError with Exception to filter by it.

## [6.0.8-beta3] - 2017-11-23
### Added
 - New error method.
###Fixed
 - Wrong joinTime if having preroll and /init.
 - Fix type in Constants.java.

## [6.0.8-beta2] - 2017-11-23
### Added
 - Ads after stop.

## [6.0.8-beta] - 2017-11-22
### Added
 - Autostart option.

## [6.0.7] - 2017-11-15
### Fixed
 - Playhead on ad events.
### Removed
 - errorLevel fatal.

## [6.0.6] - 2017-11-14
### Fixed
 - Add null check for adInit.

## [6.0.5] - 2017-11-08
### Added
 - pauseDuration on stop event.
 - DeviceInfo class.
### Fixed
 - AdDuration is not 0 on adInit and adStart.
 - AdNumber not incrementing.
 - If no activity passed but autobackground option enabled display error log message.

## [6.0.4] - 2017-10-10
### Added
 - AdInit method.

## [6.0.3] - 2017-10-02
### Added
 - Null check when removing activity callbacks.
 - AutoDetectBackground option.

## [6.0.2] - 2017-09-29
### Added
 - Null check when removing activity callbacks.
 - New Ad events, adSkip, adClick.
 - Remove unnecessary callback.
 - Offline events.
### Removed
 - GenericAdsAdapter.

## [6.0.1] - 2017-08-25
### Fixed
 - Join time calculation.

## [6.0.0] - 2017-07-18
### Fixed
 - First release.
